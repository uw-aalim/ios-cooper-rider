//
//  passbookViewController.m
//  User
//
//  Created by MAC on 04/09/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "passbookViewController.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "HomeViewController.h"
#import "Constants.h"
#import "CSS_Class.h"
#import "config.h"
#import "Utilities.h"
#import "passbookTableViewCell.h"


@interface passbookViewController ()
{
    AppDelegate *appDelegate;
}

@end

@implementation passbookViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];

    self.screenName = @"Your Passbook";
    [self walletBtn:self];
    
    [self LocalizationUpdate];
}

-(void)LocalizationUpdate{
    [_pastBtn setTitle:LocalizedString(@"Wallet history") forState:UIControlStateNormal];
    [_upcomingBtn setTitle:LocalizedString(@"Coupon history") forState:UIControlStateNormal];
    _headerLb.text = LocalizedString(@"Passbook");
    _pastLbl.backgroundColor = BASE2Color;
    _upcomingLbl.backgroundColor = BASE2Color;

}

-(void)onGetHistory
{
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [appDelegate onStartLoader];
    
    NSString *serviceStr;
    if([identifierStr isEqualToString:@"WALLET"])
    {
        serviceStr = MD_WALLET_PASSBOOK;
    }
    else
    {
        serviceStr = MD_COUPON_PASSBOOK;
    }

    [afn getDataFromPath:serviceStr WithType:GET_METHOD WithParameters:nil WithCompletedSuccess:^(id response) {
        [appDelegate onEndLoader];

        NSArray *arrLocal=response;
        if (arrLocal.count!=0)
            {
                NSLog(@"passbook history ...%@", arrLocal);
    
                dateArray = [[NSMutableArray alloc]init];
                amountArray = [[NSMutableArray alloc]init];
                paidByArray = [[NSMutableArray alloc]init];
        
                couponArray = [[NSMutableArray alloc]init];
                offerArray = [[NSMutableArray alloc]init];
                statusArray = [[NSMutableArray alloc]init];
        
                [_noDataLbl setHidden:YES];
                [_tripTableView setHidden:NO];
        
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                NSString *currencyStr=[user valueForKey:@"currency"];
        
                for (NSDictionary *dictVal in arrLocal)
                    {
                        if([identifierStr isEqualToString:@"WALLET"])
                            {
                                NSString *strDate=[Utilities convertDateTimeToGMT:[dictVal valueForKey:@"created_at"]];
                                NSString *amount = [Utilities removeNullFromString:[NSString stringWithFormat:@"%@%@",currencyStr,[dictVal valueForKey:@"amount"]]];
                                NSString *viaStr=[Utilities removeNullFromString:[dictVal valueForKey:@"via"]];
                                NSString *statusStr=[Utilities removeNullFromString:[dictVal valueForKey:@"status"]];
        
                                [paidByArray addObject:viaStr];
                                [dateArray addObject:strDate];
                                [amountArray addObject:amount];
                                [statusArray addObject:statusStr];
                            }
                            else
                            {
                                NSString *strDate=[Utilities convertDateTimeToGMT:[dictVal valueForKey:@"created_at"]];
                                NSDictionary *promocode = [dictVal valueForKey:@"promocode"];
                                NSString *amount;
                                if([promocode[@"discount_type"] isEqualToString:@"percent"]){
                                        amount= [Utilities removeNullFromString:[NSString stringWithFormat:@"%@%% off",[promocode valueForKey:@"discount"]]];
                                }else{
                                        amount = [Utilities removeNullFromString:[NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currency"],[promocode valueForKey:@"discount"]]];
                                }
        
                                NSString *viaStr=[Utilities removeNullFromString:[promocode valueForKey:@"promo_code"]];
                                NSString *statusStr=[Utilities removeNullFromString:[promocode valueForKey:@"status"]];
        
                                [paidByArray addObject:viaStr];
                                [dateArray addObject:strDate];
                                [amountArray addObject:amount];
                                [statusArray addObject:statusStr];
                                }
                            }
                            [_tripTableView reloadData];
                        }
                        else
                        {
                            [_noDataLbl setHidden:NO];
                            [_tripTableView setHidden:YES];
                        }

    } OrValidationFailure:^(NSString *errorMessage) {
        [appDelegate onEndLoader];
        [CSS_Class alertviewController_title:@"" MessageAlert:errorMessage viewController:self okPop:NO];
    } OrErrorCode:^(NSString *error) {
        [appDelegate onEndLoader];
        [CSS_Class alertviewController_title:@"" MessageAlert:error viewController:self okPop:NO];
    } OrIntentet:^(NSString *internetFailure) {
        [appDelegate onEndLoader];
        [CSS_Class alertviewController_title:@"" MessageAlert:internetFailure viewController:self okPop:NO];
    }];
//    if ([appDelegate internetConnected])
//    {
//        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
//        [appDelegate onStartLoader];
//        
//        NSString *serviceStr;
//        if([identifierStr isEqualToString:@"WALLET"])
//        {
//            serviceStr = MD_WALLET_PASSBOOK;
//        }
//        else
//        {
//            serviceStr = MD_COUPON_PASSBOOK;
//        }
//        
//        [afn getDataFromPath:serviceStr withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
//            [appDelegate onEndLoader];
//            if (response)
//            {
//                NSArray *arrLocal=response;
//                if (arrLocal.count!=0)
//                {
//                    NSLog(@"passbook history ...%@", arrLocal);
//                    
//                    dateArray = [[NSMutableArray alloc]init];
//                    amountArray = [[NSMutableArray alloc]init];
//                    paidByArray = [[NSMutableArray alloc]init];
//                    
//                    couponArray = [[NSMutableArray alloc]init];
//                    offerArray = [[NSMutableArray alloc]init];
//                    statusArray = [[NSMutableArray alloc]init];
//                    
//                    [_noDataLbl setHidden:YES];
//                    [_tripTableView setHidden:NO];
//                    
//                    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
//                    NSString *currencyStr=[user valueForKey:@"currency"];
//                    
//                    for (NSDictionary *dictVal in arrLocal)
//                    {
//                        if([identifierStr isEqualToString:@"WALLET"])
//                        {
//                            NSString *strDate=[Utilities convertDateTimeToGMT:[dictVal valueForKey:@"created_at"]];
//                            NSString *amount = [Utilities removeNullFromString:[NSString stringWithFormat:@"%@%@",currencyStr,[dictVal valueForKey:@"amount"]]];
//                            NSString *viaStr=[Utilities removeNullFromString:[dictVal valueForKey:@"via"]];
//                            NSString *statusStr=[Utilities removeNullFromString:[dictVal valueForKey:@"status"]];
//
//                            [paidByArray addObject:viaStr];
//                            [dateArray addObject:strDate];
//                            [amountArray addObject:amount];
//                            [statusArray addObject:statusStr];
//                        }
//                        else
//                        {
//                            NSString *strDate=[Utilities convertDateTimeToGMT:[dictVal valueForKey:@"created_at"]];
//                            NSDictionary *promocode = [dictVal valueForKey:@"promocode"];
//                            NSString *amount;
//                            if([promocode[@"discount_type"] isEqualToString:@"percent"]){
//                                 amount= [Utilities removeNullFromString:[NSString stringWithFormat:@"%@%% off",[promocode valueForKey:@"discount"]]];
//                            }else{
//                                 amount = [Utilities removeNullFromString:[NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currency"],[promocode valueForKey:@"discount"]]];
//                            }
//                            
//                            NSString *viaStr=[Utilities removeNullFromString:[promocode valueForKey:@"promo_code"]];
//                            NSString *statusStr=[Utilities removeNullFromString:[promocode valueForKey:@"status"]];
//                            
//                            [paidByArray addObject:viaStr];
//                            [dateArray addObject:strDate];
//                            [amountArray addObject:amount];
//                            [statusArray addObject:statusStr];
//                        }
//                    }
//                    [_tripTableView reloadData];
//                }
//                else
//                {
//                    [_noDataLbl setHidden:NO];
//                    [_tripTableView setHidden:YES];
//                }
//            }
//            else
//            {
//                if ([errorcode intValue]==1)
//                {
//                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
//                }
//                else if ([errorcode intValue]==3)
//                {                    
//                    [self logoutMethod];
//                }
//            }
//            
//        }];
//    }
//    else
//    {
//        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
//    }
    
}

-(void)logoutMethod
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn refreshMethod_NoLoader:MD_REFRESH_TOKEN withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
                [user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
                [user setValue:response[@"refresh_token"] forKey:UD_REFERSH_TOKEN];
            }
            else
            {
                [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setDesignStyles
{
    
}

#pragma mark -- Table View Delegates Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dateArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    passbookTableViewCell *cell = (passbookTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"passbookTableViewCell"];
    
    if (cell == nil)
    {
        cell = (passbookTableViewCell *) [[[NSBundle mainBundle] loadNibNamed:@"passbookTableViewCell" owner:self options:nil] lastObject];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.dateValue.text = [NSString stringWithFormat:@"%@", [dateArray objectAtIndex:indexPath.row]];
    NSString *statusStr = [NSString stringWithFormat:@"%@", [statusArray objectAtIndex:indexPath.row]];

    cell.commonView.layer.cornerRadius = 3.0f;
    
    if([identifierStr isEqualToString:@"WALLET"])
    {
        [cell.statusLbl setHidden:YES];
        [cell.statusValue setHidden:YES];

        cell.paymentLbl.text =LocalizedString(@"Payment by");
        cell.amountLbl.text =LocalizedString(@"Amount");
        

        if ([statusStr isEqualToString:@"CREDITED"])
        {
            cell.amountValue.textColor = RGB(28, 182, 211);
            cell.amountValue.text = [NSString stringWithFormat:@"%@", [amountArray objectAtIndex:indexPath.row]];

        }
        else
        {
            cell.amountValue.textColor = [UIColor redColor];
            cell.amountValue.text = [NSString stringWithFormat:@"(%@)", [amountArray objectAtIndex:indexPath.row]];

        }
        cell.paymentValue.text = [NSString stringWithFormat:@"%@", [paidByArray objectAtIndex:indexPath.row]];
    }
    else
    {
        [cell.statusLbl setHidden:YES];
        [cell.statusValue setHidden:NO];
        cell.amountValue.textColor = RGB(104, 104, 104);
        cell.paymentLbl.text =LocalizedString(@"Coupon Code");
        cell.amountLbl.text =LocalizedString(@"Offer");
        
        cell.paymentValue.text = [NSString stringWithFormat:@"%@", [paidByArray objectAtIndex:indexPath.row]];
        cell.amountValue.text = [NSString stringWithFormat:@"%@", [amountArray objectAtIndex:indexPath.row]];
        cell.statusValue.text = statusStr;
        
        if ([statusStr isEqualToString:@"ADDED"])
        {
            cell.statusValue.textColor = RGB(28, 182, 211);
        }
        else
        {
            cell.statusValue.textColor = [UIColor redColor];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(IBAction)backBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)couponBtn:(id)sender
{
    identifierStr = @"COUPON";
    [_upcomingLbl setHidden:NO];
    [_pastLbl setHidden:YES];
    
    [self onGetHistory];
}
-(IBAction)walletBtn:(id)sender
{
    identifierStr = @"WALLET";
    [_upcomingLbl setHidden:YES];
    [_pastLbl setHidden:NO];
    
    [self onGetHistory];
}

@end
