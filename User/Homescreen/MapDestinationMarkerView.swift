//
//  MapDestinationMarkerView.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class MapDestinationMarkerView: UIControl {

	static let frameSize = CGRect(
		x: 0.0,
		y: 0.0,
		width: 200.0,
		height: 38.0)
	
	@IBOutlet weak var addressLabel: UILabel!
	
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		let _ = self.loadView(with: "MapDestinationMarkerView")
		self.performSetup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		let _ = self.loadView(with: "MapDestinationMarkerView")
		self.performSetup()
	}
	
	func performSetup() {
		
	}

	func configure(
		withAddress address: String) {
		self.addressLabel.text = address
	}
	
	@IBAction func markerTapped(_ sender: AnyObject) {
		self.sendActions(for: UIControlEvents.touchUpInside)
	}
}
