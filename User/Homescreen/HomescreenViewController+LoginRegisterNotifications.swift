//
//  HomescreenViewController+LoginRegisterNotifications.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension HomescreenViewController {
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func registrationFinishedSuccess(_ notification: Notification) {
		DispatchQueue.main.async {
			self.loginWelcomeNavigationController = nil
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func registrationFinishedFailed(_ notification: Notification) {
		DispatchQueue.main.async {
			
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
