//
//  HomescreenViewController+ExploreViewControllerDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-14.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension HomescreenViewController: ExploreViewControllerDelegate {
	
	//	------------------------------------------------------------------------------------------------------------
	func cooperToAddress(_ place: GooglePlaceObject) {
		
		print("Configure ride for cooper to explored address \(String(describing: place.name)) \(String(describing: place.address))")
		
		self.navigationController?.presentedViewController?.dismiss(animated: true, completion: {
			let ride = Ride()
			if let currentLocation = self.currentLocation,
				let currentAddress = self.currentAddress {
				
				ride.origin = RouteEndPoint(
					withLatitude: currentLocation.latitude,
					withLongitude: currentLocation.longitude,
					withAddress: currentAddress)
				ride.destination = RouteEndPoint(
					withLatitude: place.latitude!.doubleValue,
					withLongitude: place.longitude!.doubleValue,
					withAddress: place.address!)
				
				print("Configure ride for cooper to explored address \(String(describing: ride.origin))")
				if let currentBottomController = self.currentBottomController as? HomescreenRouteEndpointsViewController{
					currentBottomController.update(
						withOrigin: ride.origin?.address,
						withDestination: ride.destination?.address)
				}
				
				//	Set the current ride
				self.currentRide = ride
				//	If we have payment methods fetched from the server, set the first payment method
				if let firstPaymentMethod = self.paymentMethods.first {
					self.currentRide?.paymentMethod = firstPaymentMethod
				}
				//	Set the current ride planning state
				self.ridePlanningState = .searchingLocations
				
				//	Get a route for the ride - from google maps
				self.currentRoutePolyline = nil
				self.getRoute(withRide: ride)
				//	While getting a route also try and get available services
				self.availableFareServices.removeAll()
				RoutingAndMapServices.sharedInstance().getAvailableServices()
			}
		})
	}
	//	------------------------------------------------------------------------------------------------------------
}
