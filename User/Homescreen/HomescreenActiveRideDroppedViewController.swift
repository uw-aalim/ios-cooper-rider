//
//  HomescreenActiveRideDroppedViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-19.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class HomescreenActiveRideDroppedViewController: UIViewController {

    // MARK: Constants
    
    struct Metrict {
        static let viewHeight: CGFloat = 419.0
    }
    
    
    // MARK: UI
    
    @IBOutlet weak var bookingIDLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeTakenLabel: UILabel!
    @IBOutlet weak var baseFareLabel: UILabel!
    @IBOutlet weak var distanceFareLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var walletLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var totalFareLabel: UILabel!
    @IBOutlet weak var walletTitleLabel: UILabel!
    
    @IBOutlet weak var invoicePaymentModeLabel: UILabel!
    @IBOutlet weak var invoicePaymentImageView: UIImageView!
    
    @IBOutlet weak var waitingForPaymentView: UIView!
    @IBOutlet weak var invoicePayNowButton: UIButton!
    
    
    // MARK: Initializing
    
    @objc
    class func initController() -> HomescreenActiveRideDroppedViewController {
        return HomescreenActiveRideDroppedViewController(
            nibName: "HomescreenActiveRideDroppedViewController",
            bundle: nil)
    }
    
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func update(_ payment: ActiveRidePayment, paymentMode: String?, travelTime: Double?, distance: Double?, bookingID: String?) {
        self.loadViewIfNeeded()
        
        bookingIDLabel.text = bookingID ?? ""
        distanceLabel.text = "\(distance ?? 0) KM"
        timeTakenLabel.text = "\(travelTime ?? 0) minutes"
        baseFareLabel.text = "$ \(payment.fixed ?? 0)"
        distanceFareLabel.text = "$ \(payment.distance ?? 0)"
        taxLabel.text = "$ \(payment.tax ?? 0)"
        discountLabel.text = "$ \(payment.discount ?? 0)"
        totalFareLabel.text = "$ \(payment.total ?? 0)"
        
        if let walletAmt = payment.wallet {
            walletTitleLabel.isHidden = false
            walletLabel.text = "\(walletAmt)"
        } else {
            walletTitleLabel.isHidden = true
            walletLabel.text = ""
        }
        
        // TODO: display payment mode(cash)
        
        if Utilities.removeNull(from: paymentMode) == "CASH",
            let paymentMode = paymentMode {
            invoicePaymentModeLabel.text = paymentMode
            invoicePaymentImageView.image = #imageLiteral(resourceName: "money_icon")
            waitingForPaymentView.isHidden = false
            invoicePayNowButton.isHidden = true
        } else {
            waitingForPaymentView.isHidden = true
            invoicePayNowButton.isHidden = false
            invoicePaymentModeLabel.text = paymentMode ?? ""
            invoicePaymentImageView.image = #imageLiteral(resourceName: "visa")
            invoicePayNowButton.setTitle("PAY NOW", for: .normal)
        }
    }
    
    @IBAction func payNowButtonDidTap(_ sender: Any) {
        print("payNowButtonDidTap")
    }
    
}
