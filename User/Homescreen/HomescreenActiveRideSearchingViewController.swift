//
//  HomescreenActiveRideSearchingViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-19.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class HomescreenActiveRideSearchingViewController: UIViewController {

    
	//	============================================================================================================
	//	MARK:- Constants
	//	============================================================================================================
	
	static let height: CGFloat = 144.0
	
	
	//	============================================================================================================
	//	MARK:- View Controller Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	class func initController() -> HomescreenActiveRideSearchingViewController {
		return HomescreenActiveRideSearchingViewController(
			nibName: "HomescreenActiveRideSearchingViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Loading and Appearance
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
    }
	//	------------------------------------------------------------------------------------------------------------

	
	
	
	
	//	============================================================================================================
	//	MARK:- Action Responders
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	/**
	If the user cancels the ride they should automatically be transitioned back to the no destination searching state.
	*/
    @IBAction func cancelRideButtonDidTap(_ sender: Any) {
        RoutingAndMapServices.sharedInstance().cancelRideService(completion: { (error, success) in
            if let parent = self.parent as? HomescreenViewController {
                DispatchQueue.main.async {
                    parent.transition(toNewHomescreenState: .none)
                }
            }
        })
    }
	//	------------------------------------------------------------------------------------------------------------
}
