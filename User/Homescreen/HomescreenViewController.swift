//
//  HomescreenViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-08.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces
import GoogleMaps

class HomescreenViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
    @IBOutlet weak var btnSOS: UIButton!
    @IBOutlet weak var menuContainerView: UIView!
	@IBOutlet weak var menuLeading: NSLayoutConstraint!
	
	//@IBOutlet weak var cancelRideButton: UIButton!
	
	@IBOutlet weak var mapView: GMSMapView!
	var mapLoaded = false
	
	@IBOutlet weak var bottomDetailHeight: NSLayoutConstraint!
	@IBOutlet weak var bottomDetailContainerView: UIView!
	weak var currentBottomController: UIViewController?
	
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var tintView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var exploreView: UIView!
    
	var firstViewWillAppear: Bool = true
	var viewDidAppearTime: Date?
	var loginWelcomeNavigationController: UINavigationController? = nil
	
	var locationManager: CLLocationManager = CLLocationManager()
	var currentLocation: CLLocationCoordinate2D?
	var currentAddress: String?
	var isUpdatingLocation: Bool = false
	var mapViewLoaded: Bool = false
	var centreOnCurrentLocation: Bool = false
	
	
	
	var homescreenState: HomescreenState = .none
	
	var ridePlanningState: PlanningRideState = .none
	var rideStatus: RideStatus? = nil
	var currentRide: Ride? = nil
	var currentRoutePolyline: GMSPolyline? = nil
	
	var availableFareServices: [FareService] = [FareService]()
	var isGettingCurrentLocation = false
	
	var destinationPin: MapDestinationView? = nil
	var originMarker: MapOriginMarkerView? = nil
	var destinationMarker: MapDestinationMarkerView? = nil
    
	var activeRideTimer: Timer? = nil
	var checkingRideService: Bool = false
	var activeRideState: ActiveRideState = .none
	var activeRideResponse: ActiveRideResponse? = nil
	
	var paymentMethods = [RidePayment]()
	
	var placeHelper: GooglePlacesSearchHelper? = nil
	
	
    var markerList: [GMSMarker] = []
    var timer: Timer!
	
	//	============================================================================================================
	//	MARK:- Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	class func initController() -> HomescreenViewController {
		return UIStoryboard(
			name: "Homescreen",
			bundle: nil)
			.instantiateViewController(
				withIdentifier: "HomescreenViewController") as! HomescreenViewController
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Loading Appearance and Dissapearance
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
		
		//	Add a menuController to the homescreen view
		let menuController = MenuViewController.initController()
		self.menuContainerView.addSubview(menuController.view)
		self.addChildViewController(menuController)
		menuController.view.translatesAutoresizingMaskIntoConstraints = false
		menuController.view.constrain(toContainerView: self.menuContainerView)
		menuController.delegate = self
		self.menuContainerView.setNeedsLayout()
		
		let routeEndpointsController = HomescreenRouteEndpointsViewController.initController()
		self.addChildViewController(routeEndpointsController)
		self.bottomDetailContainerView.addSubview(routeEndpointsController.view)
		routeEndpointsController.view.translatesAutoresizingMaskIntoConstraints = false
		routeEndpointsController.view.constrain(toContainerView: self.bottomDetailContainerView)
		self.bottomDetailContainerView.setNeedsLayout()
		self.currentBottomController = routeEndpointsController
		
		self.tintView.alpha = 0.0
		
		//	Register for the login notifications
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(HomescreenViewController.registrationFinishedSuccess(_:)),
			name: CoOperNotification.loginRegisterProcessSuccess,
			object: nil)
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(HomescreenViewController.registrationFinishedFailed(_:)),
			name: CoOperNotification.loginRegisterProcessFailed,
			object: nil)
		
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(HomescreenViewController.fareServicesFetched(_:)),
			name: CoOperNotification.fairServicesFetchSuccess,
			object: nil)
        
        // --- Nami Added: Timer for fetching providers and update their location every 5 seconds
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { (_) in
            self.fetchProviders()
        })
    }
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if self.firstViewWillAppear {
			
			///NSString *tokenStr = [defaults valueForKey:@"access_token"];
			let tokenStr = UserDefaults.standard.value(forKey: "access_token") as? String
			print("HomescreenViewController viewWillAppear - access_token \(String(describing: tokenStr))")
            if !(DataStore.shared.userIsLoggedIn) {
				self.showWelcome(withAnimation: false)
            }
				/*
			else {
				print("HomescreenViewController viewWillAppear - validBoolStr \(String(describing: UserDefaults.standard.value(forKey: "is_valid") as? String))")
				if let validBoolStr = UserDefaults.standard.value(forKey: "is_valid") as? String,
					let validBoolInt = Int(validBoolStr),
					validBoolInt > 0 {
					
					//	The user is valid - don't show login view
				}
				else {
					self.showWelcome(withAnimation: false)
				}
			}*/
            

            
			self.firstViewWillAppear = false
            
            
		}
        
        
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		//	Check if user is logged in
		if DataStore.shared.userIsLoggedIn {
			//	If the user is logged in then we should perform the initial map setup
			self.locationManager.delegate = self
			
			print("View Did Appear before 5 minutes")
			if self.viewDidAppearTime != nil &&
				(self.viewDidAppearTime!.timeIntervalSinceNow * -1.0) > (60 * 5) {
				
				print("View Did Appear after 5 minutes")
				
				
			}
			
			if CLLocationManager.locationServicesEnabled() {
				let status = CLLocationManager.authorizationStatus()
				
				switch status {
				case .authorizedWhenInUse, .authorizedAlways:
					if !self.isUpdatingLocation {
						self.isUpdatingLocation = true
						self.locationManager.startUpdatingLocation()
					}
				case .notDetermined:
					self.locationManager.requestWhenInUseAuthorization()
				case .denied, .restricted:
					self.showError(
						withTitle: "Location Error",
						andMessage: "You have not enabled access to your location, to re-enable, please go to Settings and turn on Location Service for this app.")
				}
			}
			
			
			RoutingAndMapServices.sharedInstance().getAllCards { (_ error: String?, _ cards: [Any]?) in
				if let cards = cards as? [NSDictionary] {
					print("Fetched all cards \(cards)")
					for card in cards {
						
						if let brand = card["brand"] as? String,
							brand == "CASH" {
							
							let ridePayment = RidePayment()
							ridePayment.paymentType = .cash
							ridePayment.brand = brand
							ridePayment.cardId = ""
							self.paymentMethods.append(ridePayment)
						}
						else if let brand = card["brand"] as? String,
							let cardId = card["card_id"] as? String,
							let lastFour = card["last_four"] as? String {
							
							let ridePayment = RidePayment()
							ridePayment.brand = brand
							ridePayment.cardId = cardId
							ridePayment.lastFour = lastFour
							self.paymentMethods.append(ridePayment)
						}
					}
				}
			}
			
			self.viewDidAppearTime = Date()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	// --- Nami Added: ------ Fetch Providers
    func fetchProviders() {
        // get drivers and show on map
        if (self.availableFareServices.count > 0){
            if let serviceId = self.availableFareServices[0].serviceId {
                let lat: Double = Double((self.currentLocation?.latitude)!)
                let lng: Double = Double((self.currentLocation?.longitude)!)
                self.getProviders(serviceId: Int(truncating: serviceId),
                                  lat: lat,
                                  lng: lng)
            }
        }
    }
		
	
	//	============================================================================================================
	//	MARK:- View Layout
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		self.configureMenuLeading()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewWillTransition(
		to size: CGSize,
		with coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransition(
			to: size,
			with: coordinator)
		
		self.configureMenuLeading(withViewSize: size)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	/**
	Whenever the size of the view will change (when changing trait collections for example) if the menu
	is not visible then it should automatically make sure that the menu continues to be hidden by
	setting the leading constraint to be the width of the full new view.
	*/
	func configureMenuLeading(
		withViewSize viewSize: CGSize? = nil) {
		
		if self.menuLeading.constant < 0 {
			if let viewSize = viewSize {
				self.menuLeading.constant = -viewSize.width
			}
			else {
				self.menuLeading.constant = -self.menuContainerView.frame.size.width
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	
	//	------------------------------------------------------------------------------------------------------------
	func showSearchInfo() {
		self.currentRide = nil
		
		self.bottomDetailHeight.constant = HomescreenRouteEndpointsViewController.height
		let routeEndpointsController = HomescreenRouteEndpointsViewController.initController()
		self.addChildViewController(routeEndpointsController)
		self.bottomDetailContainerView.addSubview(routeEndpointsController.view)
		
		routeEndpointsController.view.translatesAutoresizingMaskIntoConstraints = false
		routeEndpointsController.view.constrain(toContainerView: self.bottomDetailContainerView)
		self.bottomDetailContainerView.setNeedsLayout()
		
		self.bottomDetailContainerView.setNeedsLayout()
		
		UIView.animate(
			withDuration: 0.2,
			delay: 0.0,
			options: .curveEaseOut,
			animations: {
				self.tintView.alpha = 0.0
				self.view.layoutIfNeeded()
				self.currentBottomController?.view.alpha = 0
		}) { (complete: Bool) in
			self.currentBottomController?.view.removeFromSuperview()
			self.currentBottomController?.removeFromParentViewController()
			
			self.currentBottomController = routeEndpointsController
			
			if let currentBottomController = self.currentBottomController as? HomescreenRouteEndpointsViewController {
				currentBottomController.update(
					withOrigin: self.currentAddress,
					withDestination: nil)
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func showFareInfo() {
		
		if self.currentRide != nil {
			
			self.bottomDetailHeight.constant = HomescreenFareInfoPageViewController.height
			let homescreenFareInfoViewController = HomescreenFareInfoPageViewController.initController()
			homescreenFareInfoViewController.fareServices = self.availableFareServices
			homescreenFareInfoViewController.ride = self.currentRide!
			
			self.addChildViewController(homescreenFareInfoViewController)
			self.bottomDetailContainerView.insertSubview(homescreenFareInfoViewController.view, at: 0)
			
			homescreenFareInfoViewController.view.translatesAutoresizingMaskIntoConstraints = false
			homescreenFareInfoViewController.view.constrain(toContainerView: self.bottomDetailContainerView)
			
			self.bottomDetailContainerView.setNeedsLayout()
			
			UIView.animate(
				withDuration: 0.2,
				delay: 0.0,
				options: .curveEaseOut,
				animations: {
					self.view.layoutIfNeeded()
					self.currentBottomController?.view.alpha = 0
			}) { (complete: Bool) in
				self.currentBottomController?.view.removeFromSuperview()
				self.currentBottomController?.removeFromParentViewController()
				
				self.currentBottomController = homescreenFareInfoViewController
				
				//	Update the markers
				self.updateMarkers()
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	//	------------------------------------------------------------------------------------------------------------
	func showScheduleInfo() {
		
		self.bottomDetailHeight.constant = HomescreenScheduleRideViewController.height
		let homescreenScheduleRideViewController = HomescreenScheduleRideViewController.initController()
		
		self.addChildViewController(homescreenScheduleRideViewController)
		self.bottomDetailContainerView.insertSubview(homescreenScheduleRideViewController.view, at: 0)
		
		homescreenScheduleRideViewController.view.translatesAutoresizingMaskIntoConstraints = false
		homescreenScheduleRideViewController.view.constrain(toContainerView: self.bottomDetailContainerView)
		homescreenScheduleRideViewController.delegate = self
		
		self.bottomDetailContainerView.setNeedsLayout()
		
		UIView.animate(
			withDuration: 0.2,
			delay: 0.0,
			options: .curveEaseOut,
			animations: {
				self.view.layoutIfNeeded()
				self.currentBottomController?.view.alpha = 0
		}) { (complete: Bool) in
			self.currentBottomController?.view.removeFromSuperview()
			self.currentBottomController?.removeFromParentViewController()
			
			self.currentBottomController = homescreenScheduleRideViewController
			
			//	Update the markers - after layout may need to adjust the height and therefore the position of the markers
			self.updateMarkers()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	//	------------------------------------------------------------------------------------------------------------
	func showConfirmRide() {
		
		self.bottomDetailHeight.constant = HomescreenConfirmRideViewController.height
		let homescreenConfirmRideViewController = HomescreenConfirmRideViewController.initController()
		homescreenConfirmRideViewController.ride = self.currentRide!
		
		self.addChildViewController(homescreenConfirmRideViewController)
		self.bottomDetailContainerView.insertSubview(homescreenConfirmRideViewController.view, at: 0)
		
		homescreenConfirmRideViewController.view.translatesAutoresizingMaskIntoConstraints = false
		homescreenConfirmRideViewController.view.constrain(toContainerView: self.bottomDetailContainerView)
		
		
		self.bottomDetailContainerView.setNeedsLayout()
		
		UIView.animate(
			withDuration: 0.2,
			delay: 0.0,
			options: .curveEaseOut,
			animations: {
				self.view.layoutIfNeeded()
				self.currentBottomController?.view.alpha = 0
		}) { (complete: Bool) in
			self.currentBottomController?.view.removeFromSuperview()
			self.currentBottomController?.removeFromParentViewController()
			
			self.currentBottomController = homescreenConfirmRideViewController
			
			//	Update the markers - after layout may need to adjust the height and therefore the position of the markers
			self.updateMarkers()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func showFindingRide() {
		
		self.bottomDetailHeight.constant = HomescreenActiveRideSearchingViewController.height
		let homescreenActiveRideViewController = HomescreenActiveRideSearchingViewController.initController()
		
		self.addChildViewController(homescreenActiveRideViewController)
		self.bottomDetailContainerView.insertSubview(homescreenActiveRideViewController.view, at: 0)
		
		homescreenActiveRideViewController.view.translatesAutoresizingMaskIntoConstraints = false
		homescreenActiveRideViewController.view.constrain(toContainerView: self.bottomDetailContainerView)
		
		self.bottomDetailContainerView.setNeedsLayout()
		
		UIView.animate(
			withDuration: 0.2,
			delay: 0.0,
			options: .curveEaseOut,
			animations: {
				self.view.layoutIfNeeded()
				self.currentBottomController?.view.alpha = 0
		}) { (complete: Bool) in
			self.currentBottomController?.view.removeFromSuperview()
			self.currentBottomController?.removeFromParentViewController()
			
			self.currentBottomController = homescreenActiveRideViewController
			
			//	Update the markers - after layout may need to adjust the height and therefore the position of the markers
			self.updateMarkers()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
    // Nami Added: ------
    func showMenuButton() {
        self.btnMenu.isHidden = false
        self.exploreView.isHidden = false
        self.btnBack.isHidden = true
    }
    func hideMenuButton() {
        self.btnMenu.isHidden = true
        self.exploreView.isHidden = true
        self.btnBack.isHidden = false
    }
	
	//	------------------------------------------------------------------------------------------------------------
	func showRideStarted() {
		self.bottomDetailHeight.constant = HomescreenActiveRideStartedArrivedPickedUpViewController.height
		let homescreenActiveRideViewController = HomescreenActiveRideStartedArrivedPickedUpViewController.initController()
		
		if let activeResponse = self.activeRideResponse {
			homescreenActiveRideViewController.update(
				withNewStatus: "Driver has accepted your request",
				
				withDriverFirstName: activeResponse.provider?.firstName,
				withDriverLastName: activeResponse.provider?.lastName,
				withDriverRating: activeResponse.provider?.rating,
				withDriverPhoneNumber: activeResponse.provider?.mobileNumber,
				
				withProviderServiceImage: activeResponse.serviceType?.image,
				withProviderServiceType: activeResponse.serviceType?.name,
				withVehicleModel: activeResponse.providerService?.serviceModel,
				
				withOtp: activeResponse.otp)
            
            // --- Nami Added: Zoom to driver
            UIView.animate(
                withDuration: 0.2,
                delay: 0.3,
                options: .curveEaseOut,
                animations: {
                    
            }) { (complete: Bool) in
                // Zoom to driver
                let lat = Double((activeResponse.provider?.latitude)!)
                let lng = Double((activeResponse.provider?.longitude)!)
                let zoom:Float = 18.0
                
                self.updateCamera(lat: lat, lng: lng, zoom: zoom)
                
                // Hide Menu Buttons
                self.hideMenuButton()
            }
            
		}
		
		self.addChildViewController(homescreenActiveRideViewController)
		self.bottomDetailContainerView.insertSubview(homescreenActiveRideViewController.view, at: 0)
		
		homescreenActiveRideViewController.view.translatesAutoresizingMaskIntoConstraints = false
		homescreenActiveRideViewController.view.constrain(toContainerView: self.bottomDetailContainerView)
		
		self.bottomDetailContainerView.setNeedsLayout()
		
		UIView.animate(
			withDuration: 0.2,
			delay: 0.0,
			options: .curveEaseOut,
			animations: {
				self.view.layoutIfNeeded()
				self.currentBottomController?.view.alpha = 0
		}) { (complete: Bool) in
			self.currentBottomController?.view.removeFromSuperview()
			self.currentBottomController?.removeFromParentViewController()
			
			self.currentBottomController = homescreenActiveRideViewController
			
			//	Update the markers - after layout may need to adjust the height and therefore the position of the markers
			self.updateMarkers()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func showRideArrived() {
		if let homescreenActiveRideViewController = self.currentBottomController as? HomescreenActiveRideStartedArrivedPickedUpViewController,
			let activeResponse = self.activeRideResponse {
			homescreenActiveRideViewController.update(
				withNewStatus: "Driver has arrived at your location",
				
				withDriverFirstName: activeResponse.provider?.firstName,
				withDriverLastName: activeResponse.provider?.lastName,
				withDriverRating: activeResponse.provider?.rating,
				withDriverPhoneNumber: activeResponse.provider?.mobileNumber,
				
				withProviderServiceImage: activeResponse.serviceType?.image,
				withProviderServiceType: activeResponse.serviceType?.name,
				withVehicleModel: activeResponse.providerService?.serviceModel,
				
				withOtp: activeResponse.otp)
		}
		/*
		self.bottomDetailHeight.constant = HomescreenConfirmRideViewController.height
		let homescreenConfirmRideViewController = HomescreenConfirmRideViewController.initController()
		
		self.addChildViewController(homescreenConfirmRideViewController)
		self.bottomDetailContainerView.insertSubview(homescreenConfirmRideViewController.view, at: 0)
		
		homescreenConfirmRideViewController.view.translatesAutoresizingMaskIntoConstraints = false
		homescreenConfirmRideViewController.view.constrain(toContainerView: self.bottomDetailContainerView)
		
		self.bottomDetailContainerView.setNeedsLayout()
		
		UIView.animate(
		withDuration: 0.2,
		delay: 0.0,
		options: .curveEaseOut,
		animations: {
		self.view.layoutIfNeeded()
		self.currentBottomController?.view.alpha = 0
		}) { (complete: Bool) in
		self.currentBottomController?.view.removeFromSuperview()
		self.currentBottomController?.removeFromParentViewController()
		
		self.currentBottomController = homescreenConfirmRideViewController
		
		//	Update the markers - after layout may need to adjust the height and therefore the position of the markers
		self.updateMarkers()
		}*/
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func showRidePickedUp() {
		if let homescreenActiveRideViewController = self.currentBottomController as? HomescreenActiveRideStartedArrivedPickedUpViewController,
			let activeResponse = self.activeRideResponse {
            self.btnSOS.isHidden=false///////////////////Zencode
            
				homescreenActiveRideViewController.update(
					withNewStatus: "You are on your way",
					
					withDriverFirstName: activeResponse.provider?.firstName,
					withDriverLastName: activeResponse.provider?.lastName,
					withDriverRating: activeResponse.provider?.rating,
					withDriverPhoneNumber: activeResponse.provider?.mobileNumber,
					
					withProviderServiceImage: activeResponse.serviceType?.image,
					withProviderServiceType: activeResponse.serviceType?.name,
					withVehicleModel: activeResponse.providerService?.serviceModel,
					
					withOtp: activeResponse.otp)
		}
		/*
		self.bottomDetailHeight.constant = HomescreenConfirmRideViewController.height
		let homescreenConfirmRideViewController = HomescreenConfirmRideViewController.initController()
		
		self.addChildViewController(homescreenConfirmRideViewController)
		self.bottomDetailContainerView.insertSubview(homescreenConfirmRideViewController.view, at: 0)
		
		homescreenConfirmRideViewController.view.translatesAutoresizingMaskIntoConstraints = false
		homescreenConfirmRideViewController.view.constrain(toContainerView: self.bottomDetailContainerView)
		
		self.bottomDetailContainerView.setNeedsLayout()
		
		UIView.animate(
		withDuration: 0.2,
		delay: 0.0,
		options: .curveEaseOut,
		animations: {
		self.view.layoutIfNeeded()
		self.currentBottomController?.view.alpha = 0
		}) { (complete: Bool) in
		self.currentBottomController?.view.removeFromSuperview()
		self.currentBottomController?.removeFromParentViewController()
		
		self.currentBottomController = homescreenConfirmRideViewController
		
		//	Update the markers - after layout may need to adjust the height and therefore the position of the markers
		self.updateMarkers()
		}*/
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func showRideDropped() {
        self.bottomDetailHeight.constant = HomescreenActiveRideDroppedViewController.Metrict.viewHeight
        let homescreenActiveRideDroppedViewController = HomescreenActiveRideDroppedViewController.initController()
                if let activeResponse = self.activeRideResponse {
            self.btnSOS.isHidden=true///////////////////Zencode
            if let payment = activeResponse.payment,
                let paymentMode = activeResponse.paymentMode {
                homescreenActiveRideDroppedViewController.update(payment, paymentMode: paymentMode, travelTime: activeResponse.travelTime, distance: activeResponse.distance, bookingID: activeResponse.bookingId)
            } else {
                print("error")
            }
        }
        
        self.addChildViewController(homescreenActiveRideDroppedViewController)
        self.bottomDetailContainerView.insertSubview(homescreenActiveRideDroppedViewController.view, at: 0)
        
        homescreenActiveRideDroppedViewController.view.translatesAutoresizingMaskIntoConstraints = false
        homescreenActiveRideDroppedViewController.view.constrain(toContainerView: self.bottomDetailContainerView)
        
        self.bottomDetailContainerView.setNeedsLayout()
        
        UIView.animate(
            withDuration: 0.2,
            delay: 0.0,
            options: .curveEaseOut,
            animations: {
                self.view.layoutIfNeeded()
                self.currentBottomController?.view.alpha = 0
        }) { (complete: Bool) in
            self.currentBottomController?.view.removeFromSuperview()
            self.currentBottomController?.removeFromParentViewController()
            
            self.currentBottomController = homescreenActiveRideDroppedViewController
            
            //    Update the markers - after layout may need to adjust the height and therefore the position of the markers
            self.updateMarkers()
            
            //
            self.showMenuButton()
        }
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func showRideCompleted() {
        if let homescreenActiveRideCompletedViewController = self.currentBottomController as? HomescreenActiveRideCompletedViewController {
            homescreenActiveRideCompletedViewController.update()
            
        } else {
            print("error")
        }
        
        self.bottomDetailHeight.constant = HomescreenActiveRideCompletedViewController.Metrict.viewHeight
        let homescreenActiveRideCompletedViewController = HomescreenActiveRideCompletedViewController.initController()
        
        if self.activeRideResponse != nil {
            // TODO: update
            homescreenActiveRideCompletedViewController.delegate = self
            homescreenActiveRideCompletedViewController.update()
        }
        
        self.addChildViewController(homescreenActiveRideCompletedViewController)
        self.bottomDetailContainerView.insertSubview(homescreenActiveRideCompletedViewController.view, at: 0)
        
        homescreenActiveRideCompletedViewController.view.translatesAutoresizingMaskIntoConstraints = false
        homescreenActiveRideCompletedViewController.view.constrain(toContainerView: self.bottomDetailContainerView)
        
        self.bottomDetailContainerView.setNeedsLayout()
        
        UIView.animate(
            withDuration: 0.2,
            delay: 0.0,
            options: .curveEaseOut,
            animations: {
                self.view.layoutIfNeeded()
                self.currentBottomController?.view.alpha = 0
        }) { (complete: Bool) in
            self.currentBottomController?.view.removeFromSuperview()
            self.currentBottomController?.removeFromParentViewController()
            
            self.currentBottomController = homescreenActiveRideCompletedViewController
            
            //    Update the markers - after layout may need to adjust the height and therefore the position of the markers
            self.updateMarkers()
            
            //
            self.showMenuButton()
        }
        
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	/*
	func finishedBookingRide() {
		self.transition(toNewHomescreenState: .none)
	}
	*/
	
	
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Show the Welcome Controller for Login and Registration
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func showWelcome(withAnimation animated: Bool) {
		if self.loginWelcomeNavigationController == nil {
			let welcomeViewController = WelcomeViewController.initController()
			self.loginWelcomeNavigationController = UINavigationController.init(rootViewController: welcomeViewController)
			self.loginWelcomeNavigationController?.isNavigationBarHidden = true
			self.navigationController?.present(
				self.loginWelcomeNavigationController!,
				animated: animated, completion: {
					//	Finished showing welcome - cleanup homescreen view controller here
			})
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Action Responders
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
    
    ///////////////////Zencode
    private func callNumber(phoneNumber:String) {
        
        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                    
                }
            }
            else
            {
                 let alertController = UIAlertController(title: nil, message: "Call facility not available!", preferredStyle: .alert)
                let okAction = UIAlertAction(title: LocalizeHelper.sharedLocalSystem().localizedString(forKey:"Ok"), style: .cancel, handler: nil)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)

            }
        }
    }

    @IBAction func sosButtonPressed(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: "Are you sure want to Call Emergency?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: LocalizeHelper.sharedLocalSystem().localizedString(forKey:"YES"), style: .default) { (action) in
            if let strEmergencyNumber = Utilities.removeNull(from: UserDefaults.standard.value(forKey:UD_SOS) as? String)
          {
            self.callNumber(phoneNumber: strEmergencyNumber)
            }
            
            
        }
        let cancelAction = UIAlertAction(title: LocalizeHelper.sharedLocalSystem().localizedString(forKey:"NO"), style: .cancel, handler: nil)
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    ///////////////////Zencode
    @IBAction func menuButtonPressed(_ sender: AnyObject) {
		
		if self.menuLeading.constant < 0 {
			
			
			
			//	This quick layout without animation ensures that the menu frame is set to be inset
			//	by the correct amount before animation.
			self.menuLeading.constant = -self.menuContainerView.frame.size.width
			self.view.layoutIfNeeded()
			
			//	Show the menu by setting the leading constraint to 0
			self.menuLeading.constant = 0
			UIView.animate(
				withDuration: 0.2,
				delay: 0,
				options: .curveEaseOut,
				animations: {
					self.view.layoutIfNeeded()
			}) { (complete: Bool) in
				//	Completed showing the menu
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
    @IBAction func onBack(_ sender: Any) {
        self.menuButtonPressed(UIButton())
    }
    
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func exploreButtonPressed(_ sender: AnyObject) {
		let exploreController = ExploreViewController.initController()
		let exploreNavigationController = UINavigationController(rootViewController: exploreController)
		exploreNavigationController.isNavigationBarHidden = false
		exploreController.delegate = self
		self.navigationController?.present(
			exploreNavigationController,
			animated: true,
			completion: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func cancelButtonPressed(_ sender: AnyObject) {
		
		let alert = UIAlertController(
			title: "Cancel Ride",
			message: "Are you sure you want to cancel planning this ride?",
			preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(
			title: "CONTINUE PLANNING",
			style: .default,
			handler: nil))
		alert.addAction(UIAlertAction(
			title: "CANCEL RIDE",
			style: .cancel,
			handler: { (action: UIAlertAction) in
				
                RoutingAndMapServices.sharedInstance().cancelRideService(completion: { (error, success) in
                    DispatchQueue.main.async {
                        self.transition(toNewHomescreenState: .none)
                    }
					print("Error cancelling ride \(String(describing: error))")
					print("Successfully cancelled ride \(String(describing: success))")
                    //print(error, success)
                })
		}))
		self.present(alert, animated: true, completion: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func centreOnCurrentLocationButtonPressed(_ sender: AnyObject) {
		
		if self.mapLoaded {
			if CLLocationManager.locationServicesEnabled() {
				let status = CLLocationManager.authorizationStatus()
				
				switch status {
				case .authorizedWhenInUse, .authorizedAlways:
					if !self.isUpdatingLocation {
						self.isUpdatingLocation = true
						self.centreOnCurrentLocation = true
						self.locationManager.startUpdatingLocation()
					}
				case .notDetermined:
					self.locationManager.requestWhenInUseAuthorization()
				case .denied, .restricted:
					self.showError(
						withTitle: "Location Error",
						andMessage: "You have not enabled access to your location, to re-enable, please go to Settings and turn on Location Service for this app.")
				}
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
