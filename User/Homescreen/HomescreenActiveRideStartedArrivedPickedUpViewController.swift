//
//  HomescreenActiveRideStartedViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-19.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit
import AlamofireImage

class HomescreenActiveRideStartedArrivedPickedUpViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Constants
	//	============================================================================================================
	
	static let height: CGFloat = 312.0
	
	
	@IBOutlet weak var driverImageView: UIImageView!
	@IBOutlet weak var driverNameLabel: UILabel!
	@IBOutlet weak var starsRatingView: UIView!
	@IBOutlet weak var starRating1: UIImageView!
	@IBOutlet weak var starRating2: UIImageView!
	@IBOutlet weak var starRating3: UIImageView!
	@IBOutlet weak var starRating4: UIImageView!
	@IBOutlet weak var starRating5: UIImageView!
	
	@IBOutlet weak var statusLabel: UILabel!
	@IBOutlet weak var otpLabel: UILabel!
	
	@IBOutlet weak var fairTypeLabel: UILabel!
	@IBOutlet weak var fairImageView: UIImageView!
	@IBOutlet weak var carTypeLabel: UILabel!
	
	var driverMobileNumber: String?
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Controller Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	class func initController() -> HomescreenActiveRideStartedArrivedPickedUpViewController {
		return HomescreenActiveRideStartedArrivedPickedUpViewController(
			nibName: "HomescreenActiveRideStartedArrivedPickedUpViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Loading, Appearance, and Dissapearance
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
    }
	//	------------------------------------------------------------------------------------------------------------
    

	//	============================================================================================================
	//	MARK:- Update and configure the view
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func update(
		withNewStatus status: String,
		
		withDriverFirstName driverFirstName: String?,
		withDriverLastName driverLastName: String?,
		withDriverRating driverRating: String?,
		withDriverPhoneNumber phoneNumber: String?,
		
		withProviderServiceImage serviceImage: String?,
		withProviderServiceType serviceType: String?,
		withVehicleModel vehicleModel: String?,
		
		withOtp otp: String? = nil) {
        self.loadViewIfNeeded()
		self.statusLabel.text = status
		if let otp = otp {
            self.otpLabel.isHidden = false
			self.otpLabel.text = "Ride Code : \(otp)"
		}
		else {
			self.otpLabel.isHidden = true
		}
		
		var name = ""
		if let firstName = driverFirstName {
			name = firstName
		}
		if let lastName = driverFirstName {
			var nameSpace = ""
			if name == "" {
				nameSpace = " "
			}
			name += nameSpace + lastName
		}
		self.driverNameLabel.text = name
		
		if let vehicleModel = vehicleModel {
			self.carTypeLabel.text = vehicleModel
		}
		else {
			self.carTypeLabel.text = ""
		}
		
		if let serviceType = serviceType {
			self.fairTypeLabel.text = serviceType
		}
		else {
			self.fairTypeLabel.text = ""
		}
		
		if let imageString = serviceImage,
			let imageUrl = URL(string: imageString) {
			self.fairImageView.af_setImage(withURL: imageUrl)
		}
		
		if let rating = driverRating,
			let ratingDouble = Double(rating) {
			self.starsRatingView.isHidden = false
			self.updateStars(withRating: ratingDouble)
		}
		else {
			self.starsRatingView.isHidden = true
		}
		
		self.driverMobileNumber = phoneNumber
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	func updateStars(withRating rating: Double) {
		if rating >= 0.5 {
			self.starRating1.image = UIImage(named:"star_half")
		}
		if rating >= 1 {
			self.starRating1.image = UIImage(named:"star_full")
		}
		if rating >= 1.5 {
			self.starRating2.image = UIImage(named:"star_half")
		}
		if rating >= 2 {
			self.starRating2.image = UIImage(named:"star_full")
		}
		if rating >= 2.5 {
			self.starRating3.image = UIImage(named:"star_half")
		}
		if rating >= 3 {
			self.starRating3.image = UIImage(named:"star_full")
		}
		if rating >= 3.5 {
			self.starRating4.image = UIImage(named:"star_half")
		}
		if rating >= 4 {
			self.starRating4.image = UIImage(named:"star_full")
		}
		if rating >= 4.5 {
			self.starRating5.image = UIImage(named:"star_half")
		}
		if rating >= 5 {
			self.starRating5.image = UIImage(named:"star_full")
		}
	}
	
	
	
	//	============================================================================================================
	//	MARK:- Action Responders
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	/**
	If the user wants to call the driver they can do so while they are waiting to be picked up.
	*/
	@IBAction func callDriverButtonDidTap(_ sender: Any) {
		// TODO - call to driver
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	/**
	If the user cancels the ride they should automatically be transitioned back to the no destination searching state.
	*/
	@IBAction func cancelRideButtonDidTap(_ sender: Any) {
		RoutingAndMapServices.sharedInstance().cancelRideService(completion: { (error, success) in
			if let parent = self.parent as? HomescreenViewController {
				DispatchQueue.main.async {
					parent.transition(toNewHomescreenState: .none)
				}
			}
		})
	}
	//	------------------------------------------------------------------------------------------------------------

}
