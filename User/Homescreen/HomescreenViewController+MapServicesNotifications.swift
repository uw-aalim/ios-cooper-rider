//
//  HomescreenViewController+MapServicesNotifications.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-15.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension HomescreenViewController {
	
	@objc
	func fareServicesFetched(_ notification: Notification) {
		
		DispatchQueue.main.async {
			self.availableFareServices.removeAll()
			if let userInfo = notification.userInfo,
				let fareServices = userInfo["fair_services"] as? [FareService],
				fareServices.count > 0 {
				
				self.availableFareServices.append(contentsOf: fareServices)
				self.checkAndTransitionRidePlanningState()
			}
		}
	}
}
