//
//  HomescreenViewController+ScheduleRideDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension HomescreenViewController: HomescreenScheduleRideViewControllerDelegate {
	
	func scheduledRide(withDate date: Date?, sender: AnyObject) {
		
		if self.currentRide == nil {
			self.currentRide = Ride()
		}
		
		//	Now have a ride we are planning - need to visually show that the date has been schedule
		self.currentRide?.scheduledTime = date
		
		//print("Homescreen state is \(self.homescreenState.rawValue) schedule date is \(self.currentRide?.scheduledTime)")
		if self.homescreenState == .planningRide {
			self.checkAndTransitionRidePlanningState()
		}
		else {
			self.showSearchInfo()
		}
	}
}
