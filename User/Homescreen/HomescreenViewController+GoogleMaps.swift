//
//  HomescreenViewController+GMSMapViewDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation
import GoogleMaps
import MBProgressHUD

extension HomescreenViewController: GMSMapViewDelegate {
	
	//	------------------------------------------------------------------------------------------------------------
	func loadMapView() {
		if !self.mapLoaded {
			/*
			do {
				// Set the map style by passing the URL of the local file.
				if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
					self.mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
				} else {
					NSLog("Unable to find style.json")
				}
			} catch {
				NSLog("One or more of the map styles failed to load. \(error)")
			}*/
			self.mapView.isMyLocationEnabled = true
			
			self.mapView.delegate = self
			//self.mapView.
			
			self.centreMapOnCurrentLocation()
			self.mapLoaded = true
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func centreMapOnCurrentLocation() {
		if let currentLocation = self.currentLocation {
			
			let camera = GMSCameraPosition.camera(
				withLatitude: currentLocation.latitude,
				longitude: currentLocation.longitude,
				zoom: 16.0)
			
			self.mapView.camera = camera
			self.mapView.animate(to: camera)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	//	------------------------------------------------------------------------------------------------------------
	func fitMarkersOnMap() {
		
		if let ride = self.currentRide,
			let origin = ride.origin,
			let destination = ride.destination {
			
			let originCoordinate = CLLocationCoordinate2D(
				latitude: origin.latitude.doubleValue,
				longitude: origin.longitude.doubleValue)
			
			let destinationCoordinate = CLLocationCoordinate2D(
				latitude: destination.latitude.doubleValue,
				longitude: destination.longitude.doubleValue)
			
			let bounds: GMSCoordinateBounds = GMSCoordinateBounds(
				coordinate: originCoordinate,
				coordinate: destinationCoordinate)
			
			let cameraUpdate = GMSCameraUpdate.fit(
				bounds,
				withPadding: 116.0)
			self.mapView.animate(
				with: cameraUpdate)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func mapView(
		_ mapView: GMSMapView,
		didTapPOIWithPlaceID placeID: String,
		name: String,
		location: CLLocationCoordinate2D) {
		
		if self.placeHelper == nil {
			self.placeHelper = GooglePlacesSearchHelper()
		}
		
		var newPlace = GooglePlaceObject()
		newPlace.placeId = placeID
		newPlace.name = name
		
		
		
		let alert = UIAlertController(
			title: "Set Place Destination",
			message: "Would you like to go to \(name) from your current location?",
			preferredStyle: .actionSheet)
		
		let yes = UIAlertAction(title: "YES", style: .default) { (_ alert: UIAlertAction) in
		
			let _ = MBProgressHUD.showAdded(to: self.view, animated: true)
			self.placeHelper?.loadPlaceDetails(
				placeId: placeID,
				placeObject: newPlace,
				completion: { (_ place: GooglePlaceObject) in
					MBProgressHUD.hide(for: self.view, animated: true)
					newPlace = place
					if let originLocation = self.currentLocation,
						let originAddress = self.currentAddress,
						let destinationLatitude = newPlace.latitude,
						let destinationLongitude = newPlace.longitude,
						let destinationAddress = newPlace.address {
						
						let destinationCoordinate = CLLocationCoordinate2D(
							latitude: destinationLatitude.doubleValue,
							longitude: destinationLongitude.doubleValue)
						
						self.didChooseNewOrigin(
							originLocation,
							withOriginAddress: originAddress,
							withDestination: destinationCoordinate,
							withDestinationAddress: destinationAddress)
					}
			})
		}
		let no = UIAlertAction(title: "NO", style: .cancel, handler: nil)
		
		alert.addAction(yes)
		alert.addAction(no)
		
		self.present(alert, animated: true, completion: nil)
		
		print("You tapped \(name): \(placeID), \(location.latitude)/\(location.longitude)")
		
		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func getCurrentLocationInfo(
		atCoordinate coordinate: CLLocationCoordinate2D) {
		
		CoOperGoogleServices.shared.getCurrentLocationAddress(
		atCoordinate: coordinate) { (_ currentAddress: String?, _ errorString: String?) in
		
			
			DispatchQueue.main.async {
				print("Should get current location info")
				
				self.isGettingCurrentLocation = false
				
				print("Get current location info \(currentAddress)")
				
				if let currentAddress = currentAddress {
					self.currentLocation = coordinate
					self.currentAddress = currentAddress
					
					if self.currentRide == nil {
						
						if let currentAddress = self.currentAddress,
							let currentLocation = self.currentLocation {
							
							print("Get Current Location Info Set current latitude and longitude in key constants")
							let newLocationInfo = [
								sourceLat:NSString(format: "%@", NSNumber(value :currentLocation.latitude)),
								sourceLang:NSString(format: "%@", NSNumber(value: currentLocation.longitude)),
								SourceLocations: NSString(string: currentAddress)]
							KeyConstants.shared().locationInfo = newLocationInfo
							
							print("Set source location in key constants \(newLocationInfo)")
							print("Set source location in key constants \(KeyConstants())")
							print("Set source location in key constants \(String(describing: KeyConstants().locationInfo))")
		 				}
						
						if let currentBottomController = self.currentBottomController as? HomescreenRouteEndpointsViewController{
							currentBottomController.update(
								withOrigin: self.currentAddress,
								withDestination: nil)
						}
                        
                        // --- Get available services & get providers for service[0].id
                        RoutingAndMapServices.sharedInstance().getAvailableServices()
					}
				}
				else if let errorString = errorString {
					print("Error - \(errorString)")
				}
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
    
    // --- Nami Added: Draw Providers
    func getProviders(serviceId: Int, lat: Double, lng: Double) {
        RoutingAndMapServices.sharedInstance().getProvidersInCurrentLocation(serviceId as NSNumber,
                                                                             lat: lat as NSNumber,
                                                                             lng: lng as NSNumber,
                                                                             withCompletion: { (_ error: String?, _ providers: [Any]?) in
            if let providers = providers as? [NSDictionary] {
                print("Fetched all providers \(providers)")
                
                self.markerList.removeAll()
                self.mapView.clear()
                
                for provider in providers {
                    let lat = provider["latitude"] as! Double
                    let lng = provider["longitude"] as! Double
                    let title = provider["first_name"] as! String
                    
                    self.drawMaker(lat: lat,
                                   lng: lng,
                                   title: title,
                                   isProvider: true,
                                   imageURL: "")
                }
                
                self.updateMarkers()
            }
        })
    }
    
    func drawMaker(lat: Double, lng: Double, title: String, isProvider: Bool, imageURL: String) {
        let position:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat, lng)
        let marker:GMSMarker = GMSMarker.init(position: position)
        marker.title = title
            
        if(isProvider){
            let animation:LOTAnimationView = LOTAnimationView.init(name: "suv")
            animation.frame = CGRect.init(x: 0, y: 0, width: 50, height: 60)
            animation.loopAnimation = true
            
            animation.play { (_) in
                
            }
            marker.iconView = animation
            
            // marker.iconView = marker;
            // marker.icon = [UIImage imageNamed:imageURL];
        }
        else {
            marker.icon = UIImage.init(named: imageURL)
            self.markerList.append(marker)
        }
            
        marker.tracksViewChanges = true
        marker.map = self.mapView
    }
	
	//	------------------------------------------------------------------------------------------------------------
	func getRoute(
		withRide ride: Ride) {
		
		CoOperGoogleServices.shared.getRoute(
			withRide: ride) { (_ polylineDictionary: String?, _ errorString: String?) in
			
				DispatchQueue.main.async {
					
					if let polylineDictionary = polylineDictionary {
						self.mapView.clear()
						
						let path = GMSPath(fromEncodedPath: polylineDictionary)
						let polyline = GMSPolyline(path: path)
						self.currentRoutePolyline = polyline
						self.checkAndTransitionRidePlanningState()
					}
					else if let errorString = errorString {
						print("Error - \(errorString)")
						self.showError(withTitle: "Search Error", andMessage: errorString)
						self.transition(toNewHomescreenState: .none)
					}
				}
		}		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func addPathForRide() {
		if let polyline = self.currentRoutePolyline {
			polyline.strokeWidth = 5.0
			polyline.strokeColor = UIColor.tealColour()
			polyline.map = self.mapView
			print("Fit polyline onto map \(polyline)")
			self.fitMarkersOnMap()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	func tintMap() {
		self.tintView.alpha = 0.25
	}
	
	// --- Nami Edited:	---------------------------------------------------------------------------------------------
	func addLocationMarkers() {
		
		self.originMarker?.removeFromSuperview()
		self.originMarker = nil
		self.destinationMarker?.removeFromSuperview()
		self.destinationMarker = nil
		self.destinationPin?.removeFromSuperview()
		self.destinationPin = nil
		
		if let ride = self.currentRide,
			let origin = ride.origin,
			let destination = ride.destination {
			
			self.originMarker = MapOriginMarkerView(frame: MapOriginMarkerView.frameSize)
			self.view.insertSubview(self.originMarker!, belowSubview: self.btnMenu)
			self.originMarker?.configure(withAddress: origin.address)
			
			self.destinationMarker = MapDestinationMarkerView(frame: MapDestinationMarkerView.frameSize)
            self.view.insertSubview(self.destinationMarker!, belowSubview: self.btnMenu)
			self.destinationMarker?.configure(withAddress: destination.address)
			
			self.destinationPin = MapDestinationView(frame: MapDestinationView.frameSize)
			self.view.insertSubview(self.destinationPin!, belowSubview: self.btnMenu)
		}
		
		self.updateMarkers()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func removeMarkers() {
		self.originMarker?.removeFromSuperview()
		self.originMarker = nil
		self.destinationMarker?.removeFromSuperview()
		self.destinationMarker = nil
		self.destinationPin?.removeFromSuperview()
		self.destinationPin = nil
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func mapView(
		_ mapView: GMSMapView,
		didChange position: GMSCameraPosition) {
		
		self.updateMarkers()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	//	------------------------------------------------------------------------------------------------------------
	func updateMarkers() {
		if let originMarker = self.originMarker,
			let destinationMarker = self.destinationMarker,
			let ride = self.currentRide {
			
			let origin = CLLocationCoordinate2D(
				latitude: ride.origin!.latitude.doubleValue,
				longitude: ride.origin!.longitude.doubleValue)
			originMarker.addTarget(
				self,
				action: #selector(HomescreenViewController.markerTapped(_:)),
				for: .touchUpInside)
			
			if let distance = ride.fareEstimate?.distance?.doubleValue {
				originMarker.configure(
					withAddress: ride.origin!.address,
					destination: "\(distance.rounded()) km")
			}
			
			let destination = CLLocationCoordinate2D(
				latitude: ride.destination!.latitude.doubleValue,
				longitude: ride.destination!.longitude.doubleValue)
			destinationMarker.addTarget(
				self,
				action: #selector(HomescreenViewController.markerTapped(_:)),
				for: .touchUpInside)
			
			var originCentre = self.mapView.projection.point(
				for: origin)
			var destinationCentre = self.mapView.projection.point(
				for: destination)
			
			self.destinationPin?.center = destinationCentre
			if originCentre.y > destinationCentre.y {
				originCentre.y = originCentre.y + 36
				destinationCentre.y = destinationCentre.y - 36
			}
			else {
				originCentre.y = originCentre.y - 36
				destinationCentre.y = destinationCentre.y + 36
			}
			if originCentre.x > destinationCentre.x {
				originCentre.x = originCentre.x - 4
				destinationCentre.x = destinationCentre.x + 4
			}
			else {
				originCentre.x = originCentre.x + 4
				destinationCentre.x = destinationCentre.x - 4
			}
			
			originMarker.center = originCentre
			destinationMarker.center = destinationCentre
            
            // --- Nami Added: Draw Polygon Path
            if let polyline = self.currentRoutePolyline {
                polyline.strokeWidth = 5.0
                polyline.strokeColor = UIColor.tealColour()
                polyline.map = self.mapView
            }
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
    
    func updateCamera(lat: Double, lng: Double, zoom: Float){
        let camera = GMSCameraPosition.camera(
            withLatitude: lat,
            longitude: lng,
            zoom: zoom)
        
        self.mapView.camera = camera
        self.mapView.animate(to: camera)
    }
	
	//	------------------------------------------------------------------------------------------------------------
	@objc func markerTapped(_ sender: AnyObject) {
		
		if let sender = sender as? MapOriginMarkerView {
			print("Tapped origin marker \(String(describing: sender.addressLabel.text))")
			
			if let ride = self.currentRide,
				let origin = ride.origin,
				let destination = ride.destination {
				let locationController = newLocationController.initController()
				locationController.delegate = self
				locationController.didPresentFromSourceLocation = true
				
				locationController.sourceCoordinate = origin.position
				locationController.sourceLocation = origin.address
				locationController.originTextField?.text = origin.address
				
				locationController.destinationCoordinate = destination.position
				locationController.destinationLocation = destination.address
				locationController.destinationTextField?.text = destination.address
				
				self.present(locationController, animated: true, completion: nil)
			}
		}
		else if let sender = sender as? MapDestinationMarkerView {
			print("Tapped destination marker \(String(describing: sender.addressLabel.text))")
			//let newLocationViewController = UI
			
			if let ride = self.currentRide,
				let origin = ride.origin,
				let destination = ride.destination {
				let locationController = newLocationController.initController()
				locationController.delegate = self
				locationController.didPresentFromSourceLocation = false
				
				locationController.sourceCoordinate = origin.position
				locationController.sourceLocation = origin.address
				locationController.originTextField?.text = origin.address
				
				locationController.destinationCoordinate = destination.position
				locationController.destinationLocation = destination.address
				locationController.destinationTextField?.text = destination.address
			
				self.present(locationController, animated: true, completion: nil)
			}
		}
		
	}
	//	------------------------------------------------------------------------------------------------------------
	
}
