//
//  HomescreenViewController+LocationManagerDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//
import CoreLocation

extension HomescreenViewController: CLLocationManagerDelegate {
	
	//	------------------------------------------------------------------------------------------------------------
	func locationManager(
		_ manager: CLLocationManager,
		didUpdateLocations locations: [CLLocation]) {
		
		print("HomescreenViewController - Location manager did update locations \(String(describing: locations.first))")
		self.isUpdatingLocation = false
		self.currentLocation = locations.first?.coordinate
		self.locationManager.stopUpdatingLocation()
		
		//self.loadMapView()
		if self.mapLoaded == false {
			self.loadMapView()
			self.mapLoaded = true
		}
		if !self.isGettingCurrentLocation {
			self.isGettingCurrentLocation = true
			
			self.getCurrentLocationInfo(atCoordinate: self.currentLocation!)
		}		
		if self.centreOnCurrentLocation == true {
			self.centreMapOnCurrentLocation()
			self.centreOnCurrentLocation = false
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func locationManager(
		_ manager: CLLocationManager,
		didChangeAuthorization status: CLAuthorizationStatus) {
		
		switch status {
		case .authorizedWhenInUse, .authorizedAlways:

			if !self.isUpdatingLocation {
				self.isUpdatingLocation = true
				self.locationManager.startUpdatingLocation()
			}
		case .notDetermined:
			self.locationManager.requestWhenInUseAuthorization()
		case .denied, .restricted:
			self.showError(
				withTitle: "Location Error",
				andMessage: "You have not enabled access to your location, to re-enable, please go to Settings and turn on Location Service for this app.")
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
