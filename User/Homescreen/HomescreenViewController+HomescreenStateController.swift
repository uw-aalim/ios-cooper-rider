//
//  HomescreenViewController+HomescreenStateController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension HomescreenViewController {
	
	//	------------------------------------------------------------------------------------------------------------
	//	MARK:- Transitioning between homescreen view states
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	/**
	
	*/
	func checkAndTransitionHomescreenState() {
		
		if self.currentRide == nil {
			
		}
		if self.homescreenState == .none {
			
			if self.ridePlanningState == .none &&
				self.currentRide != nil {
				self.transition(toNewHomescreenState: .planningRide)
			}
		}
		else if self.homescreenState == .planningRide {
		
			//	If planning ride
			if self.ridePlanningState == .reviewAndFinalize &&
				self.currentRide != nil {
				
				//	Ride is not scheduled but we have a fare estimate and 
				if self.currentRide!.scheduledTime == nil &&
					self.currentRide!.didBookRide == true {
					
					self.ridePlanningState = .none
					self.transition(toNewHomescreenState: .viewingActiveRide)
				}
				else {
					//	Scheduled ride or could not book ride
					self.transition(toNewHomescreenState: .none)
				}
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	/**
	
	*/
	func transition(toNewHomescreenState homescreenState: HomescreenState) {
		
		switch homescreenState {
		case .none:
			
			//	Clear out the current ride
			self.currentRide = nil
			self.rideStatus = nil
			self.ridePlanningState = .none
			self.currentRoutePolyline = nil
			self.availableFareServices.removeAll()
			
			self.removeMarkers()
			self.mapView.clear()
			
			self.showSearchInfo()
			
			self.homescreenState = .none
			
            self.showMenuButton()
		case .planningRide:
			//	Check to make sure that all of the different
			print("Viewing planning ride state")
			self.checkAndTransitionRidePlanningState()
			self.homescreenState = .planningRide
			
		case .viewingUpcomingRide:
			print("Viewing upcoming ride")
			
		case .viewingActiveRide:
			print("Viewing active ride")
			
			self.showFindingRide()
			
			self.homescreenState = .viewingActiveRide
			self.activeRideState = .searchingForRide
			
			if self.activeRideTimer == nil {
				self.activeRideTimer = Timer.scheduledTimer(
					timeInterval: 2.0,
					target: self,
					selector: #selector(HomescreenViewController.checkActiveRideServiceState(_:)),
					userInfo: nil,
					repeats: true)
			}
		
		
		}
		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func checkAndTransitionRidePlanningState() {
		
		if self.ridePlanningState == .searchingLocations {
			if self.currentRide != nil &&
				self.currentRoutePolyline != nil &&
				self.availableFareServices.count > 0 {
				self.transition(toNewRidePlanningState: .fareInfo)
			}
		}
		else if self.ridePlanningState == .schedule {
			//	If we already have a fare estimate
			print("Coming from schedule with fare service \(self.currentRide?.fareService) and \(self.currentRide?.fareEstimate)")
			if self.currentRide?.fareService != nil && self.currentRide?.fareEstimate != nil {
				self.transition(toNewRidePlanningState: .reviewAndFinalize)
			}
			else if self.currentRide != nil &&
				self.currentRide?.origin != nil &&
				self.currentRide?.destination != nil {
				self.transition(toNewRidePlanningState: .fareInfo)
			}
			else {
				self.transition(toNewRidePlanningState: .searchingLocations)
			}
		}
		else if self.ridePlanningState == .fareInfo {
			if self.currentRide != nil &&
				self.currentRide!.isValidRide &&
				self.currentRide!.fareEstimate != nil {
				
				self.transition(toNewRidePlanningState: .reviewAndFinalize)
			}
			else {
				print("Something is wrong?")
			}
		}
		else if self.ridePlanningState == .reviewAndFinalize {
			print("Check if booking was scheduled correctly - if yes - back to homescreen none")
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func transition(toNewRidePlanningState ridePlanningState: PlanningRideState) {
		
		if ridePlanningState == .searchingLocations {
			//	Show MBProgress HUD
			self.showFareInfo()
		}
		else if ridePlanningState == .fareInfo {
			
			
			//	Add a polyline to the map
			self.addPathForRide()
			//	Add the location markers to the map
			self.addLocationMarkers()
			//	Tint the map
			self.tintMap()
			//	Show the fair info
			self.showFareInfo()
		}
		else if ridePlanningState == .schedule {
			self.ridePlanningState = .schedule
			self.showScheduleInfo()
		}
		else if ridePlanningState == .reviewAndFinalize {
			self.showConfirmRide()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
