//
//  MapDestinationView.swift
//  User
//
//  Created by Benjamin Cortens on 2018-11-06.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class MapDestinationView: UIView {

	static let frameSize = CGRect(
		x: 0.0,
		y: 0.0,
		width: 24.0,
		height: 24.0)
	
	@IBOutlet var pinIcon: UIImageView!
	
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		let _ = self.loadView(with: "MapDestinationView")
		self.performSetup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		let _ = self.loadView(with: "MapDestinationView")
		self.performSetup()
	}
	
	func performSetup() {
		self.pinIcon.image = self.pinIcon.image?.withRenderingMode(.alwaysTemplate)
		self.pinIcon.tintColor = UIColor.orangeColour()		
	}
}
