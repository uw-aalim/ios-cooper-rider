//
//  HomescreenConfirmRideViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

protocol HomescreenConfirmRideViewControllerDelegate: class {
	func rideNowButtonPressed(_ sender: AnyObject)
	//func scheduleRideButtonPressed(_ sender: AnyObject)
}

class HomescreenConfirmRideViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Constants
	//	============================================================================================================
	
	static let height: CGFloat = 300.0
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	@IBOutlet weak var fareLabel: UILabel!
	@IBOutlet weak var distanceLabel: UILabel!
	@IBOutlet weak var etaLabel: UILabel!
	@IBOutlet weak var modelLabel: UILabel!
	
	@IBOutlet weak var scheduleButton: UIButton!
	
	weak var delegate: HomescreenConfirmRideViewControllerDelegate?
	
	var ride: Ride!
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Controller Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	class func initController() -> HomescreenConfirmRideViewController {
		return HomescreenConfirmRideViewController(
			nibName: "HomescreenConfirmRideViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Loading Appearance and Dissappearance
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if let estimatedFare = self.ride.fareEstimate?.estimatedFare?.doubleValue {
			self.fareLabel.text = "$\(estimatedFare.rounded())"
		}
		if let distance = self.ride.fareEstimate!.distance?.doubleValue {
			self.distanceLabel.text = "\(distance.rounded()) km"
		}
		if let eta = self.ride.fareEstimate!.time {
			self.etaLabel.text = "\(eta)"
		}
		if let model = self.ride.fareService!.name {
			self.modelLabel.text = "\(model)"
		}
		if self.ride.scheduledTime != nil {
			self.scheduleButton.setTitle("BOOK SCHEDULED RIDE", for: .normal)
		}
		else {
			self.scheduleButton.setTitle("SCHEDULED RIDE", for: .normal)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Action Responders
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func rideNowButtonPressed(_ sender: AnyObject) {
		
		RoutingAndMapServices.sharedInstance().bookNewRide(
		self.ride) { (error: String?) in
			DispatchQueue.main.async {
				if let error = error {
					print("Error trying to book a ride.")
					self.showError(withTitle: "Booking Error", andMessage: error)
				}
				else {
					print("Booked a ride successfully")
					if let parent = self.parent as? HomescreenViewController {
						parent.transition(toNewHomescreenState: .viewingActiveRide)
					}
				}
			}
		}		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func scheduleRideButtonPressed(_ sender: AnyObject) {
		
		if self.ride.scheduledTime == nil {
			if let parent = self.parent as? HomescreenViewController {
				parent.transition(toNewRidePlanningState: .schedule)
			}
		}
		else {
			RoutingAndMapServices.sharedInstance().bookNewRide(self.ride) { (error: String?) in
				DispatchQueue.main.async {
					if let error = error {
						print("Error trying to book a ride.")
						self.showError(withTitle: "Booking Error", andMessage: error)
					}
					else {
						print("Booked a ride successfully")
						if let parent = self.parent as? HomescreenViewController {
							parent.transition(toNewHomescreenState: .viewingActiveRide)
						}
					}
				}
			}
		}
		//self.delegate?.scheduleRideButtonPressed(self)
	}
	//	------------------------------------------------------------------------------------------------------------
}
