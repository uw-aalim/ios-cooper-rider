//
//  HomescreenFairInfoPageViewController+PaymentModeDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-15.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension HomescreenFareInfoPageViewController: CardDetailsSend {
	
	func onChangePaymentMode(_ choosedPayment: [AnyHashable : Any]!) {
		
		if let brand = choosedPayment["brand"] as? String,
			brand == "CASH" {
			
			print(choosedPayment)
			
			//self.ride.paymentMethod = brand
			let ridePayment = RidePayment()
			ridePayment.paymentType = .cash
			ridePayment.brand = brand
			ridePayment.cardId = ""
			self.ride.paymentMethod = ridePayment
		}
		else if let brand = choosedPayment["brand"] as? String,
			let cardId = choosedPayment["card_id"] as? String,
			let lastFour = choosedPayment["last_four"] as? String {
			
			let ridePayment = RidePayment()
			ridePayment.brand = brand
			ridePayment.cardId = cardId
			ridePayment.lastFour = lastFour
			self.ride.paymentMethod = ridePayment
		}
		self.updatePaymentMethodDisplay()
	}
	
	/*
	-(void)onChangePaymentMode:(NSDictionary *)choosedPayment{
	
	if([choosedPayment[@"brand"]  isEqualToString:@"CASH"]){
	[KeyConstants sharedKeyConstants].paymentInfo = choosedPayment;
	_paymentTypeimageview.image = [UIImage imageNamed:@"money_icon"];
	_paymentModeLbl.text = [KeyConstants sharedKeyConstants].paymentInfo[@"brand"];
	}else{
	[KeyConstants sharedKeyConstants].paymentInfo = @{
	@"brand" :@"CARD",
	@"card_id":[NSString stringWithFormat:@"%@",choosedPayment[@"card_id"]],
	@"last_four":[choosedPayment valueForKey:@"last_four"]
	
	};
	_paymentTypeimageview.image = [UIImage imageNamed:@"visa"];
	_paymentModeLbl.text = [NSString stringWithFormat:@"XXXX-XXXX-XXXX-%@",[choosedPayment valueForKey:@"last_four"]];
	}
	
	}
*/
}
