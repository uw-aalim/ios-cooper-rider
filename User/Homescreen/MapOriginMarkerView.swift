//
//  MapOriginMarkerView.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class MapOriginMarkerView: UIControl {

	static let frameSize = CGRect(
		x: 0.0,
		y: 0.0,
		width: 200.0,
		height: 38.0)
	
	@IBOutlet weak var addressLabel: UILabel!
	@IBOutlet weak var distanceLabel: UILabel!
	@IBOutlet weak var distanceWidth: NSLayoutConstraint!
    
	override init(frame: CGRect) {
		super.init(frame: frame)
		let _ = self.loadView(with: "MapOriginMarkerView")
		self.performSetup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		let _ = self.loadView(with: "MapOriginMarkerView")
		self.performSetup()
	}
	
	func performSetup() {
		
	}
	
	func configure(
		withAddress address: String,
		destination: String? = nil) {
		
		self.addressLabel.text = address
		
		if let destination = destination {
			self.distanceWidth.constant = 40.0
			self.distanceLabel.text = destination
		}
		else {
			self.distanceWidth.constant = 0.0
			self.distanceLabel.text = ""
		}
	}
	
	@IBAction func markerTapped(_ sender: AnyObject) {
		self.sendActions(for: UIControlEvents.touchUpInside)
	}
}
