//
//  HomescreenFairInfoViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-15.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class HomescreenFareInfoPageViewController: UIViewController {

	@IBOutlet weak var nextTitleLabel: UILabel!
	@IBOutlet weak var previousTitleLabel: UILabel!
	
	@IBOutlet weak var nextButton: UIButton!
	@IBOutlet weak var previousButton: UIButton!
	
	@IBOutlet weak var paymentImageView: UIImageView!
	@IBOutlet weak var paymentImageWidth: NSLayoutConstraint!
	@IBOutlet weak var cardElipsesImageView: UIImageView!
	@IBOutlet weak var cardElispesWidth: NSLayoutConstraint!
	@IBOutlet weak var paymentLabel: UILabel!
	
	@IBOutlet weak var distanceImageButton: UIButton!
	@IBOutlet weak var distanceLabel: UILabel!
	@IBOutlet weak var capacityImageButton: UIButton!
	@IBOutlet weak var capacityLabel: UILabel!
	@IBOutlet weak var confirmButton: UIButton!
	
	
	static let height: CGFloat = 320.0
	
	var pageController: UIPageViewController!
	var viewControllers: [UIViewController] = [UIViewController]()
	
	
	var ride: Ride!
	var fareServices: [FareService]!
	
	//	============================================================================================================
	//	MARK:- Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	class func initController() -> HomescreenFareInfoPageViewController {
		return UIStoryboard(
			name: "HomescreenFareInfo",
			bundle: nil)
			.instantiateViewController(
				withIdentifier: "HomescreenFareInfoPageViewController") as! HomescreenFareInfoPageViewController
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		for service in self.fareServices {
			let fareServiceController = HomescreenFareInfoViewController.initController()
			fareServiceController.fareService = service
			self.viewControllers.append(fareServiceController)
		}
		
		self.pageController.setViewControllers(
			[viewControllers.first!],
			direction: UIPageViewControllerNavigationDirection.forward,
			animated: false,
			completion: nil)
		
		if self.viewControllers.count > 1 {
			let nextController = self.viewControllers[1] as! HomescreenFareInfoViewController
			
			self.nextTitleLabel.text = nextController.fareService.nameClass
			self.nextTitleLabel.alpha = 1
			self.nextButton.alpha = 1
		}
		
		if self.viewControllers.count > 0 {
			//	Update the information
			//		(the next and previous title labels which are hidden to indicate the next and previous fare info types as well as the next and previous buttons)
			self.updateInfo(forControllerAtIndex: 0)
			//	Set the current ride fare service to the default service
			let currentController = self.viewControllers[0] as! HomescreenFareInfoViewController
			self.ride.fareService = currentController.fareService
		}
		
		if self.ride.paymentMethod == nil {
			print("Initialize default payment method of cash on hoemscreen fair info page view controller")
			let ridePayment = RidePayment()
			ridePayment.paymentType = .cash
			ridePayment.brand = "CASH"
			ridePayment.cardId = ""
			ridePayment.id = 0
			self.ride.paymentMethod = ridePayment
		}
		self.updatePaymentMethodDisplay()
		
    }
	
	func updatePaymentMethodDisplay() {
		
		if let payment = self.ride.paymentMethod {
			
			if payment.paymentType == .cash {
				
				self.paymentImageView.isHidden = false
				self.paymentImageWidth.constant = 40.0
				self.paymentImageView.image = UIImage(named: "money_icon")
				
				self.cardElipsesImageView.isHidden = true
				self.cardElispesWidth.constant = 0.0
				
				self.paymentLabel.text = "CASH"
			}
			else if let lastFour = payment.lastFour {
				
				self.paymentImageView.isHidden = false
				self.paymentImageWidth.constant = 40.0
				self.paymentImageView.image = UIImage(named: "visa")
				
				self.cardElipsesImageView.isHidden = false
				self.cardElispesWidth.constant = 28.0
				
				self.paymentLabel.text = lastFour
			}
		}
		self.view.setNeedsLayout()
		self.view.layoutIfNeeded()
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	
	

	//	============================================================================================================
	//	MARK:- Segue
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	/**
	Due the embedding of a page view controller within another view controller in the storyboard it is necessary to
	customize the behaviour of that page view controller here. The page view controller displays both of the two
	notification lists, the total lists
	*/
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "EmbedPageViewController" {
			self.pageController = (segue.destination as! UIPageViewController)
			self.pageController.delegate = self
			self.pageController.dataSource = self
			//self.pageController.
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func animateFareInfoUpdates(forControllerAtIndex index: Int) {
		if index > 0 {
			let previousController = self.viewControllers[(index - 1)] as! HomescreenFareInfoViewController
			self.previousTitleLabel.text = previousController.fareService.nameClass
		}
		if index < (self.viewControllers.count - 1) {
			let nextController = self.viewControllers[(index + 1)] as! HomescreenFareInfoViewController
			self.nextTitleLabel.text = nextController.fareService.nameClass
		}
		
		let currentController = self.viewControllers[index] as! HomescreenFareInfoViewController
		
		UIView.animate(
			withDuration: 0.1,
			delay: 0,
			options: .curveEaseOut,
			animations: {
				
				if index > 0 {
					self.previousTitleLabel.alpha = 1
					self.previousButton.alpha = 1
				}
				else {
					self.previousTitleLabel.alpha = 0
					self.previousButton.alpha = 0
				}
				if index < (self.viewControllers.count - 1) {
					self.nextTitleLabel.alpha = 1
					self.nextButton.alpha = 1
				}
				else {
					self.nextTitleLabel.alpha = 0
					self.nextButton.alpha = 0
				}
				
				self.updateInfo(forControllerAtIndex: index)
				
		}) { (completed: Bool) in
			
			//	Update the fare service for the ride
			self.ride.fareService = currentController.fareService
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func updateInfo(forControllerAtIndex index: Int) {
		let currentController = self.viewControllers[index] as! HomescreenFareInfoViewController
		
		if let nameClass = currentController.fareService.nameClass {
			self.confirmButton.setTitle("CONFIRM \(nameClass.uppercased())", for: .normal)
		}
		else {
			self.confirmButton.setTitle("CONFIRM", for: .normal)
		}
		
		
		if let distance = currentController.fareService.distance {
			self.distanceLabel.text = "\(distance.doubleValue.rounded()) km"
			self.distanceLabel.alpha = 1
			self.distanceImageButton.alpha = 1
		}
		else {
			self.distanceLabel.text = ""
			self.distanceLabel.alpha = 0
			self.distanceImageButton.alpha = 0
		}
		
		if let capacity = currentController.fareService.capacity {
			self.capacityLabel.text = "1-\(capacity.intValue)"
			self.capacityLabel.alpha = 1
			self.capacityImageButton.alpha = 1
		}
		else {
			self.capacityLabel.text = ""
			self.capacityLabel.alpha = 0
			self.capacityImageButton.alpha = 0
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func paymentButtonPressed(
		_ sender: AnyObject) {
	
		let main = UIStoryboard.init(
			name: "Main",
			bundle: nil)
		let paymentsController = main.instantiateViewController(
			withIdentifier: "PaymentsViewController") as? PaymentsViewController
		paymentsController?.fromWhereStr = "HOME"
		//wallet.fromWhereStr = @"HOME";
		paymentsController?.delegate = self
		self.parent?.present(paymentsController!, animated: true, completion: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func nextButtonPressed(_ sender: AnyObject) {
		if self.pageController.viewControllers!.count > 0 && self.viewControllers.contains((self.pageController.viewControllers?.first)!) {
			if let currentIndex = self.viewControllers.index(of: (self.pageController.viewControllers?.first)!),
				currentIndex < self.viewControllers.count - 1 {
				let index = currentIndex + 1
				
				self.pageController.setViewControllers(
					[self.viewControllers[index]],
					direction: .forward,
					animated: true) { (completed: Bool) in
						self.animateFareInfoUpdates(forControllerAtIndex: index)
				}
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func previousButtonPressed(_ sender: AnyObject) {
		if self.pageController.viewControllers!.count > 0 && self.viewControllers.contains((self.pageController.viewControllers?.first)!) {
			if let currentIndex = self.viewControllers.index(of: (self.pageController.viewControllers?.first)!),
				currentIndex > 0 {
				let index = currentIndex - 1
				
				self.pageController.setViewControllers(
					[self.viewControllers[index]],
					direction: .reverse,
					animated: true) { (completed: Bool) in
						self.animateFareInfoUpdates(forControllerAtIndex: index)
				}
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func confirmButtonPressed(_ sender: AnyObject) {
		
		if self.ride.isValidRide {
			print("Confirm button pressed - book ride")
			
			RoutingAndMapServices.sharedInstance().estimateFare(
			with: self.ride) { (error: String?) in
				
				if let error = error {
					self.showError(withTitle: "Ride Error", andMessage: error)
				}
				else {
					if self.ride.fareService != nil {
						if let parent = self.parent as? HomescreenViewController {
							parent.homescreenState = .planningRide
							parent.transition(toNewRidePlanningState: .reviewAndFinalize)							
						}
					}
					else {
						self.showError(withTitle: "Ride Error", andMessage: "Tried to estimate fare but could not find a fare estimate.")
					}
				}
			}
			
			/*
			RoutingAndMapServices.sharedInstance().bookNewRide(self.ride) { (error: String?) in
				//	Error booking new ride?
				print("Finished booking ride")
				if let parent = self.parent as? HomescreenViewController {
					parent.finishedBookingRide()
				}
			}*/
		}
		else {
			self.showError(withTitle: "Ride Error", andMessage: "Ride is not yet valid - ensure you have chosen a payment type.")
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func scheduleButtonPressed(_ sender: AnyObject) {
		if let parent = self.parent as? HomescreenViewController {
			parent.transition(toNewRidePlanningState: .schedule)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}



//	============================================================================================================
//	MARK:- Page Controller Methods
//	============================================================================================================

extension HomescreenFareInfoPageViewController:
	UIPageViewControllerDelegate,
	UIPageViewControllerDataSource {
	
	//	------------------------------------------------------------------------------------------------------------
	func pageViewController(
		_ pageViewController: UIPageViewController,
		viewControllerBefore viewController: UIViewController) -> UIViewController? {
		
		print("Get previous page view controler")
		
		if self.pageController.viewControllers!.count > 0 && self.viewControllers.contains((self.pageController.viewControllers?.first)!) {
			if let currentIndex = self.viewControllers.index(of: (self.pageController.viewControllers?.first)!),
				currentIndex > 0 {
				let index = currentIndex - 1
				return self.viewControllers[index]
			}
		}
		
		return nil
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func pageViewController(
		_ pageViewController: UIPageViewController,
		viewControllerAfter viewController: UIViewController) -> UIViewController? {
		
		print("Get next page view controler")
		
		if self.pageController.viewControllers!.count > 0 && self.viewControllers.contains((self.pageController.viewControllers?.first)!) {
			if let currentIndex = self.viewControllers.index(of: (self.pageController.viewControllers?.first)!),
				currentIndex < self.viewControllers.count - 1 {
				let index = currentIndex + 1
				return self.viewControllers[index]
			}
		}
		
		return nil
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func pageViewController(
		_ pageViewController: UIPageViewController,
		didFinishAnimating finished: Bool,
		previousViewControllers: [UIViewController],
		transitionCompleted completed: Bool) {
		
		if self.pageController.viewControllers!.count > 0 && self.viewControllers.contains((self.pageController.viewControllers?.first)!) {
			
			let currentIndex = self.viewControllers.index(of: (self.pageController.viewControllers?.first)!)!
			print("Did finish animating change of page view controller \(currentIndex)")
			//	Show and hide the buttons here
			
			self.animateFareInfoUpdates(forControllerAtIndex: currentIndex)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	/**
	Uncomment these methods if you want the page control dots to appear
	*/
	/*
	//	------------------------------------------------------------------------------------------------------------
	func presentationCount(
		for pageViewController: UIPageViewController) -> Int {
		
		return self.viewControllers.count
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func presentationIndex(
		for pageViewController: UIPageViewController) -> Int {
		
		guard let firstViewController = self.pageController.viewControllers?.first,
			let firstViewControllerIndex = self.viewControllers.index(of: firstViewController) else {
				return 0
		}
		
		return firstViewControllerIndex
	}
	//	------------------------------------------------------------------------------------------------------------
	*/
}
