//
//  HomescreenViewController+newLocationControllerDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-15.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension HomescreenViewController: newLocationControllerDelegate {
	
	
	func didChooseNewOrigin(
		_ originPoint: CLLocationCoordinate2D,
		withOriginAddress originAddress: String!,
		withDestination destinationPoint: CLLocationCoordinate2D,
		withDestinationAddress destinationAddress: String!) {
		
		let ride = Ride()
		
		ride.origin = RouteEndPoint(
			withLatitude: originPoint.latitude,
			withLongitude: originPoint.longitude,
			withAddress: originAddress)
		ride.destination = RouteEndPoint(
			withLatitude: destinationPoint.latitude,
			withLongitude: destinationPoint.longitude,
			withAddress: destinationAddress)
		
		print("Configure ride for cooper to explored address \(String(describing: ride.origin))")
		
		if let currentBottomController = self.currentBottomController as? HomescreenRouteEndpointsViewController{
			currentBottomController.update(
				withOrigin: ride.origin?.address,
				withDestination: ride.destination?.address)
		}
		
		
		//	Set the current ride
		self.currentRide = ride
		//	If we have payment methods fetched from the server, set the first payment method
		if let firstPaymentMethod = self.paymentMethods.first {
			self.currentRide?.paymentMethod = firstPaymentMethod
		}		
		//	Set the current ride planning state
		self.ridePlanningState = .searchingLocations
		
		//	Get a route for the ride - from google maps
		self.currentRoutePolyline = nil
		self.getRoute(withRide: ride)
		//	While getting a route also try and get available services
		self.availableFareServices.removeAll()
		RoutingAndMapServices.sharedInstance().getAvailableServices()
	}
}
