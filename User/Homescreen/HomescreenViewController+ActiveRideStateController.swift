//
//  HomescreenViewController+ActiveRideStateController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-19.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension HomescreenViewController {
	
	
	@objc
	func checkActiveRideServiceState(_ sender: Timer) {
		
		if !self.checkingRideService {
			self.checkingRideService = true
			
			RoutingAndMapServices.sharedInstance().checkRideServiceForActiveRide { (_ error: String?, _ activeResponse:  ActiveRideResponse?) in
				
				DispatchQueue.main.async {
					self.checkingRideService = false
					
					if let activeResponse = activeResponse {
						
						self.activeRideResponse = activeResponse
						
						if activeResponse.status != nil {
							
							print("Active status \(String(describing: activeResponse.status)) current status \(self.activeRideState)")
							if activeResponse.status! == .started && self.activeRideState == .searchingForRide {
								self.activeRideState = .started
								print("Status is started")
								self.showRideStarted()
							}
							else if activeResponse.status! == .arrived && self.activeRideState == .started {
								self.activeRideState = .arrived
								print("Status is arrived")
								self.showRideArrived()
							}
							else if activeResponse.status! == .pickedUp && self.activeRideState == .arrived {
								self.activeRideState = .pickedUp
								print("Status is picked up")
								self.showRidePickedUp()
							}
                            else if activeResponse.status! == .dropped && self.activeRideState == .pickedUp {
                                self.activeRideState = .dropped
                                print("Status is dropped")
                                self.showRideDropped()
                            }
                            else if activeResponse.status! == .completed && self.activeRideState == .dropped {
                                self.activeRideState = .completed
                                print("Status is completed")
                                self.showRideCompleted()
                            }
                        }
					}
					else {
						print("Attempting to get response but got error \(String(describing: error))")
					}
				}
			}
		}
		
	}
}
