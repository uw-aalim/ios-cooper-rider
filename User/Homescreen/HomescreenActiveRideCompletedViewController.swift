//
//  HomescreenActiveRideCompletedViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-19.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit


protocol HomescreenActiveRideCompletedViewControllerDelegate: class {
    func didFinishRateDriver()
}


class HomescreenActiveRideCompletedViewController: UIViewController {
    
    // MARK: Constants
    
    struct Metrict {
        static let viewHeight: CGFloat = 293.0
    }
    
    
    // MARK: Properties
    
    var strRating = ""
    weak var delegate: HomescreenActiveRideCompletedViewControllerDelegate?
    
    
    // MARK: UI
    
    @IBOutlet weak var starRatingView: HCSStarRatingView!
    @IBOutlet weak var commentTextView: UITextView!
    
    
    // MARK: Initializing
    
    @objc
    class func initController() -> HomescreenActiveRideCompletedViewController {
        return HomescreenActiveRideCompletedViewController(
            nibName: "HomescreenActiveRideCompletedViewController",
            bundle: nil)
    }
    
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func SubmitButtonDidTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func update() {
        self.loadViewIfNeeded()
    }
    
    @IBAction func rateValueChanged(_ sender: HCSStarRatingView) {
        
        strRating = NSString.localizedStringWithFormat("%.f", sender.value) as String
    }
    
    @IBAction func submitButtonDidTap(_ sender: Any) {
        if(strRating == "") {
            strRating = NSString.localizedStringWithFormat("%.f", starRatingView.value) as String
        }
        RoutingAndMapServices.sharedInstance().rateDriver(strRating, comment: commentTextView.text)  { [weak self] (response, error, errorCode) in
            guard let `self` = self else { return }
            
            DispatchQueue.main.async {
                if let _ = response {
                    self.delegate?.didFinishRateDriver()
                } else if let errorCode = errorCode {
                    if errorCode == "1" {
                        
                    } else if errorCode == "2" {
                        
                    } else if errorCode == "3" {
                        
                    }
                }
            }
        }
    }
}
