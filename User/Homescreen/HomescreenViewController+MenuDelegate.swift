//
//  HomescreenViewController+MenuDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-08.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension HomescreenViewController: MenuViewControllerDelegate {
	
	//	============================================================================================================
	//	MARK:- MenuViewControllerDelegate
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func closeMenuButtonPressed(_ sender: AnyObject) {
		
		if self.menuLeading.constant >= 0 {
			self.menuLeading.constant = -self.menuContainerView.frame.size.width
			UIView.animate(
				withDuration: 0.2,
				delay: 0,
				options: .curveEaseOut,
				animations: {
					self.view.layoutIfNeeded()
			}) { (complete: Bool) in
				//	Completed hiding the menu
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func profileButtonPressed(_ sender: AnyObject) {
		let profileViewController = ProfileViewController.initController()!
		let profileNavigationController = UINavigationController(rootViewController: profileViewController)
		profileNavigationController.isNavigationBarHidden = false
		self.navigationController?.present(
			profileNavigationController,
			animated: true, completion: {
				//	Hides the menu behind the scenes after the explore view controller has been presented.
				self.closeMenuButtonPressed(self)
		})
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func makeMoneyButtonPressed(_ sender: AnyObject) {
		let viewController = MakeMoneyDrivingViewController.initController()
		let navigationController = UINavigationController(rootViewController: viewController)
		navigationController.isNavigationBarHidden = true
		self.navigationController?.present(
			navigationController,
			animated: true, completion: {
				//	Hides the menu behind the scenes after the explore view controller has been presented.
				self.closeMenuButtonPressed(self)
		})
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func paymentButtonPressed(_ sender: AnyObject) {
		let paymentsViewController = PaymentsViewController.initController()!
		let paymentsNavigationController = UINavigationController(rootViewController: paymentsViewController)
		paymentsNavigationController.isNavigationBarHidden = true
		self.navigationController?.present(
			paymentsNavigationController,
			animated: true, completion: {
				//	Hides the menu behind the scenes after the explore view controller has been presented.
				self.closeMenuButtonPressed(self)
		})
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func yourTripsButtonPressed(_ sender: AnyObject) {
		let yourTripController = YourTripViewController.initController()
		let yourTripNavigationController = UINavigationController(rootViewController: yourTripController!)
		yourTripNavigationController.isNavigationBarHidden = true
		self.navigationController?.present(
			yourTripNavigationController,
			animated: true, completion: {
				//	Hides the menu behind the scenes after the explore view controller has been presented.
				self.closeMenuButtonPressed(self)
		})
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func exploreMenuButtonPressed(_ sender: AnyObject) {
		let exploreController = ExploreViewController.initController()
		let exploreNavigationController = UINavigationController(rootViewController: exploreController)
		exploreNavigationController.isNavigationBarHidden = false
		exploreController.delegate = self
		self.navigationController?.present(
			exploreNavigationController,
			animated: true, completion: {
				//	Hides the menu behind the scenes after the explore view controller has been presented.
				self.closeMenuButtonPressed(self)
		})
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func walletMenuButtonPressed(_ sender: AnyObject) {
		let viewController = WalletViewController.initController()!
		let navigationController = UINavigationController(rootViewController: viewController)
		navigationController.isNavigationBarHidden = false
		self.navigationController?.present(
			navigationController,
			animated: true, completion: {
				//	Hides the menu behind the scenes after the explore view controller has been presented.
				self.closeMenuButtonPressed(self)
		})
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func freeRidesButtonPressed(_ sender: AnyObject) {
		let viewController = FreeRidesViewController.initController()
		let navigationController = UINavigationController(rootViewController: viewController)
		navigationController.isNavigationBarHidden = false
		self.navigationController?.present(
			navigationController,
			animated: true, completion: {
				//	Hides the menu behind the scenes after the explore view controller has been presented.
				self.closeMenuButtonPressed(self)
		})
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func helpButtonPressed(_ sender: AnyObject) {
		let helpViewController = HelpViewController.initController()!
		let helpNavigationController = UINavigationController(rootViewController: helpViewController)
		helpNavigationController.isNavigationBarHidden = false
		self.navigationController?.present(
			helpNavigationController,
			animated: true, completion: {
				//	Hides the menu behind the scenes after the explore view controller has been presented.
				self.closeMenuButtonPressed(self)
		})
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func settingsButtonPressed(_ sender: AnyObject) {
		let settingsViewController = SettingsController.initController()!
		let settingsNavigationController = UINavigationController(rootViewController: settingsViewController)
		settingsNavigationController.isNavigationBarHidden = false
		self.navigationController?.present(
			settingsNavigationController,
			animated: true, completion: {
				//	Hides the menu behind the scenes after the explore view controller has been presented.
				self.closeMenuButtonPressed(self)
		})
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func legalButtonPressed(_ sender: AnyObject) {
		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func logoutButtonPressed(_ sender: AnyObject) {
		
		let alertController = UIAlertController(
			title: "LOGOUT",
			message: "Are you sure you want to logout?",
			preferredStyle: .alert)
		
		let noAction = UIAlertAction(
			title: "NO",
			style: .cancel,
			handler: nil)
		let yesAction = UIAlertAction(
			title: "YES",
			style: .default) { (_ action: UIAlertAction) in
		
				UserDefaults.standard.removeObject(forKey: "isLoggedin")
				if let bundleIdentifier = Bundle.main.bundleIdentifier {
					UserDefaults.standard.removePersistentDomain(forName: bundleIdentifier)
				}
				if let menuController = sender as? MenuViewController {
					menuController.closeMenuButtonPressed(self)
				}
				self.showWelcome(withAnimation: true)
		}
		alertController.addAction(noAction)
		alertController.addAction(yesAction)
		self.present(
			alertController,
			animated: true,
			completion: nil)
		
	}
	//	------------------------------------------------------------------------------------------------------------
}
