//
//  HomescreenFairInfoViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-15.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit
import AlamofireImage

class HomescreenFareInfoViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	@IBOutlet weak var vehicleClassLabel: UILabel!
	@IBOutlet weak var vehicleClassDescriptionLabel: UILabel!
	@IBOutlet weak var vehicleClassTitleLabel: UILabel!
	@IBOutlet weak var vehicleClassPriceLabel: UILabel!
	@IBOutlet weak var vehicleImageView: UIImageView!
	
	var fareService: FareService!
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Controller Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	class func initController() -> HomescreenFareInfoViewController {
		return HomescreenFareInfoViewController(
			nibName: "HomescreenFareInfoViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
		if let serviceName = self.fareService.name {
			self.vehicleClassLabel.text = serviceName
			self.vehicleClassTitleLabel.text = serviceName
		}
		if let servicePrice = self.fareService.price {
			self.vehicleClassPriceLabel.text = "$\(servicePrice.doubleValue.rounded())"
		}
		if let image = self.fareService.image,
			let imageUrl = URL(string: image) {
			//self.vehicleImageView.af
			self.vehicleImageView.af_setImage(withURL:imageUrl)
		}
    }
	//	------------------------------------------------------------------------------------------------------------
}
