//
//  HomescreenScheduleRideViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

protocol HomescreenScheduleRideViewControllerDelegate: class {
	func scheduledRide(withDate date: Date?, sender: AnyObject)
} 

class HomescreenScheduleRideViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Constants
	//	============================================================================================================
	
	static let height: CGFloat = 400.0
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var datePicker: UIDatePicker!
	
	weak var delegate: HomescreenScheduleRideViewControllerDelegate?
	
	
	//	============================================================================================================
	//	MARK:- View Controller Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	class func initController() -> HomescreenScheduleRideViewController {
		return HomescreenScheduleRideViewController(
			nibName: "HomescreenScheduleRideViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Loading Appearance and Dissappearance
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

		self.datePicker.addTarget(
			self,
			action: #selector(HomescreenScheduleRideViewController.dateDidChange(_:)),
			for: .valueChanged)
		self.displayFormattedDate()
    }
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func displayFormattedDate() {
		let dateFormatter = DateFormatter()
		dateFormatter.dateStyle = .medium
		dateFormatter.timeStyle = .short
		self.dateLabel.text = dateFormatter.string(from: self.datePicker.date)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Action Responders
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func setPickupTimeButtonPressed(_ sender: AnyObject) {
		self.delegate?.scheduledRide(withDate: self.datePicker.date, sender: self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func pickUpNowButtonPressed(_ sender: AnyObject) {
		self.delegate?.scheduledRide(withDate: nil, sender: self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func dateDidChange(_ sender: UIDatePicker) {
		self.displayFormattedDate()
	}
	//	------------------------------------------------------------------------------------------------------------
}



