//
//  HomescreenRouteEndpointsViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class HomescreenRouteEndpointsViewController: UIViewController {

	static let height: CGFloat = 184.0
	
	@IBOutlet weak var originButton: UIButton!
	@IBOutlet weak var destinationButton: UIButton!
	@IBOutlet weak var scheduleRideButton: UIButton!
	@IBOutlet weak var scheduleRideButtonOutlineView: UIView!
	
	var ride: Ride?
	
	
	//	============================================================================================================
	//	MARK:- View Controller Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	class func initController() -> HomescreenRouteEndpointsViewController {
		return HomescreenRouteEndpointsViewController(
			nibName: "HomescreenRouteEndpointsViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Loading Appearance and Dissapearance
	//	============================================================================================================
	
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		self.originButton.titleLabel?.adjustsFontSizeToFitWidth = true
		self.originButton.titleLabel?.minimumScaleFactor = 0.5
		self.destinationButton.titleLabel?.adjustsFontSizeToFitWidth = true
		self.destinationButton.titleLabel?.minimumScaleFactor = 0.5
		
		//	If we have a scheduld time and a ride then set the button to be teal and
		//	add an outline
		if self.ride != nil &&
			self.ride!.scheduledTime != nil {
			self.scheduleRideButton.tintColor = UIColor.tealColour()
			self.scheduleRideButtonOutlineView.isHidden = false
		}
		else {
			self.scheduleRideButton.tintColor = UIColor.black
			self.scheduleRideButtonOutlineView.isHidden = true
		}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	func update(withOrigin origin: String?,
				withDestination destination: String?) {
		if let origin = origin {
			self.originButton.setTitle(origin, for: .normal)
		}
		else {
			self.originButton.setTitle("Where are you starting from?", for: .normal)
		}
		
		if let destination = destination {
			self.destinationButton.setTitle(destination, for: .normal)
		}
		else {
			self.destinationButton.setTitle("Where would you like to go?", for: .normal)
		}
	}

	
	@IBAction func originButtonPressed(_ sender: AnyObject) {
		//let storyboard = UIStoryboard(name: "New", bundle: nil)
		let locationController = newLocationController.initController()
		//newLocationController.tagIdentifier = "Source"
		if let homescreen = self.parent as? HomescreenViewController {
			locationController.delegate = homescreen
			locationController.sourceLocation = homescreen.currentAddress
			if let currentLocation = homescreen.currentLocation {
				locationController.sourceCoordinate = currentLocation
			}
		}
		locationController.didPresentFromSourceLocation = true
		
		self.parent?.present(locationController, animated: true, completion: nil)
	}
	
	@IBAction func destinationButtonPressed(_ sender: AnyObject) {
		//let storyboard = UIStoryboard(name: "New", bundle: nil)
		let locationController = newLocationController.initController()
		
	//	newLocationController.tagIdentifier = "Destination"
		if let homescreen = self.parent as? HomescreenViewController {
			locationController.delegate = homescreen
			locationController.sourceLocation = homescreen.currentAddress
			if let currentLocation = homescreen.currentLocation {
				locationController.sourceCoordinate = currentLocation
			}
			//newLocationController.destinationLocation = self.destinationButton.titleLabel?.text
		}
		locationController.didPresentFromSourceLocation = false
		
		self.parent?.present(locationController, animated: true, completion: nil)
	}

	//	------------------------------------------------------------------------------------------------------------
	@IBAction func scheduleButtonPressed(_ sender: AnyObject) {
		if let parent = self.parent as? HomescreenViewController {
			parent.transition(toNewRidePlanningState: .schedule)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
