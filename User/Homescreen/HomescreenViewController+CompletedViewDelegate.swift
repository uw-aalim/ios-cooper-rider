//
//  HomescreenViewController+CompletedViewDelegate.swift
//  User
//
//  Created by aliasapps on 2018-06-20.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation


extension HomescreenViewController: HomescreenActiveRideCompletedViewControllerDelegate {
    
    func didFinishRateDriver() {
        self.mapView.clear()
        self.transition(toNewHomescreenState: .none)
    }
    
}
