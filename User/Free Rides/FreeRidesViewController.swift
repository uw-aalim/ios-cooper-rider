//
//  FreeRidesViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-10-26.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class FreeRidesViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	@IBOutlet var inviteHelpButton: UIButton!
	@IBOutlet var inviteFriendsButton: UIButton!
	@IBOutlet var codeContainerView: UIView!
	@IBOutlet var codeLabel: UILabel!
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	class func initController() -> FreeRidesViewController {
		return FreeRidesViewController(
			nibName: "FreeRidesViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Loading
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

		self.configureNavigationController()
		
		self.inviteHelpButton.tintColor = UIColor.tealColour()
		
		self.codeContainerView.clipsToBounds = true
		self.codeContainerView.layer.cornerRadius = 6
		self.codeContainerView.layer.borderWidth = 1.0
		self.codeContainerView.layer.borderColor = UIColor.darkGray.cgColor
    }
	//	------------------------------------------------------------------------------------------------------------

	
	
	
	
	

	//	============================================================================================================
	//	MARK:- View Configuration
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func configureNavigationController() {
		
		if #available(iOS 11.0, *) {
			self.navigationController?.navigationBar.prefersLargeTitles = true
			self.navigationItem.largeTitleDisplayMode = .always
		}
		
		let closeButton = UIBarButtonItem(
			image: UIImage(named: "close_x_icon"), style: .plain,
			target: self, action: #selector(FreeRidesViewController.backPressed(_:)))
		self.navigationItem.leftBarButtonItem = closeButton
		
		self.navigationItem.title = "Free Rides"
		
		self.navigationController?.navigationBar.isTranslucent = false
		self.navigationController?.navigationBar.barTintColor = .black
		self.navigationController?.navigationBar.barStyle = .blackOpaque
		self.navigationController?.navigationBar.backgroundColor = .black
		self.navigationController?.navigationBar.tintColor = .white
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	//	============================================================================================================
	//	MARK:- Action Responders
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc func backPressed(_ sender: AnyObject) {
		if self.navigationController != nil && self.navigationController!.viewControllers.count > 1 {
			self.navigationController?.popViewController(animated: true)
		}
		else if self.navigationController != nil {
			self.navigationController?.presentingViewController?.dismiss(
				animated: true,
				completion: nil)
		}
		else {
			self.presentingViewController?.dismiss(
				animated: true,
				completion: nil)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
