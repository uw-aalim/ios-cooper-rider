//
//  passbookViewController.h
//  User
//
//  Created by MAC on 04/09/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface passbookViewController : GAITrackedViewController
{
    NSMutableArray *dateArray, *paidByArray, *amountArray, *statusArray, *couponArray, *offerArray;
    
    NSString *identifierStr, *requestIdStr;
}

@property (weak, nonatomic) IBOutlet UILabel *headerLb;
@property (weak, nonatomic) IBOutlet UITableView *tripTableView;
@property (weak, nonatomic) IBOutlet UIButton *pastBtn;
@property (weak, nonatomic) IBOutlet UIButton *upcomingBtn;
@property (weak, nonatomic) IBOutlet UILabel *upcomingLbl;
@property (weak, nonatomic) IBOutlet UILabel *pastLbl;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;

@end
