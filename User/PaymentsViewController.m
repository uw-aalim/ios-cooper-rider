//
//  PaymentsViewController.m
//  User
//
//  Created by iCOMPUTERS on 18/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "PaymentsViewController.h"
#import "CreditCardViewController.h"
#import "PayTableViewCell.h"
#import "CSS_Class.h"
#import "config.h"
#import "Colors.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "AppDelegate.h"
#import "CommenMethods.h"
#import "ViewController.h"

@interface PaymentsViewController () {
    AppDelegate *appDelegate;
    NSMutableArray *arrPaymentCardList;
    NSDictionary *dictSend;
}
@end

@implementation PaymentsViewController

+(instancetype)initController {
	UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Payments" bundle: nil];
	PaymentsViewController *paymentsViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"PaymentsViewController"];
	return paymentsViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
   
    
    selectedIndex = -1;
    [_addCardBtn setHidden:NO];
    
    [self setupViewDesign];
    
//    if ([_fromWhereStr isEqualToString:@"LEFT MENU"]||[_fromWhereStr isEqualToString:@"WALLET"] )
//    {
//        [_addCardBtn setHidden:NO];
//    }
//    else
//    {
//        [_addCardBtn setHidden:YES];
//    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];

    self.screenName = @"Payment";
    [self getAllCards];
    [self LocalizationUpdate];
}

-(void)setupViewDesign {
    self.navigationController.navigationBarHidden = NO;
    
    if (@available(iOS 11.0, *)) {
        [self navigationController].navigationBar.prefersLargeTitles = TRUE;
        [self navigationItem].largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAlways;
    }
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]
                                    initWithImage:[UIImage imageNamed:@"close_x_icon"]
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction:)];
    
    [[self navigationController].navigationBar setTranslucent:FALSE];
    [self navigationController].navigationBar.barTintColor = [UIColor blackColor];
    [self navigationController].navigationBar.barStyle = UIBarStyleBlackOpaque;
    [self navigationController].navigationBar.backgroundColor = [UIColor blackColor];
    [self navigationController].navigationBar.tintColor = [UIColor whiteColor];
    [self navigationItem].leftBarButtonItem = closeButton;
}

- (void)LocalizationUpdate{
    _navicationTitleLbl.text = LocalizedString(@"Payment");
    _paymentTitle.text =  LocalizedString(@"Payment method");
}

-(void)getAllCards
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [appDelegate onStartLoader];
        [afn getDataFromPath:MD_ADD_CARD withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            [appDelegate onEndLoader];
            if (response)
            {
                arrPaymentCardList=[[NSMutableArray alloc] init];
                
                if ([_fromWhereStr isEqualToString:@"WALLET"])
                {
                    //No need to show cash to add money in wallet.
                }
                else
                {
                    NSDictionary *dictCash=@{@"brand":@"CASH",@"card_id":@"",@"id":@"0"};
                    [arrPaymentCardList addObject:dictCash];
                }
                
                
                [arrPaymentCardList addObjectsFromArray:response];
                if(arrPaymentCardList.count)
                  {
                     NSLog(@"%@",arrPaymentCardList);
                     [_payTableView reloadData];
                  }
                else {
                    
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:LocalizedString(@"No Cards Added!!")
                                                  message:LocalizedString(@"Please add you card details.")
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* cancelButton = [UIAlertAction
                                               actionWithTitle:LocalizedString(@"OK")
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                               }];
                    
                    [alert addAction:cancelButton];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            else
            {
                if ([errorcode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                    [self logoutMethod];
                    
//                    [CommenMethods onRefreshToken];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
//                    
//                    PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
//                    [self.navigationController pushViewController:wallet animated:YES];
                }
            }
            
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}

-(void)logoutMethod
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn refreshMethod_NoLoader:MD_REFRESH_TOKEN withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
                [user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
                [user setValue:response[@"refresh_token"] forKey:UD_REFERSH_TOKEN];
            }
            else
            {
                [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)onGetPayment
{
    
}
#pragma mark -- Table View Delegates Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrPaymentCardList count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PayTableViewCell *cell = (PayTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"PayTableViewCell"];
    
    if (cell == nil)
    {
        cell = (PayTableViewCell *) [[[NSBundle mainBundle] loadNibNamed:@"PayTableViewCell" owner:self options:nil] lastObject];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    

    NSDictionary *dictVal=[arrPaymentCardList objectAtIndex:indexPath.row];
    
    if ([_fromWhereStr isEqualToString:@"WALLET"])
    {
        cell.payLbl.text=[NSString stringWithFormat:@"XXXX-XXXX-XXXX-%@",[dictVal valueForKey:@"last_four"]];
        cell.payImg.image = [UIImage imageNamed:@"visa"];
    }
    else
    {
        if (indexPath.row==0)
        {
            cell.payLbl.text=@"CASH";
            cell.payImg.image = [UIImage imageNamed:@"money_icon"];
            
        }
        else
        {
            cell.payLbl.text=[NSString stringWithFormat:@"XXXX-XXXX-XXXX-%@",[dictVal valueForKey:@"last_four"]];
            cell.payImg.image = [UIImage imageNamed:@"visa"];
        }
    }
    
    
//    cell.payLbl.text=[NSString stringWithFormat:@"XXXX-XXXX-XXXX-%@",[dictVal valueForKey:@"last_four"]];
//    cell.payImg.image = [UIImage imageNamed:@"visa"];
    
    [CSS_Class APP_fieldValue_Small:cell.payLbl];
    
    [cell.tickImg setHidden:YES];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_fromWhereStr isEqualToString:@"LEFT MENU"])
    {
        selectedIndex = -1;
    }
    else if ([_fromWhereStr isEqualToString:@"WALLET"])
    {
        [_delegate onChangePaymentMode:[arrPaymentCardList objectAtIndex:indexPath.row]];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        selectedIndex = indexPath.row;
        [_delegate onChangePaymentMode:[arrPaymentCardList objectAtIndex:indexPath.row]];
         [self dismissViewControllerAnimated:YES completion:nil];
    }
    
//    selectedIndex = indexPath.row;
//    [_payTableView reloadData];
}
-(BOOL)tableView:(UITableView* )tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_fromWhereStr isEqualToString:@"LEFT MENU"])
    {
        if (indexPath.row!=0)
          return YES;
    }
    else
        return NO;
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row!=0)
    {
        if (editingStyle == UITableViewCellEditingStyleDelete)
        {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:@"Are you sure want to Delete this card?" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:nil];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [self cardFuncDeleteOptions:indexPath];
                
            }];
            [alertController addAction:ok];
            [alertController addAction:no];
            [self presentViewController:alertController animated:YES completion:nil];
            
            
            
//            if ([appDelegate internetConnected])
//            {
//                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
//                [appDelegate onStartLoader];
//                [afn getDataFromPath:MD_CARD_DELETE withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
//                    [appDelegate onEndLoader];
//                    if (response)
//                    {
//                        [arrPaymentCardList removeObjectAtIndex:indexPath.row];
//                        NSLog(@"%@",arrPaymentCardList);
//                        [_payTableView reloadData];
//                        
//                    }
//                    else
//                    {
//                        if ([errorcode intValue]==1)
//                        {
//                            [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
//                        }
//                        else if ([errorcode intValue]==3)
//                        {
//                            [self logoutMethod];
//                            
////                            [CommenMethods onRefreshToken];
////                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
////                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
////                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
////                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
////                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
////                            
////                            PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
////                            [self.navigationController pushViewController:wallet animated:YES];
//                        }
//                    }
//                    
//                }];
//            }
//            else
//            {
//                [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
//            }
//            
//            
//        }
        }
    }
}

-(void)cardFuncDeleteOptions :(NSIndexPath *)index{
    NSDictionary *dictLocal=[arrPaymentCardList objectAtIndex:index.row];
    
    NSDictionary *params=@{@"_method":@"DELETE",@"card_id":[dictLocal valueForKey:@"card_id"]};
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [appDelegate onStartLoader];
    
    [afn getDataFromPath:MD_CARD_DELETE WithType:POST_METHOD WithParameters:params WithCompletedSuccess:^(id response) {
        [appDelegate onEndLoader];
        [arrPaymentCardList removeObjectAtIndex:index.row];
        NSLog(@"%@",arrPaymentCardList);
        [_payTableView reloadData];
        
    } OrValidationFailure:^(NSString *errorMessage) {
        [appDelegate onEndLoader];
        [CSS_Class alertviewController_title:@"" MessageAlert:errorMessage viewController:self okPop:NO];
    } OrErrorCode:^(NSString *error) {
        [appDelegate onEndLoader];
        [CSS_Class alertviewController_title:@"" MessageAlert:error viewController:self okPop:NO];
    } OrIntentet:^(NSString *internetFailure) {
        [appDelegate onEndLoader];
        [CSS_Class alertviewController_title:@"" MessageAlert:internetFailure viewController:self okPop:NO];
    }];
}
- (IBAction)backBtnAction:(id)sender
{
	if (self.navigationController.viewControllers.count <= 1) {
		[self.navigationController.presentingViewController dismissViewControllerAnimated:true completion:NULL];
	}
	else {
		[self.navigationController popViewControllerAnimated:YES];
	}
}

//    MARK:- Action Responders

- (void)backAction:(UIButton *)sender {
    if (self.navigationController.viewControllers.count <= 1) {
        [self.navigationController.presentingViewController dismissViewControllerAnimated:true completion:NULL];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)addBtnAction:(id)sender {
	CreditCardViewController *viewController = [CreditCardViewController initController];
	UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:viewController];
	[self presentViewController:navigationController animated:YES completion:nil];
}

@end
