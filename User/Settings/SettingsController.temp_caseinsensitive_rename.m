//
//  settingsController.m
//  User
//
//  Created by CSS on 03/10/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "settingsController.h"

@implementation settingHeader
-(void)awakeFromNib{
    [super awakeFromNib];
}

@end


@implementation EmptyAddressCell

-(void)awakeFromNib{
    [super awakeFromNib];
}

@end

@implementation languageCell

-(void)awakeFromNib{
    [super awakeFromNib];
}

@end


@implementation settingAddresscell

-(void)awakeFromNib{
    [super awakeFromNib];
}

@end





@interface SettingsController ()
{
    AppDelegate *appDelegate;

}
@end

@implementation SettingsController


+(instancetype)initController {
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle: nil];
	SettingsController *settingsController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsController"];
	return settingsController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self onGetAddress];

    _headerArray = @[@"Favorites",@"Change Language"];
    _languageArray = @[@{@"name":@"English",@"code":@"en"}];
    
   
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"SelectLanguageIndex"]==nil){
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:0] forKey:@"SelectLanguageIndex"];
        [[NSUserDefaults standardUserDefaults]setObject:_languageArray[0][@"code"] forKey:@"selectLanguage"];

    }

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self LocalizationUpdate];
}


-(void)LocalizationUpdate{
    //_navTitle.text = LocalizedString(@"SETTINGS");
}
#pragma TableviewDelegate and Datasource


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
        return _headerArray.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(section == 0){
        if([_AddressArray count] > 0){
            return _AddressArray.count;
        }else{
            return 1;
        }
        
    }else{
        return _languageArray.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        if([_AddressArray count]>0){
            settingAddresscell * cell = [tableView dequeueReusableCellWithIdentifier:@"settingAddresscell" forIndexPath:indexPath];
            cell.deletBtn.tag = indexPath.row;
            if([_AddressArray[indexPath.row][@"type"] isEqualToString:@"home"]){
                cell.addressTitleLbl.text = @"Home";
                cell.iconView.image = [UIImage imageNamed:@"home"];
            }else{
                cell.addressTitleLbl.text = @"Work";
                cell.iconView.image = [UIImage imageNamed:@"work"];
                
                
            }
            [cell.deletBtn setTitle:LocalizedString(@"Delete") forState:UIControlStateNormal];
            [cell.deletBtn addTarget:self action:@selector(DeleteAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.addressLbl.text = _AddressArray[indexPath.row][@"address"];
            
            return cell;
        }else{
            EmptyAddressCell * cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyAddressCell" forIndexPath:indexPath];
            cell.infoLbl.text = LocalizedString(@"No favourate address available");
            
            return cell;
        }

    }else{
        languageCell * cell = [tableView dequeueReusableCellWithIdentifier:@"languageCell" forIndexPath:indexPath];
        if([[[NSUserDefaults standardUserDefaults]objectForKey:@"SelectLanguageIndex"]integerValue] == indexPath.row){
            cell.iconimage.image = [UIImage imageNamed:@"radio_select"];
        }else{
            cell.iconimage.image = [UIImage imageNamed:@"radio_unselect"];
        }
        cell.languageName.text = LocalizedString(_languageArray[indexPath.row][@"name"]);
        return cell;

    }
    
}

-(void)DeleteAction:(UIButton *)sender{
	/*
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.SettingsTable];
    NSIndexPath *indexPath = [self.SettingsTable indexPathForRowAtPoint:buttonPosition];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"Are you sure want to Delete the address?") preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:nil];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self DeleteApicall:indexPath];
            
        }];
        [alertController addAction:ok];
        [alertController addAction:no];
        [self presentViewController:alertController animated:YES completion:nil];
	 */
}

- (void)DeleteApicall :(NSIndexPath *)index
{
    if ([appDelegate internetConnected])
    {
		NSLog(@"settingsController - DeleteApicall");
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:DELETE_METHOD];
        [appDelegate onStartLoader];
        NSString * url = [NSString stringWithFormat:@"/api/user/location/%@",_AddressArray[index.row][@"id"]];
        
        [afn getDataFromPath:url withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            [appDelegate onEndLoader];
            if (response)
            {
                [self.AddressArray removeObjectAtIndex:index.row];
//                [self.SettingsTable deleteRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            else
            {
                if ([errorcode intValue]==1)
                {
                    // [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                    [self logoutMethod];
                    
                    //                    [CommenMethods onRefreshToken];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
                    //
                    //                    PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
                    //                    [self.navigationController pushViewController:wallet animated:YES];
                }
            }
            
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
    [self.AddressArray removeObjectAtIndex:index.row];
//    [self.SettingsTable deleteRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
 //   [_SettingsTable reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        
    }else{
        [[NSUserDefaults standardUserDefaults]setObject:[NSNumber numberWithInteger:indexPath.row] forKey:@"SelectLanguageIndex"];
       // [[NSUserDefaults standardUserDefaults]setObject:_languageArray[indexPath.row][@"code"] forKey:@"LanguageCode"];
        LocalizationSetLanguage(_languageArray[indexPath.row][@"code"]);
        [[NSUserDefaults standardUserDefaults]setObject:_languageArray[indexPath.row][@"code"] forKey:@"selectLanguage"];
        [self LocalizationUpdate];
        [tableView reloadData];
    }
 
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        if([_AddressArray count]>0){
             return 80;
        }
    }
    return 44;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    settingHeader * header = [tableView dequeueReusableCellWithIdentifier:@"settingHeader"];
    header.titleLbl.text =LocalizedString(_headerArray[section]);
    return header;
}


-(void)onGetAddress
{
    if ([appDelegate internetConnected])
    {
		NSLog(@"settingsController - onGetAddress");
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [appDelegate onStartLoader];
        [afn getDataFromPath:@"/api/user/location" withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            [appDelegate onEndLoader];
            if (response)
            {
                [self funcCheckAddressAvailable:response];
                
            }       else
            {
                if ([errorcode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                    [self logoutMethod];
                    
                    //                    [CommenMethods onRefreshToken];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
                    //
                    //                    PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
                    //                    [self.navigationController pushViewController:wallet animated:YES];
                }
            }
            
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
    
    
}



-(void)funcCheckAddressAvailable:(NSDictionary *)AddressDict{
    _AddressArray = [[NSMutableArray alloc]init];
//    NSDictionary * homeAddress = @{
//                                   @"address" : @"Add your home address",
//                                   @"id" : @0,
//                                   @"latitude" : @"13.99",
//                                   @"longitude" : @"80.3",
//                                   @"type" : @"home"
//                                   };
//    
//    NSDictionary * WorkAddress = @{
//                                   @"address" : @"Add your Work address",
//                                   @"id" : @0,
//                                   @"latitude" : @"13.99",
//                                   @"longitude" : @"80.3",
//                                   @"type" : @"work"
//                                   };
    
    
    if([AddressDict[@"home"] count]>0){
        [_AddressArray addObject:AddressDict[@"home"][0]];
    }
//    else{
//        [_AddressArray addObject:homeAddress];
//    }
    
    if([AddressDict[@"work"] count] >0){
        [_AddressArray addObject:AddressDict[@"work"][0]];
    }
//    else{
//        [_AddressArray addObject:WorkAddress];
//        
//    }
    //[_SettingsTable reloadData];
}

-(void)logoutMethod
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn refreshMethod_NoLoader:MD_REFRESH_TOKEN withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
                [user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
                [user setValue:response[@"refresh_token"] forKey:UD_REFERSH_TOKEN];
            }
            else
            {
                [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}

- (IBAction)backAction:(UIButton *)sender {
	if (self.navigationController.viewControllers.count <= 1) {
		[self.navigationController.presentingViewController dismissViewControllerAnimated:true completion:NULL];
	}
	else {
		[self.navigationController popViewControllerAnimated:YES];
	}
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
