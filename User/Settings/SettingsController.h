//
//  SettingsController.h
//  User
//
//  Created by CSS on 03/10/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking/AFNetworking.h"
#import "ViewController.h"
#import "CSS_Class.h"
#import "config.h"
#import "Colors.h"
#import "Utilities.h"
#import "UIColor+CoOper-Objc.h"

#import "CommenMethods.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "ServiceListCollectionViewCell.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "UIImage+animatedGIF.h"
#import "FLAnimatedImage.h"


#ifndef SettingsController_h
#define SettingsController_h
@interface EmptyAddressCell :UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *infoLbl;

@end

@interface settingHeader : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@end


@interface settingAddresscell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *addressTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIButton *deletBtn;

@end

@interface languageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconimage;
@property (weak, nonatomic) IBOutlet UILabel *languageName;
@end






@interface SettingsController : UIViewController <UITableViewDataSource,UITableViewDelegate>

//	The user profile properties
@property (strong, nonatomic) IBOutlet UIButton *editProfileButton;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *profileNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *profileAddressPhoneLabel;

//	The favourites properties
@property (strong, nonatomic) IBOutlet UILabel *favouritesLabel;
@property (strong, nonatomic) IBOutlet UILabel *homeFavouriteLabel;
@property (strong, nonatomic) IBOutlet UILabel *workFavouriteLabel;
@property (strong, nonatomic) IBOutlet UILabel *moreSavedLabel;

@property (strong, nonatomic) IBOutlet UILabel *privacyLabel;

@property (strong, nonatomic) IBOutlet UILabel *signOutLabel;




@property(strong,nonatomic)NSArray * languageArray,* headerArray;
@property(strong,nonatomic)NSMutableArray *AddressArray;

+(instancetype)initController;

- (IBAction)editProfilePressed:(UIButton *)sender;
-(IBAction)signOutPressed:(UIButton *)sender;

@end
#endif
