//
//  Constants.m
//  Truck
//
//  Created by veena on 1/12/17.
//  Copyright © 2017 appoets. All rights reserved.
//
//

#import "Constants.h"

//NSString *const PICTURE=@"picture"; //
NSString *const PICTURE=@"picture"; //


#pragma mark - userdefaults
#pragma mark -
NSString *const UD_TOKEN_TYPE =@"token_type";
NSString *const UD_ACCESS_TOKEN =@"access_token";
NSString *const UD_REFERSH_TOKEN =@"ref_token";
NSString *const UD_PROFILE_IMG =@"profile_img";
NSString *const UD_PROFILE_NAME =@"profile_name";
NSString *const UD_REQUESTID=@"request_id";
NSString *const UD_SOCIAL =@"socialId";
NSString *const UD_ID =@"id";
NSString *const UD_SOS =@"sos";
//NSString *const SERVICE_URL =@"http://schedule.tranxit.co/";

NSString * SERVICE_URL = @"http://34.204.43.32";

//NSString * SERVICE_URL = @"http://128.199.235.122/cooper-web/public"; // zencode

NSString * APP_IMG_URL = @"";
NSString * APP_NAME = @"CoOper";

//client_id = 1  &&& client_secret = LxkjKzSznvTzF14qcT1MsEC5WNloNyLXzLfyKGMr
//client_id = 2 &&& client_secret = fHe39PiU8xroenWDIBH3L9pMZEWKFFo3nt6n6Z2F
//Client ID: 6
//Client Secret: zpTsmHxa4A5BLHN02WvpiD0CSkUBnoSaBZtj5FMf
NSString * Client_SECRET = @"zpTsmHxa4A5BLHN02WvpiD0CSkUBnoSaBZtj5FMf";
NSString * ClientID = @"6";
NSString * WEB_SOCKET = @"http://34.204.43.32:8000";
NSString * GMSMAP_KEY = @"AIzaSyAsVN1AHgbMr2n3SYtNZxFiT7e1bz3tP5s";
NSString * GMSPLACES_KEY = @"AIzaSyAsVN1AHgbMr2n3SYtNZxFiT7e1bz3tP5s";
NSString * Stripe_KEY = @"pk_test_9IoCaC1bKd9DUQ6x3E0YOTXa";
NSString * GOOGLE_API_KEY = @"AIzaSyAsVN1AHgbMr2n3SYtNZxFiT7e1bz3tP5s";





#pragma mark - Parameters
#pragma mark - --   Seque

NSString *const LOGIN=@"segLogin";
NSString *const REGISTER=@"segRegister";

NSString *const MD_LOGIN = @"/oauth/token";
NSString *const MD_REGISTER =@"/api/user/signup";
NSString *const MD_GETPROFILE =@"/api/user/details";
NSString *const MD_UPDATEPROFILE =@"/api/user/update/profile";
NSString *const MD_UPDATELOCATION =@"/api/user/update/location";
NSString *const MD_CHANGEPASSWORD =@"/api/user/change/password";
NSString *const MD_GET_SERVICE =@"/api/user/services";
NSString *const MD_GET_FAREESTIMATE =@"/api/user/estimated/fare";
NSString *const MD_CREATE_REQUEST =@"/api/user/send/request";
NSString *const MD_CANCEL_REQUEST =@"/api/user/cancel/request";
NSString *const MD_REQUEST_CHECK =@"/api/user/request/check";
NSString *const MD_RATE_PROVIDER =@"/api/user/rate/provider";
NSString *const MD_GET_HISTORY =@"/api/user/trips";
NSString *const MD_GET_SINGLE_HISTORY =@"/api/user/trip/details";
NSString *const MD_ADD_CARD =@"/api/user/card";
NSString *const MD_PAYMENT =@"/api/user/payment";
NSString *const MD_CARD_DELETE =@"/api/user/card/destory";
NSString *const MD_WALLET =@"/api/user/add/money";
NSString *const MD_GETPROMO =@"/api/user/promocodes";
NSString *const MD_ADDPROMO =@"/api/user/promocode/add";
NSString *const MD_UPCOMING =@"/api/user/upcoming/trips";
NSString *const MD_UPCOMING_HISTORYDETAILS =@"/api/user/upcoming/trip/details";

NSString *const MD_GETPROVIDERS =@"/api/user/show/providers";

NSString *const MD_RESETPASSWORD =@"/api/user/reset/password";
NSString *const MD_FORGOTPASSWORD =@"/api/user/forgot/password";
NSString *const MD_FACEBOOK =@"/api/user/auth/facebook";
NSString *const MD_GOOGLE =@"/api/user/auth/google";

NSString *const MD_LOGOUT =@"/api/user/logout";
NSString *const MD_HELP =@"/api/user/help";
NSString *const MD_REFRESH_TOKEN= @"/oauth/token";

NSString *const MD_EMAILVERIFY= @"/api/user/verify";

NSString *const MD_WALLET_PASSBOOK= @"/api/user/wallet/passbook";
NSString *const MD_COUPON_PASSBOOK= @"/api/user/promo/passbook";
