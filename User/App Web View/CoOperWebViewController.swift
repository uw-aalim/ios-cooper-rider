//
//  CoOperWebViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-11-05.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit
import WebKit

class CoOperWebViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	@IBOutlet var titleLabel: UILabel!
	@IBOutlet var webview: UIWebView!
	
	var webAddressString: String = ""
	var titleString: String = ""
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	class func initController() -> CoOperWebViewController {
		return CoOperWebViewController(
			nibName: "CoOperWebViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Loading and Appearance
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
		if let url = URL(string: self.webAddressString) {
			let urlRequest = URLRequest(url: url)
			self.webview.loadRequest(urlRequest)
		}
		self.titleLabel.text = self.titleString
    }
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Action Responder
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func backPressed(_ sender: AnyObject) {
		let _ = self.navigationController?.popViewController(animated: true)
	}
	//	------------------------------------------------------------------------------------------------------------
}
