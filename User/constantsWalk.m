//
//  constants.m
//  WalkThroughObjc
//
//  Created by CSS on 11/09/17.
//  Copyright © 2017 Appoets. All rights reserved.
//

#import "constantsWalk.h"

@implementation constantsWalk

+ (id)sharedManager {
    
    static constantsWalk *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


@end
