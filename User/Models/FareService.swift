//
//  FairService.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-15.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class FareService: NSObject {

    @objc var serviceId: NSNumber?
	@objc var calculatorType: String?
	@objc var capacity: NSNumber?
	@objc var serviceDescription: String?
	@objc var distance: NSNumber?
	@objc var fixed: NSNumber?
	@objc var hour: NSNumber?
	@objc var minute: NSNumber?
	@objc var image: String?
	@objc var name: String? {
		didSet {
			if let name = name {
				let newServiceNames = name.components(separatedBy: CharacterSet.whitespaces)
				if newServiceNames.count > 1 {
					self.nameClass = newServiceNames.last
				}
				else {
					self.nameClass = newServiceNames.first
				}
			}
			else {
				self.nameClass = nil
			}
		}
	}
	@objc var nameClass: String?
	@objc var providerName: String?
	@objc var status: NSNumber?
	@objc var idValue: NSNumber?
	@objc var price: NSNumber?
	
	
}
