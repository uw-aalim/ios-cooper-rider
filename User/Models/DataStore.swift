//
//  DataStore.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class DataStore: NSObject {
	
	//	============================================================================================================
	//	MARK:- Shared Singleton
	//	============================================================================================================
	static let shared = DataStore()
	
	
	
	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	var userIsLoggedIn: Bool {
		get {
			return self.checkForValidLoggedInUser()
		}
	}
	
	//	MARK:- User Properties 
	var userServiceUrl: NSString? {
		set {
			UserDefaults.standard.set(newValue, forKey: "serviceurl")
			UserDefaults.standard.synchronize()
		}
		get {
			return UserDefaults.standard.value(forKey: "serviceurl") as? NSString
		}
	}
	var clientSecret: NSString? {
		set {
			UserDefaults.standard.set(newValue, forKey: "passport")
			UserDefaults.standard.synchronize()
		}
		get {
			return UserDefaults.standard.value(forKey: "passport") as? NSString
		}
	}
	var clientId: NSString? {
		set {
			UserDefaults.standard.set(newValue, forKey: "clientid")
			UserDefaults.standard.synchronize()
		}
		get {
			return UserDefaults.standard.value(forKey: "clientid") as? NSString
		}
	}
	/*
	Client_SECRET = [user objectForKey:@"passport"];
	ClientID = [user objectForKey:@"clientid"];
	WEB_SOCKET = [user objectForKey:@"websocket"];
	APP_NAME = [user objectForKey:@"appname"];
	APP_IMG_URL = [user objectForKey:@"appimg"];
	isValid = [user objectForKey:@"is_valid"];
	usrnameStr = [user objectForKey:@"username"];
	passwordStr =  [user objectForKey:@"password"];
	GMSMAP_KEY = [user objectForKey:@"googleApiKey"];
	GMSPLACES_KEY = [user objectForKey:@"googleApiKey"];
	///GMSPLACES_KEY = @"AIzaSyBs7Ct-_9SrgBE2rw0pN6U5dgErrm7jZSA";
	Stripe_KEY = [user objectForKey:@"stripekey"];
*/
	
	fileprivate func checkForValidLoggedInUser() -> Bool {
		let tokenStr = UserDefaults.standard.value(forKey: "access_token") as? String
		if tokenStr == nil ||
			tokenStr!.count <= 0 ||
			tokenStr! == "" {
			return false
		}
		else {
			if let validBoolStr = UserDefaults.standard.value(forKey: "is_valid") as? String,
				let validBoolInt = Int(validBoolStr),
				validBoolInt > 0 {
				return true
			}
			else {
				return false
			}
		}
	}
}
