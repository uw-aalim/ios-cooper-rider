//
//  Ride.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-14.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

class Ride: NSObject {
	
	/**
	If this is a ride that is currently being planned then set the planning state.
	*/
	var planningState: PlanningRideState?
	
	/**
	What is the origin point for this ride?
	*/
	@objc
	var origin: RouteEndPoint?
	/**
	What is the destination point for this ride?
	*/
	@objc
	var destination: RouteEndPoint?
	
	/**
	What method of payment has been chosen for the ride?
	*/
	@objc
	var paymentMethod: RidePayment?
	
	@objc
	var fareService: FareService?
	
	/**
	What is the scheduled pickup date and time for the ride?
	If scheduledTime is left empty then it is assumed that the ride should be called immidiately.
	*/
	@objc
	var scheduledTime: Date?
	
	@objc
	var fareEstimate: FareEstimate?
	
	@objc
	var didBookRide: Bool = false
	
	
	
	/**
	Determine if the ride is valid based on the current state of the configured ride. This is
	especially useful when planning a new ride as it enables you to check to determine if
	there are fields that the user still has to fill out before booking the ride on the server.
	*/
	var canGeneratePath: Bool {
		if self.origin != nil &&
			self.destination != nil {
			return true
		}
		else {
			return false
		}
	}
	
	var isValidRide: Bool {
		if self.origin != nil &&
			self.destination != nil &&
			self.paymentMethod != nil &&
			self.fareService != nil {
			return true
		}
		else {
			return false
		}
	}
	
	@objc
	override init() {
		super.init()
	}
}
