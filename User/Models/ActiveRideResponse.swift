//
//  ActiveRideResponse.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

struct ActiveRidePayment {
	var comission: Int?
	var discount: Int?
	var distance: Double?
	var fixed: Int?
	var idValue: Int64?
	var payable: Int64?
	var paymentId: String?
	var paymentMode: String?
	var promoCodeId: String?
	var providerComission: Int?
	var providerPay: Int?
	var requestId: Int64?
	var surge: Int?
	var tax: Int?
	var total: Int?
	var wallet: Int?
}

struct ActiveRideProvider {
	var avatar: String?
	var createdAt: String?
	var email: String?
	
	var firstName: String?
	var lastName: String?
	
	var fleet: Int?
	var gender: String?
	var idValue: Int64?
	
	var latitude: Double?
	var longitude: Double?
	
	var loginBy: String?
	var mobileNumber: String?
	var otp: String?
	var rating: String?
	var socialUniqueId: String?
	var status: String?
	var updatedAt: String?
}

struct ActiveRideProviderService {
	var idValue: Int64?
	var providerId: Int64?
	var serviceModel: String?
	var serviceNumber: String?
	var serviceTypeId: Int?
	var status: String?
}

struct ActiveRideServiceType {
	
	var calculator: String?
	var capacity: Int?
	var description: String?
	var distance: Double?
	var fixed: Int?
	var hour: Int?
	var minute: Int?
	var idValue: Int64?
	var image: String?
	var name: String?
	var price: Double?
	var providerName: String?
	var status: Int?
	
	
}

struct ActiveRideUser {
	
	var deviceId: String?
	var deviceToken: String?
	var deviceType: String?
	
	var email: String?
	var firstName: String?
	var lastName: String?
	var gender: String?
	
	var idValue: Int64?
	
	var latitude: Double?
	var longitude: Double?
	
	var loginBy: String?
	var mobile: String?
	var otp: String?
	
	var paymentMode: String?
	var picture: String?
	var rating: Double?
	var socialUniqueId: String?
	var stripeCustId: String?
	var updatedAt: String?
	var walletBalance: Double?
	
}

class ActiveRideResponse: NSObject {
	
	var assignedAt: String?
	var bookingId: String?
	var cancelReason: String?
	var cancelledBy: String?
	var createdAt: String?
	var currentProviderId: Int64?
	
	var origin: RouteEndPoint?
	var destination: RouteEndPoint?
	
	var deletedAt: String?
	var distance: Double?
	var finishedAt: String?
	@objc
	var idValue: NSNumber?
	var isTrack: Bool?
	var otp: String?
	var paid: Int?
	var payment: ActiveRidePayment?
		
	var paymentMode: String?
	
	var provider: ActiveRideProvider?
	var providerId: Int64?
	var providerRated: Double?
	var providerService: ActiveRideProviderService?
		
	var rating: Double?
	var rentalHours: String?
	var routeKey: String?
		
	var scheduledAt: String?
	
	var serviceType: ActiveRideServiceType?
	var serviceTypeId: Int64?
	
	var status: ActiveRideState?
	
	var surge: Int?
		
	var trackDistance: Double?
	var trackLatitude: Double?
	var trackLongitude: Double?
	var travelTime: Double?
	
	var startedAt: String?
	var updatedAt: String?
		
	var useWallet: Int?
	
	var user: ActiveRideUser?
		
	var userId: Int64?
	var userRated: Double?
	
	@objc
	class func create(withDictionary dictionary: NSDictionary) -> ActiveRideResponse {
		
		let response = ActiveRideResponse()
		
		if let value = dictionary["assigned_at"] as? String {
			response.assignedAt = value
		}
		if let value = dictionary["booking_id"] as? String {
			response.bookingId = value
		}
		if let value = dictionary["cancel_reason"] as? String {
			response.cancelReason = value
		}
		if let value = dictionary["cancelled_by"] as? String {
			response.cancelledBy = value
		}
		if let value = dictionary["created_at"] as? String {
			response.createdAt = value
		}
		if let value = dictionary["current_provider_id"] as? NSNumber {
			response.currentProviderId = value.int64Value
		}
		if let destinationAddress = dictionary["d_address"] as? String,
			let destinationLatitude = dictionary["d_latitude"] as? NSNumber,
			let destinationLongitude = dictionary["d_longitude"] as? NSNumber {
			
			response.destination = RouteEndPoint(
				withLatitude: destinationLatitude.doubleValue,
				withLongitude: destinationLongitude.doubleValue,
				withAddress: destinationAddress)
		}
		if let value = dictionary["deleted_at"] as? String {
			response.deletedAt = value
		}
		if let value = dictionary["distance"] as? NSNumber {
			response.distance = value.doubleValue
		}
		if let value = dictionary["finished_at"] as? String {
			response.finishedAt = value
		}
		if let value = dictionary["id"] as? NSNumber {
			response.idValue = value
		}
		
		if let value = dictionary["is_track"] as? String {
			if value == "NO" {
				response.isTrack = false
			}
			else {
				response.isTrack = true
			}
		}
		if let value = dictionary["otp"] as? String {
			response.otp = value
		}
		if let value = dictionary["paid"] as? NSNumber {
			response.paid = value.intValue
		}
		
		if let paymentDict = dictionary["payment"] as? NSDictionary {
			
			let payment = ActiveRidePayment(
				comission: (paymentDict["commision"] as? NSNumber)?.intValue,
				discount: (paymentDict["discount"] as? NSNumber)?.intValue,
				distance: (paymentDict["distance"] as? NSNumber)?.doubleValue,
				fixed: (paymentDict["fixed"] as? NSNumber)?.intValue,
				idValue: (paymentDict["id"] as? NSNumber)?.int64Value,
				payable: (paymentDict["payable"] as? NSNumber)?.int64Value,
				paymentId: (paymentDict["payment_id"] as? String),
				paymentMode: (paymentDict["payment_mode"] as? String),
				promoCodeId: (paymentDict["promocode_id"] as? String),
				providerComission: (paymentDict["provider_commission"] as? NSNumber)?.intValue,
				providerPay: (paymentDict["provider_pay"] as? NSNumber)?.intValue,
				requestId: (paymentDict["request_id"] as? NSNumber)?.int64Value,
				surge: (paymentDict["surge"] as? NSNumber)?.intValue,
				tax: (paymentDict["tax"] as? NSNumber)?.intValue,
				total: (paymentDict["total"] as? NSNumber)?.intValue,
				wallet: (paymentDict["wallet"] as? NSNumber)?.intValue)
			response.payment = payment
			
		}
		if let value = dictionary["payment_mode"] as? String {
			response.paymentMode = value
		}
		if let providerDict = dictionary["provider"] as? NSDictionary {
			
			print("Driver rating is \(String(describing: (providerDict["rating"] as? String)))")
			
			
			
			let provider = ActiveRideProvider(
				avatar: (providerDict["avatar"] as? String),
				createdAt: (providerDict["created_at"] as? String),
				email: (providerDict["email"] as? String),
				firstName: (providerDict["first_name"] as? String),
				lastName: (providerDict["last_name"] as? String),
				fleet: (providerDict["fleet"] as? NSNumber)?.intValue,
				gender: (providerDict["gender"] as? String),
				idValue: (providerDict["id"] as? NSNumber)?.int64Value,
				latitude: (providerDict["latitude"] as? NSNumber)?.doubleValue,
				longitude: (providerDict["longitude"] as? NSNumber)?.doubleValue,
				loginBy: (providerDict["login_by"] as? String),
				mobileNumber: (providerDict["mobile"] as? String),
				otp: (providerDict["otp"] as? String),
				rating: (providerDict["rating"] as? String),
				socialUniqueId: (providerDict["social_unique_id"] as? String),
				status: (providerDict["status"] as? String),
				updatedAt: (providerDict["updated_at"] as? String))
			response.provider = provider
		}
		
		if let value = dictionary["provider_id"] as? NSNumber {
			response.providerId = value.int64Value
		}
		if let value = dictionary["provider_id"] as? NSNumber {
			response.providerRated = value.doubleValue
		}
		
		if let providerServiceDict = dictionary["provider_service"] as? NSDictionary {
			
			let providerService = ActiveRideProviderService(
				idValue: (providerServiceDict["id"] as? NSNumber)?.int64Value,
				providerId: (providerServiceDict["provider_id"] as? NSNumber)?.int64Value,
				serviceModel: (providerServiceDict["service_model"] as? String),
				serviceNumber: (providerServiceDict["service_number"] as? String),
				serviceTypeId: (providerServiceDict["service_type_id"] as? NSNumber)?.intValue,
				status: (providerServiceDict["status"] as? String))
			response.providerService = providerService
		}
		if let value = dictionary["rating"] as? NSNumber {
			response.rating = value.doubleValue
		}
		
		if let value = dictionary["rental_hours"] as? String {
			response.rentalHours = value
		}
		if let value = dictionary["route_key"] as? String {
			response.routeKey = value
		}
		
		if let originAddress = dictionary["s_address"] as? String,
			let originLatitude = dictionary["s_latitude"] as? NSNumber,
			let originLongitude = dictionary["s_longitude"] as? NSNumber {
			
			response.origin = RouteEndPoint(
				withLatitude: originLatitude.doubleValue,
				withLongitude: originLongitude.doubleValue,
				withAddress: originAddress)
		}
		
		if let value = dictionary["schedule_at"] as? String {
			response.scheduledAt = value
		}
		
		if let serviceTypeDict = dictionary["service_type"] as? NSDictionary {
			let serviceType = ActiveRideServiceType(
				calculator: (serviceTypeDict["calculator"] as? String),
				capacity: (serviceTypeDict["capacity"] as? NSNumber)?.intValue,
				description: (serviceTypeDict["description"] as? String),
				distance: (serviceTypeDict["distance"] as? NSNumber)?.doubleValue,
				fixed: (serviceTypeDict["fixed"] as? NSNumber)?.intValue,
				hour: (serviceTypeDict["hour"] as? NSNumber)?.intValue,
				minute: (serviceTypeDict["minute"] as? NSNumber)?.intValue,
				idValue: (serviceTypeDict["id"] as? NSNumber)?.int64Value,
				image: (serviceTypeDict["image"] as? String),
				name: (serviceTypeDict["name"] as? String),
				price: (serviceTypeDict["price"] as? NSNumber)?.doubleValue,
				providerName: (serviceTypeDict["provider_name"] as? String),
				status: (serviceTypeDict["status"] as? NSNumber)?.intValue)
			response.serviceType = serviceType
		}
		if let value = dictionary["service_type_id"] as? NSNumber {
			response.serviceTypeId = value.int64Value
		}
		
		if let value = dictionary["started_at"] as? String {
			response.startedAt = value
		}
		
		if let value = dictionary["status"] as? String {
			response.status = ActiveRideState(rawValue: value)
		}
		
		if let value = dictionary["surge"] as? NSNumber {
			response.surge = value.intValue
		}
		
		if let value = dictionary["track_distance"] as? NSNumber {
			response.trackDistance = value.doubleValue
		}
		if let value = dictionary["track_latitude"] as? NSNumber {
			response.trackLatitude = value.doubleValue
		}
		if let value = dictionary["track_longitude"] as? NSNumber {
			response.trackLongitude = value.doubleValue
		}
		if let value = dictionary["travel_time"] as? NSNumber {
			response.travelTime = value.doubleValue
		}
		
		if let value = dictionary["updated_at"] as? String {
			response.updatedAt = value
		}
		
		if let value = dictionary["use_wallet"] as? NSNumber {
			response.useWallet = value.intValue
		}
		
		if let userDict = dictionary["service_type"] as? NSDictionary {
			
			let user = ActiveRideUser(
				deviceId: (userDict["device_id"] as? String),
				deviceToken: (userDict["device_token"] as? String),
				deviceType: (userDict["device_type"] as? String),
				email: (userDict["email"] as? String),
				firstName: (userDict["first_name"] as? String),
				lastName: (userDict["last_name"] as? String),
				gender: (userDict["gender"] as? String),
				idValue: (userDict["id"] as? NSNumber)?.int64Value,
				latitude: (userDict["latitude"] as? NSNumber)?.doubleValue,
				longitude: (userDict["longitude"] as? NSNumber)?.doubleValue,
				loginBy: (userDict["login_by"] as? String),
				mobile: (userDict["mobile"] as? String),
				otp: (userDict["otp"] as? String),
				paymentMode: (userDict["payment_mode"] as? String),
				picture: (userDict["picture"] as? String),
				rating: (userDict["rating"] as? NSNumber)?.doubleValue,
				socialUniqueId: (userDict["social_unique_id"] as? String),
				stripeCustId: (userDict["stripe_cust_id"] as? String),
				updatedAt: (userDict["updated_at"] as? String),
				walletBalance: (userDict["wallet_balance"] as? NSNumber)?.doubleValue)
			response.user = user
		}
		
		if let value = dictionary["user_id"] as? NSNumber {
			response.userId = value.int64Value
		}
		if let value = dictionary["user_rated"] as? NSNumber {
			response.userRated = value.doubleValue
		}
		
		
		
		return response
	}
	
	/*
	{
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	"assigned_at" = "2018-06-19 01:30:33";
	"booking_id" = TRNX836260;
	"cancel_reason" = "<null>";
	"cancelled_by" = NONE;
	"created_at" = "2018-06-19 01:30:33";
	"current_provider_id" = 120;
	"d_address" = "88 Grant St, Kentville, NS B4N 2S4, Canada";
	"d_latitude" = "45.071335";
	"d_longitude" = "-64.518505";
	"deleted_at" = "<null>";
	distance = 15;
	"finished_at" = "2018-06-19 01:31:23";
	id = 24;
	"is_track" = NO;
	otp = 1608;
	paid = 0;
	payment =             {
	commision = 0;
	discount = 0;
	distance = 140;
	fixed = 20;
	id = 8;
	payable = 160;
	"payment_id" = "<null>";
	"payment_mode" = "<null>";
	"promocode_id" = "<null>";
	"provider_commission" = 0;
	"provider_pay" = 160;
	"request_id" = 24;
	surge = 0;
	tax = 0;
	total = 160;
	wallet = 0;
	};
	
	"payment_mode" = CASH;
	provider =             {
	avatar = "<null>";
	"created_at" = "2018-06-13 23:52:30";
	email = "driver@aliasapps.com";
	"first_name" = Mike;
	fleet = 0;
	gender = MALE;
	id = 120;
	"last_name" = Driver;
	latitude = "45.07147315";
	"login_by" = manual;
	longitude = "-64.51894077";
	mobile = "+14168094255";
	otp = 0;
	rating = "3.00";
	"social_unique_id" = "<null>";
	status = approved;
	"updated_at" = "2018-06-19 01:31:19";
	};
	
	"provider_id" = 120;
	"provider_rated" = 0;
	
	"provider_service" =             {
	id = 121;
	"provider_id" = 120;
	"service_model" = "Volkswagen Jetta";
	"service_number" = Cooper2;
	"service_type_id" = 1;
	status = riding;
	};
	
	rating = "<null>";
	"rental_hours" = "<null>";
	"route_key" = "mlerGnxzgK`@^\\f@eAZkCn@gCd@qAVX|CTvCZrFTtDTjBx@hD~AlF~@lCj@|AZp@fBhDb@x@z@~BfBnFd@fAXh@tAhCt@|A\\dAd@bBX~AL`BJfCBxCA~CGlEGrBWbDQnBMtEBjIHvGNnCZ`EN|An@~DvBfLfBlIVjAI`@ITUVg@@g@M_@Ua@a@sAqA_@Qe@I[?a@HqE~A{MbFw@b@e@\\}EnFqEdFoCbDcCbDg@t@u@tAwCdHw@`Bg@t@u@~@`CbDjAtBpGxNb@|BjC`Zr@zHt@jFnDvRb@pAVf@`CrCdChBd@f@^j@Zj@ZhAvA~MVxCBdCg@|ZoAdWuIns@_BxOQ`E@~@FfArBdP|Ivp@|Eve@v@|F`@pCbChM^vCv@rE^zA`AlDXx@j@pB`CjJPbARbBD|@?fBEz@MpAS`AYfAgAbD_@lBYlDK~E?rABl@PzAlA~G|@nENn@Rf@Zb@r@h@p@^lDlA\\V`@Zv@vAVx@Jl@NxDFjBZnAFTdAtDPh@P`A?dA`A`@zBz@z@|@Vx@GDGNs@fTFfATpAP^^p@^NpAb@`@LJ?ZMNRLNVLzB\\u@zR@TPdBvAzI~B~NXvBLvAFzA@vAMvVGtL~Ds@`@APDLJTTt@w@h@i@d@KL?jH|A\\RhCjEhC`E";
	"s_address" = "15 University Ave, Wolfville, NS B4P 2R6, Canada";
	"s_latitude" = "45.088573";
	"s_longitude" = "-64.366817";
	"schedule_at" = "<null>";
	
	"service_type" =             {
	calculator = DISTANCE;
	capacity = 4;
	description = "<null>";
	distance = 1;
	fixed = 20;
	hour = "<null>";
	id = 1;
	image = "http://18.216.251.57/uploads/523e19e98c367007a473532e01e53057d4cce508.png";
	minute = 0;
	name = "CoOper Cool";
	price = 10;
	"provider_name" = Driver;
	status = 1;
	
	
	};
	"service_type_id" = 1;
	"started_at" = "2018-06-19 01:31:19";
	status = DROPPED;
	surge = 0;
	"track_distance" = 0;
	"track_latitude" = 0;
	"track_longitude" = 0;
	"travel_time" = 0;
	"updated_at" = "2018-06-19 01:31:23";
	"use_wallet" = 0;
	
	user =             {
	"device_id" = "63DABC00-2A06-482D-B8A2-4D246A07F512";
	"device_token" = "no device";
	"device_type" = ios;
	email = "myJunkEmail@aliasapps.com";
	"first_name" = myJunkEmail;
	gender = MALE;
	id = 129;
	"last_name" = myjunkEmail;
	latitude = "37.786088";
	"login_by" = manual;
	longitude = "-122.405994";
	mobile = "+15198305643";
	otp = 0;
	"payment_mode" = CASH;
	picture = "<null>";
	rating = "5.00";
	"social_unique_id" = "<null>";
	"stripe_cust_id" = "<null>";
	"updated_at" = "2018-06-19 01:30:33";
	"wallet_balance" = 0;
	};
	"user_id" = 129;
	"user_rated" = 0;
	}
	*/
}
