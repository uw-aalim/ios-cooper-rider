//
//  GooglePlaceObject.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces

//	============================================================================================================
/**
Wrap a GMSPlace inside of a custom object so that the initial place search is enough to get the GMSPlace insetad
of having to wait for the call into GooglePlaces to get the real GMSPlace object. The results of the place
search is a JSON array not an array of GMSPlaces so need to store those results in these objects.
*/
@objc
class GooglePlaceObject: NSObject {

	@objc var latitude: NSNumber?
	@objc var longitude: NSNumber?
	var placeId: String!
	var name: String?
	@objc var address: String?
	var website: URL?
	
	var placeImage: UIImage?
	
	@objc
	var gmsPlace: GMSPlace?
}
//	============================================================================================================
