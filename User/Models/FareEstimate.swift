//
//  FairEstimate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class FareEstimate: NSObject {

	@objc var basePrice: NSNumber?
	@objc var distance: NSNumber?
	@objc var estimatedFare: NSNumber?
	@objc var surge: NSNumber?
	@objc var surgeValue: String?
	@objc var taxPrice: NSNumber?
	@objc var time: String?
	@objc var walletBalance: NSNumber?
	
	@objc var isValidFareEstimate: Bool {
		get {
			if self.distance == nil ||
				self.estimatedFare == nil {
				
				return false
			}
			else {
				return true
			}
		}
	}
}
