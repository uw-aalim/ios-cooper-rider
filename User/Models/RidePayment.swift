//
//  RidePayment.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-15.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class RidePayment: NSObject {

	var paymentType: PaymentType?
	
	@objc
	var brand: String?
	@objc
	var cardId: String?	
	var id: Int?
	@objc
	var lastFour: String?
	
	
}
