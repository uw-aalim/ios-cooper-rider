//
//  RoutingSystem.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class RouteEndPoint: NSObject {
	
	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	@objc var latitude: NSNumber!
	@objc var longitude: NSNumber!
	@objc var address: String!
	
	@objc var latitudeString: NSString {
		get {
			let string = String(format: "%.05f", arguments: [self.latitude.doubleValue])
			return NSString(string: string)
		}
	}
	@objc var longitudeString: NSString {
		get {
			let string = String(format: "%.05f", arguments: [self.longitude.doubleValue])
			return NSString(string: string)
		}
	}
	
	@objc var position: CLLocationCoordinate2D {
		get {
			return CLLocationCoordinate2D(latitude: self.latitude.doubleValue, longitude: self.longitude.doubleValue)
		}
	}
	
	
	
	//	============================================================================================================
	//	MARK:- Initialize
	//	============================================================================================================
	
	@objc
	init(
		withLatitude latitude: Double,
		withLongitude longitude: Double,
		withAddress address: String) {
		
		super.init()
		
		self.latitude = NSNumber(value: latitude)
		self.longitude = NSNumber(value: longitude)
		self.address = address
	}
}
