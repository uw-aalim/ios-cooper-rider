//
//  MenuViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-08.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit
import AlamofireImage

protocol MenuViewControllerDelegate: class {
	func closeMenuButtonPressed(_ sender: AnyObject)
	
	func profileButtonPressed(_ sender: AnyObject)
	func makeMoneyButtonPressed(_ sender: AnyObject)
	func paymentButtonPressed(_ sender: AnyObject)
	func yourTripsButtonPressed(_ sender: AnyObject)
	func exploreMenuButtonPressed(_ sender: AnyObject)
	func freeRidesButtonPressed(_ sender: AnyObject)
	func helpButtonPressed(_ sender: AnyObject)
	func settingsButtonPressed(_ sender: AnyObject)
	func legalButtonPressed(_ sender: AnyObject)
	func walletMenuButtonPressed(_ sender: AnyObject)
	func logoutButtonPressed(_ sender: AnyObject)
}

class MenuViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Constants
	//	============================================================================================================
	
	let minimumProfileImageSize: CGFloat = 32.0
	let normalProfileImageSize: CGFloat = 72.0
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	@IBOutlet weak var profileImageWidth: NSLayoutConstraint!
	@IBOutlet weak var scrollView: UIScrollView!
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	
	weak var delegate: MenuViewControllerDelegate?
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Controller Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	class func initController() -> MenuViewController {
		return MenuViewController(
			nibName: "MenuViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Loading Appearance and Dissapearance
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidLoad() {
		super.viewDidLoad()
		
		//	Register for the login notifications
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(MenuViewController.loginRegistrationFinishedSuccess(_:)),
			name: CoOperNotification.loginRegisterProcessSuccess,
			object: nil)
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(MenuViewController.loginRegistrationFinishedFailed(_:)),
			name: CoOperNotification.loginRegisterProcessFailed,
			object: nil)
		
		self.updateNamesAndImage()
		
		
		self.scrollView.delegate = self
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func updateNamesAndImage() {
		if let name = UserDefaults.standard.value(forKey: UD_PROFILE_NAME) as? String {
			self.nameLabel.text = "\(name)"
		}
		if let imageString = UserDefaults.standard.value(forKey: UD_PROFILE_IMG) as? String,
			let imageUrl = URL(string:imageString) {
			//print("Setting image url from imageString \(imageUrl)")
			self.imageView.af_setImage(withURL: imageUrl)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Action Responders
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func closeMenuButtonPressed(_ sender: AnyObject) {
		self.delegate?.closeMenuButtonPressed(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func profileButtonPressed(_ sender: AnyObject) {
		self.delegate?.profileButtonPressed(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func makeMoneyButtonPressed(_ sender: AnyObject) {
		self.delegate?.makeMoneyButtonPressed(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func paymentButtonPressed(_ sender: AnyObject) {
		self.delegate?.paymentButtonPressed(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func walletButtonPressed(_ sender: AnyObject) {
		self.delegate?.walletMenuButtonPressed(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func yourTripsButtonPressed(_ sender: AnyObject) {
		self.delegate?.yourTripsButtonPressed(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func exploreButtonPressed(_ sender: AnyObject) {
		self.delegate?.exploreMenuButtonPressed(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func freeRidesButtonPressed(_ sender: AnyObject) {
		self.delegate?.freeRidesButtonPressed(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func helpButtonPressed(_ sender: AnyObject) {
		self.delegate?.helpButtonPressed(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func settingsButtonPressed(_ sender: AnyObject) {
		self.delegate?.settingsButtonPressed(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func legalButtonPressed(_ sender: AnyObject) {
		self.delegate?.legalButtonPressed(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func logoutButtonPressed(_ sender: AnyObject) {
		self.delegate?.logoutButtonPressed(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
}
