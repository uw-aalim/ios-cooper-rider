//
//  MenuViewController+LoginRegisterNotifications.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-22.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension MenuViewController {
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func loginRegistrationFinishedSuccess(_ notification: Notification) {
		DispatchQueue.main.async {
			self.updateNamesAndImage()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func loginRegistrationFinishedFailed(_ notification: Notification) {
		DispatchQueue.main.async {
			
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
