//
//  MenuViewController+ScrollViewDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-08.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension MenuViewController: UIScrollViewDelegate {
	
	//	============================================================================================================
	//	MARK:- UIScrollViewDelegate
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func scrollViewDidScroll(
		_ scrollView: UIScrollView) {
		
		if scrollView.contentOffset.y > 0 {
			
			var newSize: CGFloat = 72.0 - scrollView.contentOffset.y
			if newSize < 32.0 {
				newSize = 32.0
			}
			self.profileImageWidth.constant = newSize			
		}
		else {
			self.profileImageWidth.constant = 72 + (-scrollView.contentOffset.y)
		}
		self.imageView.layer.cornerRadius = (self.profileImageWidth.constant)/2.0
	}
	//	------------------------------------------------------------------------------------------------------------
}
