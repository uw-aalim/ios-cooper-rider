//
//  WalletViewController.m
//  Provider
//
//  Created by iCOMPUTERS on 14/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "WalletViewController.h"
#import "CreditCardViewController.h"
#import "ViewController.h"
#import "Constants.h"
#import "Colors.h"

@interface WalletViewController ()
{
    NSMutableArray *arrPaymentCardList;
}

@end

@implementation WalletViewController

+(instancetype)initController {
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Wallet" bundle: nil];
	WalletViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"WalletViewController"];
	return viewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view.
	[self configureNavigationController];
    [_doneBtn setHidden:YES];
    [self setDesignStyles];
    UITapGestureRecognizer *tapGesture_condition=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ViewOuterTap)];
    tapGesture_condition.cancelsTouchesInView=NO;
    tapGesture_condition.delegate=self;
    [self.view addGestureRecognizer:tapGesture_condition];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
   
    
    [self addBottomBorderWithColor:[UIColor lightGrayColor] andWidth:1 :_enterAmtTxtfiedl];
    [self addBottomBorderWithColor:[UIColor lightGrayColor] andWidth:1 :_symbolLbl];

    _symbolLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    self.screenName = @"Your Wallet";
    LOTAnimationView *animation = [LOTAnimationView animationNamed:@"wallet"];
    animation.frame = _WalletImageview.bounds;
    animation.loopAnimation = YES;
    animation.autoresizingMask = UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleLeftMargin |
    UIViewAutoresizingFlexibleBottomMargin;
    [_WalletImageview addSubview:animation];
    [animation playWithCompletion:^(BOOL animationFinished) {
        // Do Something
    }];
    [self onGetProfile];
    [self LocalizationUpdate];
}

-(void)configureNavigationController {
	if (@available(iOS 11.0, *)) {
		[self navigationController].navigationBar.prefersLargeTitles = TRUE;
		[self navigationItem].largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAlways;
	}
	
	[[self navigationController].navigationBar setTranslucent:FALSE];
	[self navigationController].navigationBar.barTintColor = [UIColor blackColor];
	[self navigationController].navigationBar.barStyle = UIBarStyleBlackOpaque;
	[self navigationController].navigationBar.backgroundColor = [UIColor blackColor];
	[self navigationController].navigationBar.tintColor = [UIColor whiteColor];
	
	if (self.navigationController.viewControllers.count <= 1) {
		NSString *backCloseString = @"close_x_icon";
		UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]
										initWithImage:[UIImage imageNamed:backCloseString]
										style:UIBarButtonItemStylePlain
										target:self
										action:@selector(backBtn:)];
		[self navigationItem].leftBarButtonItem = closeButton;
	}
}

-(void)LocalizationUpdate{
    _navuicationTitleLble.text = LocalizedString(@"WALLET");
    _addMoneyLbl.text = LocalizedString(@"Add Money");
    _walletLbl.text = LocalizedString(@"Your wallet amount is");
    [_walletBtn setTitle:LocalizedString(@"ADD AMOUNT") forState:UIControlStateNormal];
    [_changeBtn setTitle:LocalizedString(@"Change") forState:UIControlStateNormal];


}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Own Method

-(void)onGetProfile
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [appDelegate onStartLoader];
        
        NSString* UDID_Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
        NSLog(@"output is : %@", UDID_Identifier);
        
        NSDictionary *params=@{@"device_token":appDelegate.strDeviceToken,@"device_type":@"ios", @"device_id":UDID_Identifier};
        
        [afn getDataFromPath:MD_GETPROFILE withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            [appDelegate onEndLoader];
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                NSString *currencyStr=[Utilities removeNullFromString: [user valueForKey:@"currency"]];
                _amountLbl.text = [NSString stringWithFormat:@"%@%@", currencyStr, [response valueForKey:@"wallet_balance"]];
                
                [self getAllCards];
            }
            else
            {
                if ([errorcode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                    [self logoutMethod];
                    
//                    [CommenMethods onRefreshToken];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
//                    
//                    PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
//                    [self.navigationController pushViewController:wallet animated:YES];
                }
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}


-(void)logoutMethod
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn refreshMethod_NoLoader:MD_REFRESH_TOKEN withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
                [user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
                [user setValue:response[@"refresh_token"] forKey:UD_REFERSH_TOKEN];
            }
            else
            {
                [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}


-(void)getAllCards
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [appDelegate onStartLoader];
        [afn getDataFromPath:MD_ADD_CARD withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            [appDelegate onEndLoader];
            if (response)
            {
                arrPaymentCardList=[[NSMutableArray alloc] init];
                [arrPaymentCardList addObjectsFromArray:response];
                NSLog(@"%@",arrPaymentCardList);
                
                if (arrPaymentCardList.count ==0)
                {
                    [_cardView setHidden:YES];
                    ///Add cards
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT")  message:@"Please add a card to proceed" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
						CreditCardViewController *viewController = [CreditCardViewController initController];
						UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:viewController];
						[self presentViewController:navigationController animated:YES completion:nil];
                    }];
                    
                    UIAlertAction* Cancel = [UIAlertAction actionWithTitle:LocalizedString(@"Cancel") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                   
                    [alertController addAction:Cancel];
                    [alertController addAction:ok];

                    [self presentViewController:alertController animated:YES completion:nil];
                }
                else
                {
                    [_cardView setHidden:NO];
                    NSDictionary *dictVal=[arrPaymentCardList objectAtIndex:0];
                    _cardLbl.text =[NSString stringWithFormat:@"XXXX-XXXX-XXXX-%@",[dictVal valueForKey:@"last_four"]];
                    strCardID = [dictVal valueForKey:@"card_id"];
                }
            }
            else
            {
                if ([errorcode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                    [self logoutMethod];
                    
//                    [CommenMethods onRefreshToken];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
//                    
//                    PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
//                    [self.navigationController pushViewController:wallet animated:YES];
                }
            }
            
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
    
    
}


-(void)setDesignStyles
{
    [CSS_Class APP_textfield_Outfocus:_amountText];
    [CSS_Class APP_Blackbutton:_addMoneyBtn];
    [CSS_Class APP_Blackbutton:_walletBtn];
    
    UIBezierPath *shadows = [UIBezierPath bezierPathWithRect:_commonRateView.frame];
    _commonRateView.layer.masksToBounds = NO;
    _commonRateView.layer.shadowColor = [UIColor blackColor].CGColor;
    _commonRateView.layer.shadowOffset = CGSizeMake(1.5f, 3.0f);
    _commonRateView.layer.shadowOpacity = 1.5f;
    _commonRateView.layer.shadowPath = shadows.CGPath;
    
    [CSS_Class APP_fieldValue_Small:_cardLbl];
    [CSS_Class APP_labelName:_enterAmountLbl];
    [CSS_Class APP_labelName:_walletLbl];
    [CSS_Class App_Header:_amountLbl];
    [_amountLbl setTextColor:BASE2Color];
}

-(void)backBtn:(id)sender {
	if (self.navigationController.viewControllers.count <= 1) {
		[self.navigationController.presentingViewController dismissViewControllerAnimated:true completion:NULL];
	}
	else {
		[self.navigationController popViewControllerAnimated:YES];
	}
}
- (IBAction)amountSelectBtnAction:(UIButton *)sender {
    
    if(sender.tag == 111){
        sender.backgroundColor = BASE2Color;
        _amt599Btn.backgroundColor = [UIColor clearColor];
        _amt1099Btn.backgroundColor = [UIColor clearColor];
    }else if(sender.tag == 222){
        sender.backgroundColor = BASE2Color;
        _amy199Btn.backgroundColor = [UIColor clearColor];
        _amt1099Btn.backgroundColor = [UIColor clearColor];
    }else{
        sender.backgroundColor = BASE2Color;
        _amy199Btn.backgroundColor = [UIColor clearColor];
        _amt599Btn.backgroundColor = [UIColor clearColor];
    }
    _enterAmtTxtfiedl.text = sender.titleLabel.text;
}

-(IBAction)addBtn:(id)sender
{
    if (arrPaymentCardList.count ==0)
    {
        ///Add cards
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT")  message:NSLocalizedString(@"ADD_CARD", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            CreditCardViewController *viewController = [CreditCardViewController initController];
			UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:viewController];
            [self presentViewController:navigationController animated:YES completion:nil];
        }];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
//        [UIView animateWithDuration:0.45 animations:^{
//            if(![_enterAmtTxtfiedl.text  isEqualToString:@""]){
//                _Amount = _enterAmtTxtfiedl.text;
// 
//            }
//            _amountText.text = _Amount;
//            
//            _commonRateView.frame = CGRectMake(0, (self.view.frame.origin.y +self.view.frame.size.height - 300), self.view.frame.size.width,  300);
//            
//            waitingBGView = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width  ,self.view.frame.size.height)];
//            
//            [waitingBGView setBackgroundColor:[UIColor blackColor]];
//            [waitingBGView setAlpha:0.6];
//            [self.view addSubview:waitingBGView];
//            [self.view bringSubviewToFront:_commonRateView];
//            
//        }];
    }
}

- (void)ViewOuterTap
{
    [UIView animateWithDuration:0.45 animations:^{
        
        _commonRateView.frame = CGRectMake(0, (self.view.frame.origin.y +self.view.frame.size.height +300), self.view.frame.size.width,  300);
        [waitingBGView removeFromSuperview];
        
    }];
    [self.view endEditing:YES];
}

-(IBAction)done:(id)sender
{
     [self.view endEditing:YES];
     [_doneBtn setHidden:YES];
}

-(IBAction)selectCard:(id)sender
{
    PaymentsViewController *wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentsViewController"];
    wallet.fromWhereStr = @"WALLET";
    wallet.delegate=self;
    [self presentViewController:wallet animated:YES completion:nil];
}

-(void)onChangePaymentMode:(NSDictionary *)choosedPayment
{
    strCardID=[choosedPayment valueForKey:@"card_id"] ;
    _cardLbl.text=[NSString stringWithFormat:@"XXXX-XXXX-XXXX-%@",[choosedPayment valueForKey:@"last_four"]];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer* )gestureRecognizer shouldReceiveTouch:(UITouch* )touch
{
    if ([touch.view isDescendantOfView:_commonRateView])
    {
        return NO;
    }
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField == _amountText)
    {
        int length = [self getLength:textField.text];
        if(length >= 4)
        {
            if(range.length == 0)
                return NO;
        }
        
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= 4 || returnKey;
    }
    else
    {
        return YES;
    }
}

- (int)getLength:(NSString*)mobileNumber
{
    @try
    {
        mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
        mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
        mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
        mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
        
        int length = (int)[mobileNumber length];
        
        return length;
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

- (void)addBottomBorderWithColor:(UIColor *)color andWidth:(CGFloat) borderWidth :(UIView *)view {
    CALayer *border = [CALayer layer];
    border.backgroundColor = color.CGColor;
    
    border.frame = CGRectMake(0, view.frame.size.height - borderWidth, view.frame.size.width, borderWidth);
    [view.layer addSublayer:border];
}

-(IBAction)addMoneyBtn:(id)sender
{
    if ([_enterAmtTxtfiedl.text isEqualToString:@""])
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:NSLocalizedString(@"AMOUNT_WALLET", nil) viewController:self okPop:NO];
    }
    else
    {
        
        NSDictionary *params = @{@"amount":_enterAmtTxtfiedl.text, @"card_id":strCardID};
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [appDelegate onStartLoader];
        
        [afn getDataFromPath:MD_WALLET WithType:POST_METHOD WithParameters:params WithCompletedSuccess:^(id response) {
             [appDelegate onEndLoader];
            [CommenMethods alertviewController_title:@"Success!" MessageAlert:[response valueForKey:@"message"]viewController:self okPop:NO];
            _enterAmtTxtfiedl.text = @"";
            NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
            NSString *currencyStr=[Utilities removeNullFromString: [user valueForKey:@"currency"]];
            
            NSDictionary *userDict = [response valueForKey:@"user"];
            _amountLbl.text = [NSString stringWithFormat:@"%@%@",currencyStr,[userDict valueForKey:@"wallet_balance"]];
            [UIView animateWithDuration:0.45 animations:^{
            
                _commonRateView.frame = CGRectMake(0, (self.view.frame.origin.y +self.view.frame.size.height +300), self.view.frame.size.width,  300);
                [waitingBGView removeFromSuperview];
                
            }];
            
        } OrValidationFailure:^(NSString *errorMessage) {
            [appDelegate onEndLoader];
            [CommenMethods alertviewController_title:@"" MessageAlert:errorMessage viewController:self okPop:NO];
        } OrErrorCode:^(NSString *error) {
            [appDelegate onEndLoader];
            [CommenMethods alertviewController_title:@"" MessageAlert:error viewController:self okPop:NO];
        } OrIntentet:^(NSString *internetFailure) {
            [appDelegate onEndLoader];
             [CommenMethods alertviewController_title:@"" MessageAlert:internetFailure viewController:self okPop:NO];
        }];
        
//        if ([appDelegate internetConnected])
//        {
//            NSDictionary *params = @{@"amount":_amountText.text, @"card_id":strCardID};
//            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
//            [appDelegate onStartLoader];
//            [afn getDataFromPath:MD_WALLET withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode)
//             {
//                 [appDelegate onEndLoader];
//                 if (response)
//                 {
//                     [CommenMethods alertviewController_title:@"Success!" MessageAlert:[response valueForKey:@"message"]viewController:self okPop:NO];
//                     
//                     NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
//                     NSString *currencyStr=[Utilities removeNullFromString: [user valueForKey:@"currency"]];
//                     
//                     NSDictionary *userDict = [response valueForKey:@"user"];
//                     _amountLbl.text = [NSString stringWithFormat:@"%@%@",currencyStr,[userDict valueForKey:@"wallet_balance"]];
//                     [UIView animateWithDuration:0.45 animations:^{
//                         
//                         _commonRateView.frame = CGRectMake(0, (self.view.frame.origin.y +self.view.frame.size.height +300), self.view.frame.size.width,  300);
//                         [waitingBGView removeFromSuperview];
//                         
//                     }];
//                 }
//                 else
//                 {
//                     if ([errorcode intValue]==1)
//                     {
//                         [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
//                     }
//                     else if ([errorcode intValue]==3)
//                     {
//                         [self logoutMethod];
////                         [CommenMethods onRefreshToken];
////                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
////                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
////                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
////                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
////                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
////                         
////                         PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
////                         [self.navigationController pushViewController:wallet animated:YES];
//                     }
//                     else if ([errorcode intValue]==2)
//                     {
//                         if ([error objectForKey:@"rating"]) {
//                             [CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"rating"] objectAtIndex:0]  viewController:self okPop:NO];
//                         }
//                         else if([error objectForKey:@"comments"]) {
//                             [CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"comments"] objectAtIndex:0]  viewController:self okPop:NO];
//                         }
//                         else if([error objectForKey:@"is_favorite"]) {
//                             [CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"is_favorite"] objectAtIndex:0]  viewController:self okPop:NO];
//                         }
//                         
//                     }
//                     
//                 }
//                 
//             }];
//        }
//        else
//        {
//            [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
//        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_amountText)
    {
        [_amountText resignFirstResponder];
    }
    else
    {
         [textField resignFirstResponder];
    }
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [CSS_Class APP_textfield_Infocus:textField];
    [_doneBtn setHidden:NO];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [CSS_Class APP_textfield_Outfocus:textField];
    return YES;
}


@end
