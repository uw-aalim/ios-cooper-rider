//
//  ActiveRideState.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

enum ActiveRideState: String {
	case none = "NONE"
	case searchingForRide = "SEARCHING"
	case started = "STARTED"
	case arrived = "ARRIVED"
	case pickedUp = "PICKEDUP"
	case dropped = "DROPPED"
	case completed = "COMPLETED"	
}
