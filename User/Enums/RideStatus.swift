//
//  RideStatus.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-14.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

/**
When there is an active ride this is the ride status
*/
enum RideStatus: String {
	
	case home = "HOME"
	
	case started = "STARTED"
	case arrived = "ARRIVED"
	case pickedUp = "PICKEDUP"
	case parked = "PARKED"
	case returning = "RETURNING"
	case reached = "REACHED"
}
