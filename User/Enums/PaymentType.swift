//
//  PaymentType.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-15.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

enum PaymentType: Int {
	case cash
	case card
}
