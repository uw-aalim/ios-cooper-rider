//
//  HomescreenState.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-14.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

enum HomescreenState: String {
	
	/**
	When the homescreen is not viewing any rides simply show the map.
	*/
	case none = "NONE"
	
	/**
	When setting up a new ride the homescreen state should be in planning ride which will allow the user to
	configure the type of vehicle they are being picked up by, schedule the time of pickup, and potentially
	show the payment configuraiton screen to setup the payment if no payment is setup.
	*/
	case planningRide = "PLANNINGRIDE"
	
	/**
	When the user is viewing an upcoming ride which is a ride which will not have an active car to track but
	instead will simply show a user the start and endpoints on the map. (May allow editing)
	*/
	case viewingUpcomingRide = "VIEWINGUPCOMINGRIDE"
	
	/**
	When the user is viewing an active ride which is a ride, which will have an active car to track and will
	also show a user the start and endpoints on the map and, if in the car, their current progress.
	*/
	case viewingActiveRide = "VIEWINGACTIVERIDE"
}
