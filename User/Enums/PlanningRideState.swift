//
//  PlanningRideState.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-14.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

enum PlanningRideState: String {
	
	case none = "NONE"
	case searchingLocations = "SEARCHING_LOCATIONS"
	case fareInfo = "FAREINFO"
	case schedule = "SCHEDULE"
	case setupPayment = "SETUPPAYMENT"
	case reviewAndFinalize = "REVIEW"
	case cancelPlan = "CANCEL"
	
	/**
	When planning a ride it is often the case that you will want to know which of the available states
	you can get to from the current state, this should ensure that the user never gets to a step
	without proceeding through the previous steps.
	*/
	func nextStates() -> [PlanningRideState] {
		switch self {
		case .none:
			return [.searchingLocations]
		case .searchingLocations:
			return [.fareInfo,.schedule,.cancelPlan]
		case .fareInfo:
			return [.schedule,.setupPayment,.reviewAndFinalize,.cancelPlan]
		case .schedule:
			return [.searchingLocations,.fareInfo]
		case .setupPayment:
			return [.fareInfo]
		case .reviewAndFinalize:
			return [.fareInfo,.cancelPlan]
		case .cancelPlan:
			return []
		}
	}
}
