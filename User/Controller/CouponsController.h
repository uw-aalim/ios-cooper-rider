//
//  CouponsController.h
//  User
//
//  Created by CSS on 06/10/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "customTextfield.h"
#import "AppDelegate.h"


@interface couponCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *validateLable;
@property (weak, nonatomic) IBOutlet UILabel *offreLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;

@end

@interface CouponsController : UIViewController <UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>{
    AppDelegate *appDelegate;

}

@property (weak, nonatomic) IBOutlet UIImageView *couponEmpty;
@property (weak, nonatomic) IBOutlet UILabel *navTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *addLbl;
@property (weak, nonatomic) IBOutlet UIButton *addCouponBtn;

@property (weak, nonatomic) IBOutlet UIView *noCouponView;
@property (weak, nonatomic) IBOutlet UITableView *couponTableview;
@property (weak, nonatomic) IBOutlet customTextfield *couponCodetxt;
@property (strong,nonatomic)NSMutableArray * CouponArray;
- (IBAction)AddCouponDidTab:(UIButton *)sender;
- (IBAction)backButtonAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *plusBtn;

@end
