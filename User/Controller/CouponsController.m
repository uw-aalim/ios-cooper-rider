//
//  CouponsController.m
//  User
//
//  Created by CSS on 06/10/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "CouponsController.h"
#import <Lottie/Lottie.h>

@implementation couponCell

-(void)awakeFromNib{
    
    [super awakeFromNib];
}

@end

@interface CouponsController ()

@end

@implementation CouponsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
 
    [self getPromoCode];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapReceived:)];
    [tapGestureRecognizer setDelegate:self];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self LocalizationUpdate];
    LOTAnimationView *animation = [LOTAnimationView animationNamed:@"wallet"];
    animation.frame = _couponEmpty.bounds;
    animation.loopAnimation = YES;
    animation.autoresizingMask = UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleLeftMargin |
    UIViewAutoresizingFlexibleBottomMargin;
    [_couponEmpty addSubview:animation];
    [animation playWithCompletion:^(BOOL animationFinished) {
        // Do Something
    }];
}
-(void)LocalizationUpdate{
    _navTitleLbl.text = LocalizedString(@"Promotions");
    _addLbl.text = LocalizedString(@"Add coupon code");
    [_addCouponBtn setTitle:LocalizedString(@"ADD COUPON") forState:UIControlStateNormal];
    _couponCodetxt.placeholder = LocalizedString(@"Enter Coupon code");
}
-(void)tapReceived:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.view endEditing:YES];
}

-(void)getPromoCode
{
    _CouponArray = [[NSMutableArray alloc]init];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [appDelegate onStartLoader];
    [afn getDataFromPath:@"api/user/promocodes" WithType:GET_METHOD WithParameters:nil WithCompletedSuccess:^(id response) {
        [appDelegate onEndLoader];
        if([response count] == 0){
            [_noCouponView setHidden:NO];
            [_couponTableview setHidden:YES];
            [self.view bringSubviewToFront:_noCouponView];
            
        }else{
            _CouponArray = [NSMutableArray arrayWithArray:response];
            _couponCodetxt.text =@"";
            [_plusBtn setHidden:NO];

            [_noCouponView setHidden:YES];
            [_couponTableview setHidden:NO];
            [self.view bringSubviewToFront:_couponTableview];

            [_couponTableview reloadData];
        }
        
    } OrValidationFailure:^(NSString *errorMessage) {
        [appDelegate onEndLoader];
        [CommenMethods alertviewController_title:@"" MessageAlert:errorMessage viewController:self okPop:NO];
    } OrErrorCode:^(NSString *error) {
        [appDelegate onEndLoader];
        [CommenMethods alertviewController_title:@"" MessageAlert:error viewController:self okPop:NO];
        
    } OrIntentet:^(NSString *internetFailure) {
        [appDelegate onEndLoader];
        [CommenMethods alertviewController_title:@"" MessageAlert:internetFailure viewController:self okPop:NO];
    }];
    
}
- (IBAction)addNewCoponView:(UIButton *)sender {
    
    if(_noCouponView.hidden){
        [_noCouponView setHidden:NO];
        [_plusBtn setHidden:YES];
        [_couponTableview setHidden:YES];
        [self.view bringSubviewToFront:_noCouponView];

    }else{
        [_noCouponView setHidden:YES];
        [_plusBtn setHidden:NO];
        [_couponTableview setHidden:NO];
        [self.view bringSubviewToFront:_couponTableview];
    }
}

- (IBAction)AddCouponDidTab:(UIButton *)sender {
    
    [self.view endEditing:YES];
    
    if(_couponCodetxt.text.length==0){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"PROMO_CODE") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        [appDelegate onStartLoader];
        NSDictionary * params= @{@"promocode":_couponCodetxt.text};
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn getDataFromPath:@"api/user/promocode/add" WithType:POST_METHOD WithParameters:params WithCompletedSuccess:^(id response) {
            
            [appDelegate onEndLoader];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[response valueForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                 {
                                 }];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
            
            if ([[response valueForKey:@"code"] isEqualToString:@"promocode_applied"])
            {
                _couponCodetxt.text = @"";
                [self getPromoCode];
            }
            
        } OrValidationFailure:^(NSString *errorMessage) {
            [appDelegate onEndLoader];
            [CommenMethods alertviewController_title:@"" MessageAlert:errorMessage viewController:self okPop:NO];
        } OrErrorCode:^(NSString *error) {
            [appDelegate onEndLoader];
            [CommenMethods alertviewController_title:@"" MessageAlert:error viewController:self okPop:NO];
        } OrIntentet:^(NSString *internetFailure) {
            [appDelegate onEndLoader];
            [CommenMethods alertviewController_title:@"" MessageAlert:internetFailure viewController:self okPop:NO];
        }];
    }

}

- (IBAction)backButtonAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _CouponArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    couponCell * Cell = [tableView dequeueReusableCellWithIdentifier:@"couponCell" forIndexPath:indexPath];
    if (Cell == nil)
    {
        Cell = [[couponCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"couponCell"];
    }
    [Cell setBackgroundColor:[UIColor clearColor]];
    
    Cell.nameLbl.text = [NSString stringWithFormat:@"%@", _CouponArray[indexPath.row][@"promocode"][@"promo_code"]];
    
    if([_CouponArray[indexPath.row][@"promocode"][@"discount_type"] isEqualToString:@"amount"])
    {
        Cell.offreLbl.text = [NSString stringWithFormat:@"%@ %@ OFF",[[NSUserDefaults standardUserDefaults] objectForKey:@"currency"], _CouponArray[indexPath.row][@"promocode"][@"discount"]];
    }
    else
    {
        Cell.offreLbl.text = [NSString stringWithFormat:@"%@ %@ OFF", _CouponArray[indexPath.row][@"promocode"][@"discount"],@"%"];
    }
    
    Cell.validateLable.text = [NSString stringWithFormat:@"Valid until %@",[CommenMethods DateConvertFomater:_CouponArray[indexPath.row][@"promocode"][@"expiration"]]];
    
    
//    [CSS_Class APP_fieldValue:Cell.nameLbl];
//    [CSS_Class APP_fieldValue:Cell.offreLbl];
//    [CSS_Class APP_fieldValue:Cell.validateLable];
    
    return Cell;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
