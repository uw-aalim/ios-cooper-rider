//
//  passbookTableViewCell.m
//  User
//
//  Created by MAC on 04/09/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "passbookTableViewCell.h"

@implementation passbookTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _paymentLbl.text = LocalizedString(@"Payment by");
    _amountLbl.text = LocalizedString(@"Amount");
    _statusLbl.text = LocalizedString(@"Status");
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
