//
//  CouponTableViewCell.m
//  User
//
//  Created by veena on 8/30/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "CouponTableViewCell.h"

@implementation CouponTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _codeNameLbl.text = LocalizedString(@"Code");
    _DiscoundNameLbl.text = LocalizedString(@"Discount:");

    _exxpireNameLbl.text = LocalizedString(@"Expires on:");

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
