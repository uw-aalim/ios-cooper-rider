//
//  CouponTableViewCell.h
//  User
//
//  Created by veena on 8/30/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CouponTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *DiscoundNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *codeNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *exxpireNameLbl;

@property (strong, nonatomic) IBOutlet UILabel *codeLbl;
@property (strong, nonatomic) IBOutlet UILabel *discountLbl;
@property (strong, nonatomic) IBOutlet UILabel *expiresLbl;
    
@end
