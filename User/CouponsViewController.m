//
//  CouponsViewController.m
//  User
//
//  Created by iCOMPUTERS on 18/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "CouponsViewController.h"
#import "CouponTableViewCell.h"
#import "CSS_Class.h"
#import "Colors.h"
#import "config.h"
#import "AFNHelper.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "CommenMethods.h"
#import "ViewController.h"

@interface CouponsViewController ()
{
    AppDelegate *appDelegate;
    NSMutableArray * discountArr;
    NSMutableArray * expireArr;
}

@end

@implementation CouponsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self setDesignStyles];
    [self getPromoCode];
}

#pragma View will appear
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.screenName = @"Coupon View";
    [self LocalizationUpdate];
}

-(void)LocalizationUpdate{
    _navicationTitleLbl.text = LocalizedString(@"Coupons");
    _couponText.placeholder = LocalizedString(@"Enter the coupon code");
    [_addBtn setTitle:LocalizedString(@"ADD COUPON") forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setDesignStyles
{
    [CSS_Class APP_Blackbutton:_addBtn];
    [CSS_Class APP_textfield_Outfocus:_couponText];
    [_listTableView setBackgroundColor:[UIColor clearColor]];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_couponText)
    {
        [_couponText resignFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField == _couponText)
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= 20 || returnKey;
    }
    else
    {
        return YES;
    }
    
    return NO;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [CSS_Class APP_textfield_Infocus:textField];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [CSS_Class APP_textfield_Outfocus:textField];
    return YES;
}

-(IBAction)Nextbtn:(id)sender
{
    [self.view endEditing:YES];
    
    if(_couponText.text.length==0)
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"PROMO_CODE") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        
        [appDelegate onStartLoader];
        NSDictionary * params= @{@"promocode":_couponText.text};
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn getDataFromPath:@"api/user/promocode/add" WithType:POST_METHOD WithParameters:params WithCompletedSuccess:^(id response) {
            
              [appDelegate onEndLoader];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[response valueForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                     {
                                                     }];
                    [alertController addAction:ok];
                    [self presentViewController:alertController animated:YES completion:nil];
            
                    if ([[response valueForKey:@"code"] isEqualToString:@"promocode_applied"])
                    {
                        [self getPromoCode];
                    }

        } OrValidationFailure:^(NSString *errorMessage) {
              [appDelegate onEndLoader];
            [CSS_Class alertviewController_title:@"" MessageAlert:errorMessage viewController:self okPop:NO];
        } OrErrorCode:^(NSString *error) {
            [appDelegate onEndLoader];
            [CSS_Class alertviewController_title:@"" MessageAlert:error viewController:self okPop:NO];
        } OrIntentet:^(NSString *internetFailure) {
            [appDelegate onEndLoader];
            [CSS_Class alertviewController_title:@"" MessageAlert:internetFailure viewController:self okPop:NO];
        }];

        
//        if ([appDelegate internetConnected])
//        {
//            [appDelegate onStartLoader];
//            NSDictionary * params= @{@"promocode":_couponText.text};
//            
//            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
//           [afn getDataFromPath:@"api/user/promocode/add" withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
//                [appDelegate onEndLoader];
//                if (response)
//                {
//                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[response valueForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
//                    UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
//                                         {
//                                         }];
//                    [alertController addAction:ok];
//                    [self presentViewController:alertController animated:YES completion:nil];
//                    
//                    if ([[response valueForKey:@"code"] isEqualToString:@"promocode_applied"])
//                    {
//                        [self getPromoCode];
//                    }
//                }
//                else
//                {
//                    if ([errorcode intValue]==1)
//                    {
//                        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
//                    }
//                    else if([errorcode  intValue]==3)
//                    {
//                        [self logoutMethod];
//                        
////                        [CommenMethods onRefreshToken];
//                        
////                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
////                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
////                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
////                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
////                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
////                        
////                        PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
////                        [self.navigationController pushViewController:wallet animated:YES];
//                    }
//                    else{
//                        if ([error objectForKey:@"promocode"]) {
//                            [CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"promocode"]objectAtIndex:0]  viewController:self okPop:NO];
//                        }
//                    }
//                    
//                }
//            }];
//        }
//        else
//        {
//            [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
//        }
    }
}

-(void)logoutMethod
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn refreshMethod_NoLoader:MD_REFRESH_TOKEN withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
                [user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
                [user setValue:response[@"refresh_token"] forKey:UD_REFERSH_TOKEN];
            }
            else
            {
                [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}


-(void)getPromoCode
{
    _CouponArray = [[NSMutableArray alloc]init];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [appDelegate onStartLoader];
    [afn getDataFromPath:@"api/user/promocodes" WithType:GET_METHOD WithParameters:nil WithCompletedSuccess:^(id response) {
         [appDelegate onEndLoader];
        if([response count] > 0){
            
        }else{
            _CouponArray = [NSMutableArray arrayWithArray:response];
            _couponText.text =@"";
            [_listTableView reloadData];
        }
        
    } OrValidationFailure:^(NSString *errorMessage) {
         [appDelegate onEndLoader];
        [CommenMethods alertviewController_title:@"" MessageAlert:errorMessage viewController:self okPop:NO];
    } OrErrorCode:^(NSString *error) {
         [appDelegate onEndLoader];
        [CommenMethods alertviewController_title:@"" MessageAlert:error viewController:self okPop:NO];
        
    } OrIntentet:^(NSString *internetFailure) {
         [appDelegate onEndLoader];
        [CommenMethods alertviewController_title:@"" MessageAlert:internetFailure viewController:self okPop:NO];
    }];
    
    

//    if ([appDelegate internetConnected])
//    {
//        _CouponArray = [[NSMutableArray alloc]init];
//        
//        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
//        [appDelegate onStartLoader];
//        [afn getDataFromPath:@"api/user/promocodes" withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
//            [appDelegate onEndLoader];
//            if (response)
//            {
//                if (response ==0)
//                {
//                    //No PromoCode Avaliable
//                }
//                else
//                {
////                    promoArray = [[NSMutableArray alloc]init];
////                    discountArr = [[NSMutableArray alloc]init];
////                    expireArr = [[NSMutableArray alloc]init];
//
//                    _CouponArray = [NSMutableArray arrayWithArray:response];
//                    
////                    for (int i=0; i<[response count]; i++)
////                    {
////                        NSDictionary *dict = [response objectAtIndex:i];
////                        NSDictionary *promoDict = [dict valueForKey:@"promocode"];
////                        NSString *str = [promoDict valueForKey:@"promo_code"];
////                        NSString *discountStr;
////                        if([dict[@"discount_type"] isEqualToString:@"amount"]){
////                            discountStr = [NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"currency"],[promoDict valueForKey:@"discount"]];
////
////                        }else{
////                            discountStr = [NSString stringWithFormat:@"%@ %@",[promoDict valueForKey:@"discount"],@"%"];
////                        }
////                        //NSString *discountStr = [promoDict valueForKey:@"discount"];
////                        NSString *expireStr = [promoDict valueForKey:@"expiration"];
////                        
////                        [promoArray addObject:str];
////                        [discountArr addObject:discountStr];
////                        [expireArr addObject:expireStr];
////                    }
//                    _couponText.text =@"";
//                    [_listTableView reloadData];
//                }
//            }
//            else
//            {
//                if ([errorcode intValue]==1)
//                {
//                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
//                }
//                else if ([errorcode intValue]==3)
//                {
////                    [CommenMethods onRefreshToken];
//                }
//                else
//                {
//                    [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
//                }
//            }
//            
//        }];
//    }
//    else
//    {
//        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
//    }
}


#pragma mark -- Table View Delegates Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_CouponArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    CouponTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[CouponTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    
    cell.codeLbl.text = [NSString stringWithFormat:@"%@", _CouponArray[indexPath.row][@"promocode"][@"promo_code"]];
    
    if([_CouponArray[indexPath.row][@"promocode"][@"discount_type"] isEqualToString:@"amount"])
    {
        cell.discountLbl.text = [NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"currency"], _CouponArray[indexPath.row][@"promocode"][@"discount"]];
    }
    else
    {
        cell.discountLbl.text = [NSString stringWithFormat:@"%@ %@", _CouponArray[indexPath.row][@"promocode"][@"discount"],@"%"];
    }
    
    cell.expiresLbl.text = [NSString stringWithFormat:@"%@", _CouponArray[indexPath.row][@"promocode"][@"expiration"]];
    
    
    [CSS_Class APP_fieldValue:cell.codeLbl];
    [CSS_Class APP_fieldValue:cell.discountLbl];
    [CSS_Class APP_fieldValue:cell.expiresLbl];

    return cell;
}


-(IBAction)backBtn:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
