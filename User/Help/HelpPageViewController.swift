//
//  HelpPageViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-10-26.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

@objc
class HelpPageViewController: UIViewController {

	//	MARK:- Properties
	
	@IBOutlet var topicTitleLabel: UILabel!
	@IBOutlet var topicDescriptionTextView: UITextView!
	
	@objc var navigationTitle: NSString = ""
	@objc var topicTitle: NSString = ""
	@objc var topicText: NSString = ""
	
	
	
	//	MARK:- Initialization
	@objc
	class func initController() -> HelpPageViewController {
		let storyboard = UIStoryboard(name: "Help", bundle: nil)
		let viewController = storyboard.instantiateViewController(withIdentifier: "HelpPageViewController") as! HelpPageViewController
		return viewController
	}
	
	
	
	//	MARK:- View Loading and Configuration
	
    override func viewDidLoad() {
        super.viewDidLoad()

		self.configureView()
		self.topicTitleLabel.text = self.topicTitle as String
		self.topicDescriptionTextView.text = self.topicText as String
    }
	
	func configureView() {
		
		if #available(iOS 11.0, *) {
			self.navigationController?.navigationBar.prefersLargeTitles = true
			self.navigationItem.largeTitleDisplayMode = .always
		}
		
		self.navigationItem.title = self.navigationTitle as String
		
		self.navigationController?.navigationBar.isTranslucent = false
		self.navigationController?.navigationBar.barTintColor = .black
		self.navigationController?.navigationBar.barStyle = .blackOpaque
		self.navigationController?.navigationBar.backgroundColor = .black
		self.navigationController?.navigationBar.tintColor = .white
	}
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
