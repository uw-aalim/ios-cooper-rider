//
//  HelpViewController.h
//  User
//
//  Created by iCOMPUTERS on 21/06/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"
#import "GAITrackedViewController.h"

#ifndef HelpViewController_h
#define HelpViewController_h

@interface HelpViewController :GAITrackedViewController <MFMailComposeViewControllerDelegate> {
    NSString *strProviderCell, *mailAddress;
    AppDelegate *appDelegate;
}

+(instancetype)initController;



@end
#endif
