//
//  HelpViewController.m
//  User
//
//  Created by iCOMPUTERS on 21/06/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "HelpViewController.h"
#import "config.h"
#import "CSS_Class.h"
#import "Colors.h"
#import "HomeViewController.h"
#import "Constants.h"
#import "ViewController.h"
#import "Utilities.h"
#import "User-Swift.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

+(instancetype)initController {
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Help" bundle: nil];
	HelpViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
	return viewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self->appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
	[self setupViewDesign];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

}

-(void)setupViewDesign {
	
	if (@available(iOS 11.0, *)) {
		[self navigationController].navigationBar.prefersLargeTitles = TRUE;
		[self navigationItem].largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAlways;
	}
	
	[[self navigationController].navigationBar setTranslucent:FALSE];
	[self navigationController].navigationBar.barTintColor = [UIColor blackColor];
	[self navigationController].navigationBar.barStyle = UIBarStyleBlackOpaque;
	[self navigationController].navigationBar.backgroundColor = [UIColor blackColor];
	[self navigationController].navigationBar.tintColor = [UIColor whiteColor];
	
	if (self.navigationController.viewControllers.count <= 1) {
		NSString *backCloseString = @"close_x_icon";
		UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]
										initWithImage:[UIImage imageNamed:backCloseString]
										style:UIBarButtonItemStylePlain
										target:self
										action:@selector(backBtn:)];
		[self navigationItem].leftBarButtonItem = closeButton;
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backBtn:(id)sender {
	if (self.navigationController.viewControllers.count <= 1) {
		[self.navigationController.presentingViewController dismissViewControllerAnimated:true completion:NULL];
	}
	else {
    	[self.navigationController popViewControllerAnimated:YES];
	}
}


-(IBAction)tripFarePressed:(id)sender {
	NSString *topicTitle = @"Understaning Trips and Fares in CoOper";
	NSString *topixText = @"Some text will go here";
	NSString *navTitle = @"Trip and fare review";
	
	HelpPageViewController *viewController = [HelpPageViewController initController];
	viewController.topicTitle = topicTitle;
	viewController.topicText = topixText;
	viewController.navigationTitle = navTitle;
	[self.navigationController pushViewController:viewController animated:TRUE];
}


-(IBAction)accountPaymentPressed:(id)sender {
	NSString *topicTitle = @"Creating an Account and Making Payments in CoOper";
	NSString *topixText = @"Some text will go here";
	NSString *navTitle = @"Account and payments";
	
	HelpPageViewController *viewController = [HelpPageViewController initController];
	viewController.topicTitle = topicTitle;
	viewController.topicText = topixText;
	viewController.navigationTitle = navTitle;
	[self.navigationController pushViewController:viewController animated:TRUE];
}

-(IBAction)guidePressed:(id)sender {
	
	NSString *topicTitle = @"Using CoOper promotions, coupons, and credits";
	NSString *topixText = @"CoOper offers special promotions to provide discounts on rids to riders. To apply a promotional discount to a trip’s fare, enter the promo code anytime before your trip begins.\n\nValid promo codes are automatically applied in revers order. You’ll be unable to select your preferred promo code when you add more than one. Instead, your most recently added promo code will be applied to your current or next trip. A promo code’s value will only apply to one trip. ";
	NSString *navTitle = @"A Guide to CoOper";
	
	HelpPageViewController *viewController = [HelpPageViewController initController];
	viewController.topicTitle = topicTitle;
	viewController.topicText = topixText;
	viewController.navigationTitle = navTitle;
	[self.navigationController pushViewController:viewController animated:TRUE];
	
}

-(IBAction)signingUpPressed:(id)sender {
	NSString *topicTitle = @"What You Get When Signing Up to CoOper";
	NSString *topixText = @"Some text will go here";
	NSString *navTitle = @"Signing Up";
	
	HelpPageViewController *viewController = [HelpPageViewController initController];
	viewController.topicTitle = topicTitle;
	viewController.topicText = topixText;
	viewController.navigationTitle = navTitle;
	[self.navigationController pushViewController:viewController animated:TRUE];
}

-(IBAction)accessibilityPressed:(id)sender {
	NSString *topicTitle = @"How CoOper is Making Ride Sharing Accessibile";
	NSString *topixText = @"Some text will go here";
	NSString *navTitle = @"Accessibility";
	
	HelpPageViewController *viewController = [HelpPageViewController initController];
	viewController.topicTitle = topicTitle;
	viewController.topicText = topixText;
	viewController.navigationTitle = navTitle;
	[self.navigationController pushViewController:viewController animated:TRUE];
}

-(IBAction)morePressed:(id)sender {
	NSString *topicTitle = @"More CoOper Information";
	NSString *topixText = @"Some text will go here";
	NSString *navTitle = @"More Help";
	
	HelpPageViewController *viewController = [HelpPageViewController initController];
	viewController.topicTitle = topicTitle;
	viewController.topicText = topixText;
	viewController.navigationTitle = navTitle;
	[self.navigationController pushViewController:viewController animated:TRUE];
}


/*
-(IBAction)browserBtn:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:SERVICE_URL]];
}*/


-(void)logoutMethod
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn refreshMethod_NoLoader:MD_REFRESH_TOKEN withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
                [user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
                [user setValue:response[@"refresh_token"] forKey:UD_REFERSH_TOKEN];
            }
            else
            {
                [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}



@end
