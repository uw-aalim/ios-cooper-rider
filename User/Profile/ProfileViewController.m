//
//  ProfileViewController.m
//  Provider
//
//  Created by iCOMPUTERS on 17/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "ProfileViewController.h"
#import "CSS_Class.h"
#import "config.h"
#import "Colors.h"
#import "UIScrollView+EKKeyboardAvoiding.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "CommenMethods.h"
#import "ForgotPasswordViewController.h"
#import "Utilities.h"
#import "ViewController.h"
#import "ChangePasswordViewController.h"
#import "CoOperNotificationStrings.h"

@interface ProfileViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate> {
    AppDelegate *appDelegate;
}

@end

@implementation ProfileViewController

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}

+(instancetype)initController {
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Profile" bundle: nil];
	//UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileNavigationController"];
	ProfileViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
	return viewController;
}

//	MARK:- View Loading, Appearance and Dissapearance
- (void)viewDidLoad {
    [super viewDidLoad];
	
	self->appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	[self setupViewDesign];
    [self onGetProfile];
	
	
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
        
    self.screenName = @"profile";
    [self Localizationupdate];
}

-(void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(keyboardWillShow:)
	 name:UIKeyboardWillShowNotification
	 object:NULL];
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(keyboardWillShow:)
	 name:UIKeyboardWillChangeFrameNotification
	 object:NULL];
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(keyboardWillHide:)
	 name:UIKeyboardWillHideNotification
	 object:NULL];
}

-(void) viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	
	[[NSNotificationCenter defaultCenter]
	 removeObserver:self];
}

-(void)Localizationupdate{
	
	/*
    _firstNameLb.text = LocalizedString(@"First");
    _phoneLb.text = LocalizedString(@"Phone Number");
    _lastNameLb.text = LocalizedString(@"Last");
    _navicationTitleLbl.text = LocalizedString(@"Edit profile");
    _emailtLb.text = LocalizedString(@"E-mail");
    [_saveBtn setTitle:LocalizedString(@"SAVE") forState:UIControlStateNormal];
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"SocialLogin"] isEqualToString:@"User"]){
        _changePassBtn.hidden = NO;
    }else{
        _changePassBtn.hidden = YES;

    }
    [_changePassBtn setTitle:LocalizedString(@"Change Password") forState:UIControlStateNormal];
	 */
}

//	MARK:- Remote Data Loading

-(void)onGetProfile
{
     if ([appDelegate internetConnected])
    {
		NSLog(@"ProfileViewController - onGetProfile");
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [appDelegate onStartLoader];
        [afn getDataFromPath:MD_GETPROFILE withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
			[self->appDelegate onEndLoader];
            if (response)
            {
                NSLog(@"PROFILE RES...%@", response);
                
				self->_firstNameText.text=  [Utilities removeNullFromString: response[@"first_name"]];
                self->_lastNameText.text= [Utilities removeNullFromString: response[@"last_name"]];
                self->_phoneText.text= [Utilities removeNullFromString: response[@"mobile"]];
                self->_emailText.text= [Utilities removeNullFromString: response[@"email"]];
            
                    NSString *socialIdStr = [Utilities removeNullFromString:[response valueForKey:@"social_unique_id"]];
                    
                    NSString *strProfile=[Utilities removeNullFromString:response[@"picture"]];
                    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                    [user setValue:strProfile forKey:UD_PROFILE_IMG];
                
					NSString *nameStr = [NSString stringWithFormat:@"%@ %@", self->_firstNameText.text, self->_lastNameText.text];
                
                    [user setValue:nameStr forKey:UD_PROFILE_NAME];
                    [user setValue:socialIdStr forKey:UD_SOCIAL];
                    [user setValue:[response valueForKey:@"id"] forKey:UD_ID];
                    [user setValue:[response valueForKey:@"sos"] forKey:UD_SOS];
				
				
                if (![strProfile isEqualToString:@""]) {
                    
                    NSString *strSub = [strProfile stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                    NSURL *imgUrl;
                    
                    if ([strProfile containsString:@"http"]) {
                        imgUrl = [NSURL URLWithString:strProfile];
                    }
                    else {
                        imgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/storage/%@", SERVICE_URL, strSub]];
                    }
					
					[user setValue:[imgUrl absoluteString] forKey:UD_PROFILE_IMG];
					
					
                    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                    dispatch_async(q, ^{
                        /* Fetch the image from the server... */
                        NSData *data = [NSData dataWithContentsOfURL:imgUrl];
                        UIImage *img = [[UIImage alloc] initWithData:data];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
							[self->appDelegate onEndLoader];
                            //                            [_btnProfilePic setBackgroundImage:img forState:UIControlStateNormal];
							if (img != NULL) {
								[self->_profileImg setImage:img];
							}
                        });
                    });
                }
				
				[[NSNotificationCenter defaultCenter]
				 postNotificationName:LoginRegisterProcessSuccess
				 object:NULL];
            }
            else
            {
                if ([errorcode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                    [self logoutMethod];
                    
//                    [CommenMethods onRefreshToken];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
//                    
//                    PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
//                    [self.navigationController pushViewController:wallet animated:YES];
                }
            }
            
        }];
    }
    else
    {
         [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
    
    
}

-(void)logoutMethod
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn refreshMethod_NoLoader:MD_REFRESH_TOKEN withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
                [user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
                [user setValue:response[@"refresh_token"] forKey:UD_REFERSH_TOKEN];
            }
            else
            {
                [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//	MARK:- View Design

-(void)setupViewDesign {
	if (@available(iOS 11.0, *)) {
		[self navigationController].navigationBar.prefersLargeTitles = TRUE;
		[self navigationItem].largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAlways;
	}
	
	[[self navigationController].navigationBar setTranslucent:FALSE];
	[self navigationController].navigationBar.barTintColor = [UIColor blackColor];
	[self navigationController].navigationBar.barStyle = UIBarStyleBlackOpaque;
	[self navigationController].navigationBar.backgroundColor = [UIColor blackColor];
	[self navigationController].navigationBar.tintColor = [UIColor whiteColor];
	
	
	if (self.navigationController.viewControllers.count <= 1) {
		NSString *backCloseString = @"back_button";
		backCloseString = @"close_x_icon";
		UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]
										initWithImage:[UIImage imageNamed:backCloseString]
										style:UIBarButtonItemStylePlain
										target:self
										action:@selector(backBtn:)];
		[self navigationItem].leftBarButtonItem = closeButton;
	}
	
	self.profileOutline.layer.borderColor = UIColor.blackColor.CGColor;
	self.profileOutline.layer.borderWidth = 1.0;
	//[self navigationController].navigationBar title
	
	/*
    [CSS_Class APP_labelName:_firstNameLb];
    [CSS_Class APP_labelName:_lastNameLb];
    [CSS_Class APP_labelName:_phoneLb];
    [CSS_Class APP_labelName:_carNumLb];
    [CSS_Class APP_labelName:_carNameLb];
    [CSS_Class APP_labelName:_emailtLb];
    [CSS_Class APP_fieldValue:_emailValueLb];
    
//    [CSS_Class APP_textfield_Outfocus:_carNumText];
//    [CSS_Class APP_textfield_Outfocus:_firstNameText];
//    [CSS_Class APP_textfield_Outfocus:_lastNameText];
//    [CSS_Class APP_textfield_Outfocus:_phoneText];
//    [CSS_Class APP_textfield_Outfocus:_carNameText];
    
    [CSS_Class APP_Blackbutton:_saveBtn];
    [CSS_Class APP_Blackbutton:_changePassBtn];
    
    _profileImg.layer.cornerRadius = _profileImg.frame.size.height/2;
    _profileImg.clipsToBounds = YES;
	 */
}


//	MARK:- UITextFieldDelegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField==_firstNameText) {
        [_lastNameText becomeFirstResponder];
    }
    else if(textField==_lastNameText) {
        [_phoneText becomeFirstResponder];
    }
    else if(textField==_phoneText) {
        [_emailText becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if((textField == _firstNameText) || (textField == _lastNameText)) {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= INPUTLENGTH || returnKey;
    }
    else if (textField == _phoneText) {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= PHONELENGTH || returnKey;
    }
    else {
        return YES;
    }
    
    return NO;
}

//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    [CSS_Class APP_textfield_Infocus:textField];
//    return YES;
//}
//
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    [CSS_Class APP_textfield_Outfocus:textField];
//    return YES;
//}

//	MARK:- Action Responders

-(void)backBtn:(id)sender {
	if (self.navigationController.viewControllers.count <= 1) {
		[self.navigationController.presentingViewController dismissViewControllerAnimated:true completion:NULL];
	}
	else {
		[self.navigationController popViewControllerAnimated:YES];
	}
}

-(IBAction)saveBtn:(id)sender {
        [self.view endEditing:YES];
	
	//	MARK: FIX THIS VALIDATION STEP - MORE LIKE REGISTER VIEW CONTROLLER
	if ([_firstNameText.text isEqualToString:@""] || [_lastNameText.text isEqualToString:@""] || [_phoneText.text isEqualToString:@""] || [_emailText.text isEqualToString:@""] ) {
		UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:NSLocalizedString(@"VALIDATE", nil) preferredStyle:UIAlertControllerStyleAlert];
		UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
		[alertController addAction:ok];
		[self presentViewController:alertController animated:YES completion:nil];
	}
	else {
		
		if ([appDelegate internetConnected]) {
			NSLog(@"ProfileViewController - saveButtonPressed");
			[appDelegate onStartLoader];
			NSMutableDictionary * params= [[NSMutableDictionary alloc]initWithDictionary:
										   @{@"email":_emailText.text,
											 @"first_name":_firstNameText.text,
											 @"last_name":_lastNameText.text,
											 @"mobile":_phoneText.text}];
			
			//                UIImage *imag=_btnProfilePic.currentBackgroundImage;
			
			UIImage *imag = _profileImg.image;
			
			AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
			
			NSLog(@"%@",params);
			[afn getDataFromPath:MD_UPDATEPROFILE withParamDataImage:params andImage:imag withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
				NSLog(@"Update profile with image %@, response %@, error dict %@, and error code %@",imag,response,error,errorcode);
				//NSLog(@"Update profile with image %@",response);
				[self->appDelegate onEndLoader];
				if (response) {
					[CommenMethods alertviewController_title:@"Success!" MessageAlert:@"Profile Updated." viewController:self okPop:NO];
					
					[self onGetProfile];
				}
				else {
					if ([errorcode intValue]==1) {
						
						
						[CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
					}
					else if([errorcode  intValue]==3) {
						//                            [CommenMethods onRefreshToken];
						[self logoutMethod];
						
						//                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
						//                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
						//                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
						//                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
						//                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
						//
						//                            PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
						//                            [self.navigationController pushViewController:wallet animated:YES];
					}
					else {
						if ([error objectForKey:@"email"]) {
							[CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
						}
						else if ([error objectForKey:@"first_name"]) {
							[CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"first_name"] objectAtIndex:0]  viewController:self okPop:NO];
						}
						else if ([error objectForKey:@"last_name"]) {
							[CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"last_name"] objectAtIndex:0]  viewController:self okPop:NO];
						}
						else if ([error objectForKey:@"mobile"]) {
							[CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"mobile"] objectAtIndex:0]  viewController:self okPop:NO];
						}
						else if ([error objectForKey:@"picture"]) {
							[CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"picture"] objectAtIndex:0]  viewController:self okPop:NO];
						}
					}
				}
			}];
		}
		else {
			[CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
		}
	}
}
    
- (IBAction)onChangePwd:(id)sender {
		ChangePasswordViewController *controller = [ChangePasswordViewController initController];
        [self.navigationController pushViewController:controller animated:YES];
}


-(IBAction)onProfilePic:(id)sender {
	UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}



//	MARK:- ImagePickerDelegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
//    [_btnProfilePic setBackgroundImage:image forState:UIControlStateNormal];
    
    [_profileImg setImage:[self resizeImage:image]];
    
    [picker dismissViewControllerAnimated:true completion:nil];
}

-(UIImage *)resizeImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 300.0;
    float maxWidth = 400.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


//	MARK:- Keyboard Notifications

-(void) keyboardWillShow: (NSNotification *) notification {
	CGRect keyboardFrame = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	double keyboardDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	
	self.scrollViewBottom.constant = keyboardFrame.size.height;
	NSLog(@"Setting scroll view bottom constraint %f",keyboardFrame.size.height);
	[self.view setNeedsLayout];
	[UIView animateWithDuration:keyboardDuration animations:^{
		[self.view layoutIfNeeded];
	}];
}




-(void) keyboardWillHide: (NSNotification *) notification {
	
	double keyboardDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	self.scrollViewBottom.constant = 0.0;
	[self.view setNeedsLayout];
	[UIView animateWithDuration:keyboardDuration animations:^{
		[self.view layoutIfNeeded];
	}];
}

@end
