//
//  ProfileViewController.h
//  Provider
//
//  Created by iCOMPUTERS on 17/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageController.h"

#ifndef ProfileViewController_h
#define ProfileViewController_h
@interface ProfileViewController : GAITrackedViewController <UIGestureRecognizerDelegate>

//@property (weak, nonatomic) IBOutlet UILabel * titleBarLabel;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint * largeTitleViewTop;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottom;

@property (weak, nonatomic) IBOutlet UIView *profileOutline;

@property (weak, nonatomic) IBOutlet UIImageView *profileImg;

@property (weak, nonatomic) IBOutlet UITextField *firstNameText;
@property (weak, nonatomic) IBOutlet UITextField *lastNameText;
@property (weak, nonatomic) IBOutlet UITextField *phoneText;
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;

@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

-(void) backBtn: (id) sender;
-(IBAction) saveBtn: (id) sender;
-(IBAction) onChangePwd: (id) sender;
-(IBAction) onProfilePic: (id) sender;

+(instancetype)initController;

@end
#endif

