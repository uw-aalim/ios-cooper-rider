//
//  ChangePasswordViewController.m
//  Provider
//
//  Created by iCOMPUTERS on 01/02/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "EmailViewController.h"
#import "CSS_Class.h"
#import "config.h"
#import "AFNHelper.h"
#import "ViewController.h"
#import "Constants.h"
#import "CommenMethods.h"
#import "User-Swift.h"
@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

+(instancetype)initController {
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ChangePassword" bundle: nil];
	ChangePasswordViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
	return viewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setDesignStyles];
     appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
	
	self.scrollView.delegate = self;
	
	if (@available(iOS 11.0, *)) {
		[self navigationController].navigationBar.prefersLargeTitles = TRUE;
		[self navigationItem].largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAlways;
	}
	self.navigationItem.title = @"Change Password";
	
    //UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapReceived:)];
    //[tapGestureRecognizer setDelegate:self];
    //[self.view addGestureRecognizer:tapGestureRecognizer];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.screenName = @"Change password";
    [self LocalizationUpdate];
}

-(void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillChangeFrame:)
												 name:UIKeyboardWillShowNotification
											   object:NULL];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillChangeFrame:)
												 name:UIKeyboardWillChangeFrameNotification
											   object:NULL];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillHide:)
												 name:UIKeyboardWillHideNotification
											   object:NULL];
}

-(void) viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)LocalizationUpdate{
    /*_titleLbl.text = LocalizedString(@"Enter the Password");
    _oldPassLbl.text = LocalizedString(@"Current Password");
    _passLbl.text = LocalizedString(@"New Password");
    _confirmPassLbl.text = LocalizedString(@"Confirm New Password");
    [_changePasswordBtn setTitle:LocalizedString(@"CHANGE PASSWORD") forState:UIControlStateNormal];
    */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tapReceived:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.view endEditing:YES];
}

-(void)setDesignStyles
{
	/*
    [CSS_Class APP_labelName:_helpLbl];
    [CSS_Class APP_textfield_Outfocus:_passText];
    [CSS_Class APP_textfield_Outfocus:_confirmPassText];
    [CSS_Class APP_textfield_Outfocus:_oldPassText];
    
    [CSS_Class APP_labelName_Small:_oldPassLbl];
    [CSS_Class APP_labelName_Small:_passLbl];
    [CSS_Class APP_labelName_Small:_confirmPassLbl];
    [CSS_Class APP_Blackbutton:_changePasswordBtn];*/
}

-(IBAction)backBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_oldPassText)
    {
        [_passText becomeFirstResponder];
    }
    if(textField==_passText)
    {
        [_confirmPassText becomeFirstResponder];
    }
    else if(textField==_confirmPassText)
    {
        [_confirmPassText resignFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if((textField == _passText) || (textField == _oldPassText) || (textField == _confirmPassText))
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= PASSWORDLENGTH || returnKey;
    }
    else
    {
        return YES;
    }
    
    return NO;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [CSS_Class APP_textfield_Infocus:textField];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [CSS_Class APP_textfield_Outfocus:textField];
    return YES;
}

-(IBAction)Nextbtn:(id)sender
{
    [self.view endEditing:YES];
    
//    if((_passText.text.length==0) || (_confirmPassText.text.length==0) || (_oldPassText.text.length==0))
//    {
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"PWD_REQ") preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
//        [alertController addAction:ok];
//        [self presentViewController:alertController animated:YES completion:nil];
//    }
//    else if(![_passText.text isEqualToString:_confirmPassText.text])
//    {
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"MATCH_PWD") preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
//        [alertController addAction:ok];
//        [self presentViewController:alertController animated:YES completion:nil];
//        
//    }
//    else
//    {
    
    if([self ChangePasswordValidation:_passText.text :_confirmPassText.text]){
        
        NSDictionary*params;
            params=@{@"password":_passText.text, @"password_confirmation":_confirmPassText.text, @"old_password":_oldPassText.text};
            [appDelegate onStartLoader];
        
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:MD_CHANGEPASSWORD WithType:POST_METHOD WithParameters:params WithCompletedSuccess:^(id response) {
             [appDelegate onEndLoader];
            NSLog(@"RESPONSE ...%@", response);
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Success!") message:LocalizedString(@"Your password changed successfully.") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
                [self afterSucccessPasswordChange];
            }];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
            
        } OrValidationFailure:^(NSString *errorMessage) {
            [appDelegate onEndLoader];
            [CSS_Class alertviewController_title:@"" MessageAlert:errorMessage viewController:self okPop:NO];
            
        } OrErrorCode:^(NSString *error) {
            [appDelegate onEndLoader];
            [CSS_Class alertviewController_title:@"" MessageAlert:error viewController:self okPop:NO];
        } OrIntentet:^(NSString *internetFailure) {
            [appDelegate onEndLoader];
            [CSS_Class alertviewController_title:@"" MessageAlert:internetFailure viewController:self okPop:NO];
        }];
        
//        if([appDelegate internetConnected])
//        {
//            NSDictionary*params;
//            params=@{@"password":_passText.text, @"password_confirmation":_confirmPassText.text, @"old_password":_oldPassText.text};
//            [appDelegate onStartLoader];
//            
//            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
//            [afn getDataFromPath:MD_CHANGEPASSWORD  withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode)
//             {
//                 [appDelegate onEndLoader];
//
//                 if (response)
//                 {
//                     NSLog(@"RESPONSE ...%@", response);
//                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success!" message:NSLocalizedString(@"PWD_CHD", nil)preferredStyle:UIAlertControllerStyleAlert];
//                     UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                         
//                         [self backBtn:self];
//                     }];
//                     [alertController addAction:ok];
//                     [self presentViewController:alertController animated:YES completion:nil];
//                 }
//                 else
//                 {
//                     if ([errorcode intValue]==1)
//                     {
//                         [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
//                     }
//                     else if([errorcode  intValue]==3)
//                     {
////                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
////                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
////                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
////                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
////                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
////                         
////                         ViewController *logout = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
////                         [self.navigationController pushViewController:logout animated:YES];
//                         
//                         [self logoutMethod];
//                     }
//                     else{
//                         if ([error objectForKey:@"old_password"]) {
//                             [CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"old_password"] objectAtIndex:0]  viewController:self okPop:NO];
//                         }
//                         else if ([error objectForKey:@"password"]) {
//                             [CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"password"] objectAtIndex:0]  viewController:self okPop:NO];
//                         }
//                         else if ([error objectForKey:@"password_confirmation"]) {
//                             [CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"password_confirmation"] objectAtIndex:0]  viewController:self okPop:NO];
//                         }
//                         
//                     }
//                 }
//             }];
//        }
//        else
//        {
//            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:LocalizedString(@"CONNECTION")preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
//            [alertController addAction:ok];
//            [self presentViewController:alertController animated:YES completion:nil];
//        }
    }
}

-(void)afterSucccessPasswordChange{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        //PageViewController *controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];
		[self.navigationController popViewControllerAnimated:YES];//:controller animated:YES];
    });
}
-(BOOL)ChangePasswordValidation :(NSString *)password :(NSString *)confirmpassword{
    
    if([self Passwordvalidaton:_oldPassText.text :LocalizedString(@"The Old password must be minimum 8 characters and 1 Uppercase, 1 Lowercase , 1 number , and 1 special characters and maximum 16 characters") ]){
        if([self Passwordvalidaton:password :LocalizedString(@"The password must be minimum 8 characters and 1 Uppercase, 1 Lowercase , 1 number , and 1 special characters and maximum 16 characters ")]){
            if([self Passwordvalidaton:confirmpassword :LocalizedString(@"The confirm password must be minimum 8 characters and 1 Uppercase, 1 Lowercase , 1 number , and 1 special characters and maximum 16 characters ") ]){
                
                if([self ConfirmPasswordValidation:password withConfirmPassword:confirmpassword]){
                    if(![_oldPassText.text isEqualToString:password]){
                        return  YES;

                    }else{
                        [CommenMethods alertviewController_title:@"" MessageAlert:@"Old password and confirm password same" viewController:self okPop:NO];
                    }
                }
            }
        }
    }
    return  NO;
}
-(BOOL)Passwordvalidaton:(NSString *)password :(NSString * )alert{
    
    if(password.length != 0){
        NSString *stricterFilterString = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,15}$";
        NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
        if( [passwordTest evaluateWithObject:password]){
            
            return true;
        }else{
            
            [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:alert viewController:self okPop:NO];
        }
        
    }else{
        [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:LocalizedString(@"Password is required") viewController:self okPop:NO];
    }
    return false;
}


-(BOOL)ConfirmPasswordValidation :(NSString *)password withConfirmPassword :(NSString *)ConfirmPassword{
    if([password isEqualToString:ConfirmPassword]){
        return YES;
    }else{
        [CSS_Class alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"New password and confirm password must be same") viewController:self okPop:NO];
    }
    return NO;
}

-(void)logoutMethod
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn refreshMethod_NoLoader:MD_REFRESH_TOKEN withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
                [user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
                [user setValue:response[@"refresh_token"] forKey:UD_REFERSH_TOKEN];
            }
            else
            {
                [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}



//	============================================================================================================
//	MARK:- Keyboard Responders
//	============================================================================================================

-(void) keyboardWillChangeFrame: (NSNotification *) notification {
	
	CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
	double duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	
	self.scrollViewBottom.constant = keyboardFrame.size.height;
	
	[UIView animateWithDuration:duration animations:^{
		[[self view] layoutIfNeeded];
	} completion:^(BOOL finished) {
		//Finishd keyboard animateion
	}];
}

-(void) keyboardWillHide: (NSNotification *) notification {
	
	double duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	
	self.scrollViewBottom.constant = 0;
	
	[UIView animateWithDuration:duration animations:^{
		[[self view] layoutIfNeeded];
	} completion:^(BOOL finished) {
		//Finishd keyboard animateion
	}];
}





//	============================================================================================================
//	MARK:- Scroll View Delegate
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
	
	
}
//	------------------------------------------------------------------------------------------------------------


@end
