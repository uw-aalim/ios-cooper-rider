//
//  SocialMediaViewController+SocialLoginNotifications.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-14.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension SocialMediaViewController2 {
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func loginRegistrationFinishedSuccess(_ notification: Notification) {
		DispatchQueue.main.async {
			self.navigationController?.presentingViewController?.dismiss(
				animated: true,
				completion: {
					//	May need to perform some login related functionality here
			})
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func loginRegistrationFinishedFailed(_ notification: Notification) {
		DispatchQueue.main.async {
			var error = "There was an error during the registration process."
			if let userInfo = notification.userInfo,
				let errorMessage = userInfo["ERRORMSG"] as? String {
				
				error = errorMessage
			}
			self.showError(
				withTitle: "Registration Error",
				andMessage: error)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
