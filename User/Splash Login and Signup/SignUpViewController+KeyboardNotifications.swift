//
//  SignUpViewController+KeyboardNotifications.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-07.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension SignUpViewController {
	
	//	============================================================================================================
	//	MARK:- Keyboard Notification Responders
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func keyboardWillShowOrChangeFrame(
		_ notification: Notification) {
		
		let info = notification.userInfo!
		let keyboardFrame = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
		var animationDuration = (info[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
		
		self.scrollViewBottom.constant = keyboardFrame.size.height
		if animationDuration == nil {
			animationDuration = 0.2
		}
		self.view.setNeedsLayout()
		
		//print("Before keyboard animation \(self.scrollContainerView.frame)")
		//print("Before keyboard animation \(self.scrollView.frame)")
		
		UIView.animate(
			withDuration: animationDuration!,
			animations: {
				self.view.layoutIfNeeded()
		}, completion: { (completed: Bool) in
			//print("Finished keyboard animation \(self.scrollContainerView.frame)")
			//print("Finished keyboard animation \(self.scrollView.frame)")
		})
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func keyboardWillHide(
		_ notification: Notification) {
		
		let info = notification.userInfo!
		var animationDuration = (info[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
		
		self.scrollViewBottom.constant = 0.0
		if animationDuration == nil {
			animationDuration = 0.2
		}
		self.view.setNeedsLayout()
		
		UIView.animate(
			withDuration: animationDuration!) {
				self.view.layoutIfNeeded()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
