//
//  LoginViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-08.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Constants
	//	============================================================================================================
	
	let termsURLString = ""
	let privacyURLString = ""
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	@IBOutlet weak var titleBarLabel: UILabel!
	@IBOutlet weak var largeTitleViewTop: NSLayoutConstraint!
	
	//	The view which contains the scroll view
	@IBOutlet weak var scrollContainerView: UIView!
	//	The scroll view
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var scrollViewBottom: NSLayoutConstraint!
	
	@IBOutlet weak var travelSplashImageView: UIImageView!
	
	//	============================================================================================================
	//	MARK: Text Fields for user signup
	//	============================================================================================================
	
	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	
	@IBOutlet weak var termsAndPrivacyTextView: UITextView!
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Controller Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	class func initController() -> LoginViewController {
		return LoginViewController(
			nibName: "LoginViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Loading Appearance and Dissapearance
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.scrollView.delegate = self
		
		self.configureViewLabels()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidAppear(
		_ animated: Bool) {
		super.viewDidAppear(animated)
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(LoginViewController.keyboardWillShowOrChangeFrame(_:)),
			name: Notification.Name.UIKeyboardWillShow,
			object: nil)
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(LoginViewController.keyboardWillShowOrChangeFrame(_:)),
			name: Notification.Name.UIKeyboardWillChangeFrame,
			object: nil)
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(LoginViewController.keyboardWillHide(_:)),
			name: Notification.Name.UIKeyboardWillHide,
			object: nil)
		
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(LoginViewController.registrationFinishedSuccess(_:)),
			name: CoOperNotification.loginRegisterProcessSuccess,
			object: nil)
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(LoginViewController.registrationFinishedFailed(_:)),
			name: CoOperNotification.loginRegisterProcessFailed,
			object: nil)
		
		self.navigationController?.interactivePopGestureRecognizer?.delegate = self
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		NotificationCenter.default.removeObserver(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	//	============================================================================================================
	//	MARK: View Load Configuring
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func configureViewLabels() {
		
		let textString = self.termsAndPrivacyTextView.text! as NSString
		let termsRange = textString.range(of: "Terms and Conditions")
		let privacyRange = textString.range(of: "Privacy Policy")
		//let range = textString.range(of: textString as String)
		let attributedString = NSMutableAttributedString(string: textString as String)
		attributedString.addAttribute(.link, value: self.termsURLString, range: termsRange)
		attributedString.addAttribute(.link, value: self.privacyURLString, range: privacyRange)
		//attributedString.addAttribute(.link, value: UIFont.koindFontRegular(14.0)!, range: range)
		//attributedString.addAttribute(NSFontAttributeName, value: UIFont.koindFontRegular(12.0))
		attributedString.addAttribute(.foregroundColor, value: UIColor.tealColour(), range: termsRange)
		attributedString.addAttribute(.foregroundColor, value: UIColor.tealColour(), range: privacyRange)
		
		self.termsAndPrivacyTextView.text = ""
		
		self.termsAndPrivacyTextView.tintColor = UIColor.tealColour()
		self.termsAndPrivacyTextView.attributedText = attributedString
		self.termsAndPrivacyTextView.textAlignment = .center
		self.termsAndPrivacyTextView.isUserInteractionEnabled = true
		self.termsAndPrivacyTextView.delegate = self
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Layout
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		//	Whenever the subviews are layed out it is possible that it may be necessary to hide or show the
		//	image views depending on the height of the new layout.
		self.checkHeightForImageViews(
			withHeight: self.view.frame.height)
		
		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewWillTransition(
		to size: CGSize,
		with coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransition(to: size, with: coordinator)
		
		/*
		Call into the check height method to make sure that during transitions from one size class to another
		that the view correctly hides image views.
		*/
		self.checkHeightForImageViews(
			withHeight: size.height)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func checkHeightForImageViews(
		withHeight height: CGFloat) {
		
		
		if height <= 568 {
			//	On an device with iPhone SE height the travelSplashImageView
			self.travelSplashImageView.isHidden = true
		}
		else {
			//	On all other device heights hide the travelSplashImageView
			self.travelSplashImageView.isHidden = false
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Data Validation
	//	============================================================================================================
	
	
	//	------------------------------------------------------------------------------------------------------------
	func validateEmail() -> String? {
		
		var errorString: String? = nil
	
		let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
		let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
	
		if self.emailTextField.text == nil ||
			self.emailTextField.text == "" ||
			!emailPredicate.evaluate(with: self.emailTextField.text!) {
			errorString = "Please enter your email address to reset your password."
		}
		
		return errorString
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func validateTextFields() -> String? {
		
		var errorString: String? = nil
		
		if let emailError = self.validateEmail() {
			errorString = emailError
		}
		else if self.passwordTextField.text == nil ||
			self.passwordTextField.text == "" {
			errorString = "Please enter your password."
		}
		
		return errorString
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Action Responders
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func backButtonPressed(sender: AnyObject) {
		let _ = self.navigationController?.popViewController(animated: true)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func logInButtonPressed(sender: AnyObject) {
		
		if let error = self.validateTextFields() {
			self.showError(withTitle: "Log In Error", andMessage: error)
		}
		else {
			//	Attempt to login by hitting the server.
			LoginRegisterService.sharedInstance().loginAccount(
				withEmail: self.emailTextField.text!,
				withPassword: self.passwordTextField.text!)
		}
		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func forgotPasswordButtonPressed(sender: AnyObject) {
		
		//	Send an email to the user (which will contain an OTP code they can use to change their password)
		//	When the API says that the email was sent -> push into forgot password view controller
		//if let email = self.emailTextField.text !=
		if let error = self.validateEmail() {
			self.showError(withTitle: "Reset Password Error", andMessage: error)
		}
		else {
			let viewController = ForgotPasswordViewController.initController()!
			viewController.emailAddressString = self.emailTextField.text!
			self.navigationController?.pushViewController(viewController, animated: true)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func signupButtonPressed(sender: AnyObject) {
		
		//	Push into signup view controller
		let signUpViewController = SignUpViewController.initController()
		self.navigationController?.pushViewController(signUpViewController, animated: true)
	}
	//	------------------------------------------------------------------------------------------------------------

}
