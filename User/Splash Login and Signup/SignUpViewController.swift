//
//  SignUpViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-07.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Constants
	//	============================================================================================================
	
	let termsURLString = "www.trycooper.com/privacy"
	let privacyURLString = "www.trycooper.com/terms"
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	@IBOutlet weak var titleBarLabel: UILabel!
	@IBOutlet weak var largeTitleViewTop: NSLayoutConstraint!
	
	//	The view which contains the scroll view
	@IBOutlet weak var scrollContainerView: UIView!
	//	The scroll view
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var scrollViewBottom: NSLayoutConstraint!
	
	@IBOutlet weak var travelSplashImageView: UIImageView!
	
	//	============================================================================================================
	//	MARK: Text Fields for user signup
	//	============================================================================================================
	
	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var firstNameTextField: UITextField!
	@IBOutlet weak var lastNameTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	@IBOutlet weak var confirmPasswordTextField: UITextField!
	
	@IBOutlet weak var termsAndPrivacyTextView: UITextView!
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Controller Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	class func initController() -> SignUpViewController {
		return SignUpViewController(
			nibName: "SignUpViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Loading Appearance and Dissapearance
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.scrollView.delegate = self
		
		self.configureViewLabels()
    }
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidAppear(
		_ animated: Bool) {
		super.viewDidAppear(animated)
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(SignUpViewController.keyboardWillShowOrChangeFrame(_:)),
			name: Notification.Name.UIKeyboardWillShow,
			object: nil)
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(SignUpViewController.keyboardWillShowOrChangeFrame(_:)),
			name: Notification.Name.UIKeyboardWillChangeFrame,
			object: nil)
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(SignUpViewController.keyboardWillHide(_:)),
			name: Notification.Name.UIKeyboardWillHide,
			object: nil)
		
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(SignUpViewController.registrationFinishedSuccess(_:)),
			name: CoOperNotification.loginRegisterProcessSuccess,
			object: nil)
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(SignUpViewController.registrationFinishedFailed(_:)),
			name: CoOperNotification.loginRegisterProcessFailed,
			object: nil)
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(SignUpViewController.emailAddressValidationSuccess(_:)),
			name: CoOperNotification.registerAccountEmailValidationSuccess,
			object: nil)
		
		self.navigationController?.interactivePopGestureRecognizer?.delegate = self
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		NotificationCenter.default.removeObserver(self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	//	============================================================================================================
	//	MARK: View Load Configuring
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func configureViewLabels() {
		
		let textString = self.termsAndPrivacyTextView.text! as NSString
		let termsRange = textString.range(of: "Terms and Conditions")
		let privacyRange = textString.range(of: "Privacy Policy")
		//let range = textString.range(of: textString as String)
		let attributedString = NSMutableAttributedString(string: textString as String)
		attributedString.addAttribute(.link, value: self.termsURLString, range: termsRange)
		attributedString.addAttribute(.link, value: self.privacyURLString, range: privacyRange)
		//attributedString.addAttribute(.link, value: UIFont.koindFontRegular(14.0)!, range: range)
		//attributedString.addAttribute(NSFontAttributeName, value: UIFont.koindFontRegular(12.0))
		attributedString.addAttribute(.foregroundColor, value: UIColor.tealColour(), range: termsRange)
		attributedString.addAttribute(.foregroundColor, value: UIColor.tealColour(), range: privacyRange)
		
		self.termsAndPrivacyTextView.text = ""
		
		self.termsAndPrivacyTextView.tintColor = UIColor.tealColour()
		self.termsAndPrivacyTextView.attributedText = attributedString
		self.termsAndPrivacyTextView.textAlignment = .center
		self.termsAndPrivacyTextView.isUserInteractionEnabled = true
		self.termsAndPrivacyTextView.delegate = self
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- View Layout
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		//	Whenever the subviews are layed out it is possible that it may be necessary to hide or show the
		//	image views depending on the height of the new layout.
		self.checkHeightForImageViews(
			withHeight: self.view.frame.height)
		
		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewWillTransition(
		to size: CGSize,
		with coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransition(to: size, with: coordinator)
		
		/*
		Call into the check height method to make sure that during transitions from one size class to another
		that the view correctly hides image views.
		*/
		self.checkHeightForImageViews(
			withHeight: size.height)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func checkHeightForImageViews(
		withHeight height: CGFloat) {
		
		
		if height <= 568 {
			//	On an device with iPhone SE height the travelSplashImageView
			self.travelSplashImageView.isHidden = true
		}
		else {
			//	On all other device heights hide the travelSplashImageView
			self.travelSplashImageView.isHidden = false
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Data Validation
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func validateTextFields() -> String? {
		var errorString: String? = nil
		
		let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
		let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
		
		let passwordFormat = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,15}$"
		let passwordPredicate = NSPredicate(format:"SELF MATCHES %@", passwordFormat)
		
		if self.emailTextField.text == nil ||
			self.emailTextField.text == "" ||
			!emailPredicate.evaluate(with: self.emailTextField.text!) {
			errorString = "Please enter a valid email address."
		}
		else if self.firstNameTextField.text == nil ||
			self.firstNameTextField.text!.count < 2 {
			errorString = "Please enter a first name that is at least 2 characters long."
		}
		else if self.lastNameTextField.text == nil ||
			self.lastNameTextField.text!.count < 2 {
			errorString = "Please enter a last name that is at least 2 characters long."
		}
		else if self.passwordTextField.text == nil ||
			self.passwordTextField.text!.count < 8 ||
			!passwordPredicate.evaluate(with: self.passwordTextField.text) {
			errorString = "Please enter a password that is at least 8 characters long, contains at least 1 Uppercase, 1 Lowercase, 1 number, and 1 special character and maximum 16 characters."
		}
		else if self.confirmPasswordTextField.text == nil ||
			self.confirmPasswordTextField.text == "" {
			errorString = "Please confirm your password."
		}
		else if self.confirmPasswordTextField.text != nil &&
			self.confirmPasswordTextField.text!.count >= 6 &&
			self.passwordTextField.text != nil &&
			self.passwordTextField.text!.count >= 6 &&
			self.passwordTextField.text! != self.confirmPasswordTextField.text! {
			errorString = "Passwords do not match."
		}
		
		return errorString
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Action Responders
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func backButtonPressed(sender: AnyObject) {
		let _ = self.navigationController?.popViewController(animated: true)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func signupButtonPressed(sender: AnyObject) {
		
		if let error = self.validateTextFields() {
			self.showError(withTitle: "Sign Up Error", andMessage: error)
		}
		else {
			LoginRegisterService.sharedInstance().validateEmailAddress(self.emailTextField.text!)
		}		
	}
	//	------------------------------------------------------------------------------------------------------------

}


