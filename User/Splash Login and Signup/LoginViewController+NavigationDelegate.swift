//
//  LoginViewController+NavigationDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-08.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension LoginViewController: UIGestureRecognizerDelegate {
	
	//	============================================================================================================
	//	MARK:- UIGestureRecognizerDelegate
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func gestureRecognizerShouldBegin(
		_ gestureRecognizer: UIGestureRecognizer) -> Bool {
		
		return true
	}
	//	------------------------------------------------------------------------------------------------------------
}
