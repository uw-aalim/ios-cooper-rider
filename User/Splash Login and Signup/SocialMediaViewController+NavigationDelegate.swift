//
//  SocialMediaViewController+NavigationDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-14.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension SocialMediaViewController2: UIGestureRecognizerDelegate {
	
	//	============================================================================================================
	//	MARK:- UIGestureRecognizerDelegate
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	func gestureRecognizerShouldBegin(
		_ gestureRecognizer: UIGestureRecognizer) -> Bool {
		
		return true
	}
	//	------------------------------------------------------------------------------------------------------------
}
