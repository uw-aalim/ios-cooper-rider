//
//  YourTripViewController.h
//  Provider
//
//  Created by iCOMPUTERS on 17/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TripsObject : NSObject

@property(assign,nonatomic) NSString *date;
@property(assign,nonatomic) NSString *time;
@property(assign,nonatomic) NSString *Amount;
@property(assign,nonatomic) NSString *imageString;
@property(assign,nonatomic) NSString *ID;
@property(assign,nonatomic) NSString *booking;

@end

@interface YourTripViewController : GAITrackedViewController <UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *dateArray, *timeArray, *amountArray, *imageArray, *idArray, *bookingArray;
    NSString *identifierStr, *requestIdStr;
}
@property (weak, nonatomic) IBOutlet UILabel *headerLb;
@property (weak, nonatomic) IBOutlet UITableView *tripTableView;
@property (weak, nonatomic) IBOutlet UIButton *pastBtn;
@property (weak, nonatomic) IBOutlet UIButton *upcomingBtn;
@property (weak, nonatomic) IBOutlet UILabel *upcomingLbl;
@property (weak, nonatomic) IBOutlet UILabel *pastLbl;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;

@property (weak, nonatomic) NSString *navigateStr;

@property (strong,nonatomic)NSMutableArray * TripsArray;

+(instancetype)initController;

@end
