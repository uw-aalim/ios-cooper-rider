//
//  AFNHelper.h
//  Truck
//
//  Created by veena on 1/12/17.
//  Copyright © 2017 appoets. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define POST_METHOD @"POST"
#define GET_METHOD  @"GET"
#define DELETE_METHOD  @"DELETE"


typedef void (^RequestCompletionBlock)(id response, NSDictionary *error, NSString *errorcode);

typedef void (^successTask)(BOOL Status);

typedef void (^ResponeSucess)(id response);

typedef void (^FailurewithValidations)(NSString *errorMessage);

typedef void (^FailurewithErrors)(NSString * error);

typedef void (^FailurewithInternet)(NSString *internetFailure);


@interface AFNHelper : NSObject
{
//blocks
    RequestCompletionBlock dataBlock;
}

@property(nonatomic,copy)NSString *strReqMethod;

-(id)initWithRequestMethod:(NSString *)method;

-(void)getDataFromPath:(NSString *)path withParamData:(NSDictionary *)dictParam withBlock:(RequestCompletionBlock)block;

-(void)getExtraDataFromPath:(NSString *)constantPath withParamData:(NSDictionary *)dictParam withBlock:(RequestCompletionBlock)block;

-(void)getDataFromPath:(NSString *)path withParamDataImage:(NSDictionary *)dictParam andImage:(UIImage *)image withBlock:(RequestCompletionBlock)block;

-(void)getAddressFromGooglewithParamData:(NSDictionary *)dictParam withBlock:(RequestCompletionBlock)block;

-(void)getAddressFromGooglewAutoCompletewithParamData:(NSDictionary *)dictParam withBlock:(RequestCompletionBlock)block;

-(void)refreshMethod_NoLoader:(NSString *)path withBlock:(RequestCompletionBlock)block;
//
//-(void)getDataFromPath:(NSString *)path WithType:(NSString *)type : WithParameters :(NSDictionary *)params WithCompletedSuccess:(ResponeSucess)success OrValidationFailure:(FailurewithValidations)Errorvalidations OrErrorCode:(FailurewithErrors)ErrorCode OrIntentet:(FailurewithInternet) failureNetwork;
//
//NEW METHOD API CALL


-(void)getDataFromPath:(NSString *)path WithType:(NSString *)type WithParameters :(NSDictionary *)params WithCompletedSuccess:(ResponeSucess)success OrValidationFailure:(FailurewithValidations)Errorvalidations OrErrorCode:(FailurewithErrors)ErrorCode OrIntentet:(FailurewithInternet) failureNetwork;



@end
