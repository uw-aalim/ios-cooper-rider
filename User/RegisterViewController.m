//
//  RegisterViewController.m
//  User
//
//  Created by iCOMPUTERS on 12/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "RegisterViewController.h"
#import "CSS_Class.h"
#import "EmailViewController.h"
#import "config.h"
#import "UIScrollView+EKKeyboardAvoiding.h"
#import "newHomeController.h"
#import "HomeViewController.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "CommenMethods.h"
#import "ViewController.h"
#import "Utilities.h"
#import "Colors.h"

@interface RegisterViewController ()
{
    AppDelegate *appDelegate;
    NSString *strLoginType;
}

@end

@implementation RegisterViewController
{
    AKFAccountKit *_accountKit;
    UIViewController<AKFViewController> *_pendingLoginViewController;
    NSString *_authorizationCode;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isLocationUpdate = YES;
    [_firstNameText setUserInteractionEnabled:NO];
    [_lastNameText setUserInteractionEnabled:NO];
    [_passwordText setUserInteractionEnabled:NO];
    
    // initialize Account Kit
    if (_accountKit == nil) {
        // may also specify AKFResponseTypeAccessToken
        _accountKit = [[AKFAccountKit alloc] initWithResponseType:AKFResponseTypeAccessToken];
    }
    
    // view controller for resuming login
    _pendingLoginViewController = [_accountKit viewControllerForLoginResume];
    
    
    strLoginType=@"manual";
    
    _emailText.text=appDelegate.strEmail;
    
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapReceived:)];
    [tapGestureRecognizer setDelegate:self];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    [_detailsScrollView setContentSize:[_detailsScrollView frame].size];
    [_detailsScrollView setKeyboardAvoidingEnabled:YES];
    
    [self setDesignStyles];
}


-(void)LocalizationUpdate{
    _headerLbl.text = LocalizedString(@"Enter the details to register");
    _emailLbl.text = LocalizedString(@"Email");
    _firstNameLbl.text = LocalizedString(@"Name");
    _firstNameText.placeholder =LocalizedString(@"First name");
    _lastNameText.placeholder =LocalizedString(@"Last name");
    _passwordLbl.text = LocalizedString(@"Password");
    //_confirmPassLbl.text = LocalizedString(@"Confirm password");
    _phoneLbl.text = LocalizedString(@"Phone Number");
    
}
- (void)_prepareLoginViewController:(UIViewController<AKFViewController> *)loginViewController {
    loginViewController.delegate = self;
    // Optionally, you may use the Advanced UI Manager or set a theme to customize the UI.
    loginViewController.uiManager = [[AKFSkinManager alloc]
                                     initWithSkinType:AKFSkinTypeTranslucent
                                     primaryColor:BLACKCOLOR
                                     backgroundImage:[UIImage imageNamed:@"bg-1537"]
                                     backgroundTint:AKFBackgroundTintWhite
                                     tintIntensity:0.32];
    loginViewController.uiManager.theme.buttonTextColor = [UIColor whiteColor];
//    loginViewController.uiManager.theme.backgroundColor = [UIColor whiteColor];

}

-(void)tapReceived:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self LocalizationUpdate];
}

- (void)loginWithPhone:(id)sender {
	
	NSLog(@"RegisterViewController loginWithPhone");
    [[NSUserDefaults standardUserDefaults]setObject:@"User" forKey:@"SocialLogin"];
    NSString *inputState = [[NSUUID UUID] UUIDString];
    UIViewController<AKFViewController> *viewController = [_accountKit viewControllerForPhoneLoginWithPhoneNumber:nil state:inputState];
    viewController.enableSendToFacebook = YES; // defaults to NO
    [self _prepareLoginViewController:viewController]; // see below
    [self presentViewController:viewController animated:YES completion:NULL];
}

- (void) viewController:(UIViewController<AKFViewController> *)viewController
didCompleteLoginWithAccessToken:(id<AKFAccessToken>)accessToken state:(NSString *)state {
    //    [self proceedToMainScreen];
	
	NSLog(@"RegisterViewController viewController - AKFViewController - didCompleteLoginWithAccessToken");
	
    AKFAccountKit *accountKit = [[AKFAccountKit alloc] initWithResponseType:AKFResponseTypeAccessToken];
    [accountKit requestAccount:^(id<AKFAccount> account, NSError *error) {
        // account ID
        
        [appDelegate onStartLoader];
        
        NSLog(@"accountID ... %@",account.accountID);
        if ([account.emailAddress length] > 0) {
            NSLog(@"accountID ... %@",account.emailAddress);
        }
        else if ([account phoneNumber] != nil) {
            NSLog(@"accountID ... %@",[[account phoneNumber] stringRepresentation]);
            _phoneText.text =[[account phoneNumber] stringRepresentation];
        }
        
        if([appDelegate internetConnected])
        {
            NSString* UDID_Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
            NSLog(@"output is : %@", UDID_Identifier);
            
            NSDictionary*params;
            if([strLoginType isEqualToString:@"manual"])
            {
                params=@{@"email":_emailText.text,@"password":_passwordText.text,@"first_name":_firstNameText.text,@"last_name":_lastNameText.text,@"mobile":_phoneText.text,@"device_token":appDelegate.strDeviceToken,@"login_by":strLoginType,@"device_type":@"ios", @"device_id":UDID_Identifier};
            }
            //        else
            //        {
            //            params=@{@"email":_txtEmail.text,@"password":_txtPassword.text,@"first_name":_txtFirstName.text,@"last_name":_txtLastName.text,@"mobile":_txtPhnNo.text,@"device_token":appDelegate.strDeviceToken,@"login_by":strLoginType,@"device_type":@"ios",@"social_unique_id":strSocialUniqueID};
            //        }
			
			NSLog(@"AFNHelper - POST - MD_REGISTER");
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:MD_REGISTER withParamData:params withBlock:^(id response, NSDictionary *Error,NSString *strCode) {
                [appDelegate onEndLoader];
                if(response)
                {
                    [self onLogin];
                }
                else
                {
                    if ([strCode intValue]==1)
                    {
                        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                    }
                    else
                    {
                        if ([Error objectForKey:@"email"]) {
                            [CommenMethods alertviewController_title:@"" MessageAlert:[[Error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                        }
                        else if ([Error objectForKey:@"first_name"]) {
                            [CommenMethods alertviewController_title:@"" MessageAlert:[[Error objectForKey:@"first_name"] objectAtIndex:0]  viewController:self okPop:NO];
                        }
                        else if ([Error objectForKey:@"last_name"]) {
                            [CommenMethods alertviewController_title:@"" MessageAlert:[[Error objectForKey:@"last_name"] objectAtIndex:0]  viewController:self okPop:NO];
                        }
                        else if ([Error objectForKey:@"mobile"]) {
                            [CommenMethods alertviewController_title:@"" MessageAlert:[[Error objectForKey:@"mobile"] objectAtIndex:0]  viewController:self okPop:NO];
                        }
                        else if ([Error objectForKey:@"password"]) {
                            [CommenMethods alertviewController_title:@"" MessageAlert:[[Error objectForKey:@"password"] objectAtIndex:0]  viewController:self okPop:NO];
                        }
                    }
                    
                }
            }];
        }
        else
        {
            
            [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
        }
    }];
    [accountKit logOut];
}

- (void)                 viewController:(UIViewController<AKFViewController> *)viewController
  didCompleteLoginWithAuthorizationCode:(NSString *)code
                                  state:(NSString *)state
{
    
}

- (void)viewController:(UIViewController<AKFViewController> *)viewController didFailWithError:(NSError *)error
{
    // ... implement appropriate error handling ...
    NSLog(@"%@ did fail with error: %@", viewController, error);
}

- (void)viewControllerDidCancel:(UIViewController<AKFViewController> *)viewController
{
    // ... handle user cancellation of the login process ...
}

-(void)setDesignStyles
{
    [CSS_Class APP_labelName:_headerLbl];
    [CSS_Class APP_textfield_Outfocus:_emailText];
    [CSS_Class APP_textfield_Outfocus:_firstNameText];
    
    [CSS_Class APP_textfield_Outfocus:_lastNameText];
    [CSS_Class APP_textfield_Outfocus:_passwordText];
    [CSS_Class APP_textfield_Outfocus:_phoneText];
    
    [CSS_Class APP_labelName_Small:_emailLbl];
    [CSS_Class APP_labelName_Small:_firstNameLbl];
    [CSS_Class APP_labelName_Small:_lastNameLbl];
    [CSS_Class APP_labelName_Small:_passwordLbl];
    [CSS_Class APP_labelName_Small:_phoneLbl];
    
}

-(IBAction)backBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    //    EmailViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"EmailViewController"];
    //    [self.navigationController pushViewController:controller animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_emailText)
    {
        [self emailValidateMethod];
        
    }
    else if(textField==_firstNameText)
    {
        [_lastNameText becomeFirstResponder];
    }
    else if(textField==_lastNameText)
    {
        [_passwordText becomeFirstResponder];
    }
    else if(textField==_passwordText)
    {
        [_passwordText resignFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField == _emailText)
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= INPUTLENGTH || returnKey;
    }
    else if ((textField == _firstNameText) || (textField == _lastNameText))
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    else if (textField == _phoneText)
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= PHONELENGTH || returnKey;
    }
    else
    {
        return YES;
    }
    return NO;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [CSS_Class APP_textfield_Infocus:textField];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if(textField==_emailText)
    {
        [self emailValidateMethod];
    }
    [CSS_Class APP_textfield_Outfocus:textField];
    return YES;
}

-(IBAction)Nextbtn:(id)sender
{
    [self.view endEditing:YES];
    
    if((_emailText.text.length==0) || (_firstNameText.text.length==0) ||(_passwordText.text.length==0) || (_lastNameText.text.length==0))
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"ALERT") message:NSLocalizedString(@"VALIDATE", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    else if(_passwordText.text.length != 0){
            NSString *stricterFilterString = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,15}$";
            NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
            if( [passwordTest evaluateWithObject:_passwordText.text]){
            
            }else{
                [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:LocalizedString(@"The password must be minimum 8 characters and 1 Uppercase, 1 Lowercase , 1 number , and 1 special characters and maximum 16 characters ") viewController:self okPop:NO];
            }
            
        }else{
            [CSS_Class alertviewController_title:LocalizedString(@"Alert!") MessageAlert:LocalizedString(@"Password is required") viewController:self okPop:NO];
        }
    
    {
        if([self emailValidation:_emailText.text]){
            [self loginWithPhone:self];
        }
    }
}

-(void)emailValidateMethod
{
	NSLog(@"RegisterViewController emailValidateMethod - call to server post MD_EMAILVERIFY");
    if([appDelegate internetConnected])
    {
        NSDictionary * params=@{@"email":_emailText.text};
        
        [appDelegate onStartLoader];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:MD_EMAILVERIFY withParamData:params withBlock:^(id response, NSDictionary *Error,NSString *strCode) {
            [appDelegate onEndLoader];
            if(response)
            {
                NSLog(@"EmailVAlid ..%@", response);
                [_firstNameText becomeFirstResponder];
                [_firstNameText setUserInteractionEnabled:YES];
                [_lastNameText setUserInteractionEnabled:YES];
                [_passwordText setUserInteractionEnabled:YES];
            }
            else
            {
                
                if ([strCode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else
                {
                    if ([Error objectForKey:@"email"]) {
                        [CommenMethods alertviewController_title:@"" MessageAlert:[[Error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                    }
                    else if ([Error objectForKey:@"password"]) {
                        [CommenMethods alertviewController_title:@"" MessageAlert:[[Error objectForKey:@"password"] objectAtIndex:0]  viewController:self okPop:NO];
                    }
                }
                
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}

-(BOOL) emailValidation:(NSString *)emailTxt
{
	NSLog(@"RegisterViewController emailValidationMethodCalled");
    if([emailTxt length]==0)
    {
        [CommenMethods alertviewController_title:@"Please enter email" MessageAlert:@"" viewController:self okPop:NO];
    }
    else
    {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if([emailTest evaluateWithObject:emailTxt])
            return YES;
        else
            [CommenMethods alertviewController_title:@"Please enter valid email" MessageAlert:@"" viewController:self okPop:NO];
    }
    return NO;
}
-(void)onLogin
{
	NSLog(@"onLogin Register View Controller Method Called");
    if([appDelegate internetConnected])
    {
        NSDictionary * params=@{@"username":_emailText.text,@"password":_passwordText.text,@"device_token":appDelegate.strDeviceToken,@"grant_type":@"password",@"device_type":@"ios",@"client_id":ClientID,@"client_secret":Client_SECRET};
        
        [appDelegate onStartLoader];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:MD_LOGIN withParamData:params withBlock:^(id response, NSDictionary *Error,NSString *strCode) {
            [appDelegate onEndLoader];
            if(response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
                [user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
                [user setValue:response[@"refresh_token"] forKey:UD_REFERSH_TOKEN];
                [user setValue:@"" forKey:UD_SOCIAL];
                
                [user setBool:true forKey:@"isLoggedin"];
                [user synchronize];
                
                //                HomeViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                //                [self.navigationController pushViewController:controller animated:YES];
                
                [self onGetProfile];
            }
            else
            {
                
                if ([strCode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else
                {
                    if ([Error objectForKey:@"email"]) {
                        [CommenMethods alertviewController_title:@"" MessageAlert:[[Error objectForKey:@"email"] objectAtIndex:0]  viewController:self okPop:NO];
                    }
                    else if ([Error objectForKey:@"password"]) {
                        [CommenMethods alertviewController_title:@"" MessageAlert:[[Error objectForKey:@"password"] objectAtIndex:0]  viewController:self okPop:NO];
                    }
                }
                
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}

-(void)onGetProfile
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [appDelegate onStartLoader];
        [afn getDataFromPath:MD_GETPROFILE withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            [appDelegate onEndLoader];
            if (response)
            {
                NSLog(@"GET PROFILE....%@", response);
                
                
                NSString *strProfile=[Utilities removeNullFromString:response[@"picture"]];
                NSString *socialIdStr = [Utilities removeNullFromString:[response valueForKey:@"social_unique_id"]];
                
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:strProfile forKey:UD_PROFILE_IMG];
                [user setValue:socialIdStr forKey:UD_SOCIAL];
                [user setValue:[response valueForKey:@"id"] forKey:UD_ID];
                [user setValue:[response valueForKey:@"sos"] forKey:UD_SOS];
                
                NSString *nameStr = [NSString stringWithFormat:@"%@ %@", [Utilities removeNullFromString: response[@"first_name"]], [Utilities removeNullFromString: response[@"last_name"]]];
                
                [user setValue:nameStr forKey:UD_PROFILE_NAME];
                
                // MARK: Set user
                
                [user setObject:SERVICE_URL forKey:@"serviceurl"];
                [user setObject:Client_SECRET forKey:@"passport"];
                [user setObject:ClientID forKey:@"clientid"];
                [user setObject:WEB_SOCKET forKey:@"websocket"];
                [user setObject:APP_NAME forKey:@"appname"];
                [user setObject:nameStr forKey:@"username"];
                [user setObject:GMSMAP_KEY forKey:@"googleApiKey"];
                [user setObject:Stripe_KEY forKey:@"stripekey"];
                
                [user setObject:@"1" forKey:@"is_valid"];
                [user synchronize];
                
                [self nxtController];
            }
            else
            {
                if ([errorcode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
                    //
                    //                    ViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                    //                    [self.navigationController pushViewController:viewCont animated:YES];
                    
                    [self logoutMethod];
                    
                }
            }
            
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}
-(void)nxtController{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"New" bundle: nil];
    
    newHomeController * infoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"newHomeController"];
    [self.navigationController pushViewController:infoController animated:YES];
    
}
-(void)logoutMethod
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn refreshMethod_NoLoader:MD_REFRESH_TOKEN withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
                [user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
                [user setValue:response[@"refresh_token"] forKey:UD_REFERSH_TOKEN];
            }
            else
            {
                [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}




@end
