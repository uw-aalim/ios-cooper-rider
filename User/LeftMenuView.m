//
//  LeftMenuView.m
//  caretaker_user
//
//  Created by apple on 12/15/16.
//  Copyright © 2016 Mac. All rights reserved.
//

#import "LeftMenuView.h"
#import "LeftMenuTableViewCell.h"
#import "config.h"
#import "CSS_Class.h"
#import "Colors.h"
#import "ProfileViewController.h"
#import "Constants.h"
#import "Utilities.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import <SDWebImage/UIButton+WebCache.h>
#import "CommenMethods.h"
#import "AppDelegate.h"

@implementation LeftMenuView
@synthesize menuImages,menuImagesText,menuTableView;

@synthesize nameLbl,proPicImgBtn;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        [self setDesign];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self setDesign];
}

- (void)setDesign
{
    
    //
    menuImagesText=[[NSArray alloc]initWithObjects:@"Payment",@"Your Trips",@"Coupon",@"Wallet",@"Passbook",@"Help",@"Share",@"Settings",@"Logout",nil];
    [menuTableView reloadData];
    
}


#pragma mark -- Table View Delegates Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{   return [menuImagesText count];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LeftMenuTableViewCell *cell = (LeftMenuTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"LeftMenuTableViewCellID"];
    
    if (cell == nil)
    {
        cell = (LeftMenuTableViewCell *) [[[NSBundle mainBundle] loadNibNamed:@"LeftMenuTableViewCell" owner:self options:nil] lastObject];
    }
//    cell.menuImg.image=[UIImage imageNamed:[menuImages objectAtIndex:indexPath.row]];
    cell.menuLbl.text=LocalizedString([menuImagesText objectAtIndex:indexPath.row]);
    cell.menuLbl.textColor = BASE2Color;
    //[CSS_Class App_Header:cell.menuLbl];
    //[cell.menuLbl setTextColor:RGB(18, 18, 18)];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  190;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"LeftHederview" owner:self options:nil];
    
    LeftHederview *headerView = [nibArray objectAtIndex:0];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    headerView.namelbl.text =[defaults valueForKey:UD_PROFILE_NAME];
    NSString *strProfile = [defaults valueForKey:UD_PROFILE_IMG];
    
    
//    NSString *statusStr = [defaults valueForKey:@"status"];
//
//    if ([statusStr isEqualToString:@"onboarding"])
//    {
//        [headerView.green_imageview setImage:[UIImage imageNamed:@"yellow"]];
//    }
//    else if ([statusStr isEqualToString:@"approved"])
//    {
//        [headerView.green_imageview setImage:[UIImage imageNamed:@"green"]];
//    }
//    else
//    {
//        [headerView.green_imageview setImage:[UIImage imageNamed:@"red"]];
//    }
    
    headerView.profileImagebtn.layer.cornerRadius= headerView.profileImagebtn.bounds.size.height/2;
    headerView.profileImagebtn.clipsToBounds=YES;
    headerView.layer.masksToBounds = YES;
    
    
    if (![strProfile isKindOfClass:[NSNull class]])
    {
        NSURL *imgUrl;
        if ([strProfile length]!=0)
        {
            if ([strProfile containsString:@"http"])
            {
                imgUrl = [NSURL URLWithString:strProfile];
                
                [headerView.profileImagebtn sd_setBackgroundImageWithURL:imgUrl
                                                                forState:UIControlStateNormal
                                                        placeholderImage:[UIImage imageNamed:@"userProfile"]];
            }
            else
            {
                imgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/storage/%@", SERVICE_URL, strProfile]];
                
                [headerView.profileImagebtn sd_setBackgroundImageWithURL:imgUrl
                                                                forState:UIControlStateNormal
                                                        placeholderImage:[UIImage imageNamed:@"userProfile"]];
            }
        }
        else
        {
            [headerView.profileImagebtn setImage:[UIImage imageNamed:@"userProfile"] forState:UIControlStateNormal];
//            headerView.profileImagebtn.imageView.image = [UIImage imageNamed:@"user_profile"];
        }
    }
    else
    {
        //DO nothing
    }
    
    [headerView.profileImagebtn addTarget:self action:@selector(proPicImgBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    return headerView;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *viewRedirectionString = LocalizedString([menuImagesText objectAtIndex:indexPath.row]);
    
    [self.LeftMenuViewDelegate setSelectedIndex:viewRedirectionString];
//    if ([viewRedirectionString isEqualToString:LocalizedString(@"Your Trips")])
//    {
//        [self.LeftMenuViewDelegate yourTripsView];
//    }
//    else if ([viewRedirectionString isEqualToString:LocalizedString(@"Wallet")])
//    {
//        [self.LeftMenuViewDelegate walletView];
//    }
//    else if ([viewRedirectionString isEqualToString:LocalizedString(@"Help")])
//    {
//        [self.LeftMenuViewDelegate helpView];
//    }
//    else if ([viewRedirectionString isEqualToString:LocalizedString(@"Share")])
//    {
//        [self.LeftMenuViewDelegate shareView];
//    }
//    else if ([viewRedirectionString isEqualToString:LocalizedString(@"Logout")])
//    {
//        [self.LeftMenuViewDelegate logOut];
//    }
//    else if ([viewRedirectionString isEqualToString:LocalizedString(@"Coupon")])
//    {
//        [self.LeftMenuViewDelegate coupons];
//    }
//    else if ([viewRedirectionString isEqualToString:LocalizedString(@"Payment")])
//    {
//        [self.LeftMenuViewDelegate payments];
//    }
//    else if ([viewRedirectionString isEqualToString:LocalizedString(@"Passbook")])
//    {
//        [self.LeftMenuViewDelegate passbook];
//    }
//    else if ([viewRedirectionString isEqualToString:LocalizedString(@"Settings")]){
//
//        [self.LeftMenuViewDelegate Settings];
//    }

}

- (IBAction)proPicImgBtnAction:(id)sender{
    
    [self.LeftMenuViewDelegate profileView];
}
@end
