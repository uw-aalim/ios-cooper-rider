//
//  CommonAnimations.m
//  WalkThroughObjc
//
//  Created by CSS on 11/09/17.
//  Copyright © 2017 Appoets. All rights reserved.
//

#import "CommonAnimations.h"

@implementation CommonAnimations

#pragma Animation for walkthirugh appear
+(void)AnimationAppearenceforWalkthrough:(UIView *)View withSideOfAnimation:(BOOL)isRight{
    
    if(isRight){
        View.transform = CGAffineTransformMakeTranslation([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        [UIView animateKeyframesWithDuration:0.5 delay:0.0 options:0 animations:^{
            // End
            View.transform = CGAffineTransformMakeTranslation(0, 0);
            
        } completion:^(BOOL finished) { }];
    }else{
        
        
        View.transform = CGAffineTransformMakeTranslation(-[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        [UIView animateKeyframesWithDuration:0.5 delay:0.0 options:0 animations:^{
            // End
            View.transform = CGAffineTransformMakeTranslation(0, 0);
            
        } completion:^(BOOL finished) { }];
    }
    
}


#pragma animation for taxi move
+(void)AnimationForMoveview:(UIView *)view withAnimationDirations:(CGFloat)duration{
    
    CGPoint startPoint = (CGPoint){0.f,view.frame.origin.y};
   // CGPoint middlePoint = (CGPoint){400.f, 400.f};
    CGPoint endPoint = (CGPoint){view.frame.size.width+50 , view.frame.origin.y};
    
    CGMutablePathRef thePath = CGPathCreateMutable();
    CGPathMoveToPoint(thePath, NULL, startPoint.x, startPoint.y);
   // CGPathAddLineToPoint(thePath, NULL, middlePoint.x, middlePoint.y);
    CGPathAddLineToPoint(thePath, NULL, endPoint.x, endPoint.y);
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    animation.duration = duration;
    animation.path = thePath;
    animation.autoreverses = false ;
    animation.repeatCount = INFINITY;
    [view.layer addAnimation:animation forKey:@"position"];
    view.layer.position = endPoint;
}

#pragma corner appearence in views
+ (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
    
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0;
        if (tl) {corner = corner | UIRectCornerTopLeft;}
        if (tr) {corner = corner | UIRectCornerTopRight;}
        if (bl) {corner = corner | UIRectCornerBottomLeft;}
        if (br) {corner = corner | UIRectCornerBottomRight;}
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}

@end
