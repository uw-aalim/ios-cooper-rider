//
//  NewLocationPlaceTableViewCell.m
//  User
//
//  Created by Benjamin Cortens on 2018-06-25.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import "NewLocationPlaceTableViewCell.h"
#import "User-Swift.h"

@interface NewLocationPlaceTableViewCell() {
	UIView *selfView;
}
@end

@implementation NewLocationPlaceTableViewCell

-(id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	//selfView = [self loadViewWith:@"NewLocationPlaceTableViewCell"];
	//selfView.translatesAutoresizingMaskIntoConstraints = FALSE;
	//[selfView constrainToContainerView:self];
	NSLog(@"Initialize frame NewLocationPlaceTableViewCell");
	return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	//selfView = [self loadViewWith:@"NewLocationPlaceTableViewCell"];
	//selfView.translatesAutoresizingMaskIntoConstraints = FALSE;
	//[selfView constrainToContainerView:self];
	NSLog(@"Initialize coder NewLocationPlaceTableViewCell");
	return self;
}

-(void) awakeFromNib {
	[super awakeFromNib];
	NSLog(@"NewLocationPlaceTableViewCell Awake from nib");
	//selfView = [self loadViewWith:@"NewLocationPlaceTableViewCell"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
	//[self contentView].backgroundColor = UIColor.whiteColor;
	//self.backgroundColor = UIColor.whiteColor;
}

-(void)configureWithAddress: (NSString *) address
			 favouriteImage: (NSString * _Nullable) favouriteImage
			  favouriteName: (NSString * _Nullable) favouriteName {
	
	NSLog(@"Loading cell with address %@",address);
	if (favouriteName == NULL || favouriteImage == NULL) {
		self.favouriteWidth.constant = 0;
		self.addressLabel.textAlignment = NSTextAlignmentLeft;
	}
	else {
		self.favouriteWidth.constant = 148.0;
		self.addressLabel.textAlignment = NSTextAlignmentRight;
	}
	self.addressLabel.text = address;
}

@end




