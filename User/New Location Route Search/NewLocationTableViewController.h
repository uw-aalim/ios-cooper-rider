//
//  NewLocationTableViewController.h
//  User
//
//  Created by Benjamin Cortens on 2018-06-25.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol NewLocationTableViewControllerDelegate
- (void)didSelectCellWithAddress: (NSString *) addressString
				   andCoordinate: (CLLocationCoordinate2D) coordinate
					   andSender: (id) sender;
@end


@interface NewLocationTableViewController : NSObject<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, weak) id<NewLocationTableViewControllerDelegate> delegate;
 

-(id)initWithTableView: (UITableView *) tableView;
-(void)updateWithPlaces: (NSArray *) places;
-(void)updateWithSearchResults: (NSArray * _Nullable) newSearchResults;
-(void)updateWithSearchSuggestions: (NSArray * _Nullable) newSuggestions;

@end
