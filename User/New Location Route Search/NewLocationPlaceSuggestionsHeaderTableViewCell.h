//
//  NewLocationPlaceSuggestionsHeaderTableViewCell.h
//  User
//
//  Created by Benjamin Cortens on 2018-06-25.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewLocationPlaceSuggestionsHeaderTableViewCell : UITableViewCell

@end
