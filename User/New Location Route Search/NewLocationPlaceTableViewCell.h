//
//  NewLocationPlaceTableViewCell.h
//  User
//
//  Created by Benjamin Cortens on 2018-06-25.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewLocationPlaceTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *favouriteWidth;
@property (nonatomic, weak) IBOutlet UILabel *favouriteLabel;
@property (nonatomic, weak) IBOutlet UIImageView *favouriteImageView;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;

-(void)configureWithAddress: (NSString *) address
			 favouriteImage: (NSString * _Nullable) favouriteImage
			  favouriteName: (NSString * _Nullable) favouriteName;

@end


