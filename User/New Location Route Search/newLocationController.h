//
//  newLocationController.h
//  User
//
//  Created by CSS on 19/01/18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "config.h"
#import "CSS_Class.h"
#import "Colors.h"
#import "NSString+StringValidation.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "HomeViewController.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "Utilities.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import <CoreLocation/CoreLocation.h>

#ifndef NewLocationController_H
#define NewLocationController_H

/*
@interface FavourateCell :UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *picImageview;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@end

@interface SetpinCell: UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *setPinBtn;
@end
*/

@protocol newLocationControllerDelegate
- (void)didChooseNewOrigin: (CLLocationCoordinate2D) originPoint
		 withOriginAddress: (NSString * _Nullable) originAddress
		   withDestination: (CLLocationCoordinate2D) destinationPoint
	withDestinationAddress: (NSString *  _Nullable) destinationAddress;
@end


@interface newLocationController : UIViewController<UITextFieldDelegate>{
    AppDelegate *appDelegate;
    //CLLocation * CurrentUpdateLocation;
    //CLGeocoder *geocoder;
    //GMSCameraPosition *lastCameraPosition;
    //CLLocationCoordinate2D newCoords;
    //NSString * locationString;
    //NSString * changeLocationsAddress;
    //BOOL isBack;
}

@property (weak, nonatomic) IBOutlet UIButton * _Nullable closeButton;
@property (weak, nonatomic) IBOutlet UITextField * _Nullable originTextField;
@property (weak, nonatomic) IBOutlet UITextField * _Nullable destinationTextField;
@property (weak, nonatomic) IBOutlet UITableView * _Nullable tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * _Nullable bottomConstraint;

/**
 The location from which the user would like to be picked up. If this property is set by the view controlelr
 which presents the newLocationController then it will be the default location when this controller is presented.
 */
@property (strong, nonatomic) NSString  * _Nullable sourceLocation;
/**
 The location where the user would like to be dropped off. If this property is set by the view controlelr
 which presents the newLocationController then it will be the default location when this controller is presented.
 */
@property (strong, nonatomic) NSString * _Nullable destinationLocation;

@property (nonatomic) CLLocationCoordinate2D sourceCoordinate;
@property (nonatomic) CLLocationCoordinate2D destinationCoordinate;

/**
 If the newLocationController is presented from sourceLocation it should default have this property set to true
 soas that the controller will be able to automatically set this controller to be active when presentation is finished
 so the user can clear the text field and start typing right away.
 */
@property (nonatomic) BOOL didPresentFromSourceLocation;

@property (weak, nonatomic) id<newLocationControllerDelegate> _Nullable delegate;

+(instancetype _Nonnull)initController;

-(IBAction) closeButtonPressed: (UIButton * _Nonnull) sender;
-(IBAction) findRouteButtonPressed: (UIButton * _Nonnull) sender;
/*
@property(strong,nonatomic)NSString *tagIdentifier;
@property (weak, nonatomic) IBOutlet GMSMapView *Mapview;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;

@property (weak, nonatomic) NSString * AddressType;
@property(strong,nonatomic) NSDictionary *identifierDict;
@property(nonatomic) int  AddressIdentifier;
@property (weak, nonatomic) IBOutlet UIButton *sourceBtn;
@property (weak, nonatomic) IBOutlet UIButton *destinationBtn;
@property (weak, nonatomic) IBOutlet UITableView *favpurteTable;
@property(strong,nonatomic)NSMutableArray *FavArray,* recentArray;
@property (weak, nonatomic) IBOutlet UIButton *destinationClearBtn;
@property (weak, nonatomic) IBOutlet UIButton *sourceBtnClearbTn;
*/

#pragma ConstrainsUpdate

/*
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sourceTopConstraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sourceBtnTop;
*/

#pragma Actions

/*
- (IBAction)doneBtnDidTab:(UIButton *)sender;
- (IBAction)dropLocationMarkerAction:(UIButton *)sender;
- (IBAction)clearBtnAction:(UIButton *)sender;
- (IBAction)locatinBtnAction:(UIButton *)sender;*/



@end
#endif
