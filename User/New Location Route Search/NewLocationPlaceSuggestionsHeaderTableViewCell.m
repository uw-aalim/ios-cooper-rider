//
//  NewLocationPlaceSuggestionsHeaderTableViewCell.m
//  User
//
//  Created by Benjamin Cortens on 2018-06-25.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import "NewLocationPlaceSuggestionsHeaderTableViewCell.h"

@implementation NewLocationPlaceSuggestionsHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
