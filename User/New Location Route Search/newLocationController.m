//
//  newLocationController.m
//  User
//
//  Created by CSS on 19/01/18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import "newLocationController.h"
#import "NewLocationTableViewController.h"
#import "User-Swift.h"
/*
@implementation FavourateCell

-(void)awakeFromNib{
    [super awakeFromNib];
    
}

@end

@implementation SetpinCell

-(void)awakeFromNib{
    [super awakeFromNib];
}
@end
*/

/*
@interface newLocationController ()<GMSAutocompleteViewControllerDelegate>
//@property CLLocationManager * location;
@end */

@interface newLocationController()<GooglePlacesSearchHelperDelegate,NewLocationTableViewControllerDelegate> {
	NewLocationTableViewController *tableViewController;
	GooglePlacesSearchHelper *searchPlacesHelper;
	BOOL searchingOrigin;
}
@end

@implementation newLocationController

+(instancetype)initController {
	return [[newLocationController alloc] initWithNibName:@"newLocationController" bundle:NULL];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	tableViewController = [[NewLocationTableViewController alloc]initWithTableView:self.tableView];
	tableViewController.delegate = self;
	
	appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	searchPlacesHelper = [[GooglePlacesSearchHelper alloc]init];
	searchPlacesHelper.delegate = self;
	
	self.originTextField.delegate = self;
	self.destinationTextField.delegate = self;
	
	//searchPlacesHelper.delegate = self
	/*
    isBack = YES;
    _location = [[CLLocationManager alloc] init];
    self.location.desiredAccuracy = kCLLocationAccuracyBest;
    self.location.distanceFilter = kCLDistanceFilterNone;
    self.location.delegate = self;
    [_location requestWhenInUseAuthorization];
    [_location startUpdatingLocation];

    if ([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                             message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
            [alert show];
        }
    }
    _favpurteTable.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
	 
    [self onGetAddress];
    
    if([_tagIdentifier isEqualToString:@"Destination"]){
        _AddressIdentifier = 222;
        [KeyConstants sharedKeyConstants].DestinationInfo = nil;
    }else{
        _AddressIdentifier = 111;

        [KeyConstants sharedKeyConstants].LocationInfo = nil;
    }
	
	
	NSLog(@"Should set title %@",[KeyConstants sharedKeyConstants].LocationInfo[SourceLocations]);
	NSLog(@"Should set title with key constants %@",[KeyConstants sharedKeyConstants]);
    [_sourceBtn setTitle:[KeyConstants sharedKeyConstants].LocationInfo[SourceLocations] forState:UIControlStateNormal];
    if([KeyConstants CheckKeyExcistswithDict:[KeyConstants sharedKeyConstants].DestinationInfo withKey:DestinationLocations]){
        [_destinationBtn setTitle:[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLocations] forState:UIControlStateNormal];

    }
	
	if (self.sourceLocation != NULL) {
		[_sourceBtn setTitle: self.sourceLocation forState:UIControlStateNormal];
	}
	
    [self.view sendSubviewToBack:_Mapview];
    [self.view sendSubviewToBack:_doneBtn];
  
//    if([_identifierDict[@"name"] isEqualToString:@"No Destination"]){
//        _destinationClearBtn.hidden = YES;
//        _sourceBtnTop.constant += 25;
//        _destinationBtn.hidden = YES;
//    }else{
//        _destinationClearBtn.hidden = NO;
//        _sourceBtnTop.constant += 0;
//        _destinationBtn.hidden = NO;
//    }

    // Do any additional setup after loading the view.
	 */
	
	if (self.sourceLocation != NULL) {
		self.originTextField.text = self.sourceLocation;
	}
	if (self.destinationLocation != NULL) {
		self.destinationTextField.text = self.destinationLocation;
	}
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    //_doneBtn.backgroundColor = BASE1Color;
}

-(void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	[[NSNotificationCenter defaultCenter]addObserver:self
											selector:@selector(keyboardWillShow:)
												name:UIKeyboardWillShowNotification
											  object:NULL];
	[[NSNotificationCenter defaultCenter]addObserver:self
											selector:@selector(keyboardWillHide:)
												name:UIKeyboardWillHideNotification
											  object:NULL];
	
	if (self.didPresentFromSourceLocation == TRUE) {
		[self.originTextField becomeFirstResponder];
		searchingOrigin = TRUE;
	}
	else {
		[self.destinationTextField becomeFirstResponder];
		searchingOrigin = FALSE;
	}
}

-(void) viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	[[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma GetSavedAddress

-(void)onGetAddress {
    if ([appDelegate internetConnected])
    {
		NSLog(@"newLocationController - onGetAddress");
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [appDelegate onStartLoader];
        [afn getDataFromPath:@"/api/user/location" withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
			[self->appDelegate onEndLoader];
            if (response) {
                [self funcCheckAddressAvailable:response];
            }
			else {
                if ([errorcode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                   // [self logoutMethod];
                    
                    //                    [CommenMethods onRefreshToken];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
                    //
                    //                    PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
                    //                    [self.navigationController pushViewController:wallet animated:YES];
                }
            }
            
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
    
    
}




//	MARK:- Action Responders
- (IBAction) closeButtonPressed: (UIButton * _Nonnull) sender {
	[self dismissViewControllerAnimated:true completion:NULL];
}

-(IBAction) findRouteButtonPressed: (UIButton * _Nonnull) sender {
	if (self.sourceLocation != NULL && self.destinationLocation != NULL) {
		[self.delegate didChooseNewOrigin:self.sourceCoordinate
						withOriginAddress:self.sourceLocation
						  withDestination:self.destinationCoordinate
				   withDestinationAddress:self.destinationLocation];
		[self dismissViewControllerAnimated:true completion:NULL];
	}
	else {
		
	}
}



//	MARK:- TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	[tableViewController updateWithSearchResults:[[NSArray alloc]init]];
	[tableViewController updateWithSearchSuggestions:NULL];
	if ([self.originTextField isFirstResponder]) {
		searchingOrigin = TRUE;
	}
	else {
		searchingOrigin = FALSE;
	}
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	NSString *newTextFieldText = [textField.text stringByReplacingCharactersInRange:range withString:string];
	
	if (newTextFieldText.length > 0) {
		//[searchPlacesHelper performPlaceSearch
		[searchPlacesHelper performPlaceSearchWithTextString:newTextFieldText isSearch:TRUE];
		[searchPlacesHelper searchSuggestionsWithTextString:newTextFieldText];
		//[searchPlacesHelper ]ant
	}
	
	return true;
}


-(BOOL)textFieldShouldClear:(UITextField *)textField {
	[tableViewController updateWithSearchResults:[[NSArray alloc]init]];
	return true;
}


//	MARK:- Google Place Search Helper Delegate

-(void)foundPlacesWithPlaces:(NSArray<GooglePlaceObject *> *)placeArray forPlaceType:(NSString *)placeType sender:(id)sender {
	NSLog(@"Found Places with Places - %lu", (unsigned long)placeArray.count);
	[tableViewController updateWithSearchResults:placeArray];
}

-(void)foundSuggestionsWithPlaces:(NSArray<GooglePlaceObject *> *)placeArray sender:(id)sender {
	[tableViewController updateWithSearchSuggestions:placeArray];
}

-(void)determinedAuthorizationWithAuthorizationStatus:(CLAuthorizationStatus)status sender:(id)sender {
	
}

-(void)determinedLocation:(id)sender {
	
}



//	MARK:- NewLocationTableViewControllerDelegate
		 
- (void)didSelectCellWithAddress:(NSString *)addressString
				   andCoordinate: (CLLocationCoordinate2D) coordinate
					   andSender:(id)sender {
	
	if (searchingOrigin) {
		[self.originTextField resignFirstResponder];
		[tableViewController updateWithSearchResults:NULL];
		[tableViewController updateWithSearchSuggestions:NULL];
		self.originTextField.text = addressString;
		self.sourceCoordinate = coordinate;
		self.sourceLocation = addressString;
	}
	else {
		[self.destinationTextField resignFirstResponder];
		[tableViewController updateWithSearchResults:NULL];
		[tableViewController updateWithSearchSuggestions:NULL];
		self.destinationTextField.text = addressString;
		self.destinationCoordinate = coordinate;
		self.destinationLocation = addressString;
	}
}


//	MARK:- Keyboard Notificaiton Responders

-(void) keyboardWillShow: (NSNotification *) notification {
	CGRect keyboardFrame = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	double keyboardDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	
	self.bottomConstraint.constant = keyboardFrame.size.height;
	NSLog(@"Keyboard will show - Should update bottom constraint %f",keyboardFrame.size.height);
	NSLog(@"Keyboard will show - Should update bottom constraint %f",self.bottomConstraint.constant);
	[self.view setNeedsLayout];
	[UIView animateWithDuration:keyboardDuration animations:^{
		[self.view layoutIfNeeded];
	}];
}


-(void) keyboardWillHide: (NSNotification *) notification {
	
	double keyboardDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	
	self.bottomConstraint.constant = 0.0;
	NSLog(@"Keyboard will hide - Should update bottom constraint");
	[self.view setNeedsLayout];
	[UIView animateWithDuration:keyboardDuration animations:^{
		[self.view layoutIfNeeded];
	}];
}













-(void)funcCheckAddressAvailable:(NSDictionary *)AddressDict{
	/*
    _FavArray = [[NSMutableArray alloc]init];
    _recentArray = [[NSMutableArray alloc]init];
    NSDictionary * homeAddress = @{
                                   @"address" : @"Add your home address",
                                   @"id" : @0,
                                   @"latitude" : @"13.99",
                                   @"longitude" : @"80.3",
                                   @"type" : @"home"
                                   };
    
    NSDictionary * WorkAddress = @{
                                   @"address" : @"Add your work address",
                                   @"id" : @0,
                                   @"latitude" : @"13.99",
                                   @"longitude" : @"80.3",
                                   @"type" : @"work"
                                   };
    
    
    if([AddressDict[@"home"] count]>0){
        [_FavArray addObject:AddressDict[@"home"][0]];
    }else{
        [_FavArray addObject:homeAddress];
    }
    
    if([AddressDict[@"work"] count] >0){
        [_FavArray addObject:AddressDict[@"work"][0]];
    }else{
        [_FavArray addObject:WorkAddress];
        
    }
    [_recentArray addObjectsFromArray:AddressDict[@"recent"]];
    [_favpurteTable reloadData];*/
}



#pragma Tableview Delegate and Datasource
/*
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return _FavArray.count;
    }else if (section == 1){
        return _recentArray.count;
    }else{
        return 1;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if(indexPath.section == 0){
        FavourateCell * cell = [tableView dequeueReusableCellWithIdentifier:@"FavourateCell" forIndexPath:indexPath];
        cell.titleLbl.text = [_FavArray[indexPath.row][@"type"] uppercaseString];
        cell.addressLbl.text =_FavArray[indexPath.row][@"address"];
        
        if([_FavArray[indexPath.row][@"type"]isEqualToString:@"home"]){
            cell.picImageview.image = [UIImage imageNamed:@"home"];
        }else{
            cell.picImageview.image = [UIImage imageNamed:@"work"];
            
        }
        return cell;
    }else if (indexPath.section == 1){
        FavourateCell * cell = [tableView dequeueReusableCellWithIdentifier:@"FavourateCell" forIndexPath:indexPath];
        cell.picImageview.image = [UIImage imageNamed:@"recent"];
        cell.titleLbl.text = [_recentArray[indexPath.row][@"type"] uppercaseString];
        cell.addressLbl.text =_recentArray[indexPath.row][@"address"];
        return cell;
    }else{
        SetpinCell * cell = [tableView dequeueReusableCellWithIdentifier:@"SetpinCell" forIndexPath:indexPath];
        [cell.setPinBtn addTarget:self action:@selector(dropLocationMarkerAction) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
   
   
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [KeyConstants sharedKeyConstants].RideUpdateStatus = @"";

    if(indexPath.section == 0){
        if([_FavArray[indexPath.row][@"address"] isEqualToString:@"Add your work address"] || [_FavArray[indexPath.row][@"address"] isEqualToString:@"Add your home address"]){
            _AddressType = _FavArray[indexPath.row][@"type"];
            GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
            acController.delegate = self;
            [self presentViewController:acController animated:YES completion:nil];
            
        }else{
            if([_tagIdentifier isEqualToString:@"Destination"]){
                [KeyConstants sharedKeyConstants].DestinationInfo = @{
                                                             DestinationLat :_FavArray[indexPath.row][@"latitude"],
                                                             DestinationLang :_FavArray[indexPath.row][@"longitude"],
                                                             DestinationLocations : _FavArray[indexPath.row][@"address"],
                                                             };
                
                
                [_destinationBtn setTitle:_FavArray[indexPath.row][@"address"] forState:UIControlStateNormal];
            }else{
                
                [KeyConstants sharedKeyConstants].LocationInfo = @{
                                                          sourceLat :_FavArray[indexPath.row][@"latitude"],
                                                          sourceLang :_FavArray[indexPath.row][@"longitude"],
                                                          SourceLocations : _FavArray[indexPath.row][@"address"],
                                                          };
                [_sourceBtn setTitle:_FavArray[indexPath.row][@"address"] forState:UIControlStateNormal];
            }
            NSLog(@"%@",[KeyConstants sharedKeyConstants].LocationInfo);
            [KeyConstants sharedKeyConstants].RideStatus = @"PAYMENT";
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }else if(indexPath.section == 1){
       
            if([_tagIdentifier isEqualToString:@"Destination"]){
                [KeyConstants sharedKeyConstants].DestinationInfo = @{
                                                             DestinationLat :_recentArray[indexPath.row][@"latitude"],
                                                             DestinationLang :_recentArray[indexPath.row][@"longitude"],
                                                             DestinationLocations : _recentArray[indexPath.row][@"address"],
                                                             };
                
                
                [_destinationBtn setTitle:_recentArray[indexPath.row][@"address"] forState:UIControlStateNormal];
            }else{
                
                
                    [KeyConstants sharedKeyConstants].LocationInfo = @{
                                                          sourceLat :_recentArray[indexPath.row][@"latitude"],
                                                          sourceLang :_recentArray[indexPath.row][@"longitude"],
                                                          SourceLocations : _recentArray[indexPath.row][@"address"],
                                                          };
                [_sourceBtn setTitle:_recentArray[indexPath.row][@"address"] forState:UIControlStateNormal];
            }
            [KeyConstants sharedKeyConstants].RideStatus = @"PAYMENT";
            [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    

   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
 */

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
- (IBAction)doneBtnDidTab:(UIButton *)sender {

    [KeyConstants sharedKeyConstants].RideUpdateStatus = @"";
    NSLog(@"%f",lastCameraPosition.target.latitude);
    NSLog(@"%@",changeLocationsAddress);
 if(_AddressIdentifier != 111){
    [KeyConstants sharedKeyConstants].DestinationInfo = @{
                                                 DestinationLat :[NSString stringWithFormat:@"%f", lastCameraPosition.target.latitude],
                                                 DestinationLang :[NSString stringWithFormat:@"%f",lastCameraPosition.target.longitude],
                                                 DestinationLocations : changeLocationsAddress,
                                                 
                                                 };
 } else{
     [KeyConstants sharedKeyConstants].LocationInfo = @{
                                                  sourceLat :[NSString stringWithFormat:@"%f", lastCameraPosition.target.latitude],
                                                  sourceLang :[NSString stringWithFormat:@"%f",lastCameraPosition.target.longitude],
                                                  SourceLocations : changeLocationsAddress,
                                                  currentLocations : changeLocationsAddress,
                                                  
                                                  };
 }
   NSLog(@"%@",[KeyConstants sharedKeyConstants].DestinationInfo);
    if([KeyConstants sharedKeyConstants].LocationInfo !=nil){
        NSLog(@"%@",[KeyConstants sharedKeyConstants].LocationInfo);
        [KeyConstants sharedKeyConstants].RideStatus = @"PAYMENT";
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)dropLocationMarkerAction{
    isBack = NO;
    [self.view bringSubviewToFront:_Mapview];
    [self.view bringSubviewToFront:_doneBtn];
    [self.view sendSubviewToBack:_favpurteTable];
    
    _Mapview.myLocationEnabled = YES;
    _Mapview.delegate=self;
    NSError *error;
    NSURL *url1 =[[NSBundle mainBundle] URLForResource:@"map_style" withExtension:@"json"];
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:url1 error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    _Mapview.mapStyle = style;
    
    CGRect visibleRect = CGRectMake(_Mapview.frame.origin.x, _Mapview.frame.origin.x, _Mapview.frame.size.width, _Mapview.frame.size.height);
    CGPoint centerPoint = CGPointMake(visibleRect.size.width/2, visibleRect.size.height/2-31);
    
    NSLog(@"CenterPoint...%f %f", centerPoint.x, centerPoint.y);
    
    
    UIButton *markerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    markerBtn.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, 33, 48);
    markerBtn.backgroundColor = [UIColor clearColor];
    UIImage *buttonImage = [UIImage imageNamed:@"MoveMapMarker"];
    [markerBtn setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [markerBtn setCenter:centerPoint];
    [_Mapview addSubview:markerBtn];
    
    CLLocationCoordinate2D coor = [_Mapview.projection coordinateForPoint:centerPoint];
    
    NSLog(@"coor.Location: %@", [NSString stringWithFormat:@"%.8f", coor.latitude]);
    NSLog(@"coor.Location: %@", [NSString stringWithFormat:@"%.8f", coor.longitude]);
    
    
    NSLog(@"myLocation: %@", [NSString stringWithFormat:@"%.8f", CurrentUpdateLocation.coordinate.latitude]);
    NSLog(@"myLocation: %@", [NSString stringWithFormat:@"%.8f", CurrentUpdateLocation.coordinate.longitude]);
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:CurrentUpdateLocation.coordinate.latitude longitude:CurrentUpdateLocation.coordinate.longitude zoom:14];
    
    [_Mapview animateToCameraPosition:camera];
}

#pragma Map Load for start and update Locations
-(void)loadMapview: (double)lat :(double)lang{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                            longitude:lang
                                                                 zoom:12];
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"map_style" withExtension:@"json"];
    NSError *error;
    
    // Set the map style by passing the URL for style.json.
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    _Mapview.mapStyle = style;
    _Mapview.myLocationEnabled = YES;
    _Mapview.camera = camera;
    [_Mapview animateToCameraPosition:camera];
}

- (IBAction)clearBtnAction:(UIButton *)sender {
    if(sender.tag == 111){
        [_sourceBtn setTitle:@"Where from?" forState:UIControlStateNormal];
    }else{
        [_destinationBtn setTitle:@"Where to?" forState:UIControlStateNormal];
    }
}

- (void)mapView:(GMSMapView* )mapView idleAtCameraPosition:(GMSCameraPosition* )position
{
    lastCameraPosition = nil; // reset pin moving, no ice skating pins ;)
    
    lastCameraPosition = position;
    newCoords = CLLocationCoordinate2DMake(position.target.latitude, position.target.longitude);
    
    changeLocationsAddress = [self getAddressFromLatLon:[[NSString stringWithFormat:@"%f", newCoords.latitude] doubleValue] withLongitude:[[NSString stringWithFormat:@"%f", newCoords.longitude] doubleValue]];
    
    return;
    
}

-(NSString *)getAddressFromLatLon:(double)pdblLatitude withLongitude:(double)pdblLongitude
{
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:pdblLatitude longitude:pdblLongitude];
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  if (placemark)
                  {
                      NSLog(@"placemark %@",placemark);
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      NSLog(@"addressDictionary %@", placemark.addressDictionary);
                      
                      NSLog(@"placemark %@",placemark.region);
                      NSLog(@"placemark %@",placemark.country);  // Give Country Name
                      NSLog(@"placemark %@",placemark.locality); // Extract the city name
                      NSLog(@"location %@",placemark.name);
                      NSLog(@"location %@",placemark.ocean);
                      NSLog(@"location %@",placemark.postalCode);
                      NSLog(@"location %@",placemark.subLocality);
                      
                      NSLog(@"location %@",placemark.location);
                      //Print the location to console
                      locationString = locatedAt;
                  }
                  else {
                      NSLog(@"Could not locate");
                      locationString = @"";
                  }
              }
     ];
    return locationString;
}

- (IBAction)locatinBtnAction:(UIButton *)sender {
	
	NSLog(@"newLocationController - locationBtnAction");
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    _AddressIdentifier = sender.tag;
    [self presentViewController:acController animated:YES completion:nil];
}


#pragma mark Get placePicker Delegate and Datasource

// Handle the user's selection.
- (void)viewController: (GMSAutocompleteViewController *) viewController didAutocompleteWithPlace:(GMSPlace *)place {
	
	//	Dismiss the GMSAutocompleteViewController
	[self dismissViewControllerAnimated:YES completion:nil];
	
	//	Set the ride update status to blank
    [KeyConstants sharedKeyConstants].RideUpdateStatus = @"";

    if([_AddressType isEqualToString:@"home"] ||
	   [_AddressType isEqualToString:@"work"]) {
		
		//	Teh selected place is either home or work
		[self SelectedAddressAction:place];
    }
	else {
        // Do something with the selected place.
		if (_AddressIdentifier == 111) {
			
			//	If we returned from tapping the starting locaiton area - update LocationInfo (which is our current location analogue) in KeyConstants
			[KeyConstants sharedKeyConstants].LocationInfo = @{
													  sourceLat :[NSString stringWithFormat:@"%f", place.coordinate.latitude],
													  sourceLang :[NSString stringWithFormat:@"%f",place.coordinate.longitude],
													  SourceLocations : place.formattedAddress,
													  
													  };
			[_sourceBtn setTitle:place.formattedAddress forState:UIControlStateNormal];
			
		}
		else {
			
			//	If we returned from tapping the destination locaiton area - update DestinationInfo (which is our current location analogue) in KeyConstants
			[KeyConstants sharedKeyConstants].DestinationInfo = @{
														 DestinationLat :[NSString stringWithFormat:@"%f", place.coordinate.latitude],
														 DestinationLang :[NSString stringWithFormat:@"%f",place.coordinate.longitude],
														 DestinationLocations : place.formattedAddress,
														 };
			[_destinationBtn setTitle:place.formattedAddress forState:UIControlStateNormal];
		}
		NSLog(@"newLocationController - didAutocompleteWithPlace - current location %@",[KeyConstants sharedKeyConstants].LocationInfo);
		NSLog(@"newLocationController - didAutocompleteWithPlace - destination location %@",[KeyConstants sharedKeyConstants].DestinationInfo);
		//    if(![_tagIdentifier isEqualToString:@"Destination"]){
		
		if([KeyConstants sharedKeyConstants].LocationInfo != nil &&
		   [KeyConstants sharedKeyConstants].DestinationInfo != nil) {
			
			//	There is both a source and a destination - should dismiss this view controller and set the current status to Payment - which corresponds to needing payment
			NSLog(@"newLocationController - set start and end points - dismiss view controller");
			
			[KeyConstants sharedKeyConstants].RideStatus = @"PAYMENT";
			NSString *originLatitude = [KeyConstants sharedKeyConstants].LocationInfo[sourceLat];
			NSString *originLongitude = [KeyConstants sharedKeyConstants].LocationInfo[sourceLang];
			NSString *originAddress = [KeyConstants sharedKeyConstants].LocationInfo[SourceLocations];
			CLLocationCoordinate2D origin = CLLocationCoordinate2DMake(originLatitude.doubleValue, originLongitude.doubleValue);
			NSString *destinationLatitude = [KeyConstants sharedKeyConstants].DestinationInfo[DestinationLat];
			NSString *destinationLongitude = [KeyConstants sharedKeyConstants].DestinationInfo[DestinationLang];
			NSString *destinationAddress = [KeyConstants sharedKeyConstants].DestinationInfo[DestinationLocations];
			CLLocationCoordinate2D destination = CLLocationCoordinate2DMake(destinationLatitude.doubleValue, destinationLongitude.doubleValue);
												  

			[self.delegate didChooseNewOrigin:origin
							withOriginAddress:originAddress
							  withDestination:destination
					   withDestinationAddress:destinationAddress];
			
			[self dismissViewControllerAnimated:YES completion:nil];
		}
		//    }else{
		//
		//             [KeyConstants sharedKeyConstants].RideStatus = @"PAYMENT";
		//            [self dismissViewControllerAnimated:YES completion:nil];
		//        }
		
	}
    
    
}

-(void)SelectedAddressAction :(GMSPlace *)place {
	
	NSLog(@"newLocationController - SelectedAddressAction");
	
	NSDictionary * params =@{@"type":_AddressType,@"address":place.formattedAddress,@"latitude" :[NSString stringWithFormat:@"%f",place.coordinate.latitude],@"longitude":[NSString stringWithFormat:@"%f",place.coordinate.longitude]};
    [appDelegate onStartLoader];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    if([appDelegate internetConnected]){
        
        [afn getDataFromPath:@"/api/user/location" withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            [appDelegate onEndLoader];
            if(error == nil){
                if(response){
                    [CSS_Class alertviewController_title:@"Alert" MessageAlert:@"Address added Successfully" viewController:self okPop:NO];
                    [self onGetAddress];
                }
            }else{
                if ([errorcode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                    [self logoutMethod];
                }
            }
         }];
    
         }else{
        
         }
    
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


-(void)logoutMethod
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn refreshMethod_NoLoader:MD_REFRESH_TOKEN withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
                [user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
                [user setValue:response[@"refresh_token"] forKey:UD_REFERSH_TOKEN];
            }
            else
            {
                [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}

#pragma Location update Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    NSLog(@"called didUpdateLocations");
  
    CurrentUpdateLocation = [locations lastObject];
    
    [geocoder reverseGeocodeLocation:CurrentUpdateLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0)
        {
            CLPlacemark *placemark = [placemarks lastObject];
            NSString * currentAddress = [Utilities removeNullFromString: [NSString stringWithFormat:@"%@,%@,%@",placemark.name,placemark.locality,placemark.subAdministrativeArea]];
            NSLog(@"Placemark %@",currentAddress);
            
            [self loadMapview:CurrentUpdateLocation.coordinate.latitude :CurrentUpdateLocation.coordinate.longitude];
//            [KeyConstants sharedKeyConstants].DestinationInfo = @{
//                                                      DestinationLocations : currentAddress,
//                                                      DestinationLat : [NSString stringWithFormat:@"%f",CurrentUpdateLocation.coordinate.latitude],
//                                                      DestinationLang:[NSString stringWithFormat:@"%f",CurrentUpdateLocation.coordinate.longitude],
//
//                                                      };//Location Update to server
        }
        else
        {
            NSLog(@"%@", error.debugDescription);
        }
    } ];
    
}

- (IBAction)backBtnAction:(id)sender {
    
    if(isBack){
         [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        isBack = YES;
        [self.view sendSubviewToBack:_Mapview];
        [self.view sendSubviewToBack:_doneBtn];
    }
   
}
*/



@end
