//
//  NewLocationTableViewController.m
//  User
//
//  Created by Benjamin Cortens on 2018-06-25.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import "NewLocationTableViewController.h"
#import "NewLocationPlaceTableViewCell.h"
#import "NewLocationPlaceSuggestionsHeaderTableViewCell.h"
#import "User-Swift.h"

@interface NewLocationTableViewController() {
	UITableView *tableView;
	NSArray *favouritePlaces;
	NSArray *places;
	NSArray *searchResults;
	NSArray *searchSuggestions;
	BOOL isSearching;
}
@end

@implementation NewLocationTableViewController

-(id)initWithTableView: (UITableView *) newTableView {
	self =  [super init];
	
	tableView = newTableView;
	
	//[tableView registerClass:[NewLocationPlaceTableViewCell class]
	  //forCellReuseIdentifier:@"NewLocationPlaceTableViewCell"];
	//UINib *nib =
	[tableView registerNib:[UINib nibWithNibName:@"NewLocationPlaceTableViewCell" bundle:NULL]
	forCellReuseIdentifier:@"NewLocationPlaceTableViewCell"];
	[tableView registerNib:[UINib nibWithNibName:@"NewLocationPlaceSuggestionsHeaderTableViewCell" bundle:NULL]
	forCellReuseIdentifier:@"NewLocationPlaceSuggestionsHeaderTableViewCell"];
	tableView.delegate = self;
	tableView.dataSource = self;
	
	isSearching = FALSE;
	
	return self;
}

-(void)updateWithPlaces: (NSArray *) newPlaces {
	places = newPlaces;
	[tableView reloadData];
}


-(void)updateWithSearchResults: (NSArray * _Nullable) newSearchResults {
	
	//NSLog(@"Should update search results");
	if (newSearchResults == NULL) {
		isSearching = FALSE;
	}
	else {
		isSearching = TRUE;
	}
	
	searchResults = newSearchResults;
	
	NSLog(@"Should update search results %d",isSearching);
	NSLog(@"Should update search results %lu",(unsigned long)searchResults.count);
	
	[tableView reloadData];
}


-(void)updateWithSearchSuggestions: (NSArray * _Nullable) newSuggestions {
	
	searchSuggestions = newSuggestions;
	[tableView reloadData];
}




//	MARK:- Table View Delegate and Source Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	if (isSearching == TRUE) {
		NSLog(@"numberOfSectionsInTableView is 1");
		return 2;
	}
	else {
		return 3;
	}
}

/**
 If the user is not searching then:
 Section 0 corresponds to the favourite items.
 Section 1 corresponds to the set pin item
 Section 2 corresponds to recent items
 
 If the user is searching then there is only 1 section and all items are found in searchResults
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	if (isSearching == TRUE) {
		NSLog(@"numberOfRowsInSection is %lu",(unsigned long)searchResults.count);
		if (section == 0) {
			if (searchResults != NULL) {
				return searchResults.count;
			}
			else {
				return 0;
			}
		}
		else {
			if (searchSuggestions != NULL) {
				return searchSuggestions.count + 1;
			}
			else {
				return 1;
			}
		}
	}
	else {
		if (section == 0) {
			if (favouritePlaces != NULL) {
				return favouritePlaces.count;
			}
			else {
				return 0;
			}
		}
		else if (section == 1) {
			//	TODO: Add support for dropping a pin on the map.
			return 0;
		}
		else if (section >= 2) {
			if (places != NULL) {
				return places.count;
			}
			else {
				return 0;
			}
		}
		else {
			return 0;
		}
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (isSearching == TRUE) {
		if (indexPath.section == 0) {
			NewLocationPlaceTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NewLocationPlaceTableViewCell" forIndexPath:indexPath];
			GooglePlaceObject *currentPlace = [searchResults objectAtIndex:indexPath.row];
			[cell configureWithAddress:currentPlace.address
						favouriteImage:NULL
						 favouriteName:NULL];
			NSLog(@"cell is %@",cell);
			for (UIView *subview in cell.subviews) {
				NSLog(@"cell subviews is %@",subview);
			}
			NSLog(@"cell in section has address is %@",currentPlace.address);
			return cell;
		}
		else {
			if (indexPath.row == 0) {
				NewLocationPlaceSuggestionsHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewLocationPlaceSuggestionsHeaderTableViewCell" forIndexPath:indexPath];
				return cell;
			}
			else {
				NewLocationPlaceTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NewLocationPlaceTableViewCell" forIndexPath:indexPath];
				GooglePlaceObject *currentPlace = [searchSuggestions objectAtIndex:(indexPath.row - 1)];
				[cell configureWithAddress:currentPlace.address
							favouriteImage:NULL
							 favouriteName:NULL];
				NSLog(@"cell is %@",cell);
				for (UIView *subview in cell.subviews) {
					NSLog(@"cell subviews is %@",subview);
				}
				NSLog(@"cell in section has address is %@",currentPlace.address);
				return cell;
			}
		}
	}
	else {
		if (indexPath.section == 0) {
			NewLocationPlaceTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NewLocationPlaceTableViewCell" forIndexPath:indexPath];
			/*
			 cell.titleLbl.text = [_FavArray[indexPath.row][@"type"] uppercaseString];
			 cell.addressLbl.text =_FavArray[indexPath.row][@"address"];
			 
			 if([_FavArray[indexPath.row][@"type"]isEqualToString:@"home"]){
			 cell.picImageview.image = [UIImage imageNamed:@"home"];
			 }else{
			 cell.picImageview.image = [UIImage imageNamed:@"work"];
			 
			 }
			 */
			return cell;
		}
		else if (indexPath.section >= 2) {
			NewLocationPlaceTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NewLocationPlaceTableViewCell" forIndexPath:indexPath];
			
			GooglePlaceObject *currentPlace = [places objectAtIndex:indexPath.row];
			[cell configureWithAddress:currentPlace.address
						favouriteImage:NULL
						 favouriteName:NULL];
			/*FavourateCell * cell = [tableView dequeueReusableCellWithIdentifier:@"FavourateCell" forIndexPath:indexPath];
			 cell.picImageview.image = [UIImage imageNamed:@"recent"];
			 cell.titleLbl.text = [_recentArray[indexPath.row][@"type"] uppercaseString];
			 cell.addressLbl.text =_recentArray[indexPath.row][@"address"];*/
			return cell;
		}
		else {
			/*SetpinCell * cell = [tableView dequeueReusableCellWithIdentifier:@"SetpinCell" forIndexPath:indexPath];
			 [cell.setPinBtn addTarget:self action:@selector(dropLocationMarkerAction) forControlEvents:UIControlEventTouchUpInside];
			 return cell;
			 */
			NewLocationPlaceTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NewLocationPlaceTableViewCell" forIndexPath:indexPath];
			return cell;
		}
	}	
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (isSearching == TRUE) {
		if (indexPath.section == 0) {
			GooglePlaceObject *currentPlace = [searchResults objectAtIndex:indexPath.row];
			if (currentPlace.latitude != NULL && currentPlace.longitude != NULL && currentPlace.address != NULL) {
				CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(currentPlace.latitude.doubleValue, currentPlace.longitude.doubleValue);
				[self.delegate didSelectCellWithAddress:currentPlace.address andCoordinate: coordinate  andSender:self];
			}
		}
		else {
			
			GooglePlaceObject *currentPlace = [searchSuggestions objectAtIndex:(indexPath.row - 1)];
			NSLog(@"didSelect row at index path %ld and plcae %@",(long)indexPath.row,currentPlace);
			NSLog(@"didSelect row at gms place %@",currentPlace.gmsPlace);
			NSLog(@"didSelect row at gms place %@",currentPlace.gmsPlace.formattedAddress);
			NSLog(@"didSelect row at gms place %@",currentPlace.gmsPlace.name);
			if (currentPlace.gmsPlace != NULL && currentPlace.gmsPlace.formattedAddress != NULL) {
				NSLog(@"Delegate did select place");
				[self.delegate didSelectCellWithAddress:currentPlace.gmsPlace.formattedAddress andCoordinate: currentPlace.gmsPlace.coordinate andSender:self];
			}
		}		
	}
	else {
		if (indexPath.section == 0) {
			
		}
		else if (indexPath.section >= 2) {
			GooglePlaceObject *currentPlace = [places objectAtIndex:indexPath.row];
			if (currentPlace.latitude != NULL && currentPlace.longitude != NULL && currentPlace.address != NULL) {
				CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(currentPlace.latitude.doubleValue, currentPlace.longitude.doubleValue);
				[self.delegate didSelectCellWithAddress:currentPlace.address andCoordinate: coordinate  andSender:self];
			}
		}
		else {
			
		}
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (isSearching && indexPath.section == 1 && indexPath.row == 0) {
		return 44.0;
	}
	else {
		return 64.0;
	}
}

@end
