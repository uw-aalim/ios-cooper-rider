//
//  RoutingAndMapServices.h
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import <CoreLocation/CoreLocation.h>

@class RouteEndPoint;
@class Ride;
@class ActiveRideResponse;

@interface RoutingAndMapServices : NSObject {
	
	GMSPlacesClient *_placesClient;
	CLGeocoder *geocoder;
}


+ (_Nonnull instancetype) sharedInstance;

@property (nonatomic, nullable) RouteEndPoint *currentAddress;
@property (nonatomic, nullable) RouteEndPoint *originPoint;
@property (nonatomic, nullable) RouteEndPoint *destinationPoint;

-(void) getCurrentLocationPlace: (CLLocation * _Nonnull) location;
-(void) getAvailableServices;

-(void) estimateFareWithRide: (Ride * _Nonnull) ride
			 withCompletion:(void (^ _Nullable) (NSString  * _Nullable error))completion;
-(void) bookNewRide: (Ride *_Nonnull) ride
	 withCompletion:(void (^_Nullable) (NSString  * _Nullable error))completion;
-(void) checkRideServiceForActiveRideWithCompletion:(void (^_Nullable) (NSString  * _Nullable error, ActiveRideResponse * _Nullable activeRide))completion;
-(void) cancelRideServiceWithCompletion:(void (^_Nullable) (NSString  * _Nullable error, NSString * _Nullable success))completion;
-(void) rateDriver: (NSString * _Nullable) rate
           comment: (NSString * _Nullable) comment
    WithCompletion:(RequestCompletionBlock _Nonnull)completion;

-(void) getAllCardsWith: (void (^_Nullable) (NSString  * _Nullable error, NSArray * _Nullable success))completion;
-(void)getProvidersInCurrentLocation : (NSNumber *)serviceId
                                  lat: (NSNumber *)lat
                                  lng: (NSNumber *)lng
                      withCompletion : (void (^_Nullable) (NSString  * _Nullable error, NSArray * _Nullable success))completion;
@end

