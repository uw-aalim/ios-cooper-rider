//
//  RoutingAndMapServices.m
//  User
//
//  Created by Benjamin Cortens on 2018-06-11.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import "RoutingAndMapServices.h"
#import "Constants.h"
#import "User-Swift.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "AFNetworking.h"
#import "Colors.h"

@interface RoutingAndMapServices () {
	AppDelegate *appDelegate;
	BOOL gettingCurrentLocationPlace;
	BOOL isBookingRide;
}
@end

@implementation RoutingAndMapServices


//	============================================================================================================
//	MARK:- Initialization and Setup
//	============================================================================================================

//	------------------------------------------------------------------------------------------------------------
+ (instancetype)sharedInstance {
	
	static RoutingAndMapServices *sharedInstance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedInstance = [[RoutingAndMapServices alloc] init];
		// Do any other initialisation stuff here
	});
	
	[sharedInstance performSetup];
	
	return sharedInstance;
}
//	------------------------------------------------------------------------------------------------------------

//	------------------------------------------------------------------------------------------------------------
-(void)performSetup {
	geocoder = [[CLGeocoder alloc]init];
	appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	gettingCurrentLocationPlace = FALSE;
	isBookingRide = FALSE;
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void) getCurrentLocationPlace: (CLLocation *) location {
	
	NSLog(@"RoutingAndMapServices Get current locationplace %@ %d",location, gettingCurrentLocationPlace);
	
	http://maps.googleapis.com/maps/api/geocode/json?latlng=19.017615,72.856164&sensor=true
	dispatch_async(dispatch_get_main_queue(), ^{
		
		if (self->gettingCurrentLocationPlace == FALSE) {
			NSLog(@"Should try reverse geocoding");
			self->gettingCurrentLocationPlace = TRUE;
			if (self->geocoder.isGeocoding == FALSE) {
				[self->geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
					
					NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
					self->gettingCurrentLocationPlace = FALSE;
					if (error == nil && [placemarks count] > 0) {
						
						CLPlacemark *placemark = [placemarks lastObject];
						NSString * currentAddressString = [Utilities removeNullFromString: [NSString stringWithFormat:@"%@,%@,%@",placemark.name,placemark.locality,placemark.subAdministrativeArea]];
						NSLog(@"Placemark %@",currentAddressString);
						
						RouteEndPoint *currentLocationPoint = [[RouteEndPoint alloc]
															   initWithLatitude:location.coordinate.latitude
															   withLongitude:location.coordinate.longitude
															   withAddress:currentAddressString];
						
						self.currentAddress = currentLocationPoint;
						//self.originPoint = currentAddress;
						/*
						 RouteEndPoint *origin
						 [KeyConstants sharedKeyConstants].LocationInfo = @{
						 currentLocations : currentAddress,
						 sourceLat : [NSString stringWithFormat:@"%f",CurrentUpdateLocation.coordinate.latitude],
						 sourceLang:[NSString stringWithFormat:@"%f",CurrentUpdateLocation.coordinate.longitude],
						 SourceLocations :currentAddress
						 
						 
						 };//Location Update to server
						 
						 //	Tells the server where the user is after getting their current lcoation place
						 */
						
						[self getAvailableServices];
						
						NSDictionary *params=@{@"latitude":self.currentAddress.latitudeString,
											   @"longitude":self.currentAddress.longitudeString,
											   @"address":self.currentAddress.address};
						
						AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
						[afn getDataFromPath:MD_UPDATELOCATION withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
							if (response) {
							}
						}];
						
						
					}
					else {
						NSLog(@"%@", error.debugDescription);
					}
				} ];
			}
		}
	});
}
//	------------------------------------------------------------------------------------------------------------

-(void)getProvidersInCurrentLocation : (NSNumber *)serviceId
                                  lat: (NSNumber *)lat
                                  lng: (NSNumber *)lng
                      withCompletion : (void (^_Nullable) (NSString  * _Nullable error, NSArray * _Nullable success))completion {
    
    if ([appDelegate internetConnected])
    {
        NSDictionary *params=@{@"latitude":lat,
                               @"longitude":lng,
                               @"service": serviceId};
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:MD_GETPROVIDERS withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSLog(@"GET PROVIDERS");
                
                if([response isKindOfClass:[NSArray class]])
                {
                    if ([response count]!=0)
                    {
                        completion(NULL, response);
                    }
                    else {
                        completion(@"No providers found.", NULL);
                    }
                    
                }
                
                else if([response isKindOfClass:[NSDictionary class]])
                {
                    NSString *error = [Utilities removeNullFromString:[response objectForKey:@"message"]];
                    NSLog(@"NO PROVIDERS....%@",error);
                    
                    completion(@"No providers found.", NULL);
                }
                else
                {
                    completion(@"No providers found.", NULL);
                }
            }
            else
            {
                completion(@"No providers found.", NULL);
            }
            
        }];
    }
    else
    {
        completion(@"No providers found.", NULL);
    }
}

//	------------------------------------------------------------------------------------------------------------
-(void)getAvailableServices {
	
	if ([appDelegate internetConnected]) {
		
		NSLog(@"RoutingAndMapServices get Services");
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
		// [appDelegate onStartLoader];
		
		
		[afn getDataFromPath:MD_GET_SERVICE withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
			if (response){
				//_categoryList = [[NSMutableArray alloc]init];
				//_categoryList = response;
				NSLog(@"RoutingAndMapServices Services....%@", response);
				
				NSArray *jsonArray = (NSArray *)response;
				NSMutableArray *fareServiceArray = [[NSMutableArray alloc]init];
				if (jsonArray != NULL) {
					for (NSDictionary * service in jsonArray) {
						FareService *fareService = [[FareService alloc]init];
						
                        if ([service[@"id"] isKindOfClass: [NSNumber class]]) {
                            fareService.serviceId = service[@"id"];
                        }
						if ([service[@"calculator"] isKindOfClass: [NSString class]]) {
							fareService.calculatorType = service[@"calculator"];
						}
						if ([service[@"capacity"] isKindOfClass: [NSNumber class]]) {
							fareService.capacity = service[@"capacity"];
						}
						if ([service[@"description"] isKindOfClass: [NSString class]]) {
							fareService.serviceDescription = service[@"description"];
						}
						if ([service[@"distance"] isKindOfClass: [NSNumber class]]) {
							fareService.distance = service[@"distance"];
						}
						if ([service[@"fixed"] isKindOfClass: [NSNumber class]]) {
							fareService.fixed = service[@"fixed"];
						}
						if ([service[@"hour"] isKindOfClass: [NSNumber class]]) {
							fareService.hour = service[@"hour"];
						}
						if ([service[@"id"] isKindOfClass: [NSNumber class]]) {
							fareService.idValue = service[@"id"];
						}
						if ([service[@"image"] isKindOfClass: [NSString class]]) {
							fareService.image = service[@"image"];
						}
						if ([service[@"minute"] isKindOfClass: [NSNumber class]]) {
							fareService.minute = service[@"minute"];
						}
						if ([service[@"name"] isKindOfClass: [NSString class]]) {
							fareService.name = service[@"name"];
						}
						if ([service[@"price"] isKindOfClass: [NSNumber class]]) {
							fareService.price = service[@"price"];
						}
						if ([service[@"provider_name"] isKindOfClass: [NSString class]]) {
							fareService.providerName = service[@"provider_name"];
						}
						if ([service[@"status"] isKindOfClass: [NSNumber class]]) {
							fareService.status = service[@"status"];
						}
						[fareServiceArray addObject:fareService];
					}
				}
				
				NSDictionary *userInfo = @{@"fair_services":fareServiceArray};
				[[NSNotificationCenter defaultCenter]
				 postNotificationName:FairServicesFetchSuccessString
				 object:self
				 userInfo: userInfo];
				//[self AssignValuesToList:response];
			}
			else{
				if ([errorcode intValue]==1){
					//[CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
				}
				else if ([errorcode intValue]==3){
					//                    [CommenMethods onRefreshToken];
					//[self logOut];
				}
			}
		}];
		
	}
	else {
		//        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
		
	}
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void)estimateFareWithRide: (Ride *) ride
			 withCompletion:(void (^) (NSString  * _Nullable error))completion {
	
	if ([appDelegate internetConnected]) {
		
		NSLog(@"Estimate Fare - currentLocatoin :%@",[KeyConstants sharedKeyConstants].LocationInfo);
		NSLog(@"Estimate Fare - Destination:%@",[KeyConstants sharedKeyConstants].DestinationInfo);
		NSMutableDictionary *params = [[NSMutableDictionary alloc]
									   initWithDictionary:@{@"s_latitude":ride.origin.latitudeString,
				@"s_longitude":ride.origin.longitudeString,
															@"d_latitude":ride.destination.latitudeString,
															@"d_longitude":ride.destination.longitudeString,
															@"service_type": [NSString stringWithFormat: @"%@",ride.fareService.idValue],}];
		NSLog(@"Estimate Fare... params %@", params);
		
		//NSLog(@"LoginRegisterService - validateEmailAddress");
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
		[appDelegate onStartLoader];
		[afn getDataFromPath:MD_GET_FAREESTIMATE withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
			[self->appDelegate onEndLoader];
			if (response) {
				
				NSLog(@"ESTIMATE...%@", response);
				//NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
				//NSString *currencyStr=[Utilities removeNullFromString: [user valueForKey:@"currency"]];
				
				FareEstimate *estimate = [[FareEstimate alloc]init];
				
				if ([response[@"base_price"] isKindOfClass: [NSNumber class]]) {
					estimate.basePrice = response[@"base_price"];
				}
				if ([response[@"distance"] isKindOfClass: [NSNumber class]]) {
					estimate.distance = response[@"distance"];
				}
				if ([response[@"estimated_fare"] isKindOfClass: [NSNumber class]]) {
					estimate.estimatedFare = response[@"estimated_fare"];
				}
				if ([response[@"surge"] isKindOfClass: [NSNumber class]]) {
					estimate.surge = response[@"surge"];
				}
				if ([response[@"surge_value"] isKindOfClass: [NSString class]]) {
					estimate.surgeValue = response[@"surge_value"];
				}
				if ([response[@"tax_price"] isKindOfClass: [NSNumber class]]) {
					estimate.taxPrice = response[@"tax_price"];
				}
				if ([response[@"time"] isKindOfClass: [NSString class]]) {
					estimate.time = response[@"time"];
				}
				if ([response[@"wallet_balance"] isKindOfClass: [NSNumber class]]) {
					estimate.walletBalance = response[@"wallet_balance"];
				}
				
				if (estimate.isValidFareEstimate == TRUE) {
					NSLog(@"Valid Fare Found");
					ride.fareEstimate = estimate;
					completion(NULL);
				}
				else {
					NSLog(@"Error Validating Fare");
					completion(@"Error Validating Fare");
				}
				
					
					
					/* if ([surge isEqualToString:@"1"])
					{
						[_surgeInfoLbl setText:@"Due to high demand price may vary"];
						_surgeLbl.text = [NSString stringWithFormat:@"%@", [response valueForKey:@"surge_value"]];
						_surgeInfoLbl.hidden = NO;
						_surgeView.hidden = NO;
					}else{
						_surgeLbl.text = [NSString stringWithFormat:@"%@", [response valueForKey:@"surge_value"]];
						_surgeInfoLbl.hidden = YES;
						_surgeView.hidden = NO;
					}
					if([response[@"wallet_balance"]intValue] > 0){
						_useWalletBtn.hidden = NO;
						_availableWalletTitleLbl.hidden = NO;
						_AvailableWalletAmountValueLbl.text = [NSString stringWithFormat:@"%@ %.2f",currencyStr,[response[@"wallet_balance"]doubleValue]];
						_AvailableWalletAmountValueLbl.hidden = NO;
					}else{
						_useWalletBtn.hidden = YES;
						_AvailableWalletAmountValueLbl.hidden = YES;
						_availableWalletTitleLbl.hidden= YES;
					}
					[KeyConstants sharedKeyConstants].RideStatus = @"FAIRINFO";
					[self checkViewUpdateStatusChange]; */

			}
			else {
				if ([errorcode intValue]==1) {
					
					
					//[CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
					NSLog(@"Errorcode is 1 %@",error);
					completion(LocalizedString(@"ERRORMSG"));
				}
				else if ([errorcode intValue]==3) {
					//[CommenMethods onRefreshToken];
					//[self logOut];
					NSLog(@"Errorcode is 3 should logout");
					completion(@"Error");
				}
			}
		}];
	}
	else {
		completion(LocalizedString(@"CHKNET"));
		//[CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
	}
}
//	------------------------------------------------------------------------------------------------------------



//	------------------------------------------------------------------------------------------------------------
-(void) bookNewRide: (Ride *) ride
	 withCompletion:(void (^) (NSString  * _Nullable error))completion {
	
	if ([appDelegate internetConnected]) {
		NSMutableDictionary *params;
		if (ride.scheduledTime != NULL) {
			
			NSCalendar *calendar = [NSCalendar currentCalendar];
			calendar.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
			[calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute
						 fromDate:ride.scheduledTime];
			//NSString *dateString =
			
			NSLog(@"Book a scheduled ride");
			[KeyConstants sharedKeyConstants].WalletStatus = @"0";
			params= [NSMutableDictionary dictionaryWithDictionary:@{
																	@"s_latitude":ride.origin.latitudeString,
																	@"s_longitude":ride.origin.longitudeString,
																	@"d_latitude":ride.destination.latitudeString,
																	@"d_longitude":ride.destination.longitudeString,
																	@"service_type": [NSString stringWithFormat: @"%@",ride.fareService.idValue],
																	@"distance":[NSString stringWithFormat: @"%@",ride.fareService.distance],
																	@"payment_mode":ride.paymentMethod.brand,
																	@"card_id":ride.paymentMethod.cardId,
																	@"s_address":ride.origin.address,
																	@"d_address":ride.destination.address,
																	@"use_wallet":[KeyConstants sharedKeyConstants].WalletStatus //,
																	//@"schedule_date":ride.scheduledTime!.dateString,
																	//@"schedule_time":[KeyConstants sharedKeyConstants].SheduleInfo[@"time"]*/
																	
																	}];
			
			//            NSArray *dateArr = [scheduleDate.text componentsSeparatedByString:@" "];
			//            NSString *schDate = [dateArr objectAtIndex:0];
			//            NSString *schtime = [dateArr objectAtIndex:1];
			//
			//            params=@{@"s_latitude":_sourceLat,@"s_longitude":_sourceLng,@"d_latitude":_destLat,@"d_longitude":_destLng,@"service_type":strServiceID,@"distance":strKM,@"payment_mode":strPay,@"card_id":strCardID,@"s_address":strSourceAddress,@"d_address":strDestAddress, @"use_wallet":walletFlag, @"schedule_date": schDate, @"schedule_time": schtime};
		}
		else {
			NSLog(@"Book an unscheduled ride");
			[KeyConstants sharedKeyConstants].WalletStatus = @"0";
			params= [NSMutableDictionary dictionaryWithDictionary:@{
																	@"s_latitude":ride.origin.latitudeString,
																	@"s_longitude":ride.origin.longitudeString,
																	@"d_latitude":ride.destination.latitudeString,
																	@"d_longitude":ride.destination.longitudeString,
																	@"service_type": [NSString stringWithFormat: @"%@",ride.fareService.idValue],
																	@"distance":[NSString stringWithFormat: @"%@",ride.fareService.distance],
																	@"payment_mode":ride.paymentMethod.brand,
																	@"card_id":ride.paymentMethod.cardId,
																	@"s_address":ride.origin.address,
																	@"d_address":ride.destination.address,
																	@"use_wallet":[KeyConstants sharedKeyConstants].WalletStatus}];
		}
		
		NSLog(@"RoutingAndMapService - bookNewRide");
		NSLog(@"Parameters %@",params);
		
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
		[appDelegate onStartLoader];
		[afn getDataFromPath:MD_CREATE_REQUEST withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
			[self->appDelegate onEndLoader];
			
			if (response) {
				
				NSString *reqStr = [[response objectForKey:@"request_id"] stringValue];
				if ([reqStr isEqualToString:@""] || reqStr.length ==0) {
					
					NSLog(@"Create Ride Request request_id is blank");
					NSLog(@"Error message is: %@",[response objectForKey:@"message"]);
					//[CommenMethods alertviewController_title:LocalizedString(@"ALERT")  MessageAlert:[response objectForKey:@"message"] viewController:self okPop:NO];
					completion(@"Error");
				}
				else {
                    [[NSUserDefaults standardUserDefaults] setValue:reqStr forKey:UD_REQUESTID];

					NSLog(@"Create Ride Request not blank - searching for ride animation? %@",response);
					ride.didBookRide = TRUE;
					completion(NULL);
					
					/*
					 LOTAnimationView *animation = [LOTAnimationView animationNamed:@"search"];
					 animation.frame = CGRectMake(0.0, 0.0, 200, 200);
					 animation.loopAnimation = YES;
					 animation.center =CGPointMake(_searchAnimationView.frame.size.width  / 2,
					 _searchAnimationView.frame.size.height / 2);;
					 
					 [_searchAnimationView addSubview:animation];
					 
					 
					 
					 
					 [animation playWithCompletion:^(BOOL animationFinished) {
					 // Do Something
					 }];
					 */
				}
			}
			else {
				if ([errorcode intValue]==1) {
					if ([error objectForKey:@"error"]) {
						//[CommenMethods alertviewController_title:@"" MessageAlert:[error objectForKey:@"error"] viewController:self okPop:NO];
						NSLog(@"Errorcode is 1 %@",error);
						completion(@"Error");
					}
					else {
						//[CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
						NSLog(@"Errorcode is 0");
						completion(@"Error");
					}
				}
				else if ([errorcode intValue]==3) {
					//[self logOut];
					NSLog(@"Errorcode is 3 should logout");
					completion(@"Error");
				}
			}
			
		}];
	}
	else {
		completion(LocalizedString(@"CHKNET"));
		//[CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
	}
	
}
//	------------------------------------------------------------------------------------------------------------


//	------------------------------------------------------------------------------------------------------------
-(void) checkRideServiceForActiveRideWithCompletion:(void (^_Nullable) (NSString  * _Nullable error, ActiveRideResponse * _Nullable activeRide))completion {
	
	if ([appDelegate internetConnected]){
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
		[afn getDataFromPath:MD_REQUEST_CHECK withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
			
			if (response){
				
				NSLog(@"%@",response);
				
				if([response[@"data"] count] == 0) {
					//	No data response
					NSLog(@"Data response is empty");
					completion(@"No data response - may be a scheduled ride?",NULL);
				}
				else {
					
					ActiveRideResponse *rideResponse;
					rideResponse = [ActiveRideResponse createWithDictionary:[response[@"data"] lastObject]];
					
					if (rideResponse.idValue != NULL) {
						[[NSUserDefaults standardUserDefaults]setValue:rideResponse.idValue forKey:UD_REQUESTID];
					}
					
					completion(NULL,rideResponse);
				}
			}
			else{
				if ([errorcode intValue]==1){
					//completion(LocalizedString(@"ERRORMSG"),NULL);
					//[CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG")  viewController:self okPop:NO];
					completion(LocalizedString(@"ERRORMSG"),NULL);
				}
				else if ([errorcode intValue]==3){
					NSLog(@"Error code 3 - should logout-refresh token");
					completion(@"Should logout error message",NULL);
					//[self logOut];
				}
			}
		}];
	}
	else{
		completion(LocalizedString(@"CHKNET"),NULL);
	}
}
//	------------------------------------------------------------------------------------------------------------

//    ------------------------------------------------------------------------------------------------------------
-(void) cancelRideServiceWithCompletion:(void (^_Nullable) (NSString  * _Nullable error, NSString * _Nullable success))completion {
    
    if ([appDelegate internetConnected]){
		
		
		NSString *strReqID=[[NSUserDefaults standardUserDefaults] valueForKey:UD_REQUESTID];
		if (strReqID == NULL) {
			completion(NULL, @"success");
		}
		else {
			NSDictionary *params = @{UD_REQUESTID: strReqID};
			AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
			[afn getDataFromPath:MD_CANCEL_REQUEST withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
				[self->appDelegate onEndLoader];
				if (response) {
					IsShedule = NO;
					[KeyConstants sharedKeyConstants].RideStatus = @"HOME";
					[KeyConstants sharedKeyConstants].RequestInfo  = nil;
					completion(NULL, @"success");
				}
				else{
					if ([errorcode intValue]==1) {
						completion(LocalizedString(@"ERRORMSG"), NULL);
					}
					else if ([errorcode intValue]==3){
						//                    [CommenMethods onRefreshToken];
						completion(LocalizedString(@"ERRORMSG"), NULL);
					}
					else{
						completion(@"ERRORMSG", NULL);
					}
				}
			}];
		}
        
		
    }
}

-(void) rateDriver: (NSString * _Nullable) rate
           comment: (NSString * _Nullable) comment
    WithCompletion:(RequestCompletionBlock)completion {
    
    if ([appDelegate internetConnected])
    {
		NSLog(@"RoutingAndMapService - rateDriver");
        NSString *strReqID=[[NSUserDefaults standardUserDefaults] valueForKey:UD_REQUESTID];
        NSDictionary *params=@{@"request_id":strReqID,@"rating":rate,@"comment":comment};
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [appDelegate onStartLoader];
        [afn getDataFromPath:MD_RATE_PROVIDER withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
			[self->appDelegate onEndLoader];
            if (response) {
                completion(response, NULL, NULL);
            } else {
                completion(NULL, error, errorcode);
            }
        }];
    }
    else
    {
        completion(NULL, @"internet disconnected", @"-1");
    }
    
}



-(void)getAllCardsWith: (void (^_Nullable) (NSString  * _Nullable error, NSArray * _Nullable success))completion {
	
	if ([appDelegate internetConnected]) {
		
		NSLog(@"RoutingAndMapService - getAllCards");
		[appDelegate onStartLoader];
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
		
		[afn getDataFromPath:MD_ADD_CARD withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode){
			
			NSLog(@"RoutingAndMapService - getAllCards - Should call onEndLoader %@",(self->appDelegate));
			[self->appDelegate onEndLoader];
			
			if (response) {
				
				NSMutableArray * arrPaymentCardList= [[NSMutableArray alloc] init];
				
				NSDictionary *dictCash=@{@"brand":@"CASH",@"card_id":@"",@"id":@"0"};
				[arrPaymentCardList addObject:dictCash];
				
				[arrPaymentCardList addObjectsFromArray:response];
				if(arrPaymentCardList.count) {
					NSLog(@"The list of payment cards %@",arrPaymentCardList);
					completion(NULL,arrPaymentCardList);
				}
				else {
					completion(@"No payment methods found.",NULL);
				}
			}
			else {
				if ([errorcode intValue]==1) {
					//[CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
					completion(LocalizedString(@"ERRORMSG"),NULL);
				}
				else if ([errorcode intValue]==3) {
					
					//[self logoutMethod];
					completion(@"REFRESH TOKEN",NULL);
					//                    [CommenMethods onRefreshToken];
					//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
					//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
					//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
					//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
					//                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
					//
					//                    PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
					//                    [self.navigationController pushViewController:wallet animated:YES];
				}
			}
			
		}];
	}
	else {
		completion(LocalizedString(@"CHKNET"),NULL);
		//[CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
	}
}

@end
