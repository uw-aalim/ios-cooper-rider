//
//  GoogleServices.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-15.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

class CoOperGoogleServices: NSObject {
	
	static let shared = CoOperGoogleServices()
	
	let GoogleAPIKey = "AIzaSyAsVN1AHgbMr2n3SYtNZxFiT7e1bz3tP5s"
	
	
	
	var isGettingCurrentLocation: Bool = false
	
	
	//	------------------------------------------------------------------------------------------------------------
	func getCurrentLocationAddress(
		atCoordinate coordinate: CLLocationCoordinate2D,
		completion: ((_ currentAddress: String?, _ errorString: String?)->())?) {
		
		print("Get Google curent location address \(coordinate)")
		
		if !self.isGettingCurrentLocation {
			self.isGettingCurrentLocation = true
			
			var googleUrl = "https://maps.googleapis.com/maps/api/geocode/json"
			googleUrl = googleUrl + "?latlng=\(coordinate.latitude),\(coordinate.longitude)&key=\(self.GoogleAPIKey)"
			//let newUrl = URL(string: googleUrl)
			
			let session = URLSession.shared
			session.dataTask(
			with: URL(string: googleUrl)!) { (data: Data?, response: URLResponse?, error: Error?) in
				
				self.isGettingCurrentLocation = false
				
				if let data = data {
					do {
						if let jsonObject = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? NSDictionary,
							let results = jsonObject["results"] as? NSArray,
							results.count > 0,
							let resultsDict = results[0] as? NSDictionary,
							let resultsAddress = resultsDict["formatted_address"] as? String {
							
							completion?(resultsAddress,nil)
						}
						else {
							completion?(nil,"No address found for current location from google maps request.")
						}
					}
					catch {
						completion?(nil,"Error parsing json response from google maps request.")
					}
				}
				else {
					completion?(nil,"No data response from google maps request.")
				}
				}.resume()
		}
		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Get a Route Path for a Ride
	//	============================================================================================================
	
	var isGettingPath: Bool = false
	
	//	------------------------------------------------------------------------------------------------------------
	func getRoute(
		withRide ride: Ride,
		completion: ((_ polylineDictionary: String?, _ errorString: String?) -> ())?) {
		
		if ride.canGeneratePath,
			self.isGettingPath == false {
			
			self.isGettingPath = true
			let googleUrl = "https://maps.googleapis.com/maps/api/directions/json"
			
			let waypointsString = ""
			let urlString = "\(googleUrl)?origin=\(ride.origin!.latitude!),\(ride.origin!.longitude!)&destination=\(ride.destination!.latitude!),\(ride.destination!.longitude!)&sensor=false&waypoints=\(waypointsString)&mode=driving&key=\(self.GoogleAPIKey)"
			//print("my driving api URL --- \(urlString)")
			let newUrl = URL(string: urlString)
			print("my driving api URL --- \(String(describing: newUrl))")
			
			let session = URLSession.shared
			session.dataTask(
			with: URL(string: urlString)!) { (data: Data?, response: URLResponse?, error: Error?) in
				
				self.isGettingPath = false
				DispatchQueue.main.async {
					print("GoogleServices - getPath - Recieved response from server \(String(describing: data)), \(String(describing: response)) \(String(describing: error))")
				}
				
				if let data = data {
					do {
						if let jsonObject = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? NSDictionary,
							let routesArray = jsonObject["routes"] as? NSArray,
							routesArray.count > 0 {
							
							let routeDictionary = routesArray[0] as! NSDictionary
							let routeOverlayPolylineDictionary = routeDictionary["overview_polyline"] as! NSDictionary
							let points = routeOverlayPolylineDictionary["points"] as! String
							
							completion?(points,nil)
						}
						else {
							completion?(nil,"Error - no routes found between origin and destination")
						}
					}
					catch {
						completion?(nil,"Error - error parsing json response from google driving url request")
					}
				}
				else {
					completion?(nil,"Error - no data response from google driving url request")
				}
				}.resume()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
