//
//  GooglePlacesSearchHelper.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation
import GooglePlaces

@objc
protocol GooglePlacesSearchHelperDelegate: class {
	//	When authorization status is determined pass back to whatever class is using this so that it can display appropriate messages
	@objc
	func determinedAuthorization(authorizationStatus status: CLAuthorizationStatus, sender: AnyObject)
	//	When location has been determiend it calls back to let the calling class know that it can now request places
	@objc
	func determinedLocation(_ sender: AnyObject)
	
	@objc
	func foundPlaces(places placeArray: [GooglePlaceObject], forPlaceType placeType: String?, sender: AnyObject)
	@objc
	func foundSuggestions(places placeArray: [GooglePlaceObject], sender: AnyObject)
	//func loadedImages(_ sender: AnyObject)
}

/**
The google places search helper is used to wrap the requests for places inside of this object so that
the user doesn't have to view them directly and instead gets back the places objects without haveing
to deal with parsing or custom search strings.
*/
class GooglePlacesSearchHelper: NSObject {

	let GoogleAPIKey = "AIzaSyAsVN1AHgbMr2n3SYtNZxFiT7e1bz3tP5s"
	
	@objc
	weak var delegate: GooglePlacesSearchHelperDelegate?
	var locationManager: CLLocationManager!
	var location: CLLocationCoordinate2D?
	
	override init() {
		super.init()
		
		//	Determine the authorization status of the location manager and perform the appropriate action
		//	If denied or restricted only heading to the settings page will be able to re-enable for this app.
		let status = CLLocationManager.authorizationStatus()
		switch status {
		case .authorizedAlways, .authorizedWhenInUse:
			print("GooglePlacesSearchHelper - Already authorized start updating location")
			self.locationManager = CLLocationManager()
			self.locationManager.delegate = self
			self.locationManager.startUpdatingLocation()
			self.delegate?.determinedAuthorization(
				authorizationStatus: status,
				sender: self)
		case .denied, .restricted:
			print("GooglePlacesSearchHelper - Location Denied or Restricted - Send to settings")
			self.delegate?.determinedAuthorization(
				authorizationStatus: status,
				sender: self)
		case .notDetermined:
			//	Not yet decided, then request when in use authorization.
			self.locationManager = CLLocationManager()
			self.locationManager.delegate = self
			self.locationManager.requestWhenInUseAuthorization()
		}
	}
	
	@objc
	func performPlaceSearch(withTextString textString: String, isSearch search: Bool = false) {
		
		let requestBaseAddress = "https://maps.googleapis.com/maps/api/place/textsearch/json?"
		
		var query = ""
		if search == false {
			if textString == "Restaurants" {
				
				query = "type=%22restaurant%22"
			}
			else if textString == "Clubs" {
				query = "type=%22night_club%22"
			}
			else if textString == "Hotels" {
				query = "type=%22lodging%22"
			}
			else {
				let newString = textString + " Bahamas"
				let encodedSearch = newString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
				query = "query=%22\(encodedSearch!)%22"
			}
		}
		else {
			let newString = textString
			let encodedSearch = newString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
			query = "query=%22\(encodedSearch!)%22"
		}
		
		var requestAddress = requestBaseAddress + query// + key
		if !search {
			if let location = self.location {
				let locationString = "&location=\(location.latitude),\(location.longitude)"
				requestAddress = requestAddress + locationString
			}
			else {
				//	Default to the nassau bahamas
				let locationString = "&location=\(25.0480),\(-77.3554)"
				requestAddress = requestAddress + locationString
			}
		}
		
		let key = "&key=\(self.GoogleAPIKey)"
		if !search {
			let radius = "&radius=25000"
			requestAddress = requestAddress + radius + key
		}
		else {
			requestAddress = requestAddress + key
		}
		
		
		print("GooglePlacesSearchHelper - performPlaceSearch - Perform Google Search Requestion \(requestAddress)")
		
		let url: URLConvertible = URL(string: requestAddress)!
		
		Alamofire.request(url, method: .get,
						  parameters: nil,
						  encoding: URLEncoding.httpBody,
						  headers: nil)
			.responseJSON { response in
				
				
				DispatchQueue.main.async {
					print("GooglePlacesSearchHelper finished place search \(response)")
					if let jsonDict = response.result.value as? NSDictionary,
						let resultsArray = jsonDict.object(forKey: "results") as? [NSDictionary] {
						
						var places = [GooglePlaceObject]()
						
						for placeDict in resultsArray {
							let formattedAddress = placeDict["formatted_address"] as? String
							let placeId = placeDict["place_id"] as! String
							let name = placeDict["name"] as? String
							let geometry = placeDict["geometry"] as! NSDictionary
							let location = geometry["location"] as! NSDictionary
							let latitude = location["lat"] as! NSNumber
							let longitude = location["lng"] as! NSNumber
							
							let newPlace = GooglePlaceObject()
							newPlace.placeId = placeId
							newPlace.name = name
							newPlace.address = formattedAddress
							newPlace.latitude = latitude
							newPlace.longitude = longitude
							
							self.loadFirstPhotoForPlace(
								placeID: newPlace.placeId,
								placeObject: newPlace)
							self.loadMorePlaceDetails(
								placeId: newPlace.placeId,
								placeObject: newPlace)
							
							places.append(newPlace)
						}
						
						if search {
							self.delegate?.foundPlaces(
								places: places,
								forPlaceType: nil,
								sender: self)
						}
						else {
							self.delegate?.foundPlaces(
								places: places,
								forPlaceType: textString,
								sender: self)
						}
					}
				}
		}
	}
	
	
	
	
	@objc
	func searchSuggestions(withTextString textString: String) {
		
		let requestBaseAddress = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
		
		var query = ""
		let newString = textString
		let encodedSearch = newString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
		query = "input=%22\(encodedSearch!)%22"
		
		var requestAddress = requestBaseAddress + query// + key
		let key = "&key=\(self.GoogleAPIKey)"
		//requestAddress = requestAddress + key
		
		if let location = self.location {
			let locationString = "&location=\(location.latitude),\(location.longitude)"
			requestAddress = requestAddress + locationString
		}
		else {
			//	Default to the nassau bahamas
			let locationString = "&location=\(25.0480),\(-77.3554)"
			requestAddress = requestAddress + locationString
		}
		
		let radius = "&radius=25000"
		requestAddress = requestAddress + radius + key
		
		print("GooglePlacesSearchHelper - searchSuggestions - Perform Google Search Requestion \(requestAddress)")
		
		let url: URLConvertible = URL(string: requestAddress)!
		
		Alamofire.request(url, method: .get,
						  parameters: nil,
						  encoding: URLEncoding.httpBody,
						  headers: nil)
			.responseJSON { response in
				
				
				DispatchQueue.main.async {
					print("GooglePlacesSearchHelper finished place autocomplete \(response)")
					if let jsonDict = response.result.value as? NSDictionary,
						let resultsArray = jsonDict.object(forKey: "predictions") as? [NSDictionary] {
						
						var places = [GooglePlaceObject]()
						
						for placeDict in resultsArray {
							let formattedAddress = placeDict["description"] as? String
							let placeId = placeDict["place_id"] as! String
							
							let newPlace = GooglePlaceObject()
							newPlace.placeId = placeId
							newPlace.address = formattedAddress
							
							self.loadMorePlaceDetails(
								placeId: newPlace.placeId,
								placeObject: newPlace)
							
							places.append(newPlace)
						}
						
						self.delegate?.foundSuggestions(
							places: places,
							sender: self)
					}
				}
		}
	}
	
	
	
	func loadMorePlaceDetails(
		placeId: String,
		placeObject: GooglePlaceObject) {
		GMSPlacesClient.shared().lookUpPlaceID(placeId) { (place: GMSPlace?, error: Error?) in
			if let place = place {
				
				placeObject.website = place.website
				placeObject.gmsPlace = place
				
				NotificationCenter.default.post(
					name: CoOperNotification.placeUpdatedNotification,
					object: nil)
			}
		}
	}
	
	func loadPlaceDetails(
		placeId: String,
		placeObject: GooglePlaceObject,
		completion: @escaping (_ placeObject: GooglePlaceObject)->Void) {
		
		GMSPlacesClient.shared().lookUpPlaceID(placeId) { (place: GMSPlace?, error: Error?) in
			if let place = place {
				
				placeObject.website = place.website
				placeObject.gmsPlace = place
				placeObject.latitude = NSNumber(value: place.coordinate.latitude)
				placeObject.longitude = NSNumber(value: place.coordinate.longitude)
				placeObject.address = place.formattedAddress
				
				completion(placeObject)
			}
		}
	}
	
	func loadFirstPhotoForPlace(
		placeID: String,
		placeObject: GooglePlaceObject) {
		GMSPlacesClient.shared().lookUpPhotos(forPlaceID: placeID) { (photos, error) -> Void in
			if let error = error {
				// TODO: handle the error.
				print("Error: \(error.localizedDescription)")
			} else {
				if let firstPhoto = photos?.results.first {
					self.loadImageForMetadata(
						photoMetadata: firstPhoto,
						placeObject: placeObject)
				}
			}
		}
	}
	
	func loadImageForMetadata(
		photoMetadata: GMSPlacePhotoMetadata,
		placeObject: GooglePlaceObject) {
		
		GMSPlacesClient.shared().loadPlacePhoto(photoMetadata, callback: {
			(photo, error) -> Void in
			if let error = error {
				// TODO: handle the error.
				print("Error: \(error.localizedDescription)")
			} else {
				placeObject.placeImage = photo
				
				NotificationCenter.default.post(
					name: CoOperNotification.placeUpdatedNotification,
					object: nil)
			}
		})
	}
	
}


/**
The location manager delegate is responsible for handling and getting the location of the users device. It is
important to know where the user is when searching for nearby places of each of the types of category.
*/
extension GooglePlacesSearchHelper: CLLocationManagerDelegate {
	
	func locationManager(
		_ manager: CLLocationManager,
		didChangeAuthorization status: CLAuthorizationStatus) {
		
		self.delegate?.determinedAuthorization(
			authorizationStatus: status,
			sender: self)
		
		switch status {
		case .authorizedAlways, .authorizedWhenInUse:
			self.locationManager.startUpdatingLocation()
		case .denied, .restricted:
			print("GooglePlacesSearchHelper - didChangeAuthorization - Location Denied or Restricted - Send to settings")
		case .notDetermined:
			print("GooglePlacesSearchHelper - didChangeAuthorization - Location Not Determined")
		}
	}
	
	func locationManager(
		_ manager: CLLocationManager,
		didUpdateLocations locations: [CLLocation]) {
		
		print("GooglePlacesSearchHelper -didUpdateLocations \(locations)")
		if let location = locations.first {
			self.location = location.coordinate
		}
		else {
			self.location = nil
		}
		self.locationManager.stopUpdatingLocation()
		self.delegate?.determinedLocation(self)
	}
	
}
