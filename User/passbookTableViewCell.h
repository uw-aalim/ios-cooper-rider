//
//  passbookTableViewCell.h
//  User
//
//  Created by MAC on 04/09/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface passbookTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *paymentLbl;
@property (weak, nonatomic) IBOutlet UILabel *amountLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

@property (weak, nonatomic) IBOutlet UILabel *paymentValue;
@property (weak, nonatomic) IBOutlet UILabel *amountValue;
@property (weak, nonatomic) IBOutlet UILabel *dateValue;

@property (weak, nonatomic) IBOutlet UILabel *statusValue;
@property (weak, nonatomic) IBOutlet UILabel *statusLbl;
@property (weak, nonatomic) IBOutlet UIView *commonView;

@end
