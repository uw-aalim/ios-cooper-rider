//
//  ExploreViewController+CollectionView.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension ExploreViewController:
	UICollectionViewDataSource,
	UICollectionViewDelegateFlowLayout {
	
	//	------------------------------------------------------------------------------------------------------------
	/**
	The number of sections in the collection view corresponds to the number of different categories of place
	searched for using the google places API.
	*/
	func numberOfSections(
		in collectionView: UICollectionView) -> Int {
		
		if self.isSearching {
			return 1
		}
		else {
			return self.sections.count
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func collectionView(
		_ collectionView: UICollectionView,
		numberOfItemsInSection section: Int) -> Int {
		
		if self.isSearching {
			return self.searchedPlaces.count
		}
		else {
			if section < self.sections.count {
				if let places = self.places[self.sections[section]] {
					if places.count > 3 {
						return 3
					}
					else {
						return places.count
					}
				}
				else {
					return 0
				}
			}
			else {
				return 0
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func collectionView(
		_ collectionView: UICollectionView,
		cellForItemAt
		indexPath: IndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExplorePlaceCell", for: indexPath) as! ExplorePlaceCell
		
		var place: GooglePlaceObject? = nil
		
		/*
		Depending on whether or not the user is searching either fetch the place from the appropriate
		*/
		if self.isSearching {
			place = self.searchedPlaces[indexPath.row]
		}
		else {
			let section = self.sections[indexPath.section]
			if let places = self.places[section] {
				place = places[indexPath.row]
			}
		}
		
		if let place = place {
			var titleString = ""
			var addressString = ""
			if place.name != nil {
				titleString = place.name!
			}
			if place.address != nil {
				addressString = place.address!
			}
			
			cell.configure(
				withTitle: titleString,
				withAddress: addressString,
				withImage: place.placeImage)
		}
		
		return cell
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	func collectionView(
		_ collectionView: UICollectionView,
		viewForSupplementaryElementOfKind kind: String,
		at indexPath: IndexPath) -> UICollectionReusableView {
		
		let cell = collectionView.dequeueReusableSupplementaryView(
			ofKind: UICollectionElementKindSectionHeader,
			withReuseIdentifier: "ExploreHeadingCell",
			for: indexPath) as! ExploreHeadingCell
		
		//	Only bother configuring the header if it is going to be shown in a non-search context
		if !self.isSearching {
			let section = self.sections[indexPath.section]
			
			var showViewAll = false
			if let places = self.places[section],
				places.count > 3 {
				
				showViewAll = true
			}
			
			cell.configure(
				withTitle: "\(section) Near Me",
				shouldShowViewAllButton: showViewAll,
				withIndexPath: indexPath)
			
			cell.delegate = self
			cell.alpha = 1
		}
		else {
			cell.alpha = 0
		}
		//print("Header cell \(cell.frame)")
		
		return cell
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func collectionView(
		_ collectionView: UICollectionView,
		willDisplaySupplementaryView view: UICollectionReusableView,
		forElementKind elementKind: String,
		at indexPath: IndexPath) {
		
		view.layer.zPosition = 0.0
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func collectionView(
		_ collectionView: UICollectionView,
		willDisplay cell: UICollectionViewCell,
		forItemAt indexPath: IndexPath) {
		
		cell.layer.zPosition = -1.0
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func collectionView(
		_ collectionView: UICollectionView,
		didSelectItemAt indexPath: IndexPath) {
		
		// 	Show place details
		var place: GooglePlaceObject? = nil
		if self.isSearching {
			place = self.searchedPlaces[indexPath.row]
		}
		else {
			let section = self.sections[indexPath.section]
			
			if let places = self.places[section] {
				place = places[indexPath.row]
			}
		}
		
		if let place = place {
			let placeDetails = ExplorePlaceDetailsViewController.initController()
			placeDetails.place = place
			placeDetails.delegate = self.delegate
			
			self.navigationController?.pushViewController(placeDetails, animated: true)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func collectionView(
		_ collectionView: UICollectionView,
		layout collectionViewLayout: UICollectionViewLayout,
		referenceSizeForHeaderInSection section: Int) -> CGSize {
		
		//print("Size for header in section \(section)")
		
		var height: CGFloat = 64.0
		if self.isSearching {
			height = 0
		}
		return CGSize(
			width: collectionView.frame.size.width,
			height: height)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
		
	
	//	------------------------------------------------------------------------------------------------------------
	func collectionView(
		_ collectionView: UICollectionView,
		layout collectionViewLayout: UICollectionViewLayout,
		sizeForItemAt indexPath: IndexPath) -> CGSize {
		
		return CGSize(
			width: collectionView.frame.size.width,
			height: 64.0)
	}
	//	------------------------------------------------------------------------------------------------------------
}
