//
//  ExploreHeadingCell.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

protocol ExploreHeadingCellDelegate: class {
	func viewAllButtonPressed(atIndexPath indexPath: IndexPath, sender: AnyObject)
}

class ExploreHeadingCell: UICollectionReusableView {
	
	@IBOutlet weak var sectionTitleLabel: UILabel!
	@IBOutlet weak var viewAllButton: UIButton!
	
	var indexPath: IndexPath!
	weak var delegate: ExploreHeadingCellDelegate?
	
	//	------------------------------------------------------------------------------------------------------------
	override init(frame: CGRect) {
		super.init(frame: frame)
		let _ = self.loadView(with: "ExploreHeadingCell")
		self.performSetup()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		let _ = self.loadView(with: "ExploreHeadingCell")
		self.performSetup()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func performSetup() {
		self.sectionTitleLabel.textColor = UIColor.black
		self.viewAllButton.tintColor = UIColor.tealColour()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func redrawShadow() {
		/*
		self.layer.masksToBounds = false
		self.layer.shadowColor = UIColor.gray.cgColor
		self.layer.shadowOffset = CGSize.zero
		self.layer.shadowRadius = 5.0
		self.layer.shadowOpacity = 0.75
		self.layer.shadowPath = UIBezierPath(
			roundedRect: self.bounds,
			cornerRadius: 0.0).cgPath
		*/
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func prepareForReuse() {
		super.prepareForReuse()
		self.redrawShadow()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	/**
	*/
	func configure(
		withTitle title: String,
		shouldShowViewAllButton showViewAllButton: Bool,
		withIndexPath indexPath: IndexPath) {
		
		self.indexPath = indexPath
		
		self.sectionTitleLabel.text = title
		self.viewAllButton.isHidden = !showViewAllButton
		
		self.layoutIfNeeded()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func viewAllButtonPressed(_ sender: AnyObject) {
		self.delegate?.viewAllButtonPressed(atIndexPath: self.indexPath, sender: self)
	}
	//	------------------------------------------------------------------------------------------------------------
	
}
