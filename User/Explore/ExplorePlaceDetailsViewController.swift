//
//  ExplorePlaceDetailsViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class ExplorePlaceDetailsViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var placeAddressLabel: UILabel!
	
	@IBOutlet weak var websiteLabel: UILabel!
	@IBOutlet weak var websiteButton: UIButton!
	
	@IBOutlet weak var cooperMeButton: UIButton!
	
	var place: GooglePlaceObject?
	var finishedFirstLoad = false
	
	weak var delegate: ExploreViewControllerDelegate?
	
	
	
	
	//	============================================================================================================
	//	MARK:- Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	class func initController() -> ExplorePlaceDetailsViewController {
		return ExplorePlaceDetailsViewController(
			nibName: "ExplorePlaceDetailsViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.configureNavigationController()
		self.loadPlaceData()
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(ExplorePlaceDetailsViewController.placeUpdatedNotification(_:)),
			name: CoOperNotification.placeUpdatedNotification,
			object: nil)
		
		self.cooperMeButton.backgroundColor = UIColor.orangeColour()
		self.websiteButton.backgroundColor = UIColor.tealColour()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		if !self.finishedFirstLoad {
			self.redrawShadow()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		self.finishedFirstLoad = true
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func redrawShadow() {
		/*
		self.titleBarBackgroundView.layer.masksToBounds = false
		self.titleBarBackgroundView.layer.shadowColor = UIColor.gray.cgColor
		self.titleBarBackgroundView.layer.shadowOffset = CGSize.zero
		self.titleBarBackgroundView.layer.shadowRadius = 5.0
		self.titleBarBackgroundView.layer.shadowOpacity = 0.75
		self.titleBarBackgroundView.layer.shadowPath = UIBezierPath(
			roundedRect: self.titleBarBackgroundView.bounds,
			cornerRadius: 0.0).cgPath
		
		self.websiteButton.layer.masksToBounds = false
		self.websiteButton.layer.shadowColor = UIColor.gray.cgColor
		self.websiteButton.layer.shadowOffset = CGSize.zero
		self.websiteButton.layer.shadowRadius = 5.0
		self.websiteButton.layer.shadowOpacity = 0.75
		self.websiteButton.layer.shadowPath = UIBezierPath(
			roundedRect: self.websiteButton.bounds,
			cornerRadius: 4.0).cgPath
		
		self.cooperMeButton.layer.masksToBounds = false
		self.cooperMeButton.layer.shadowColor = UIColor.gray.cgColor
		self.cooperMeButton.layer.shadowOffset = CGSize.zero
		self.cooperMeButton.layer.shadowRadius = 5.0
		self.cooperMeButton.layer.shadowOpacity = 0.75
		self.cooperMeButton.layer.shadowPath = UIBezierPath(
			roundedRect: self.cooperMeButton.bounds,
			cornerRadius: 4.0).cgPath */
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	func configureNavigationController() {
		
		if #available(iOS 11.0, *) {
			self.navigationController?.navigationBar.prefersLargeTitles = true
			self.navigationItem.largeTitleDisplayMode = .always
		}
		
		self.navigationController?.navigationBar.isTranslucent = false
		self.navigationController?.navigationBar.barTintColor = .black
		self.navigationController?.navigationBar.barStyle = .blackOpaque
		self.navigationController?.navigationBar.backgroundColor = .black
		self.navigationController?.navigationBar.tintColor = .white
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	//	------------------------------------------------------------------------------------------------------------
	func loadPlaceData() {
		//self.titleLabel.text = self.place?.name
		
		self.navigationItem.title = self.place?.name
		
		self.placeAddressLabel.text = self.place?.address
		
		if let image = self.place?.placeImage {
			self.imageView.image = image
		}
		
		if self.place?.website != nil {
			self.websiteLabel.isHidden = false
			self.websiteButton.isHidden = false
		}
		else {
			self.websiteLabel.isHidden = true
			self.websiteButton.isHidden = true
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	
	//	MARK:- Action Responders
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func cooperToAddressButtonPressed(_ sender: AnyObject) {
		
		if let place = self.place,
			place.address != nil,
			place.latitude != nil,
			place.longitude != nil {
			self.delegate?.cooperToAddress(place)
			
			/*
			KeyConstants().destinationInfo = [
					DestinationLat:NSString(format: "%@", place.latitude!),
					DestinationLang:NSString(format: "%@", place.longitude!),
					DestinationLocations: NSString(string: place.address!)
			]
			KeyConstants().rideStatus = "PAYMENT"
			self.navigationController?.popToRootViewController(animated: true)
			*/
			
			
			/*[KeyConstants sharedKeyConstants].DestinationInfo = @{
			DestinationLat :[NSString stringWithFormat:@"%f", place.latitude],
			DestinationLang :[NSString stringWithFormat:@"%f",place.longitude],
			DestinationLocations : place.address,
			};
			[KeyConstants sharedKeyConstants].RideStatus = @"PAYMENT";
			//[_destinationBtn setTitle:place.formattedAddress forState:UIControlStateNormal];
			
			[self.navigationController popToRootViewControllerAnimated:true]; */
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func websiteButtonPressed(_ sender: AnyObject) {
		if let websiteUrl = self.place?.website {
			//UIApplication.shared.openURL(websiteUrl)
			UIApplication.shared.open(websiteUrl, options: [:]) { (completed: Bool) in
				//	Open website
			}
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
