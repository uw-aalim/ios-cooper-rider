//
//  ExploreViewAllViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class ExploreViewAllViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	class func initController() -> ExploreViewAllViewController {
		return ExploreViewAllViewController(
			nibName: "ExploreViewAllViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	@IBOutlet weak var collectionView: UICollectionView!
	
	var section: String!
	var places: [GooglePlaceObject] = [GooglePlaceObject]()
	weak var delegate: ExploreViewControllerDelegate?
	
	var finishedFirstLoad = false
	
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configureNavigationController()
		
		//	Setup the collection view
		self.collectionView.register(
			ExplorePlaceCell.self,
			forCellWithReuseIdentifier: "ExplorePlaceCell")
		
		self.collectionView.delegate = self
		self.collectionView.dataSource = self
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(ExploreViewAllViewController.placeUpdatedNotification(_:)),
			name: CoOperNotification.placeUpdatedNotification,
			object: nil)
    }

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		if !self.finishedFirstLoad {
			self.redrawShadow()
			self.collectionView.reloadData()
			
		}
	}
	
	
	//	------------------------------------------------------------------------------------------------------------
	func configureNavigationController() {
		
		if #available(iOS 11.0, *) {
			self.navigationController?.navigationBar.prefersLargeTitles = true
			self.navigationItem.largeTitleDisplayMode = .always
		}
		
		self.navigationItem.title = self.section + " Near Me"
		
		self.navigationController?.navigationBar.isTranslucent = false
		self.navigationController?.navigationBar.barTintColor = .black
		self.navigationController?.navigationBar.barStyle = .blackOpaque
		self.navigationController?.navigationBar.backgroundColor = .black
		self.navigationController?.navigationBar.tintColor = .white
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func redrawShadow() {
		/*
		self.titleBarBackgroundView.layer.masksToBounds = false
		self.titleBarBackgroundView.layer.shadowColor = UIColor.gray.cgColor
		self.titleBarBackgroundView.layer.shadowOffset = CGSize.zero
		self.titleBarBackgroundView.layer.shadowRadius = 5.0
		self.titleBarBackgroundView.layer.shadowOpacity = 0.75
		self.titleBarBackgroundView.layer.shadowPath = UIBezierPath(
			roundedRect: self.titleBarBackgroundView.bounds,
			cornerRadius: 0.0).cgPath
		*/
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		self.finishedFirstLoad = true
		self.collectionView.reloadData()
	}
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		if let viewControllers = self.navigationController?.viewControllers,
			viewControllers.count <= 1 {
			NotificationCenter.default.removeObserver(self)
		}
	}
	//	------------------------------------------------------------------------------------------------------------

    
	
}
