//
//  ExploreViewController+GooglePlacesSearchHelperDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import CoreLocation

extension ExploreViewController: GooglePlacesSearchHelperDelegate {
	
	func determinedLocation(_ sender: AnyObject) {
		print("ExploreViewController - determinedLocation")
		for type in self.sections {
			self.placesHelper?.performPlaceSearch(withTextString: type)
		}
	}
	
	func determinedAuthorization(authorizationStatus status: CLAuthorizationStatus, sender: AnyObject) {
		print("ExploreViewController - determinedAuthorization \(status)")
	}
	
	func foundPlaces(
		places placeArray: [GooglePlaceObject],
		forPlaceType placeType: String?,
		sender: AnyObject) {
		
		print("ExploreViewController - foundPlaces - \(String(describing: placeType))")
		
		self.loadingIndicator.stopAnimating()
		self.loadingIndicator.isHidden = true
		
		if let placeType = placeType {
			self.places[placeType] = placeArray
		}
		else {
			self.searchedPlaces = placeArray
		}
		
		//self.collectionView.collectionViewLayout.invalidateLayout()
		self.collectionView.reloadData()
	}
	
	func foundSuggestions(places placeArray: [GooglePlaceObject], sender: AnyObject) {
		//	Suggestions
	}
	
	func loadedImages(_ sender: AnyObject) {
		self.collectionView.reloadData()
	}
}
