//
//  ExploreViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

@objc
protocol ExploreViewControllerDelegate: class {
	func cooperToAddress(_ place: GooglePlaceObject)
}

class ExploreViewController: UIViewController {
	
	//	============================================================================================================
	//	MARK:- Constants
	//	============================================================================================================
	
	/**
	The different sections of the explore view correspond to the different categories which will be searched using the
	google places api.
	*/
	let sections: [String] = [
		"Restaurants",
		"Clubs",
		"Hotels",
		"Beaches",
		"Airports",
		"Water Activities",
		"Historical Sites",
		"Kid Activities",
		"Malls & Shopping",
		"Government Offices",
		"Music & Entertainment",
		"Medical Facilities",
		"Cultural Events",
		"Events Happening Now"]
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var collectionViewBottomConstraint: NSLayoutConstraint!
	
	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
	
	@IBOutlet var searchView: UIView!
	@IBOutlet var searchHeight: 			NSLayoutConstraint!
	@IBOutlet var searchCancelButton:		UIButton!
	@IBOutlet var searchTextField: 		UITextField!
	
	//	Search results
	var searchedPlaces: [GooglePlaceObject] = [GooglePlaceObject]()
	var isSearching: Bool = false
	
	var places: [String:[GooglePlaceObject]] = [String:[GooglePlaceObject]]()
	var placesHelper: GooglePlacesSearchHelper?
	
	@objc weak var delegate: ExploreViewControllerDelegate?

	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	class func initController() -> ExploreViewController {
		return ExploreViewController(
			nibName: "ExploreViewController",
			bundle: nil)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	//	============================================================================================================
	//	MARK:- View loading appearance and dissappearance
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

		//	Setup the navigation controller
		self.configureNavigationController()
		
		//	Setup the collection view
		self.collectionView.register(
			ExplorePlaceCell.self,
			forCellWithReuseIdentifier: "ExplorePlaceCell")
		self.collectionView.register(
			ExploreHeadingCell.self,
			forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
			withReuseIdentifier: "ExploreHeadingCell")
		
		self.collectionView.delegate = self
		self.collectionView.dataSource = self
		
		self.searchTextField.delegate = self
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(ExploreViewController.placeUpdatedNotification(_:)),
			name: CoOperNotification.placeUpdatedNotification,
			object: nil)
		
		//	Keyboard responders
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(ExploreViewController.keyboardWillShow(_:)),
			name: Notification.Name.UIKeyboardWillShow,
			object: nil)
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(ExploreViewController.keyboardWillShow(_:)),
			name: Notification.Name.UIKeyboardWillChangeFrame,
			object: nil)
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(ExploreViewController.keyboardWillHide(_:)),
			name: Notification.Name.UIKeyboardWillHide,
			object: nil)
    }
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		if self.placesHelper == nil {
			//self.redrawShadow()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if self.placesHelper == nil {
			self.loadingIndicator.isHidden = false
			self.loadingIndicator.startAnimating()
			
			self.placesHelper = GooglePlacesSearchHelper()
			self.placesHelper?.delegate = self
		}
		else {
			self.collectionView.reloadData()
		}
		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		if let viewControllers = self.navigationController?.viewControllers,
			viewControllers.count <= 1 {
			NotificationCenter.default.removeObserver(self)
		}
	}
	//	------------------------------------------------------------------------------------------------------------

	
	//	------------------------------------------------------------------------------------------------------------
	func configureNavigationController() {
		
		if #available(iOS 11.0, *) {
			self.navigationController?.navigationBar.prefersLargeTitles = true
			self.navigationItem.largeTitleDisplayMode = .always
		}
		
		let closeButton = UIBarButtonItem(
		image: UIImage(named: "close_x_icon"), style: .plain,
		target: self, action: #selector(ExploreViewController.backButtonPressed(_:)))
		self.navigationItem.leftBarButtonItem = closeButton
		
		let searchButton = UIBarButtonItem(
			image: UIImage(named: "search_button_small"), style: .plain,
			target: self, action: #selector(ExploreViewController.searchButtonPressed(_:)))
		self.navigationItem.rightBarButtonItem = searchButton
		
		self.navigationItem.title = "Explore"
		
		self.navigationController?.navigationBar.isTranslucent = false
		self.navigationController?.navigationBar.barTintColor = .black
		self.navigationController?.navigationBar.barStyle = .blackOpaque
		self.navigationController?.navigationBar.backgroundColor = .black
		self.navigationController?.navigationBar.tintColor = .white
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	
	
	
	//	============================================================================================================
	//	MARK:- IBActions
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc func backButtonPressed(_ sender: AnyObject) {
		
		if self.navigationController != nil && self.navigationController!.viewControllers.count > 1 {
			self.navigationController?.popViewController(animated: true)
		}
		else {
			self.navigationController?.presentingViewController?.dismiss(
				animated: true,
				completion: nil)
		}		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	@objc func searchButtonPressed(_ sender: AnyObject) {
		
		self.searchHeight.constant = 44.0
		self.view.setNeedsLayout()
		
		UIView.animate(
			withDuration: 0.15,
			animations: {
				self.searchView.alpha = 1
				self.view.layoutIfNeeded()
		}) { (complete: Bool) in
			self.isSearching = true
			self.collectionView.reloadData()
			self.searchTextField.becomeFirstResponder()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func searchCancelButtonPressed(_ sender: AnyObject) {
		
		self.searchTextField.resignFirstResponder()
		self.searchHeight.constant = 0
		self.view.setNeedsLayout()
		
		UIView.animate(
			withDuration: 0.15,
			animations: {
				self.searchView.alpha = 1
				self.view.layoutIfNeeded()
		}) { (complete: Bool) in
			self.isSearching = false
			self.searchedPlaces.removeAll()
			self.collectionView.reloadData()
		}
	}
	//	------------------------------------------------------------------------------------------------------------
	
}







//	============================================================================================================
//	MARK:- Text Field Delegate
//	============================================================================================================

extension ExploreViewController: UITextFieldDelegate {
	
	//	------------------------------------------------------------------------------------------------------------
	func textField(
		_ textField: UITextField,
		shouldChangeCharactersIn range: NSRange,
		replacementString string: String) -> Bool {
		
		if let text = textField.text {
			let newString = (text as NSString).replacingCharacters(in: range, with: string)
			if newString.count >= 1 {
				self.loadingIndicator.isHidden = false
				self.loadingIndicator.startAnimating()
				
				self.placesHelper?.performPlaceSearch(
					withTextString: newString,
					isSearch: true)
			}
			else if newString.count <= 0 {
				self.searchedPlaces.removeAll()
				self.collectionView.reloadData()
			}
		}
		
		return true
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func textFieldShouldClear(
		_ textField: UITextField) -> Bool {
		
		self.searchedPlaces.removeAll()
		self.collectionView.reloadData()
		return true
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func textFieldShouldReturn(
		_ textField: UITextField) -> Bool {
		
		if let text = textField.text,
			text.count > 0,
			self.loadingIndicator.isHidden {
			
			self.placesHelper?.performPlaceSearch(
				withTextString: text,
				isSearch: true)
		}
		else {
			self.searchCancelButtonPressed(self)
		}
		
		return true
	}
	//	------------------------------------------------------------------------------------------------------------
}


