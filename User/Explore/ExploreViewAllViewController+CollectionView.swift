//
//  ExploreViewAllViewController+CollectionView.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension ExploreViewAllViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	//	------------------------------------------------------------------------------------------------------------
	/**
	The number of sections in the collection view corresponds to the number of different categories of place
	searched for using the google places API.
	*/
	func numberOfSections(
		in collectionView: UICollectionView) -> Int {
		
		return 1
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func collectionView(
		_ collectionView: UICollectionView,
		numberOfItemsInSection section: Int) -> Int {
		
		return self.places.count
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func collectionView(
		_ collectionView: UICollectionView,
		cellForItemAt
		indexPath: IndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExplorePlaceCell", for: indexPath) as! ExplorePlaceCell
		let place = self.places[indexPath.row]
		var titleString = ""
		var addressString = ""
		if place.name != nil {
			titleString = place.name!
		}
		if place.address != nil {
			addressString = place.address!
		}
		
		cell.configure(
			withTitle: titleString,
			withAddress: addressString,
			withImage: place.placeImage)
		return cell
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func collectionView(
		_ collectionView: UICollectionView,
		didSelectItemAt indexPath: IndexPath) {
		// 	Show place details
		let place = self.places[indexPath.row]
		
		let placeDetails = ExplorePlaceDetailsViewController.initController()
		placeDetails.place = place
		placeDetails.delegate = self.delegate
		
		self.navigationController?.pushViewController(placeDetails, animated: true)
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func collectionView(
		_ collectionView: UICollectionView,
		layout collectionViewLayout: UICollectionViewLayout,
		sizeForItemAt indexPath: IndexPath) -> CGSize {
		
		return CGSize(
			width: collectionView.frame.size.width,
			height: 64.0)
	}
	//	------------------------------------------------------------------------------------------------------------
}
