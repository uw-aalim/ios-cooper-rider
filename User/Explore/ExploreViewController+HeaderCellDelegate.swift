//
//  ExploreViewController+HeaderCellDelegate.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension ExploreViewController: ExploreHeadingCellDelegate {

	func viewAllButtonPressed(
		atIndexPath indexPath: IndexPath,
		sender: AnyObject) {
		
		let section = self.sections[indexPath.section]
		if let places = self.places[section] {
			let exploreViewAllController = ExploreViewAllViewController.initController()
			exploreViewAllController.delegate = self.delegate
			exploreViewAllController.places = places
			exploreViewAllController.section = section
			self.navigationController?.pushViewController(exploreViewAllController, animated: true)
		}
	}
}
