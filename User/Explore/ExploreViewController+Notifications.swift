//
//  ExploreViewController+Notifications.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-22.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

@objc
extension ExploreViewController {
	
	//	------------------------------------------------------------------------------------------------------------
	/**
	Whenever an image is fetched the collection view reloads the data to load the image instead.
	*/
	func placeUpdatedNotification(_ notification: Notification) {
		DispatchQueue.main.async {
			self.collectionView.reloadData()
		}		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	func keyboardWillShow(_ notification: Notification) {
		
		let info = notification.userInfo!
		let keyboardFrame = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
		let animationDuration = (info[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
		
		self.collectionViewBottomConstraint.constant = keyboardFrame.size.height
		
		UIView.animate(withDuration: animationDuration!) {
			self.view.layoutIfNeeded()
		}
	}
	
	func keyboardWillHide(_ notification: Notification) {
		
		let info = notification.userInfo!
		let animationDuration = (info[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
		
		self.collectionViewBottomConstraint.constant = 0
		
		UIView.animate(withDuration: animationDuration!) {
			self.view.layoutIfNeeded()
		}
	}
}
