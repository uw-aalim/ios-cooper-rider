//
//  ExplorePlaceCell.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class ExplorePlaceCell: UICollectionViewCell {
	
	@IBOutlet weak var placeTitleLabel: UILabel!
	@IBOutlet weak var placeAddressLabel: UILabel!
	@IBOutlet weak var placeImageView: UIImageView!
	
	
	//	------------------------------------------------------------------------------------------------------------
	override init(frame: CGRect) {
		super.init(frame: frame)
		let _ = self.loadView(with: "ExplorePlaceCell")
		self.performSetup()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		let _ = self.loadView(with: "ExplorePlaceCell")
		self.performSetup()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func performSetup() {
		
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	override func prepareForReuse() {
		super.prepareForReuse()
		self.placeImageView.image = UIImage(named: "image_placeholder")
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	/**
	Configure the place cell with a place title (usually a business name) and address. This function assumes that
	the place title and address strings have already been localized. If no image is found the image will
	be hidden.
	*/
	func configure(
		withTitle title: String,
		withAddress address: String,
		withImage image: UIImage?) {
		
		self.placeTitleLabel.text = title
		self.placeAddressLabel.text = address
		
		if let image = image {
			self.placeImageView.image = image
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}
