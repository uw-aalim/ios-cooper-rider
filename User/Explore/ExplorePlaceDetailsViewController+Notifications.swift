//
//  ExplorePlaceDetailsViewController+Notifications.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-22.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

@objc
extension ExplorePlaceDetailsViewController {
	
	//	------------------------------------------------------------------------------------------------------------
	/**
	Whenever an image is fetched the collection view reloads the data to load the image instead.
	*/
	func placeUpdatedNotification(_ notification: Notification) {
		DispatchQueue.main.async {
			self.loadPlaceData()
		}		
	}
	//	------------------------------------------------------------------------------------------------------------
}
