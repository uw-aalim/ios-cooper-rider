//
//  LeftHederview.m
//  Provider
//
//  Created by CSS on 09/10/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "LeftHederview.h"

@implementation LeftHederview

- (void)awakeFromNib {
    [super awakeFromNib];
    self.profileImagebtn.layer.cornerRadius=self.profileImagebtn.frame.size.height/2;
    self.profileImagebtn.clipsToBounds=NO;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
