//
//  ViewController.h
//  User
//
//  Created by iCOMPUTERS on 12/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UIView *xuberView;
@property (weak, nonatomic) IBOutlet UILabel *socialLbl;
@property (weak, nonatomic) IBOutlet UILabel *lblAppName;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnMailId;
@property (weak, nonatomic) IBOutlet UIButton *btnSocial;
@property (weak, nonatomic) IBOutlet UILabel *btnTitle;
@property (weak, nonatomic) IBOutlet UIImageView *bgImg;

@end

