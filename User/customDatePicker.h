//
//  customDatePicker.h
//  DocterApp
//
//  Created by CSS on 27/11/17.
//  Copyright © 2017 Appoets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customDatePicker : UIView
@property (nonatomic, assign, readonly) UIDatePicker *picker;
@property(strong,nonatomic)NSString * SelectedPickervalues;
@property(strong,nonatomic)NSString * DisplayPickervalues;

@property(strong,nonatomic)NSString * identifier;
- (void) setMode: (UIDatePickerMode) mode;
-(void)SetValuesRefresh;
- (void) addTargetForDoneButton: (id) target action: (SEL) action;
@end
