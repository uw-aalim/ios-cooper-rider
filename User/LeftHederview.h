//
//  LeftHederview.h
//  Provider
//
//  Created by CSS on 09/10/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Colors.h"
#import "CSS_Class.h"
#import "LeftHederview.h"

@interface LeftHederview : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *profileImagebtn;
@property (weak, nonatomic) IBOutlet UILabel *namelbl;
@property (weak, nonatomic) IBOutlet UIImageView *green_imageview;
@property (weak, nonatomic) IBOutlet UILabel *statusLbl;
@property (weak, nonatomic) IBOutlet UIView *baseview;

@end
