//
//  UIColor+CoOper-Objc.h
//  User
//
//  Created by Benjamin Cortens on 2018-10-26.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef UIColor_CoOper_Objc_h
#define UIColor_CoOper_Objc_h

@interface UIColor (cooper)
+(UIColor *) tealColourObjC;
+(UIColor *) orangeColourObjC;
@end

#endif /* UIColor_CoOper_Objc_h */
