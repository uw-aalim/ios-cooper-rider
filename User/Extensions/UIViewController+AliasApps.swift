//
//  UIViewController+AliasApps.swift
//  User
//
//  Created by Benjamin Cortens on 2018-06-07.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

extension UIViewController {
	
	func showError(withTitle title: String?, andMessage message: String?) {
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		self.present(alert, animated: true, completion: nil)
	}
}
