//
//  UIView+AliasApps.swift
//  User
//
//  Created by Benjamin Cortens on 2018-05-18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import Foundation

extension UIView {
	
	//	============================================================================================================
	//	MARK:- View Nib Loader
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@objc
	func loadView(with nibName: String) -> UIView {
		let bundle = Bundle(for: type(of: self))
		let nib = UINib(nibName: nibName, bundle: bundle)
		let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
		self.addSubview(view)
		self.backgroundColor = UIColor.clear
		
		view.translatesAutoresizingMaskIntoConstraints = false
		
		//	Conigure the views constraints within the superview
		//	Constrain top, bottom, left and right to the edges of the superview
		let leading = NSLayoutConstraint(
			item: view,
			attribute: .leading,
			relatedBy: .equal,
			toItem: self,
			attribute: .leading,
			multiplier: 1,
			constant: 0)
		let trailing = NSLayoutConstraint(
			item: self,
			attribute: .trailing,
			relatedBy: .equal,
			toItem: view,
			attribute: .trailing,
			multiplier: 1,
			constant: 0)
		let top = NSLayoutConstraint(
			item: view,
			attribute: .top,
			relatedBy: .equal,
			toItem: self,
			attribute: .top,
			multiplier: 1,
			constant: 0)
		let bottom = NSLayoutConstraint(
			item: self,
			attribute: .bottom,
			relatedBy: .equal,
			toItem: view,
			attribute: .bottom,
			multiplier: 1,
			constant: 0)
		
		self.addConstraint(leading)
		self.addConstraint(trailing)
		self.addConstraint(top)
		self.addConstraint(bottom)
		
		return view
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	//	============================================================================================================
	//	MARK:- Constrain To a Container
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	/**
	Once you have added a view as a subview to a container view then you may want to constrain it to the container view.
	*/
	@objc
	func constrain(toContainerView containerView: UIView) {
		
		self.translatesAutoresizingMaskIntoConstraints = false
		
		//	Conigure the views constraints within the superview
		//	Constrain top, bottom, left and right to the edges of the superview
		let leading = NSLayoutConstraint(
			item: self,
			attribute: .leading,
			relatedBy: .equal,
			toItem: containerView,
			attribute: .leading,
			multiplier: 1,
			constant: 0)
		let trailing = NSLayoutConstraint(
			item: containerView,
			attribute: .trailing,
			relatedBy: .equal,
			toItem: self,
			attribute: .trailing,
			multiplier: 1,
			constant: 0)
		let top = NSLayoutConstraint(
			item: self,
			attribute: .top,
			relatedBy: .equal,
			toItem: containerView,
			attribute: .top,
			multiplier: 1,
			constant: 0)
		let bottom = NSLayoutConstraint(
			item: containerView,
			attribute: .bottom,
			relatedBy: .equal,
			toItem: self,
			attribute: .bottom,
			multiplier: 1,
			constant: 0)
		
		containerView.addConstraint(leading)
		containerView.addConstraint(trailing)
		containerView.addConstraint(top)
		containerView.addConstraint(bottom)
	}
	//	------------------------------------------------------------------------------------------------------------
}
