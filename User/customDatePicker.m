//
//  customDatePicker.m
//  DocterApp
//
//  Created by CSS on 27/11/17.
//  Copyright © 2017 Appoets. All rights reserved.
//

#import "customDatePicker.h"
#define MyDateTimePickerToolbarHeight 40
@interface customDatePicker()

@property (nonatomic, assign, readwrite) UIDatePicker *picker;

@property (nonatomic, assign) id doneTarget;
@property (nonatomic, assign) SEL doneSelector;

- (void) donePressed;

@end

@implementation customDatePicker

@synthesize picker = _picker;

@synthesize doneTarget = _doneTarget;
@synthesize doneSelector = _doneSelector;

-(void)SetValuesRefresh{
    _SelectedPickervalues = [[NSString alloc]init];
    _DisplayPickervalues = [[NSString alloc]init];
}
- (id) initWithFrame: (CGRect) frame {
    if ((self = [super initWithFrame: frame])) {
        self.backgroundColor = [UIColor clearColor];
        
        UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame: CGRectMake(0, MyDateTimePickerToolbarHeight, frame.size.width, frame.size.height - MyDateTimePickerToolbarHeight)];
        [self addSubview: picker];
        picker.minimumDate = [NSDate date];
        
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, frame.size.width, MyDateTimePickerToolbarHeight)];
        toolbar.barStyle = UIBarButtonItemStylePlain;
        toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        
//        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle: @"Done" style: UIBarButtonItemStylePlain target: self action: @selector(donePressed)];
        UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        toolbar.items = [NSArray arrayWithObjects:flexibleSpace, nil];
        
        [self addSubview: toolbar];
        
        self.picker = picker;
        picker.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
        [picker addTarget:self action:@selector(TextTitle:)   forControlEvents:UIControlEventValueChanged];

        self.autoresizesSubviews = YES;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    }
    return self;
}

-(void)TextTitle:(id)sender
{
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd-MM-yyyy hh:mma"];
        _SelectedPickervalues = [NSString stringWithFormat:@"%@",
                                 [df stringFromDate:_picker.date]];
}
- (void) setMode: (UIDatePickerMode) mode {
    
    self.picker.datePickerMode = UIDatePickerModeDateAndTime;
}

- (void) donePressed {
    
    if (self.doneTarget) {
        [self.doneTarget performSelector:self.doneSelector withObject:nil afterDelay:0];
    }
}

- (void) addTargetForDoneButton: (id) target action: (SEL) action {
    self.doneTarget = target;
    self.doneSelector = action;
}

@end
