//
//  newHomeController.h
//  User
//
//  Created by CSS on 18/01/18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftMenuView.h"
#import "LoadingViewClass.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationViewController.h"
#import "HCSStarRatingView.h"
#import "PaymentsViewController.h"
#import "HelpViewController.h"
#import <MessageUI/MessageUI.h>
#import "SettingsController.h"
#import "LeftMenuView.h"
#import <Lottie/Lottie.h>
#import "customDatePicker.h"

@protocol ExploreViewControllerDelegate;

@interface CategoryListCell :UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *categoryImageview;
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *categorySmallImageview;

@end

@interface newHomeController : UIViewController<GMSMapViewDelegate,CLLocationManagerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,LeftMenuViewprotocol,CardDetailsSend,UIGestureRecognizerDelegate,ExploreViewControllerDelegate> {
	
    GMSPlacesClient *_placesClient;
    LeftMenuView *leftMenuViewClass;
    AppDelegate *appDelegate;
    UIView *waitingBGView;
    CLGeocoder *geocoder;
    LoadingViewClass *loading;
    NSTimer *timerLocationUpdate,*timeRequestCheck;
    NSString * strKm ,*strRating;
    CLLocation * CurrentUpdateLocation;
    int contrsinsUpdate;
    customDatePicker * picker;
    NSString * isValid;
    NSString * usrnameStr;
    NSString * passwordStr;
    NSInteger selectedIndex;


}

@property (nonatomic) BOOL didAppear;

@property (weak, nonatomic) IBOutlet UIButton *homeBtn;
@property (weak, nonatomic) IBOutlet UIButton *workBtn;

@property (weak, nonatomic) IBOutlet UIButton *sosBtn;

- (IBAction)locationPickBtnDidTab:(UIButton *)sender;

#pragma RideViews
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@property (weak, nonatomic) IBOutlet UIView *WhereView;
@property (weak, nonatomic) IBOutlet UIButton *exploreButton;

@property (weak, nonatomic) IBOutlet UICollectionView *serviceListCollection;
@property (weak, nonatomic) IBOutlet UILabel *serviceCategoryLbl;
@property (weak, nonatomic) IBOutlet UIView *paymentView;
- (IBAction)whereBtnDidTab:(UIButton *)sender;
- (IBAction)sosBtnDidTab:(UIButton *)sender;


#pragma RideInfo

@property (weak, nonatomic) IBOutlet UILabel *vehicleNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *ProviderTypeLbl;
@property (weak, nonatomic) IBOutlet UIImageView *vehicleImageview;
@property (weak, nonatomic) IBOutlet UIImageView *paymentTypeimageview;
@property (weak, nonatomic) IBOutlet UILabel *paymentModeLbl;
@property (weak, nonatomic) IBOutlet UIButton *paymentChangeBtn;
@property (weak, nonatomic) IBOutlet UIButton *getPricingBtm;
@property (weak, nonatomic) IBOutlet UIView *SourceandDestinationview;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UILabel *otpLbl;

#pragma Source and destination veiw

@property (weak, nonatomic) IBOutlet UIButton *destinationTitleBtn;
@property (weak, nonatomic) IBOutlet UIButton *sourceTitleBtn;
#pragma Objects
@property (strong,nonatomic)NSMutableArray * FavArray,* workArray;

@property(strong,nonatomic)NSMutableArray * markerList;
@property(strong,nonatomic) NSMutableArray  *categoryList;
- (IBAction)getRideInfoAction:(UIButton *)sender;
- (IBAction)changePaymentModeAction:(UIButton *)sender;

#pragma contrains

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeightConstrainsWhereview;


#pragma Fareview

@property (weak, nonatomic) IBOutlet UIView *fareinfoView;
@property (weak, nonatomic) IBOutlet UILabel *surgeLbl;
@property (weak, nonatomic) IBOutlet UILabel *surgeInfoLbl;
@property (weak, nonatomic) IBOutlet UILabel *estimateFareValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *EtaValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *bookedForValueLbl;
@property (weak, nonatomic) IBOutlet UIButton *useWalletBtn;
@property (weak, nonatomic) IBOutlet UIButton *schedulriideBtn;
@property (weak, nonatomic) IBOutlet UIButton *rideNowBtn;
@property (weak, nonatomic) IBOutlet UIView *surgeView;
@property (weak, nonatomic) IBOutlet UILabel *availableWalletTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *AvailableWalletAmountValueLbl;
- (IBAction)useWalletBtnAction:(UIButton *)sender;
- (IBAction)sceduleRideBtnAction:(UIButton *)sender;
- (IBAction)rideNowBtnAction:(UIButton *)sender;
- (IBAction)menuBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *fareTotlaDistanceValue;


#pragma SeachingView

@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UIButton *cancelRequestBtn;
@property (weak, nonatomic) IBOutlet UIImageView *searchAnimationView;
- (IBAction)cancelRequestBtnAction:(UIButton *)sender;

#pragma RideStarting View

@property (weak, nonatomic) IBOutlet UIView *RideStatusView;
@property (weak, nonatomic) IBOutlet UILabel *rideStatuLbl;
@property (weak, nonatomic) IBOutlet UILabel *rideSurgeLbl;
@property (weak, nonatomic) IBOutlet UIImageView *rideProviderImageview;
@property (weak, nonatomic) IBOutlet UIImageView *rideVehicleImageview;
@property (weak, nonatomic) IBOutlet UILabel *rideProviderName;
@property (weak, nonatomic) IBOutlet UILabel *RideVehicleName;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *rideRattingView;
@property (weak, nonatomic) IBOutlet UIButton *RideCallBtn;
@property (weak, nonatomic) IBOutlet UIButton *RideCall_ShareBtn;
@property (weak, nonatomic) IBOutlet UILabel *rideVehicleModelLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rideInfoViewHeightConstrains;
@property (weak, nonatomic) IBOutlet UIButton *RideTabWhenKeyBtn;

- (IBAction)RideCallBtnAction:(UIButton *)sender;
- (IBAction)rideCall_ShareBtnAction:(UIButton *)sender;


#pragma InvoiceView Appear

@property (weak, nonatomic) IBOutlet UIView *InvoiceView;
@property (weak, nonatomic) IBOutlet UILabel *invoiceBookingIdLbl;
@property (weak, nonatomic) IBOutlet UILabel *invoiceDistanceTravelLbl;
@property (weak, nonatomic) IBOutlet UILabel *InvoiceTimeTakenLbl;
@property (weak, nonatomic) IBOutlet UILabel *InvoiceBasefareLbl;
@property (weak, nonatomic) IBOutlet UILabel *InvoiceDistanceLbl;
@property (weak, nonatomic) IBOutlet UILabel *InvoiceTaxLbl;
@property (weak, nonatomic) IBOutlet UILabel *InvoiceWalletLbl;
@property (weak, nonatomic) IBOutlet UILabel *InvoiceTotlalbl;
@property (weak, nonatomic) IBOutlet UIButton *InvoicePayNowBtn;
@property (weak, nonatomic) IBOutlet UIImageView *InvoicePaymentImageview;
@property (weak, nonatomic) IBOutlet UILabel *InvoicePaymemtmodelLbl;
@property (weak, nonatomic) IBOutlet UILabel *invoiceWalletTitleLbl;
- (IBAction)PaymentBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *waitingforPaymentView;
@property (weak, nonatomic) IBOutlet UILabel *INvoiceDivider;
@property (weak, nonatomic) IBOutlet UILabel *invoiceDiscountLbl;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *RideInfoCarKeyBtnHeightConstains;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *RideBottomConstraints;
- (IBAction)RecivedKeyBtnAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *RateView;
@property (weak, nonatomic) IBOutlet UILabel *RatelblInfo;
@property (weak, nonatomic) IBOutlet UIImageView *RateproviderImageview;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *RateRateView;
@property (weak, nonatomic) IBOutlet UITextView *RateCommentsView;
- (IBAction)RateSubmitBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *RateSubmitBtnAction;
@property (weak, nonatomic) IBOutlet UIButton *currentLocationIcon;
- (IBAction)currentLocationBtnAction:(UIButton *)sender;


#pragma Schedule View
@property (weak, nonatomic) IBOutlet UIView *scheduleView;
@property (weak, nonatomic) IBOutlet UIButton *scheduleRequestBtn;
- (IBAction)scheduleRequestBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *pickerUpdateview;


-(IBAction)exploreButtonPressed: (UIButton *) sender;


-(void)cooperToAddress: (NSString *) addressString ;

@end
