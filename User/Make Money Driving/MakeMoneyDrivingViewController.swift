//
//  MakeMoneyDrivingViewController.swift
//  User
//
//  Created by Benjamin Cortens on 2018-11-05.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

import UIKit

class MakeMoneyDrivingViewController: UIViewController {

	//	============================================================================================================
	//	MARK:- Properties
	//	============================================================================================================
	
	@IBOutlet var largeTitleViewTop: NSLayoutConstraint!
	@IBOutlet var titleBarLabel: UILabel!
	@IBOutlet var scrollView: UIScrollView!
	
	
	
	//	============================================================================================================
	//	MARK:- Initialization
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	/*@objc
	class func initController() -> MakeMoneyDrivingViewController {
		return MakeMoneyDrivingViewController(
			nibName: "MakeMoneyDrivingViewController",
			bundle: nil)
	}*/
	class func initController() -> MakeMoneyDrivingViewController {
		let storyboard = UIStoryboard(name: "MakeMoneyDriving", bundle: nil)
		let viewController = storyboard.instantiateViewController(
			withIdentifier: "MakeMoneyDrivingViewController") as! MakeMoneyDrivingViewController
		return viewController
	}
	//	------------------------------------------------------------------------------------------------------------
	

	
	
	
	//	============================================================================================================
	//	MARK:- View Loading and Appearance
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.configureView()
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	//	------------------------------------------------------------------------------------------------------------
	func configureView() {
		self.scrollView.delegate = self
	}
	//	------------------------------------------------------------------------------------------------------------
	
	
	
	//	============================================================================================================
	//	MARK:- Action Responder
	//	============================================================================================================
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func getApp(_ sender: AnyObject) {
		/*
		let urlStr = "itms://itunes.apple.com/us/app/apple-store/id375380948?mt=8"
		if #available(iOS 10.0, *) {
			UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
			
		}
		else {
			UIApplication.shared.openURL(URL(string: urlStr)!)
		}*/
		//let alertController = UIAlertController(title: "Not Implemented", message: "This feature has not yet been implemented as the driver app is not yet in the app store", preferredStyle: .alert)
		//self.present(alertController, animated: true, completion: nil)
		self.showError(
			withTitle: "Not Implemented",
			andMessage: "This feature has not yet been implemented as the driver app is not yet in the app store")
	}
	//	------------------------------------------------------------------------------------------------------------
	
	//	------------------------------------------------------------------------------------------------------------
	@IBAction func backPressed(_ sender: AnyObject) {
		//let _ = self.navigationController?.popViewController(animated: true)
		if self.navigationController != nil &&
			self.navigationController!.viewControllers.count > 1 {
			
			self.navigationController?.popViewController(animated: true)
		}
		else {
			
			self.navigationController?.presentingViewController?.dismiss(
				animated: true,
				completion: nil)
		}
	}
	//	------------------------------------------------------------------------------------------------------------
}


