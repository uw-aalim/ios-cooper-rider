//
//  AppDelegate.m
//  User
//
//  Created by iCOMPUTERS on 18/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "LoadingViewClass.h"
#import "HomeViewController.h"
#import "Stripe.h"
#import <SplunkMint/SplunkMint.h>
#import "LanguageController.h"
#import "newHomeController.h"
#import "Harpy.h"
#import "User-Swift.h"
#import "IQKeyboardManager.h"


static NSString *const kTrackingId = @"UA-107140754-1";
static NSString *const kAllowTracking = @"allowTracking";

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@import GoogleMaps;
@import GooglePlaces;
@class LoadingViewClass;
@import SplunkMint;

@interface AppDelegate ()<HarpyDelegate>
{
    LoadingViewClass *loader;
}

@end

@implementation AppDelegate
@synthesize strDeviceToken,strEmail;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	
	NSLog( @"### running FB sdk version: %@", [FBSDKSettings sdkVersion] );
    self.strDeviceToken=@"no device";
    self.strEmail=@"";
    self.isLocationUpdate = YES;

    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setPreviousNextDisplayMode:IQPreviousNextDisplayModeAlwaysHide];
    
    [self CheckversionUpdate];

    NSDictionary *appDefaults = @{kAllowTracking: @(YES)};
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    // User must be able to opt out of tracking
//    [GAI sharedInstance].optOut =
//    ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
//    
//    // If your app runs for long periods of time in the foreground, you might consider turning
//    // on periodic dispatching.  This app doesn't, so it'll dispatch all traffic when it goes
//    // into the background instead.  If you wish to dispatch periodically, we recommend a 120
//    // second dispatch interval.
//    // [GAI sharedInstance].dispatchInterval = 120;
//    [GAI sharedInstance].dispatchInterval = 120;
//    
//    [GAI sharedInstance].trackUncaughtExceptions = YES;
//    self.tracker = [[GAI sharedInstance] trackerWithName:@"coOper"
//                                              trackingId:kTrackingId];
//    
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [launchOptions valueForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    application.applicationIconBadgeNumber = 0;
    
    [GIDSignIn sharedInstance].delegate = self;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [[NSUserDefaults standardUserDefaults] setObject: strDeviceToken forKey:@"device_Token"];
    
    NSString*devType=@"ios";
    [[NSUserDefaults standardUserDefaults]setObject:devType forKey:@"device_type"];
    
    
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    
    GOOGLE_API_KEY = [user objectForKey:@"placekey"];
    
    NSString * mapKey = [defaults valueForKey:@"placekey"];
    NSLog(@"%@",mapKey);

	//	Configure the map key
    if(mapKey == nil || [mapKey isEqualToString:@""]){
        [GMSServices provideAPIKey:@"AIzaSyAsVN1AHgbMr2n3SYtNZxFiT7e1bz3tP5s"];
        [GMSPlacesClient provideAPIKey:@"AIzaSyAsVN1AHgbMr2n3SYtNZxFiT7e1bz3tP5s"];
    }
	else {
        [GMSServices provideAPIKey:GOOGLE_API_KEY];
        [GMSPlacesClient provideAPIKey:GOOGLE_API_KEY];
    }
//    GoogleAnalytics.setTrackerId
    
   // GOOGLE_API_KEY = @"AIzaSyBs7Ct-_9SrgBE2rw0pN6U5dgErrm7jZSA";
//    [GMSServices provideAPIKey:GOOGLE_API_KEY];
//    [GMSPlacesClient provideAPIKey:GOOGLE_API_KEY];
    [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:Stripe_KEY];
    
    [self googleAnaliticsImplement];
    
//    BOOL yes=  [defaults valueForKey:@"isLoggedin"];
	
	//	Get the token from the user defaults
    NSString *tokenStr = [defaults valueForKey:@"access_token"];
	
    if (tokenStr == (id)[NSNull null] ||
		tokenStr.length == 0 ||
		[tokenStr isEqualToString:@""]) {
		
		//	Instead of loading the PageViewController as the default root we are going to instead always load the new HomescreenViewControlelr
		[[NSUserDefaults standardUserDefaults]setValue:@"Load" forKey:@"FirstLoad"];
            
      /*      UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            PageViewController * infoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];
            
            self.navigationController = [[UINavigationController alloc] initWithRootViewController:infoController];
            self.window.rootViewController = self.navigationController;
            self.window.backgroundColor = [UIColor whiteColor];
            [self.window makeKeyAndVisible];*/
		
	}
	else {
		
		//NSString * validBoolStr = [defaults valueForKey:@"is_valid"];
		
		//int boolCheck = [validBoolStr intValue];
		
		/*
		if (boolCheck == 1) {
			UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"New" bundle: nil];
			
			newHomeController * infoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"newHomeController"];
			self.navigationController = [[UINavigationController alloc] initWithRootViewController:infoController];
			self.window.rootViewController = self.navigationController;
			self.window.backgroundColor = [UIColor whiteColor];
			[self.window makeKeyAndVisible];
		}
		else{
			
			UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
			PageViewController * infoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];
			
			self.navigationController = [[UINavigationController alloc] initWithRootViewController:infoController];
			self.window.rootViewController = self.navigationController;
			self.window.backgroundColor = [UIColor whiteColor];
			[self.window makeKeyAndVisible];
		} */
		
		/*
		 
		 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
		 PageViewController * infoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];
		 self.navigationController = [[UINavigationController alloc] initWithRootViewController:infoController];
		 self.window.rootViewController = self.navigationController;
		 self.window.backgroundColor = [UIColor whiteColor];
		 [self.window makeKeyAndVisible];
		 
		 */
		
	}
    LocalizationSetLanguage([[NSUserDefaults standardUserDefaults] objectForKey:@"selectLanguage"]);
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"selectLanguage"]==nil){
        LocalizationSetLanguage(@"en");
        [[NSUserDefaults standardUserDefaults]setValue:@"en" forKey:@"selectLanguage"];
        
    }
	else{
        LocalizationSetLanguage([[NSUserDefaults standardUserDefaults]objectForKey:@"selectLanguage"]);
    }
	
	//	Show the HomescreenViewController as the main view controller to be loaded.
	UIStoryboard *homescreenStoryboard = [UIStoryboard storyboardWithName:@"Homescreen" bundle: nil];
	HomescreenViewController * homescreenViewController = [homescreenStoryboard instantiateViewControllerWithIdentifier:@"HomescreenViewController"];
	self.navigationController = [[UINavigationController alloc] initWithRootViewController:homescreenViewController];
	self.window.rootViewController = self.navigationController;
	self.window.backgroundColor = [UIColor whiteColor];
	[self.window makeKeyAndVisible];
	
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self registerForRemoteNotification];
    return YES;
}

-(void)CheckversionUpdate{
    [[Harpy sharedInstance] setPresentingViewController:_window.rootViewController];
    
    // (Optional) Set the Delegate to track what a user clicked on, or to use a custom UI to present your message.
    [[Harpy sharedInstance] setDelegate:self];
    
    // (Optional) When this is set, the alert will only show up if the current version has already been released for X days.
    // By default, this value is set to 1 (day) to avoid an issue where Apple updates the JSON faster than the app binary propogates to the App Store.
    //    [[Harpy sharedInstance] setShowAlertAfterCurrentVersionHasBeenReleasedForDays:3];
    
    // (Optional) The tintColor for the alertController
    //    [[Harpy sharedInstance] setAlertControllerTintColor:[UIColor purpleColor]];
    
    // (Optional) Set the App Name for your app
    //    [[Harpy sharedInstance] setAppName:@"iTunes Connect Mobile"];
    
    /* (Optional) Set the Alert Type for your app
     By default, Harpy is configured to use HarpyAlertTypeOption */
    [[Harpy sharedInstance] setAlertType:HarpyAlertTypeOption];
    
    /* (Optional) If your application is not available in the U.S. App Store, you must specify the two-letter
     country code for the region in which your applicaiton is available. */
    //    [[Harpy sharedInstance] setCountryCode:@"en-US"];
    
    /* (Optional) Overrides system language to predefined language.
     Please use the HarpyLanguage constants defined in Harpy.h. */
    //    [[Harpy sharedInstance] setForceLanguageLocalization:HarpyLanguageRussian];
    
    // Turn on Debug statements
    [[Harpy sharedInstance] setDebugEnabled:true];
    
    // Perform check for new version of your app
    [[Harpy sharedInstance] checkVersion];
    

}
-(void)googleAnaliticsImplement{
    GAI *gai = [GAI sharedInstance];
    [gai trackerWithTrackingId:kTrackingId];
    
    // Optional: automatically report uncaught exceptions.
    gai.trackUncaughtExceptions = NO;
    
    // Optional: set Logger to VERBOSE for debug information.
    // Remove before app release.
    gai.logger.logLevel = kGAILogLevelVerbose;
    // Optional: set Logger to VERBOSE for debug information.
   
}
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    UIAlertView *notificationAlert = [[UIAlertView alloc] initWithTitle:@"Notification"  message:@"Your battery power is low to continue, Please plug it to charge"
                                                               delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    [notificationAlert show];
    // NSLog(@"didReceiveLocalNotification");
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [GAI sharedInstance].optOut =
    ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark
#pragma mark - Check NetConnetion

-(BOOL)internetConnected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
	//NSLog(@"Internet Connected %@",networkStatus);
    return (networkStatus != NotReachable);
}
-(void)onStartLoader
{
	NSLog(@"AppDelegate - onStartLoaders");
    loader = [LoadingViewClass new];
    [loader startLoading];
    
}
-(void)onEndLoader
{
	NSLog(@"AppDelegate - onEndLoader");
    [loader stopLoading];
}

#pragma mark state preservation / restoration

- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
    return YES;
}

- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{
    return YES;
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //[self.window makeKeyAndVisible];
    return YES;
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{ BOOL device;
    device=NO;
    NSString *strDevicetoken = [[NSString alloc]initWithFormat:@"%@",[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    NSLog(@"Device Token = %@",strDevicetoken);
    self.strDeviceToken = strDevicetoken;
    
    [[NSUserDefaults standardUserDefaults] setObject: strDeviceToken forKey:@"device_Token"];
    
    NSString * deviceTokenString = [[[[deviceToken description]
                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    [[NSUserDefaults standardUserDefaults] setObject: deviceTokenString forKey:@"device_Token"];
    NSLog(@"device token %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"device_Token"]);
}

-(void)showLoadingWithTitle:(NSString *)title
{
    
}

-(void)hideLoadingView
{
}
-(void)addShadowto:(UIView *)view {
    // drop shadow
}
//connected
- (BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return (networkStatus != NotReachable);
}
//shared appdelegate
+(AppDelegate *)sharedAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"Push Notification Information : %@",userInfo);
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"%@ = %@", NSStringFromSelector(_cmd), error);
    NSLog(@"Error = %@",error);
}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    NSLog(@"User Info = %@",notification.request.content.userInfo);
    
    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
    NSLog(@"User Info = %@",response.notification.request.content.userInfo);
    completionHandler();
}

#pragma mark - Class Methods

/**
 Notification Registration
 */
- (void)registerForRemoteNotification {
	
	
	
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
				
				dispatch_async(dispatch_get_main_queue(), ^{
					[[UIApplication sharedApplication] registerForRemoteNotifications];
				});
            }
        }];
    }
    else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {

    BOOL handled =([[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ] || [[GIDSignIn sharedInstance] handleURL:url
                                             sourceApplication:sourceApplication
                                                    annotation:annotation]);
    // Add any custom logic here.
    return handled;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {

    return (
            [[FBSDKApplicationDelegate sharedInstance] application:application
                                                           openURL:url
                                                 sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                        annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
             ]
            ||
            [[GIDSignIn sharedInstance] handleURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]]
            );
}

#pragma mark - HarpyDelegate
- (void)harpyDidShowUpdateDialog
{
    NSLog(@"%s", __FUNCTION__);
}

- (void)harpyUserDidLaunchAppStore
{
    NSLog(@"%s", __FUNCTION__);
}

- (void)harpyUserDidSkipVersion
{
    NSLog(@"%s", __FUNCTION__);
}

- (void)harpyUserDidCancel
{
    NSLog(@"%s", __FUNCTION__);
}

- (void)harpyDidDetectNewVersionWithoutAlert:(NSString *)message
{
    NSLog(@"%@", message);
}


@end
