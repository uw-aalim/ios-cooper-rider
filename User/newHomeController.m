//
//  newHomeController.m
//  User
//
//  Created by CSS on 18/01/18.
//  Copyright © 2018 iCOMPUTERS. All rights reserved.
//

#import "newHomeController.h"

#import "WalletViewController.h"
#import "YourTripViewController.h"
#import "CouponsController.h"
#import "passbookViewController.h"
#import "ProfileViewController.h"
#import "newLocationController.h"
#import "Stripe.h"
#import "User-Swift.h"

@implementation CategoryListCell

-(void)awakeFromNib{
    [super awakeFromNib];
    _categoryNameLbl.backgroundColor = BASE2Color;
    
}


@end


@interface newHomeController ()
@property CLLocationManager * location;
@end

@implementation newHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    selectedIndex = 0;
    [self initializeDefaultValues];
//    [self serverHit];
   
    _RideInfoCarKeyBtnHeightConstains.constant =0.0;
    _location = [[CLLocationManager alloc] init];
    _markerList = [[NSMutableArray alloc]init];
    self.location.delegate = self;
    [self StatusUpdate:[KeyConstants sharedKeyConstants].RideStatus];
    self.location.desiredAccuracy = kCLLocationAccuracyBest;
    self.location.distanceFilter = kCLDistanceFilterNone;
    [_location requestWhenInUseAuthorization];
 
    [KeyConstants sharedKeyConstants].RideStatus = @"HOME";
    [KeyConstants sharedKeyConstants].WalletStatus = @"0";
    contrsinsUpdate = _topHeightConstrainsWhereview.constant;
    if ([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                             message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
            [alert show];
        }
    }
    [self BackgroundPerformeStart];
    _placesClient = [GMSPlacesClient sharedClient];
	
	self.didAppear = NO;
    
   // NSLog(@"%@",[KeyConstants sharedKeyConstants].Status);
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
	
	[self viewStyleUpdate];
    [self BackgroundPerformeStart];
    [self onGetAddress];
	
	leftMenuViewClass = [[[NSBundle mainBundle] loadNibNamed:@"LeftMenuView" owner:self options:nil] objectAtIndex:0];
    [leftMenuViewClass setFrame:CGRectMake(-(self.view.frame.size.width - 100), 0, self.view.frame.size.width - 100, self.view.frame.size.height)];
    
    leftMenuViewClass.LeftMenuViewDelegate =self;
    leftMenuViewClass.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:leftMenuViewClass];
    [self checkViewUpdateStatusChange];
}

-(void)viewDidLayoutSubviews {
	[super viewDidLayoutSubviews];
	if (self.didAppear == NO) {
		[self redrawShadow];
	}
}

-(void)redrawShadow {
	self.exploreButton.layer.masksToBounds = NO;
	self.exploreButton.layer.shadowColor = [UIColor grayColor].CGColor;
	self.exploreButton.layer.shadowOffset = CGSizeZero;
	self.exploreButton.layer.shadowRadius = 5.0;
	self.exploreButton.layer.shadowOpacity = 0.75;
	self.exploreButton.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.exploreButton.bounds cornerRadius:4.0].CGPath;
}

-(void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	self.didAppear = YES;
}

-(void)viewStyleUpdate{
    
    _cancelRequestBtn.backgroundColor = BASE1Color;
    _RateSubmitBtnAction.backgroundColor = BASE1Color;
    _scheduleRequestBtn.backgroundColor = BASE1Color;
    _schedulriideBtn.backgroundColor = BASE2Color;

    _rideNowBtn.backgroundColor = BASE1Color;
    _RideCallBtn.backgroundColor = BASE2Color;
    _RideCall_ShareBtn.backgroundColor = BASE1Color;
    _InvoicePayNowBtn.backgroundColor = BASE1Color;
    _getPricingBtm.backgroundColor = BASE1Color;

}


-(void)initializeDefaultValues{
    
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    
    SERVICE_URL = [user objectForKey:@"serviceurl"];
    Client_SECRET = [user objectForKey:@"passport"];
    ClientID = [user objectForKey:@"clientid"];
    WEB_SOCKET = [user objectForKey:@"websocket"];
    APP_NAME = [user objectForKey:@"appname"];
    APP_IMG_URL = [user objectForKey:@"appimg"];
    isValid = [user objectForKey:@"is_valid"];
    usrnameStr = [user objectForKey:@"username"];
    passwordStr =  [user objectForKey:@"password"];
    GMSMAP_KEY = [user objectForKey:@"googleApiKey"];
    GMSPLACES_KEY = [user objectForKey:@"googleApiKey"];
    ///GMSPLACES_KEY = @"AIzaSyBs7Ct-_9SrgBE2rw0pN6U5dgErrm7jZSA";
    Stripe_KEY = [user objectForKey:@"stripekey"];
    
}
#pragma mark - Own Method

-(void)onGetProfile
{
    if ([appDelegate internetConnected])
    {
		NSLog(@"newHomeController - onGetProfile");
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [appDelegate onStartLoader];
        
        NSString* UDID_Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
        NSLog(@"output is : %@", UDID_Identifier);
        
        NSDictionary *params=@{@"device_token":appDelegate.strDeviceToken,@"device_type":@"ios", @"device_id":UDID_Identifier};
        
        [afn getDataFromPath:MD_GETPROFILE withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            [appDelegate onEndLoader];
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                if( [response[@"picture"] isKindOfClass:[NSNull class]] )
                    [user setValue:@"" forKey:UD_PROFILE_IMG];
                else
                    [user setValue:response[@"picture"] forKey:UD_PROFILE_IMG];
                
                NSString *nameStr = [NSString stringWithFormat:@"%@ %@", [Utilities removeNullFromString: response[@"first_name"]], [Utilities removeNullFromString: response[@"last_name"]]];
                
                [user setValue:nameStr forKey:UD_PROFILE_NAME];
                [user setValue:response[@"currency"] forKey:@"currency"];
                [user synchronize];
                
                [self onGetService];

            }
            else
            {
                if ([errorcode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                    [self logOut];
                }
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}


-(void)checkViewUpdateStatusChange{
    if(![[KeyConstants sharedKeyConstants].RideUpdateStatus isEqualToString:[KeyConstants sharedKeyConstants].RideStatus]){
        NSLog(@"newHomeController - checkViewUpdateStatusChange %@",[KeyConstants sharedKeyConstants].RideStatus);
        [KeyConstants sharedKeyConstants].RideUpdateStatus=[KeyConstants sharedKeyConstants].RideStatus;
        [self StatusUpdate:[KeyConstants sharedKeyConstants].RideUpdateStatus];
    }
}

-(void)BackgroundPerformeStart{
    
    if(timeRequestCheck == nil){
        timeRequestCheck= [NSTimer scheduledTimerWithTimeInterval:5.0
                                                           target:self
                                                         selector:@selector(checServiceAvailable)
                                                         userInfo:nil
                                                          repeats:YES];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    NSLog(@"called");
    CurrentUpdateLocation = [locations lastObject];
    
    
    [geocoder reverseGeocodeLocation:CurrentUpdateLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0)
        {
            CLPlacemark *placemark = [placemarks lastObject];
           NSString * currentAddress = [Utilities removeNullFromString: [NSString stringWithFormat:@"%@,%@,%@",placemark.name,placemark.locality,placemark.subAdministrativeArea]];
            NSLog(@"Placemark %@",currentAddress);
            
            [KeyConstants sharedKeyConstants].LocationInfo = @{
                                                      currentLocations : currentAddress,
                                                      sourceLat : [NSString stringWithFormat:@"%f",CurrentUpdateLocation.coordinate.latitude],
                                                      sourceLang:[NSString stringWithFormat:@"%f",CurrentUpdateLocation.coordinate.longitude],
                                                      SourceLocations :currentAddress
                                                      
                                    
                                                      };
            //Location Update to server
//            NSDictionary *params=@{@"latitude":[KeyConstants sharedKeyConstants].LocationInfo[sourceLat],@"longitude":[KeyConstants sharedKeyConstants].LocationInfo[sourceLang],@"address":currentAddress};
//            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
//            [afn getDataFromPath:MD_UPDATELOCATION withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
//                if (response) {
//
//                }
//            }];
        }
        else
        {
            NSLog(@"%@", error.debugDescription);
        }
    } ];

}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            [_location requestWhenInUseAuthorization];
            break;
            
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self.location startUpdatingLocation];
            [self GetCurrentLocationUsingPlaces];
            break;
            
        case kCLAuthorizationStatusAuthorizedAlways:
            [self.location startUpdatingLocation];
            [self GetCurrentLocationUsingPlaces];
            break;
            
        case kCLAuthorizationStatusRestricted:
            [self AlertviewForLocationFailureAction:@"Location!" :@"You have not enabled access to your location. Do you want to enable now?"];
            break;
            
        case kCLAuthorizationStatusDenied:
            [self AlertviewForLocationFailureAction:@"Location!" :@"You have not enabled access to your location. Do you want to enable now?"];
            break;
            
        default:
            break;
    }
}

#pragma Mapview Delegats
-(void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
}
-(void)AlertviewForLocationFailureAction :(NSString *)title :(NSString *)message{
    UIAlertController * alert=[UIAlertController
                               
                               alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes, please"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                    
                                    
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No, thanks"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}
//Fetch curreent locations
-(void)GetCurrentLocationUsingPlaces{
	
	NSLog(@"Get current lcoation using places");
    _markerList = [[NSMutableArray alloc]init];
    [_placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *placeLikelihoodList, NSError *error){
        if (error != nil) {
            NSLog(@"Pick Place error %@", [error localizedDescription]);
            return;
        }
        if (placeLikelihoodList != nil) {
            GMSPlace *place = [[[placeLikelihoodList likelihoods] firstObject] place];
            if (place != nil) {
                NSLog(@"name%@",place.placeID);
                
                //[self markerUpdateViews:place.coordinate.latitude :place.coordinate.longitude :place.name];
                //if([[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"CATEGORY"]){
                [self loadMapview:place.coordinate.latitude :place.coordinate.longitude];
                
                    //[self markerUpdateViews:place.coordinate.latitude :place.coordinate.longitude :place.name:@""];
                // [self fitAllMarkers];
                    //                self.nameLabel.text = place.name;
                    //self.locationTextfield.text = [[place.formattedAddress componentsSeparatedByString:@","]
                                                 //  componentsJoinedByString:@"\n"];
                NSLog(@"%@",[[place.formattedAddress componentsSeparatedByString:@","]componentsJoinedByString:@""]);
                    [KeyConstants sharedKeyConstants].LocationInfo = @{
                                                              currentLocations : place.formattedAddress,
                                                              sourceLat : [NSString stringWithFormat:@"%f",place.coordinate.latitude],
                                                              sourceLang:[NSString stringWithFormat:@"%f",place.coordinate.longitude],
                                                              SourceLocations :place.formattedAddress,
                                                              
                                                              @"placeId":place.placeID
                                                              };
                
                NSDictionary *params=@{@"latitude":[NSString stringWithFormat:@"%f",place.coordinate.latitude],@"longitude":[NSString stringWithFormat:@"%f",place.coordinate.longitude],@"address":[[place.formattedAddress componentsSeparatedByString:@","]componentsJoinedByString:@"\n"]};
//                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
//                [afn getDataFromPath:MD_UPDATELOCATION withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
//                    if (response) {
//
//                    }
//                }];
                
                //}
                
            }
        }
    }];
}

-(void)onGetAddress {
	
	NSLog(@"newHomeController - onGetAddress");
    if ([appDelegate internetConnected]) {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        //[appDelegate onStartLoader];
        [afn getDataFromPath:@"/api/user/location" withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            //  [appDelegate onEndLoader];
            if (response) {
                _FavArray = [[NSMutableArray alloc]init];
                _workArray = [[NSMutableArray alloc]init];
                
                if([response[@"home"] count]>0){
                    [_FavArray addObject:response[@"home"][0]];
                }
                if([response[@"work"] count]>0){
                    [_workArray addObject:response[@"work"][0]];
                }
            }
			else {
                if ([errorcode intValue]==1) {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3) {
                    [self logOut];
                    
                    //                    [CommenMethods onRefreshToken];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
                    //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
                    //
                    //                    PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
                    //                    [self.navigationController pushViewController:wallet animated:YES];
                }
            }
            
        }];
    }
    else {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
    
    
}


#pragma Map Load for start and update Locations
-(void)loadMapview: (double)lat :(double)lang{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                            longitude:lang
                                                                 zoom:12];
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"map_style" withExtension:@"json"];
    NSError *error;
    
    // Set the map style by passing the URL for style.json.
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    _mapView.mapStyle = style;
    _mapView.myLocationEnabled = YES;
    _mapView.camera = camera;
    [_mapView animateToCameraPosition:camera];
}

// Marker update for new loctations
-(void)markerUpdateViews :(double)lat :(double)lang  :(NSString *)title withProvider :(BOOL)isProvider :(NSString *)imageURL{
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(lat,lang);
    GMSMarker* marker = [GMSMarker markerWithPosition:position];
    marker.title = title;
    
    if(isProvider){
        LOTAnimationView *animation = [LOTAnimationView animationNamed:@"suv"];
        animation.frame = CGRectMake(0, 0,50,60);
        animation.loopAnimation = YES;
        [animation playWithCompletion:^(BOOL animationFinished) {
            // Do Something
        }];
       
        // marker.iconView = marker;
        // marker.icon = [UIImage imageNamed:imageURL];
        marker.iconView = animation;
        
    }
	else {
         marker.icon = [UIImage imageNamed:imageURL];
         [_markerList addObject:marker];
    }
    
    marker.tracksViewChanges = YES;
    marker.map = self.mapView;
}

// Marker update for new loctations



//Aftr markrt update map load for marker postions
-(void)fitAllMarkers{
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:_markerList];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    
    CLLocationCoordinate2D myLocation = ((GMSMarker *)_markerList.firstObject).position;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:myLocation coordinate:myLocation];
    
    for (GMSMarker *marker in arrayWithoutDuplicates){
        bounds = [bounds includingCoordinate:marker.position];
    }
    [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:80.0]];
    
}


-(void)StatusUpdate :(NSString *)status{
    
    if([status isEqualToString:@"HOME"]){
		
		NSLog(@"newHomeController statusUpdate: HOME");
		
        [self.view bringSubviewToFront:_homeBtn];
        [self.view bringSubviewToFront:_workBtn];

        [_menuBtn setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];

        [_markerList removeAllObjects];
        [_mapView clear];
        _sosBtn.hidden = YES;
        [self loadMapview:[[KeyConstants sharedKeyConstants].LocationInfo[sourceLat] doubleValue] :[[KeyConstants sharedKeyConstants].LocationInfo[sourceLang] doubleValue]];
        [self ShowView:_SourceandDestinationview :NO];
        [self ShowView:_WhereView :YES];
		[self ShowView:_exploreButton :YES];
        [self ShowView:_paymentView :NO];
        [self ShowView:_fareinfoView :NO];
        [self ShowView:_searchView :NO];
        [self ShowView:_RideStatusView :NO];
        [self ShowView:_InvoiceView :NO];
        [self ShowView:_RateView :NO];
        [self ShowView:_scheduleView :NO];

    }
	else if([status isEqualToString:@"PAYMENT"]){
		
		NSLog(@"newHomeController statusUpdate: PAYMENT");
		
        [_menuBtn setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
        [self.view sendSubviewToBack:_homeBtn];
        [self.view sendSubviewToBack:_workBtn];
            _destinationTitleBtn.hidden = NO;
        _sosBtn.hidden = YES;
       
            [_sourceTitleBtn setTitle:[KeyConstants sharedKeyConstants].LocationInfo[SourceLocations] forState:UIControlStateNormal];
            
            [_destinationTitleBtn setTitle:[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLocations] forState:UIControlStateNormal];
            [self getPath];
        
        [self ShowView:_SourceandDestinationview :YES];
        [self.view bringSubviewToFront:_SourceandDestinationview];
        if([KeyConstants sharedKeyConstants].paymentInfo == nil){
            [KeyConstants sharedKeyConstants].paymentInfo=@{
                                                   @"brand":@"CASH",@"card_id":@"",@"id":@"0"

                                                   };
        }
       // _ProviderTypeLbl.text = [NSString stringWithFormat:@"%@ %@",[KeyConstants sharedKeyConstants].RideInfo[@"type"],[KeyConstants sharedKeyConstants].RideInfo[@"provider_name"]];
        _vehicleNameLbl.text = [KeyConstants sharedKeyConstants].RideInfo[@"name"];
        NSString * imageURL = [Utilities removeNullFromString:[KeyConstants sharedKeyConstants].RideInfo[@"image"]];
        [_vehicleImageview sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"sedan-car-model"]];
        
        NSLog(@"%@", [KeyConstants sharedKeyConstants].paymentInfo);
        if([[KeyConstants sharedKeyConstants].paymentInfo[@"brand"] isEqualToString:@"CASH"]){
            _paymentTypeimageview.image = [UIImage imageNamed:@"money_icon"];
            _paymentModeLbl.text = [KeyConstants sharedKeyConstants].paymentInfo[@"brand"];
        }else{
            _paymentModeLbl.text = [NSString stringWithFormat:@"XXXX-XXXX-XXXX-%@",[KeyConstants sharedKeyConstants].paymentInfo[@"last_four"]];
        }
        if(_SourceandDestinationview.hidden){
            [self ShowView:_SourceandDestinationview :YES];
            [self.view bringSubviewToFront:_SourceandDestinationview];
        }
        [self ShowView:_WhereView :NO];
		[self ShowView:_exploreButton : NO];
        [_serviceListCollection reloadData];
        [self ShowView:_paymentView :YES];
        [self ShowView:_fareinfoView :NO];
        [self ShowView:_searchView :NO];
        [self ShowView:_RideStatusView :NO];
        [self ShowView:_InvoiceView :NO];
        [self ShowView:_RateView :NO];
        [self ShowView:_scheduleView :NO];

    }
	else if([status isEqualToString:@"FAIRINFO"]) {
		
		NSLog(@"newHomeController statusUpdate: FAIRINFO");
		
           _sosBtn.hidden = YES;
        if(_SourceandDestinationview.hidden){
            [self ShowView:_SourceandDestinationview :YES];
            [self.view bringSubviewToFront:_SourceandDestinationview];
        }
        [self.view sendSubviewToBack:_homeBtn];
        [self.view sendSubviewToBack:_workBtn];
        [_menuBtn setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
        _sosBtn.hidden = YES;
        [self ShowView:_WhereView :NO];
		[self ShowView:_exploreButton : NO];
        [self ShowView:_paymentView :NO];
        [self ShowView:_fareinfoView :YES];
        [self ShowView:_searchView :NO];
        [self ShowView:_RideStatusView :NO];
        [self ShowView:_InvoiceView :NO];
        [self ShowView:_RateView :NO];
        [self ShowView:_scheduleView :NO];


    }
	else if([status isEqualToString:@"SEARCHING"]) {
		
		NSLog(@"newHomeController statusUpdate: SEARCHING");
		
		[self getPath];
        [self.view sendSubviewToBack:_homeBtn];
        [self.view sendSubviewToBack:_workBtn];
        [self.view sendSubviewToBack:_SourceandDestinationview];

       _sosBtn.hidden = YES;
        [self ShowView:_WhereView :NO];
		[self ShowView:_exploreButton : NO];
        [self ShowView:_paymentView :NO];
        [self ShowView:_fareinfoView :NO];
        [self ShowView:_searchView :YES];
        [self ShowView:_RideStatusView :NO];
        [self ShowView:_InvoiceView :NO];
        [self ShowView:_RateView :NO];
        [self ShowView:_scheduleView :NO];

    }
	else if([[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"STARTED"] ||
			  [[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"ARRIVED"] ||
			  [[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"PICKEDUP"] ||
			  [[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"PARKED"] ||
			  [[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"RETURNING"] ||
			  [[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"REACHED"]) {
		
		NSLog(@"newHomeController RideStatus: %@",[KeyConstants sharedKeyConstants].RideStatus);
		
		/**
		 In this case the ride status is active and we need to do something with it.
		 */
		
        [_menuBtn setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
        [self.view sendSubviewToBack:_homeBtn];
        [self.view sendSubviewToBack:_workBtn];
        [self getPath];
        
        if([[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"STARTED"]){
            _rideStatuLbl.text = @"Driver accepted your request.";
            [_RideCall_ShareBtn setTitle:@"CANCEL" forState:UIControlStateNormal];
            _RideBottomConstraints.constant = 5;
            _otpLbl.text = [NSString stringWithFormat:@"OTP:%@",[KeyConstants sharedKeyConstants].RequestInfo[@"otp"]];
            
            _RideTabWhenKeyBtn.hidden = YES;
        }else if([[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"ARRIVED"]){
               _sosBtn.hidden = YES;
            [self.view bringSubviewToFront:_sosBtn];
            _rideStatuLbl.text = @"Driver has arrived at your location.";
            [_RideCall_ShareBtn setTitle:@"CANCEL" forState:UIControlStateNormal];
            _RideBottomConstraints.constant = 10;
            _otpLbl.hidden = NO;
            _otpLbl.text = [NSString stringWithFormat:@"OTP:%@",[KeyConstants sharedKeyConstants].RequestInfo[@"otp"]];

            _RideTabWhenKeyBtn.hidden = YES;
        }else if([[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"PICKEDUP"]){
            _rideStatuLbl.text = @"You are on ride.";
            _sosBtn.hidden = NO;
            [self.view bringSubviewToFront:_sosBtn];
            [_RideCall_ShareBtn setTitle:@"SHARE RIDE" forState:UIControlStateNormal];
            _RideBottomConstraints.constant = 10;
            _otpLbl.text = @"";
            _otpLbl.hidden = YES;

            _RideTabWhenKeyBtn.hidden = YES;
        }else if([[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"RETURNING"]){
            _RideBottomConstraints.constant = 10;
            _otpLbl.text = [NSString stringWithFormat:@"OTP:%@",[KeyConstants sharedKeyConstants].RequestInfo[@"otp"]];

            _RideTabWhenKeyBtn.hidden = YES;
            _rideStatuLbl.text = @"Driver Parked your car.";
            [_RideCall_ShareBtn setTitle:@"SHARE RIDE" forState:UIControlStateNormal];
        }else if ([[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"PARKED"]){
            _RideBottomConstraints.constant = 10;
            _otpLbl.text = [NSString stringWithFormat:@"OTP:%@",[KeyConstants sharedKeyConstants].RequestInfo[@"otp"]];

            _RideTabWhenKeyBtn.hidden = YES;
            _rideStatuLbl.text = @"Driver has arrived at your locations";
            [_RideCall_ShareBtn setTitle:@"SHARE RIDE" forState:UIControlStateNormal];
        }else{
            _RideBottomConstraints.constant = 70;
            _RideTabWhenKeyBtn.hidden = NO;
            _rideStatuLbl.text = @"Driver Returned car key.";
        }
		
        if(![[KeyConstants sharedKeyConstants].RequestInfo[@"provider_service"] isKindOfClass:[NSNull class]]){
        _rideVehicleModelLbl.text = [NSString stringWithFormat:@"%@\n%@",[Utilities removeNullFromString:[KeyConstants sharedKeyConstants].RequestInfo[@"provider_service"][@"service_model"]],[Utilities removeNullFromString:[KeyConstants sharedKeyConstants].RequestInfo[@"provider_service"][@"service_number"]]];
        }
        
        if(![[KeyConstants sharedKeyConstants].RequestInfo[@"service_type"] isKindOfClass:[NSNull class]]){
            NSString * imageURLCar = [Utilities removeNullFromString:[KeyConstants sharedKeyConstants].RequestInfo[@"service_type"][@"image"]];
            [_rideVehicleImageview sd_setImageWithURL:[NSURL URLWithString:imageURLCar] placeholderImage:[UIImage imageNamed:@"sedan-car-model"]];
            _RideVehicleName.text = [Utilities removeNullFromString:[KeyConstants sharedKeyConstants].RequestInfo[@"service_type"][@"name"]];
        }
       
        
        if(![[KeyConstants sharedKeyConstants].RequestInfo[@"provider"] isKindOfClass:[NSNull class]]){
        _rideProviderName.text = [NSString stringWithFormat:@"%@ %@",[Utilities removeNullFromString:[KeyConstants sharedKeyConstants].RequestInfo[@"provider"][@"first_name"]],[Utilities removeNullFromString:[KeyConstants sharedKeyConstants].RequestInfo[@"provider"][@"last_name"]]];
        NSString * imageURLUser = [Utilities removeNullFromString:[KeyConstants sharedKeyConstants].RequestInfo[@"provider"][@"avatar"]];
            
        [_rideProviderImageview sd_setImageWithURL:[NSURL URLWithString:[Utilities imageConvert:imageURLUser]] placeholderImage:[UIImage imageNamed:@"userProfile"]];
        }
        
        if([[KeyConstants sharedKeyConstants].RequestInfo[@"surge"] intValue] == 1){
            _rideSurgeLbl.hidden = NO;
            _rideSurgeLbl.text = @"Due to high demand price may vary";
        }else{
             _rideSurgeLbl.text = @"";
             _rideSurgeLbl.hidden = YES;
        }

        if(_SourceandDestinationview.hidden){
            [self ShowView:_SourceandDestinationview :YES];
            [self.view bringSubviewToFront:_SourceandDestinationview];
        }
        
        [self ShowView:_WhereView :NO];
		[self ShowView:_exploreButton : NO];
        [self ShowView:_paymentView :NO];
        [self ShowView:_fareinfoView :NO];
        [self ShowView:_searchView :NO];
        [self ShowView:_RideStatusView :YES];
        [self ShowView:_InvoiceView :NO];
        [self ShowView:_InvoiceView :NO];
        [self ShowView:_RateView :NO];
        [self ShowView:_scheduleView :NO];

    }else if(([status isEqualToString:@"DROPPED"] ||
			  [status isEqualToString:@"COMPLETED"]) &&
			 ([[KeyConstants sharedKeyConstants].RequestInfo[@"paid"] intValue] == 0)) {
		
		NSLog(@"newHomeController statusUpdate: DROPPED or COMPLETED and paid == 0");
		
		[_menuBtn setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
        _otpLbl.text = @"";
		
        [self.view sendSubviewToBack:_homeBtn];
        [self.view sendSubviewToBack:_workBtn];
        _sosBtn.hidden = NO;
        [self.view bringSubviewToFront:_sosBtn];
        
        if(![[KeyConstants sharedKeyConstants].RequestInfo[@"payment"] isKindOfClass:[NSNull class]]){
            
            if([[KeyConstants sharedKeyConstants].RequestInfo[@"paid"] intValue] == 0){
                [KeyConstants sharedKeyConstants].paidStatus = NO;
            }else{
                [KeyConstants sharedKeyConstants].paidStatus = YES;
            }
            
            NSLog(@"%@",[NSNumber numberWithBool:[[KeyConstants sharedKeyConstants].RequestInfo[@"paid"]integerValue]]);
            
            NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
            NSString *currencyStr=[Utilities removeNullFromString: [user valueForKey:@"currency"]];
            _invoiceBookingIdLbl.text =  [NSString stringWithFormat:@"%@",[KeyConstants sharedKeyConstants].RequestInfo[@"booking_id"]];
            _InvoiceDistanceLbl.text =  [NSString stringWithFormat:@"%@ %@",currencyStr,[KeyConstants sharedKeyConstants].RequestInfo[@"payment"][@"distance"]];
            _InvoiceTaxLbl.text = [NSString stringWithFormat:@"%@ %@",currencyStr,[KeyConstants sharedKeyConstants].RequestInfo[@"payment"][@"tax"]];
            _InvoiceTotlalbl.text =  [NSString stringWithFormat:@"%@ %@",currencyStr,[KeyConstants sharedKeyConstants].RequestInfo[@"payment"][@"payable"]];
            
            _invoiceDiscountLbl.text =  [NSString stringWithFormat:@"%@ %@",currencyStr,[KeyConstants sharedKeyConstants].RequestInfo[@"payment"][@"discount"]];
            
            _InvoiceBasefareLbl.text =  [NSString stringWithFormat:@"%@ %@",currencyStr,[KeyConstants sharedKeyConstants].RequestInfo[@"payment"][@"fixed"]];
            
             _invoiceDistanceTravelLbl.text =  [NSString stringWithFormat:@"%@ %@",[KeyConstants sharedKeyConstants].RequestInfo[@"distance"],@"KM"];
            
            
            int walletAmt = 0;
            if([[KeyConstants sharedKeyConstants].RequestInfo[@"payment"][@"wallet"] intValue] !=0){
                 _invoiceWalletTitleLbl.hidden = NO;;
                walletAmt = [[KeyConstants sharedKeyConstants].RequestInfo[@"payment"][@"wallet"]intValue];
                _InvoiceWalletLbl.text =  [NSString stringWithFormat:@"%@ %.2f",currencyStr,[[KeyConstants sharedKeyConstants].RequestInfo[@"payment"][@"wallet"]doubleValue]];
               
            }else{
                _InvoiceWalletLbl.text = @"";
                _invoiceWalletTitleLbl.hidden = YES;
            }
            if([[Utilities removeNullFromString:[KeyConstants sharedKeyConstants].RequestInfo[@"payment_mode"]] isEqualToString:@"CASH"]){
                _InvoicePaymemtmodelLbl.text =  [NSString stringWithFormat:@"%@",[KeyConstants sharedKeyConstants].RequestInfo[@"payment_mode"]];
                _InvoicePaymentImageview.image = [UIImage imageNamed:@"money_icon"];
//                if(!([KeyConstants sharedKeyConstants].paidStatus) &&  walletAmt > 0 ){
//                    _waitingforPaymentView.hidden = YES;
//                    _InvoicePayNowBtn.hidden = NO;
//                    [_InvoicePayNowBtn setTitle:@"DONE" forState:UIControlStateNormal];
//                }else{
                    _waitingforPaymentView.hidden = NO;
                    _InvoicePayNowBtn.hidden = YES;
                //}
            }else{
                _waitingforPaymentView.hidden = YES;
                _InvoicePayNowBtn.hidden = NO;
                _InvoicePaymemtmodelLbl.text =  [NSString stringWithFormat:@"%@",[KeyConstants sharedKeyConstants].RequestInfo[@"payment_mode"]];
                _InvoicePaymentImageview.image = [UIImage imageNamed:@"visa"];
//                if((![KeyConstants sharedKeyConstants].paidStatus) &&  walletAmt > 0 ){
//                    [_InvoicePayNowBtn setTitle:@"DONE" forState:UIControlStateNormal];
//                }else {
                    [_InvoicePayNowBtn setTitle:@"PAY NOW" forState:UIControlStateNormal];
               // }
            }
        
            _InvoiceTimeTakenLbl.text  = [NSString stringWithFormat:@" %d minutes ", [[KeyConstants sharedKeyConstants].RequestInfo[@"travel_time"] intValue]];
            }
        
        if(_SourceandDestinationview.hidden){
            [self ShowView:_SourceandDestinationview :YES];
            [self.view bringSubviewToFront:_SourceandDestinationview];
        }
        if(_InvoiceView.isHidden){
            [self ShowView:_WhereView :NO];
			[self ShowView:_exploreButton : NO];
            [self ShowView:_paymentView :NO];
            [self ShowView:_fareinfoView :NO];
            [self ShowView:_searchView :NO];
            [self ShowView:_RideStatusView :NO];
            [self ShowView:_InvoiceView :YES];
            [self ShowView:_RateView :NO];
            [self ShowView:_scheduleView :NO];
        }
       

        
    }else if([status isEqualToString:@"COMPLETED"] && ([KeyConstants sharedKeyConstants].RideCompleteStatus || ([[KeyConstants sharedKeyConstants].RequestInfo[@"paid"] intValue] == 1))){
        _RateCommentsView.text = @"Write your comments";
		
		NSLog(@"newHomeController statusUpdate: COMPLETED and paid == 1");

        _sosBtn.hidden = YES;
        [self.view sendSubviewToBack:_homeBtn];
        [self.view sendSubviewToBack:_workBtn];
        [_menuBtn setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];

        if(![[KeyConstants sharedKeyConstants].RequestInfo[@"provider"] isKindOfClass:[NSNull class]]){
            _RatelblInfo.text = [NSString stringWithFormat:@"Rate your Trip with %@ %@",[Utilities removeNullFromString:[KeyConstants sharedKeyConstants].RequestInfo[@"provider"][@"first_name"]],[Utilities removeNullFromString:[KeyConstants sharedKeyConstants].RequestInfo[@"provider"][@"last_name"]]];
            NSString * imageURLUser = [Utilities removeNullFromString:[KeyConstants sharedKeyConstants].RequestInfo[@"provider"][@"avatar"]];
            
            [_RateproviderImageview sd_setImageWithURL:[NSURL URLWithString:[Utilities imageConvert:imageURLUser]] placeholderImage:[UIImage imageNamed:@"userProfile"]];
        }
        _RateCommentsView.layer.borderColor = [UIColor lightGrayColor].CGColor;
		
		// Configure the view for the current status
        [self ShowView:_WhereView :NO];
		[self ShowView:_exploreButton : NO];
        [self ShowView:_paymentView :NO];
        [self ShowView:_fareinfoView :NO];
        [self ShowView:_searchView :NO];
        [self ShowView:_RideStatusView :NO];
        [self ShowView:_InvoiceView :NO];
        [self ShowView:_RateView :YES];
        [self ShowView:_scheduleView :NO];


    }
	else if([status isEqualToString:@"SCHEDULE"]) {
		
		NSLog(@"newHomeController statusUpdate: SCHEDULE");
		
        [_menuBtn setImage:[UIImage imageNamed:@"back-icon"] forState:UIControlStateNormal];
        _sosBtn.hidden = YES;
        [self.view sendSubviewToBack:_homeBtn];
        [self.view sendSubviewToBack:_workBtn];
        picker = [[customDatePicker alloc] initWithFrame:CGRectMake(0,0,self.pickerUpdateview.frame.size.width,self.pickerUpdateview.frame.size.height)];
        [picker addTargetForDoneButton:self action:@selector(donePressed)];
        picker.backgroundColor = [UIColor whiteColor];
        [self.pickerUpdateview addSubview:picker];
        [picker setMode:UIDatePickerModeDate];

		// Configure the view for the current status
        [self ShowView:_WhereView :NO];
		[self ShowView:_exploreButton : NO];
        [self ShowView:_paymentView :NO];
        [self ShowView:_fareinfoView :NO];
        [self ShowView:_searchView :NO];
        [self ShowView:_RideStatusView :NO];
        [self ShowView:_InvoiceView :NO];
        [self ShowView:_RateView :NO];
        [self ShowView:_scheduleView :YES];
    }
}
-(void)pushTransition:(CFTimeInterval)duration :(UIView *)view withDirection :(int)direction
{
    CATransition *animation = [CATransition new];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.type = kCATruncationNone;
    if(direction == 1){
        animation.subtype = kCATransitionFromRight;
    }else if(direction == 2){
        animation.subtype = kCATransitionFromLeft;
    }else if (direction == 3){
        animation.subtype = kCATransitionFromTop;
        
    }else{
        animation.subtype = kCATransitionFromBottom;
        
    }
    
    animation.duration = duration;
    [view.layer addAnimation:animation forKey:kCATransitionMoveIn];
}


-(void)ShowView :(UIView *)view :(BOOL)isShow{
    if(isShow){
        
        if([[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"STARTED"] || [[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"ARRIVED"] || [[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"PICKEDUP"]){
            [self pushTransition:0.0 :view withDirection:3];

        }else{
            [self pushTransition:0.0 :view withDirection:3];

        }
        view.hidden = NO;
        
    }else{
        view.hidden = YES;
        [self pushTransition:0.0 :view withDirection:4];
        
    }
    
}
- (IBAction)SheduleOutterTab:(id)sender {
    IsShedule = NO;
    [KeyConstants sharedKeyConstants].RideStatus = @"HOME";
    [self checkViewUpdateStatusChange];
}


-(void)donePressed {
    
        NSLog(@"%@",picker.SelectedPickervalues);
        if(![[Utilities removeNullFromString:picker.SelectedPickervalues] isEqualToString:@""]){
            
            NSArray *dateArr = [picker.SelectedPickervalues componentsSeparatedByString:@" "];
            NSString *schDate = [dateArr objectAtIndex:0];
            NSString *schtime = [dateArr objectAtIndex:1];
           
            
            [KeyConstants sharedKeyConstants].SheduleInfo = @{
                                                     @"date":schDate,
                                                     @"time":schtime
                                                     };
        }else{
            [CommenMethods alertviewController_title:@"" MessageAlert:@"Select Shedule Date" viewController:self okPop:YES];
        }
    
    NSLog(@"%@",picker.SelectedPickervalues);
    
    
    
}



//Time convert
#pragma Textview Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Write your comments"]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Write your comments";
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}

-(void)checServiceAvailable{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     dispatch_async(dispatch_get_main_queue(), ^{
    
    //stop your HUD here
    //This is run on the main thread
    if ([appDelegate internetConnected]){
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:MD_REQUEST_CHECK withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            
            if (response){
                NSLog(@"%@",response);
                if([response[@"data"] count] == 0){
                    _sourceTitleBtn.enabled = YES;
                    _destinationTitleBtn.enabled = YES;
                    
                    if([KeyConstants sharedKeyConstants].RequestInfo != nil){
                       
                        if(IsShedule){
                           
                            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

                            YourTripViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"YourTripViewController"];
                            wallet.navigateStr = @"Home";
                            [self.navigationController pushViewController:wallet animated:YES];
                            
                        }

                        [KeyConstants sharedKeyConstants].RideStatus = @"HOME";
                        [self checkViewUpdateStatusChange];
                    }
                   
                } else {
                    _sourceTitleBtn.enabled = NO;
                    _destinationTitleBtn.enabled = NO;
                    [KeyConstants sharedKeyConstants].RequestInfo = [response[@"data"] firstObject];
                   
                    [[NSUserDefaults standardUserDefaults]setValue:[KeyConstants sharedKeyConstants].RequestInfo[@"id"] forKey:UD_REQUESTID];
                    [KeyConstants sharedKeyConstants].LocationInfo = @{
                                                              sourceLat:[KeyConstants sharedKeyConstants].RequestInfo[@"s_latitude"],
                                                              sourceLang :[KeyConstants sharedKeyConstants].RequestInfo[@"s_longitude"],
                                                              SourceLocations :[KeyConstants sharedKeyConstants].RequestInfo[@"s_address"],
                                                              currentLocations:[KeyConstants sharedKeyConstants].RequestInfo[@"s_address"],
                                                              };
                    
                    [KeyConstants sharedKeyConstants].DestinationInfo = @{
                                                                 DestinationLat :[KeyConstants sharedKeyConstants].RequestInfo[@"d_latitude"],
                                                                 DestinationLang:[KeyConstants sharedKeyConstants].RequestInfo[@"d_longitude"],
                                                                 DestinationLocations:[KeyConstants sharedKeyConstants].RequestInfo[@"d_address"]
                                                                 
                                                                 };
                    
                    if([KeyConstants CheckKeyExcistswithDict:[KeyConstants sharedKeyConstants].LocationInfo withKey:SourceLocations]){
                        [_sourceTitleBtn setTitle:[KeyConstants sharedKeyConstants].LocationInfo[SourceLocations] forState:UIControlStateNormal];
                        
                    }
                    
                    if([KeyConstants CheckKeyExcistswithDict:[KeyConstants sharedKeyConstants].DestinationInfo withKey:DestinationLocations]){
                        [_destinationTitleBtn setTitle:[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLocations] forState:UIControlStateNormal];
                        
                    }
                    
                    [KeyConstants sharedKeyConstants].RideStatus = [KeyConstants sharedKeyConstants].RequestInfo[@"status"];
                    [self checkViewUpdateStatusChange];
                }
            }
            else{
                if ([errorcode intValue]==1){
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG")  viewController:self okPop:NO];

                }
                else if ([errorcode intValue]==3){
                    [self logOut];
                }
            }
        }];
    }
        else{
            [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET")  viewController:self okPop:NO];
            }
        });
     });
    
    
}


#pragma MenuView update

#pragma mark - Slide Menu controllers

-(void)LeftMenuView {
    
    if(leftMenuViewClass.frame.origin.x == -(self.view.frame.size.width - 100)){
        [UIView animateWithDuration:0.3 animations:^{
            
            leftMenuViewClass.frame = CGRectMake(0, 0, self.view.frame.size.width - 100,  self.view.frame.size.height);
            
        }];
        [self BackgroundPerformStop];
        waitingBGView = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width  ,self.view.frame.size.height)];
        UITapGestureRecognizer *tapGesture_condition=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ViewOuterTap)];
        tapGesture_condition.cancelsTouchesInView=NO;
        tapGesture_condition.delegate=self;
        [waitingBGView addGestureRecognizer:tapGesture_condition];
        [waitingBGView setBackgroundColor:[UIColor blackColor]];
        [waitingBGView setAlpha:0.6];
        [self.view addSubview:waitingBGView];
        [self.view bringSubviewToFront:leftMenuViewClass];
        
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            
            leftMenuViewClass.frame = CGRectMake(-(self.view.frame.size.width - 100), 0, self.view.frame.size.width - 100,  self.view.frame.size.height);
            
        }];
        [self BackgroundPerformeStart];
        [waitingBGView removeFromSuperview];
    }
    
    
}
- (void)ViewOuterTap{
    
    [self LeftMenuView];
}

#pragma getServiceListApi
-(void)onGetService {
    if ([appDelegate internetConnected]) {
       
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        // [appDelegate onStartLoader];
        [afn getDataFromPath:MD_GET_SERVICE withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response){
                _categoryList = [[NSMutableArray alloc]init];
                _categoryList = response;
                NSLog(@"Services....%@", response);
                
                //[self AssignValuesToList:response];
            }
            else{
                if ([errorcode intValue]==1){
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3){
                    //                    [CommenMethods onRefreshToken];
                    [self logOut];
                }
            }
        }];
        
    }
    else
    {
        //        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
        
    }
}

//-(void)AssignValuesToList :(NSArray *)lists{
//    _categoryList = [[NSMutableDictionary alloc]init];
//    NSMutableArray * array = [[NSMutableArray alloc]init];
//    NSMutableArray * array2 = [[NSMutableArray alloc]init];
//    [lists enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        if([obj[@"type"] isEqualToString:@"PERSONAL"]){
//            [array addObject:obj];
//        }else{
//            [array2 addObject:obj];
//        }
//
//    }];
//    [_categoryList setObject:array forKey:@"PERSONAL"];
//    [_categoryList setObject:array2 forKey:@"VALET"];
//    [_serviceListCollection reloadData];
//
//}


#pragma CatergoryList update collectionview
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [_categoryList count];
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((collectionView.bounds.size.width) /3.5, collectionView.bounds.size.height);
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CategoryListCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryListCell" forIndexPath:indexPath];
    
    [KeyConstants sharedKeyConstants].RideInfo = _categoryList[selectedIndex];
    cell.categoryNameLbl.layer.cornerRadius = 10;
    cell.categoryNameLbl.clipsToBounds = YES;
    cell.categoryNameLbl.text = _categoryList[indexPath.item][@"name"];
    
       NSString *strProfile = [Utilities removeNullFromString:_categoryList[indexPath.item][@"image"]];
    if(selectedIndex == indexPath.item){
        cell.categoryNameLbl.backgroundColor = BASE2Color;
        cell.categoryNameLbl.textColor = [UIColor whiteColor];

        cell.categorySmallImageview.hidden = YES;
        cell.categoryImageview.hidden = NO;
    }else{
         cell.categorySmallImageview.hidden = YES;
        cell.categoryNameLbl.backgroundColor = [UIColor whiteColor];
        cell.categoryNameLbl.textColor = [UIColor blackColor];
       // cell.categorySmallImageview.hidden = NO;
        cell.categoryImageview.hidden = NO;
    }
    [cell.categoryImageview sd_setImageWithURL:[NSURL URLWithString:strProfile]
                        placeholderImage:[UIImage imageNamed:@"sedan-car-model"]];
    [cell.categorySmallImageview sd_setImageWithURL:[NSURL URLWithString:strProfile]
                              placeholderImage:[UIImage imageNamed:@"sedan-car-model"]];
    return cell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    selectedIndex = indexPath.item;
    [KeyConstants sharedKeyConstants].RideInfo = _categoryList[indexPath.item];
    NSLog(@"%@",_categoryList[indexPath.item]);
    [self getProvidersInCurrentLocation:_categoryList[indexPath.item][@"id"]];
    [collectionView reloadData];
}

-(void)getProvidersInCurrentLocation :(NSString *)serviceId
{
    
    
    NSString *strLat=[NSString stringWithFormat:@"%.8f", CurrentUpdateLocation.coordinate.latitude];
    NSString *strLong=[NSString stringWithFormat:@"%.8f", CurrentUpdateLocation.coordinate.longitude];
    
    if ([appDelegate internetConnected])
    {
        NSDictionary *params=@{@"latitude":strLat,@"longitude":strLong,@"service":serviceId};
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:MD_GETPROVIDERS withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSLog(@"CALLED");
                [_mapView clear];
               
                if([response isKindOfClass:[NSArray class]])
                {
                    if ([response count]!=0)
                    {
                        [response enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            
                            [self markerUpdateViews:[obj[@"latitude"] doubleValue] :[obj[@"longitude"]  doubleValue] :obj[@"first_name"] withProvider:YES :@""];
                           
                        }];
                    }
                    else
                    {
                        //DO NOTHING
                    }
                   
                }
                
                else if([response isKindOfClass:[NSDictionary class]])
                {
                    NSString *error = [Utilities removeNullFromString:[response objectForKey:@"message"]];
                    NSLog(@"NO PROVIDERS....%@",error);
                
                }
                else
                {
                    
                }
                 [self getPath];
            }
            else
            {
                if ([errorcode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                    [self logOut];
                }
                
            }
            
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)logOut
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:@"Are you sure want to logout?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
       // [self logoutMethod_FromApp];
        [KeyConstants sharedKeyConstants].RideStatus = @"";
        [KeyConstants sharedKeyConstants].RideUpdateStatus = @"";

        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isLoggedin"];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        PageViewController * controller = [mainStoryboard instantiateViewControllerWithIdentifier:@"PageViewController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }];
    [alertController addAction:ok];
    [alertController addAction:no];
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void)logoutMethod_FromApp
{
    if ([appDelegate internetConnected])
    {
		NSLog(@"newHomeController - logoutMethod_FromApp");
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [appDelegate onStartLoader];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *idStr = [defaults valueForKey:UD_ID];
        NSDictionary *param = @{@"id":idStr};
        
        [afn getDataFromPath:MD_LOGOUT withParamData:param withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            [appDelegate onEndLoader];
            if (response)
            {
                [self ViewOuterTap];
               
                
                
                
            }
            else
            {
                [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
    
}


#pragma GetPathForMapView
- (void)getPath {
	
    NSString *googleUrl = @"https://maps.googleapis.com/maps/api/directions/json";
//
//    if([[KeyConstants sharedKeyConstants].LocationInfo[sourceLat] isEqualToString:@""]){
//        _sourceLat = [NSString stringWithFormat:@"%f",myLocation.coordinate.latitude];
//        _sourceLng = [NSString stringWithFormat:@"%f",myLocation.coordinate.longitude];
//
//    }
    
            if([_markerList count]>0){
                [_mapView clear];
                [_markerList removeAllObjects];
            }
    
    NSString *urlString = [NSString stringWithFormat:@"%@?origin=%@,%@&destination=%@,%@&sensor=false&waypoints=%@&mode=driving", googleUrl, [KeyConstants sharedKeyConstants].LocationInfo[sourceLat], [KeyConstants sharedKeyConstants].LocationInfo[sourceLang], [KeyConstants sharedKeyConstants].DestinationInfo[DestinationLat], [KeyConstants sharedKeyConstants].DestinationInfo[DestinationLang], @""];
    
    NSLog(@"my driving api URL --- %@", urlString);
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:urlString]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error)
      {
          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
          
          NSArray *routesArray = [json objectForKey:@"routes"];
          
          if ([routesArray count] > 0)
          {
              dispatch_async(dispatch_get_main_queue(), ^{
                  
                  [self markerUpdateViews:[[KeyConstants sharedKeyConstants].LocationInfo[sourceLat] doubleValue] :[[KeyConstants sharedKeyConstants].LocationInfo[sourceLang] doubleValue] :[KeyConstants sharedKeyConstants].LocationInfo[SourceLocations] withProvider:NO :@"ub__ic_pin_pickup"];
                  
                   [self markerUpdateViews:[[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLat] doubleValue] :[[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLang] doubleValue] :[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLocations] withProvider:NO :@"ub__ic_pin_dropoff"];
                  
                  GMSPolyline *polyline = nil;
                  [polyline setMap:nil];
                  NSDictionary *routeDict = [routesArray objectAtIndex:0];
                  NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                  NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                  GMSPath *path = [GMSPath pathFromEncodedPath:points];
                  polyline = [GMSPolyline polylineWithPath:path];
                  polyline.strokeWidth = 5.f;
                 // [self FetchTime:[routeDict[@"legs"][0][@"duration"][@"value"] intValue]];
                  polyline.strokeColor = BASE1Color;
                  polyline.map = _mapView;
                
                 
                  [self fitAllMarkers];
              });
              
          }
      }] resume];
    
}

-(void)BackgroundPerformStop{
    [timeRequestCheck invalidate];
    timeRequestCheck = nil;
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [self BackgroundPerformStop];
}

- (IBAction)getRideInfoAction:(UIButton *)sender {
    if ([appDelegate internetConnected])
    {
        
        NSLog(@"currentLocatoin :%@",[KeyConstants sharedKeyConstants].LocationInfo);
        NSLog(@"Destination:%@",[KeyConstants sharedKeyConstants].DestinationInfo);
        NSDictionary *params=@{@"s_latitude":[KeyConstants sharedKeyConstants].LocationInfo[sourceLat],@"s_longitude":[KeyConstants sharedKeyConstants].LocationInfo[sourceLang],@"d_latitude":[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLat],@"d_longitude":[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLang],@"service_type":[KeyConstants sharedKeyConstants].RideInfo[@"id"]};
		
		NSLog(@"newHomeController - getRideInfoAction");
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [appDelegate onStartLoader];
        [afn getDataFromPath:MD_GET_FAREESTIMATE withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            [appDelegate onEndLoader];
            if (response)
            {
                NSLog(@"ESTIMATE...%@", response);
                if (![response[@"estimated_fare"] isKindOfClass:[NSNull class]]) {
                    
                    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                    NSString *currencyStr=[Utilities removeNullFromString: [user valueForKey:@"currency"]];
                    _estimateFareValueLbl.text =  [NSString stringWithFormat:@"%@ %@",currencyStr, [response valueForKey:@"estimated_fare"]];
                    _EtaValueLbl.text = [Utilities removeNullFromString:response[@"time"]];
                    _bookedForValueLbl.text = [NSString stringWithFormat:@"%@",[KeyConstants sharedKeyConstants].RideInfo[@"name"]];
                    NSString *surge = [NSString stringWithFormat:@"%@",response[@"surge"]];
                    _fareTotlaDistanceValue.text = [NSString stringWithFormat:@"%@ KM",response[@"distance"]];
                    if ([surge isEqualToString:@"1"])
                        {
                            [_surgeInfoLbl setText:@"Due to high demand price may vary"];
                            _surgeLbl.text = [NSString stringWithFormat:@"%@", [response valueForKey:@"surge_value"]];
                            _surgeInfoLbl.hidden = NO;
                            _surgeView.hidden = NO;
                        }else{
                             _surgeLbl.text = [NSString stringWithFormat:@"%@", [response valueForKey:@"surge_value"]];
                            _surgeInfoLbl.hidden = YES;
                            _surgeView.hidden = NO;
                        }
                    if([response[@"wallet_balance"]intValue] > 0){
                        _useWalletBtn.hidden = NO;
                        _availableWalletTitleLbl.hidden = NO;
                        _AvailableWalletAmountValueLbl.text = [NSString stringWithFormat:@"%@ %.2f",currencyStr,[response[@"wallet_balance"]doubleValue]];
                        _AvailableWalletAmountValueLbl.hidden = NO;
                    }else{
                        _useWalletBtn.hidden = YES;
                        _AvailableWalletAmountValueLbl.hidden = YES;
                        _availableWalletTitleLbl.hidden= YES;
                    }
                    [KeyConstants sharedKeyConstants].RideStatus = @"FAIRINFO";
                    [self checkViewUpdateStatusChange];
                }
                if (![response[@"distance"] isKindOfClass:[NSNull class]]) {
                    strKm=response[@"distance"];
                }
//                [self pickUpDrop:nil];
            }
            else
            {
                if ([errorcode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                    //                    [CommenMethods onRefreshToken];
                    [self logOut];
                }
                
            }
            
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
    
    
}

-(void)onChangePaymentMode:(NSDictionary *)choosedPayment{

    if([choosedPayment[@"brand"]  isEqualToString:@"CASH"]){
        [KeyConstants sharedKeyConstants].paymentInfo = choosedPayment;
        _paymentTypeimageview.image = [UIImage imageNamed:@"money_icon"];
         _paymentModeLbl.text = [KeyConstants sharedKeyConstants].paymentInfo[@"brand"];
    }else{
        [KeyConstants sharedKeyConstants].paymentInfo = @{
                                                 @"brand" :@"CARD",
                                                 @"card_id":[NSString stringWithFormat:@"%@",choosedPayment[@"card_id"]],
                                                 @"last_four":[choosedPayment valueForKey:@"last_four"]
                                                 
                                                 };
        _paymentTypeimageview.image = [UIImage imageNamed:@"visa"];
         _paymentModeLbl.text = [NSString stringWithFormat:@"XXXX-XXXX-XXXX-%@",[choosedPayment valueForKey:@"last_four"]];
    }
    
}

- (IBAction)changePaymentModeAction:(UIButton *)sender {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    PaymentsViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"PaymentsViewController"];
    wallet.fromWhereStr = @"HOME";
    wallet.delegate=self;
    [self presentViewController:wallet animated:YES completion:nil];
}
#pragma use walletBtnAction
- (IBAction)useWalletBtnAction:(UIButton *)sender {
    if(!sender.isSelected){
        sender.selected = YES;
        [sender setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
        [KeyConstants sharedKeyConstants].WalletStatus = @"1";
    }else{
        [sender setImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
        sender.selected = NO;
        [KeyConstants sharedKeyConstants].WalletStatus = @"0";
    }
}


#pragma left menu action set
-(void)setSelectedIndex:(NSString *)title{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    [self LeftMenuView];
    if([title isEqualToString:@"Payment"]){
        PaymentsViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"PaymentsViewController"];
        wallet.fromWhereStr = @"LEFT MENU";
        [self.navigationController pushViewController:wallet animated:YES];
    }else if([title isEqualToString:@"Your Trips"]){
        YourTripViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"YourTripViewController"];
        [self.navigationController pushViewController:wallet animated:YES];
    }else if([title isEqualToString:@"Coupon"]){
        CouponsController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"CouponsController"];
        [self.navigationController pushViewController:wallet animated:YES];
    }else if([title isEqualToString:@"Wallet"]){
        
        WalletViewController *wallet = [WalletViewController initController];
        [self.navigationController pushViewController:wallet animated:YES];
        
    }else if([title isEqualToString:@"Passbook"]){
        passbookViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"passbookViewController"];
        [self.navigationController pushViewController:wallet animated:YES];
    }else if([title isEqualToString:@"Help"]){
        HelpViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
        [self.navigationController pushViewController:wallet animated:YES];
    }else if([title isEqualToString:@"Share"]){
        
        UIImage *img = [UIImage imageNamed:@"icon"];
        NSMutableArray *sharingItems = [NSMutableArray new];
        [sharingItems addObject:@"CoOper"];
         NSString *shareUrlStr = [NSString stringWithFormat:@"https://itunes.apple.com/us/app/tranxit/id1204487551?mt=8"];
        [sharingItems addObject:img];
        [sharingItems addObject:shareUrlStr];
        //[sharingItems addObject:[NSURL URLWithString:@"https://itunes.apple.com/us/app/tranxit/id1204487551?mt=8"]];
        UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
        [self presentViewController:activityController animated:YES completion:nil];
        
    }else if([title isEqualToString:@"Settings"]){
		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle: nil];
        SettingsController *settings = [storyboard instantiateViewControllerWithIdentifier:@"SettingsController"];
        [self.navigationController pushViewController:settings animated:YES];
    }else if([title isEqualToString:@"Logout"]){
        [self logOut];
    }
}

#pragma Profile action for click header

-(void)profileView{
    [self LeftMenuView];
   
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

    ProfileViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    [self.navigationController pushViewController:wallet animated:YES];
}

#pragma schedulenowBtnAction
- (IBAction)sceduleRideBtnAction:(UIButton *)sender {
    
    [KeyConstants sharedKeyConstants].RideStatus = @"SCHEDULE";
    [self checkViewUpdateStatusChange];
}

// MARK:- RIde Now
#pragma RidenowBtnAction
- (IBAction)rideNowBtnAction:(UIButton *)sender {
	
    if ([appDelegate internetConnected]) {
        NSDictionary *params;
        if (IsShedule)
        {
            
            params=@{@"s_latitude":[KeyConstants sharedKeyConstants].LocationInfo[sourceLat],@"s_longitude":[KeyConstants sharedKeyConstants].LocationInfo[sourceLang],@"d_latitude":[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLat],@"d_longitude":[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLang],@"service_type":[KeyConstants sharedKeyConstants].RideInfo[@"id"],@"distance":strKm,@"payment_mode":[KeyConstants sharedKeyConstants].paymentInfo[@"brand"],@"card_id":[KeyConstants sharedKeyConstants].paymentInfo[@"card_id"],@"s_address":[KeyConstants sharedKeyConstants].LocationInfo[SourceLocations],@"d_address":[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLocations], @"use_wallet":[KeyConstants sharedKeyConstants].WalletStatus,@"schedule_date":[KeyConstants sharedKeyConstants].SheduleInfo[@"date"],@"schedule_time":[KeyConstants sharedKeyConstants].SheduleInfo[@"time"]};
            
//            NSArray *dateArr = [scheduleDate.text componentsSeparatedByString:@" "];
//            NSString *schDate = [dateArr objectAtIndex:0];
//            NSString *schtime = [dateArr objectAtIndex:1];
//
//            params=@{@"s_latitude":_sourceLat,@"s_longitude":_sourceLng,@"d_latitude":_destLat,@"d_longitude":_destLng,@"service_type":strServiceID,@"distance":strKM,@"payment_mode":strPay,@"card_id":strCardID,@"s_address":strSourceAddress,@"d_address":strDestAddress, @"use_wallet":walletFlag, @"schedule_date": schDate, @"schedule_time": schtime};
        }
        else
        {
            params=@{@"s_latitude":[KeyConstants sharedKeyConstants].LocationInfo[sourceLat],@"s_longitude":[KeyConstants sharedKeyConstants].LocationInfo[sourceLang],@"d_latitude":[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLat],@"d_longitude":[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLang],@"service_type":[KeyConstants sharedKeyConstants].RideInfo[@"id"],@"distance":strKm,@"payment_mode":[KeyConstants sharedKeyConstants].paymentInfo[@"brand"],@"card_id":[KeyConstants sharedKeyConstants].paymentInfo[@"card_id"],@"s_address":[KeyConstants sharedKeyConstants].LocationInfo[SourceLocations],@"d_address":[KeyConstants sharedKeyConstants].DestinationInfo[DestinationLocations], @"use_wallet":[KeyConstants sharedKeyConstants].WalletStatus};
        }
		
		NSLog(@"newHomeController - rideNowButtonAction");
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [appDelegate onStartLoader];
        [afn getDataFromPath:MD_CREATE_REQUEST withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            [appDelegate onEndLoader];
            if (response)
            {
              
                NSString *reqStr = [[response objectForKey:@"request_id"] stringValue];
                if ([reqStr isEqualToString:@""] || reqStr.length ==0)
                {
                   
                    [CommenMethods alertviewController_title:LocalizedString(@"ALERT")  MessageAlert:[response objectForKey:@"message"] viewController:self okPop:NO];
                }
                else
                {
                    [KeyConstants sharedKeyConstants].RideStatus = @"SEARCHING";
                    [self checkViewUpdateStatusChange];
                    
                    
                    
                    LOTAnimationView *animation = [LOTAnimationView animationNamed:@"search"];
                    animation.frame = CGRectMake(0.0, 0.0, 200, 200);
                    animation.loopAnimation = YES;
                    animation.center =CGPointMake(_searchAnimationView.frame.size.width  / 2,
                                                  _searchAnimationView.frame.size.height / 2);;
                   
                    [_searchAnimationView addSubview:animation];
                        
                    
                    
                    
                    [animation playWithCompletion:^(BOOL animationFinished) {
                        // Do Something
                    }];
                    
                    
                }
            }
            else
            {
                if ([errorcode intValue]==1)
                {
                    if ([error objectForKey:@"error"])
                        [CommenMethods alertviewController_title:@"" MessageAlert:[error objectForKey:@"error"] viewController:self okPop:NO];
                    else
                        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                    [self logOut];
                }
            }
            
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
    
}

- (IBAction)menuBtnAction:(UIButton *)sender {
    if([[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"HOME"] || [[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@""]  ){
        [KeyConstants sharedKeyConstants].RideStatus = @"";
        [KeyConstants sharedKeyConstants].RideUpdateStatus = @"";

        [self LeftMenuView];
    }else if([[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"PAYMENT"]){
        [KeyConstants sharedKeyConstants].DestinationInfo = nil;
        [KeyConstants sharedKeyConstants].RideStatus = @"HOME";
        [KeyConstants sharedKeyConstants].RideUpdateStatus = @"HOME";

        [self StatusUpdate:[KeyConstants sharedKeyConstants].RideStatus];
    }else if ([[KeyConstants sharedKeyConstants].RideStatus isEqualToString:@"FAIRINFO"]){
        [KeyConstants sharedKeyConstants].RideStatus = @"PAYMENT";
        [KeyConstants sharedKeyConstants].RideUpdateStatus = @"PAYMENT";
        
        [self StatusUpdate:[KeyConstants sharedKeyConstants].RideStatus];
    }else{
         [self LeftMenuView];
    }
    
}
- (IBAction)cancelRequestBtnAction:(UIButton *)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:@"Are you sure you want to cancel Ride?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:nil];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([appDelegate internetConnected]){
            NSString *strReqID=[[NSUserDefaults standardUserDefaults] valueForKey:UD_REQUESTID];
            
            NSDictionary *params=@{@"request_id":strReqID};
			
			NSLog(@"newHomeController - cancelRequestButtonAction");
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [appDelegate onStartLoader];
            [afn getDataFromPath:MD_CANCEL_REQUEST withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
                [appDelegate onEndLoader];
                if (response){
                    IsShedule = NO;
                    [self BackgroundPerformeStart];
                    [KeyConstants sharedKeyConstants].RideStatus = @"HOME";
                    [KeyConstants sharedKeyConstants].RequestInfo  = nil;
                    [self GetCurrentLocationUsingPlaces];
                    [self checkViewUpdateStatusChange];
                }
                else{
                    
                    if ([errorcode intValue]==1){
                        [CommenMethods alertviewController_title:@"Error!" MessageAlert:[error objectForKey:@"error"] viewController:self okPop:NO];
                    }
                    else if ([errorcode intValue]==3){
                        //                    [CommenMethods onRefreshToken];
                        [self logOut];
                    }
                    else{
                        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                    }
                }
            }];
        }
        else{
            [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
        }
        
    }];
    [alertController addAction:ok];
    [alertController addAction:no];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}
- (IBAction)RideCallBtnAction:(UIButton *)sender {
    NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:[KeyConstants sharedKeyConstants].RequestInfo[@"provider"][@"mobile"]]];
    NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:[KeyConstants sharedKeyConstants].RequestInfo[@"provider"][@"mobile"]]];
    
    if ([UIApplication.sharedApplication canOpenURL:phoneUrl])
    {
        [UIApplication.sharedApplication openURL:phoneUrl options:@{} completionHandler:nil];
    }
    else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl])
    {
        [UIApplication.sharedApplication openURL:phoneFallbackUrl options:@{} completionHandler:nil];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"Your device does not support calling") viewController:self okPop:NO];
    }
}

//Call And ShareBtnAction
- (IBAction)rideCall_ShareBtnAction:(UIButton *)sender {
    
    if([sender.currentTitle  isEqualToString:@"CANCEL"]){
        [self cancelRequestBtnAction:sender];
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"Are you sure want to share the ride?") preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* no = [UIAlertAction actionWithTitle:LocalizedString(@"NO") style:UIAlertActionStyleDefault handler:nil];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            NSString *strLat=[NSString stringWithFormat:@"%.8f", CurrentUpdateLocation.coordinate.latitude];
            NSString *strLong=[NSString stringWithFormat:@"%.8f", CurrentUpdateLocation.coordinate.longitude];
            
            NSString *shareUrlStr = [NSString stringWithFormat:@"http://maps.google.com/maps?q=loc:%@,%@",strLat, strLong];
            NSString *strTitle=[NSString stringWithFormat:@"%@ - %@ would like to share a ride with you at ",[[NSBundle mainBundle]objectForInfoDictionaryKey:@"CFBundleDisplayName"], [KeyConstants sharedKeyConstants].RideInfo[@"user"][@"firstname"]];
            UIImage *img = [UIImage imageNamed:@"icon"];
            [self shareText:strTitle andImage:img andUrl:[NSURL URLWithString:shareUrlStr]];
        }];
        [alertController addAction:ok];
        [alertController addAction:no];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    if (text) {
        [sharingItems addObject:text];
    }
//    if (image) {
//        [sharingItems addObject:image];
//    }
    if (url) {
        [sharingItems addObject:url];
    }
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}


#pragma Ride Completed Status
- (IBAction)PaymentBtnAction:(UIButton *)sender {
    
   // if([sender.currentTitle isEqualToString:@"PAY NOW"]){
        
        if ([appDelegate internetConnected])
        {
            NSString *strReqID=[[NSUserDefaults standardUserDefaults] valueForKey:UD_REQUESTID];
            NSDictionary *params=@{@"request_id":strReqID};
			
			NSLog(@"newHomeController - PaymentBtnAction");
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [appDelegate onStartLoader];
            [afn getDataFromPath:MD_PAYMENT withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
                [appDelegate onEndLoader];
                if (response)
                {
                    [KeyConstants sharedKeyConstants].RideStatus = @"";
                    [KeyConstants sharedKeyConstants].RideCompleteStatus = YES;
                    [self checkViewUpdateStatusChange];
                }
                else
                {
                    
                    if ([errorcode intValue]==1)
                    {
                        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                    }
                    else if ([errorcode intValue]==3)
                    {
                        //                    [CommenMethods onRefreshToken];
                        [self logOut];
                    }
                    else if ([errorcode intValue]==2)
                    {
                        if ([error objectForKey:@"rating"]) {
                            [CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"rating"] objectAtIndex:0]  viewController:self okPop:NO];
                        }
                        else if([error objectForKey:@"comments"]) {
                            [CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"comments"] objectAtIndex:0]  viewController:self okPop:NO];
                        }
                        else if([error objectForKey:@"is_favorite"]) {
                            [CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"is_favorite"] objectAtIndex:0]  viewController:self okPop:NO];
                        }
                        
                    }
                    
                }
                
            }];
        }
        else
        {
            [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
        }
        
    
        
        
//    }else{
//        [KeyConstants sharedKeyConstants].RideStatus = @"";
//        [KeyConstants sharedKeyConstants].RideCompleteStatus = YES;
//        [self checkViewUpdateStatusChange];
//        
//    }
    
}
- (IBAction)RecivedKeyBtnAction:(UIButton *)sender {
    
    NSString *strReqID=[[NSUserDefaults standardUserDefaults] valueForKey:UD_REQUESTID];
	NSLog(@"newHomeController - recivedKeyBtnAction");
    [appDelegate onStartLoader];
    NSDictionary *params=@{@"id":strReqID,@"status":@"DROPPED"};
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:@"/api/user/received/key" withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
         [appDelegate onEndLoader];
        if (response) {
            [self checServiceAvailable];
        }else{
            if ([errorcode intValue]==1)
            {
                [CommenMethods alertviewController_title:@"Error!" MessageAlert:[error objectForKey:@"error"] viewController:self okPop:NO];
            }
            else if ([errorcode intValue]==3)
            {
                //                    [CommenMethods onRefreshToken];
                [self logOut];
            }
            else
            {
                [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }
    }];
}

- (IBAction)rateAction:(HCSStarRatingView *)sender {
    strRating=[NSString stringWithFormat:@"%.f",sender.value];
    NSLog(@"%@",strRating);
}
- (IBAction)RateSubmitBtnAction:(UIButton *)sender {
    
    if ([appDelegate internetConnected])
    {
        if(strRating == nil){
            strRating = [NSString stringWithFormat:@"%.f",_RateRateView.value];
        }
        NSString *strReqID=[[NSUserDefaults standardUserDefaults] valueForKey:UD_REQUESTID];
        NSDictionary *params=@{@"request_id":strReqID,@"rating":strRating,@"comment":_RateCommentsView.text};
		NSLog(@"newHomeController - RageSubmitBtnAction");
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [appDelegate onStartLoader];
        [afn getDataFromPath:MD_RATE_PROVIDER withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            [appDelegate onEndLoader];
            if (response){
               
                [self.mapView clear];
                [KeyConstants sharedKeyConstants].DestinationInfo = nil;
                [KeyConstants sharedKeyConstants].RideStatus = @"HOME";
                [KeyConstants sharedKeyConstants].RideCompleteStatus = NO;
                [KeyConstants sharedKeyConstants].RequestInfo = nil;
                [self checkViewUpdateStatusChange];
            }
            else
            {
                
                if ([errorcode intValue]==1)
                {
                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
                }
                else if ([errorcode intValue]==3)
                {
                    //                    [CommenMethods onRefreshToken];
                    [self logOut];
                }
                else if ([errorcode intValue]==2)
                {
                    if ([error objectForKey:@"rating"]) {
                        [CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"rating"] objectAtIndex:0]  viewController:self okPop:NO];
                    }
                    else if([error objectForKey:@"comments"]) {
                        [CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"comments"] objectAtIndex:0]  viewController:self okPop:NO];
                    }
                    else if([error objectForKey:@"is_favorite"]) {
                        [CommenMethods alertviewController_title:@"" MessageAlert:[[error objectForKey:@"is_favorite"] objectAtIndex:0]  viewController:self okPop:NO];
                    }
                    
                }
                
            }
            
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}
- (IBAction)currentLocationBtnAction:(UIButton *)sender {
    [self GetCurrentLocationUsingPlaces];
}
- (IBAction)scheduleRequestBtnAction:(UIButton *)sender {
    
    NSLog(@"%@",picker.SelectedPickervalues);
    if(![[Utilities removeNullFromString:picker.SelectedPickervalues] isEqualToString:@""]){
        
        NSArray *dateArr = [picker.SelectedPickervalues componentsSeparatedByString:@" "];
        NSString *schDate = [dateArr objectAtIndex:0];
        NSString *schtime = [dateArr objectAtIndex:1];
        
        
        [KeyConstants sharedKeyConstants].SheduleInfo = @{
                                                 @"date":schDate,
                                                 @"time":schtime
                                                 };
        IsShedule = YES;
        [self rideNowBtnAction:sender];
    }else{
        [CommenMethods alertviewController_title:@"" MessageAlert:@"Select Shedule Date" viewController:self okPop:YES];
    }
    
    NSLog(@"%@",picker.SelectedPickervalues);
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self BackgroundPerformStop];
    
}
- (IBAction)whereBtnDidTab:(UIButton *)sender {
	
	NSLog(@"newHomeController - whereBtnDidTab");
	
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"New" bundle: nil];
    newLocationController * infoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"newLocationController"];
    /*if (sender.tag == 111) {
        infoController.tagIdentifier = @"Source";
    }
	else {
        infoController.tagIdentifier = @"Destination";
    }*/
   
    [self presentViewController:infoController animated:YES completion:nil];
}

- (IBAction)sosBtnDidTab:(UIButton *)sender {
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *sosNumber = [Utilities removeNullFromString:[def valueForKey:UD_SOS]];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert!") message:LocalizedString(@"Are you sure want to Call Emergency?") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* no = [UIAlertAction actionWithTitle:LocalizedString(@"NO") style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([sosNumber isEqualToString:@""])
        {
            //No SOS number was provided
        }
        else
        {
            NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:sosNumber]];
            NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:sosNumber]];
            
            if ([UIApplication.sharedApplication canOpenURL:phoneUrl])
            {
                [UIApplication.sharedApplication openURL:phoneUrl options:@{} completionHandler:nil];
            }
            else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl])
            {
                [UIApplication.sharedApplication openURL:phoneFallbackUrl options:@{} completionHandler:nil];
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"Alert") message:LocalizedString(@"Your device does not support calling") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:LocalizedString(@"OK") style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];    }
        }
        
    }];
    [alertController addAction:ok];
    [alertController addAction:no];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
- (IBAction)locationPickBtnDidTab:(UIButton *)sender {
    
    if(sender.tag == 111){
        if(_FavArray.count>0){
            
            [KeyConstants sharedKeyConstants].DestinationInfo = @{
                                                         DestinationLat :[NSString stringWithFormat:@"%@",_FavArray[0][@"latitude"]],
                                                         DestinationLang:[NSString stringWithFormat:@"%@",_FavArray[0][@"longitude"]],
                                                         DestinationLocations:[NSString stringWithFormat:@"%@",_FavArray[0][@"address"]]
                                                         
                                                         };
            
            
            [_mapView clear];
            [self getPath];
            [KeyConstants sharedKeyConstants].RideStatus = @"PAYMENT";
            [self checkViewUpdateStatusChange];
            
        }else{
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"New" bundle: nil];
            
            newLocationController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"newLocationController"];
            //wallet.tagIdentifier = @"Destination";
            [self presentViewController:wallet animated:true completion:nil];
        }
        
    } else{
        if(_workArray.count>0){
            
            [KeyConstants sharedKeyConstants].DestinationInfo = @{
                                                         DestinationLat :[NSString stringWithFormat:@"%@",_workArray[0][@"latitude"]],
                                                         DestinationLang:[NSString stringWithFormat:@"%@",_workArray[0][@"longitude"]],
                                                         DestinationLocations:[NSString stringWithFormat:@"%@",_workArray[0][@"address"]]
                                                         
                                                         };
            
            
            [_mapView clear];
            [self getPath];
            [KeyConstants sharedKeyConstants].RideStatus = @"PAYMENT";
            [self checkViewUpdateStatusChange];
            
        }else{
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"New" bundle: nil];
            
            newLocationController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"newLocationController"];
            //wallet.tagIdentifier = @"Destination";
            [self presentViewController:wallet animated:true completion:nil];
        }
        
    }
    
    
}




-(IBAction)exploreButtonPressed: (UIButton *) sender {
	ExploreViewController *exploreController = [ExploreViewController initController];
	exploreController.delegate = self;
	[self.navigationController pushViewController:exploreController animated:true];
}

-(void) cooperToAddress:(GooglePlaceObject *)place {
	NSLog(@"Cooper to addresss button pressed %@",place.address);
	
	[KeyConstants sharedKeyConstants].DestinationInfo = @{
												 DestinationLat :[NSString stringWithFormat:@"%@", place.latitude],
												 DestinationLang :[NSString stringWithFormat:@"%@",place.longitude],
												 DestinationLocations : place.address,
												 };
	[KeyConstants sharedKeyConstants].RideStatus = @"PAYMENT";
	
	[self.navigationController popToRootViewControllerAnimated:true];
}

-(IBAction) testingNewWebInterface: (UIButton *) sender {
	//WelcomeViewController *welcomeController = [WelcomeViewController initController];
	//[self.navigationController pushViewController:welcomeController animated:true];
	
	HomescreenViewController *homescreenController = [HomescreenViewController initController];
	[self.navigationController pushViewController:homescreenController animated:true];
}

@end
