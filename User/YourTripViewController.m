//
//  YourTripViewController.m
//  Provider
//
//  Created by iCOMPUTERS on 17/01/17.
//  Copyright © 2017 iCOMPUTERS. All rights reserved.
//

#import "YourTripViewController.h"
#import "config.h"
#import "Colors.h"
#import "CSS_Class.h"
#import "TripsTableViewCell.h"
#import "HistoryViewController.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "CommenMethods.h"
#import "Utilities.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ViewController.h"


@implementation TripsObject


@end


@interface YourTripViewController ()
{
    AppDelegate *appDelegate;
}

@end

@implementation YourTripViewController


+(instancetype)initController {
	UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"YourTrip" bundle: nil];
	YourTripViewController *viewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"YourTripViewController"];
	return viewController;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self setupViewDesign];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];

    
    self.screenName = @"Your Trip";
    [self LocalizationUpdate];

    if ([_navigateStr isEqualToString:@"Home"])
    {
        IsShedule = NO;
        [self upcomingBtn:self];
    }
    else
    {
        [self pastBtn:self];
    }
}

-(void)setupViewDesign {
    self.navigationController.navigationBarHidden = NO;
    
    if (@available(iOS 11.0, *)) {
        [self navigationController].navigationBar.prefersLargeTitles = TRUE;
        [self navigationItem].largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAlways;
    }
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]
                                    initWithImage:[UIImage imageNamed:@"close_x_icon"]
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction:)];
    
    [[self navigationController].navigationBar setTranslucent:FALSE];
    [self navigationController].navigationBar.barTintColor = [UIColor blackColor];
    [self navigationController].navigationBar.barStyle = UIBarStyleBlackOpaque;
    [self navigationController].navigationBar.backgroundColor = [UIColor blackColor];
    [self navigationController].navigationBar.tintColor = [UIColor whiteColor];
    [self navigationController].navigationBar.layer.borderColor = [[UIColor blackColor] CGColor];
    [self navigationController].navigationBar.shadowImage = [[UIImage alloc] init];
    [self navigationItem].leftBarButtonItem = closeButton;
}


-(void)LocalizationUpdate{
    _headerLb.text = LocalizedString(@"Your Trips");
    [_pastBtn setTitle:LocalizedString(@"Past") forState:UIControlStateNormal];
    [_upcomingBtn setTitle:LocalizedString(@"Upcoming") forState:UIControlStateNormal];
    
    _pastLbl.backgroundColor = BASE2Color;
    _upcomingLbl.backgroundColor = BASE2Color;

}



-(void)onGetHistory
{
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [appDelegate onStartLoader];
    
    NSString *serviceStr;
    if([identifierStr isEqualToString:@"PAST"]){
        serviceStr = MD_GET_HISTORY;
    }else{
        serviceStr = MD_UPCOMING;
    }
    
    [afn getDataFromPath:serviceStr WithType:GET_METHOD WithParameters:nil WithCompletedSuccess:^(id response) {
        [appDelegate onEndLoader];
        if([response count]  > 0){
            
            dateArray = [[NSMutableArray alloc]init];
            timeArray = [[NSMutableArray alloc]init];
            amountArray = [[NSMutableArray alloc]init];
            imageArray = [[NSMutableArray alloc]init];
            idArray = [[NSMutableArray alloc]init];
            bookingArray = [[NSMutableArray alloc]init];

            [_noDataLbl setHidden:YES];
            [_tripTableView setHidden:NO];
            
            for (NSDictionary *dictVal in response)
                {
                if([identifierStr isEqualToString:@"PAST"])
                    {
                        NSString *strDate=[Utilities convertDateTimeToGMT:[dictVal valueForKey:@"assigned_at"]];
                        NSString *strTime=[Utilities  convertTimeFormat:[dictVal valueForKey:@"assigned_at"]];
                        [dateArray addObject:strDate];
                        [timeArray addObject:strTime];
                    }else
                        {
                            NSString *strDate=[Utilities convertDateTimeToGMT:[dictVal valueForKey:@"schedule_at"]];
                            NSString *strTime=[Utilities  convertTimeFormat:[dictVal valueForKey:@"schedule_at"]];
                            [dateArray addObject:strDate];
                            [timeArray addObject:strTime];
                        }
            
                    [imageArray addObject:[dictVal valueForKey:@"static_map"]];
                    [idArray addObject:[dictVal valueForKey:@"id"]];
                    [bookingArray addObject:[Utilities removeNullFromString:[dictVal valueForKey:@"booking_id"]]];
            
                    if (![[dictVal valueForKey:@"payment"] isKindOfClass:[NSNull class]]) {
            
                            if([identifierStr isEqualToString:@"PAST"])
                                {
                                    [amountArray addObject:[[dictVal valueForKey:@"payment"] valueForKey:@"total"]];
                                }
                                else
                                {
                                            ///
                                }
                            }
                        else
                            {
                                [amountArray addObject:@"0"];
                            }
                        }
                    [_tripTableView reloadData];
                }
                else
                {
                    [_noDataLbl setHidden:NO];
                    [_tripTableView setHidden:YES];
                }
        
    } OrValidationFailure:^(NSString *errorMessage) {
        [appDelegate onEndLoader];
        [CSS_Class alertviewController_title:@"" MessageAlert:errorMessage viewController:self okPop:NO];
    } OrErrorCode:^(NSString *error) {
        [appDelegate onEndLoader];
        [CSS_Class alertviewController_title:@"" MessageAlert:error viewController:self okPop:NO];
    } OrIntentet:^(NSString *internetFailure) {
        [appDelegate onEndLoader];
        [CSS_Class alertviewController_title:@"" MessageAlert:internetFailure viewController:self okPop:NO];
    }];
    
//    if ([appDelegate internetConnected])
//    {
//        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
//        [appDelegate onStartLoader];
//        
//        NSString *serviceStr;
//        if([identifierStr isEqualToString:@"PAST"])
//        {
//            serviceStr = MD_GET_HISTORY;
//        }
//        else
//        {
//            serviceStr = MD_UPCOMING;
//        }
//        
//        [afn getDataFromPath:serviceStr withParamData:nil withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
//            [appDelegate onEndLoader];
//            if (response)
//            {
//                NSArray *arrLocal=response;
//                if (arrLocal.count!=0)
//                {
//                    NSLog(@"history ...%@", arrLocal);
//                    
//                    dateArray = [[NSMutableArray alloc]init];
//                    timeArray = [[NSMutableArray alloc]init];
//                    amountArray = [[NSMutableArray alloc]init];
//                    imageArray = [[NSMutableArray alloc]init];
//                    idArray = [[NSMutableArray alloc]init];
//                    bookingArray = [[NSMutableArray alloc]init];
//                    
//                    [_noDataLbl setHidden:YES];
//                    [_tripTableView setHidden:NO];
//                    
//                    for (NSDictionary *dictVal in arrLocal)
//                    {
//                        if([identifierStr isEqualToString:@"PAST"])
//                        {
//                            NSString *strDate=[Utilities convertDateTimeToGMT:[dictVal valueForKey:@"assigned_at"]];
//                            NSString *strTime=[Utilities  convertTimeFormat:[dictVal valueForKey:@"assigned_at"]];
//                            [dateArray addObject:strDate];
//                            [timeArray addObject:strTime];
//                        }
//                        else
//                        {
//                            NSString *strDate=[Utilities convertDateTimeToGMT:[dictVal valueForKey:@"schedule_at"]];
//                            NSString *strTime=[Utilities  convertTimeFormat:[dictVal valueForKey:@"schedule_at"]];
//                            [dateArray addObject:strDate];
//                            [timeArray addObject:strTime];
//                        }
//                        
//                        [imageArray addObject:[dictVal valueForKey:@"static_map"]];
//                        [idArray addObject:[dictVal valueForKey:@"id"]];
//                        [bookingArray addObject:[Utilities removeNullFromString:[dictVal valueForKey:@"booking_id"]]];
//
//                        if (![[dictVal valueForKey:@"payment"] isKindOfClass:[NSNull class]]) {
//                            
//                            if([identifierStr isEqualToString:@"PAST"])
//                            {
//                                [amountArray addObject:[[dictVal valueForKey:@"payment"] valueForKey:@"total"]];
//                            }
//                            else
//                            {
//                                ///
//                            }
//                        }
//                        else
//                        {
//                            [amountArray addObject:@"0"];
//                        }
//                    }
//                    [_tripTableView reloadData];
//                }
//                else
//                {
//                    [_noDataLbl setHidden:NO];
//                    [_tripTableView setHidden:YES];
//                }
//                
//              }
//            else
//            {
//                if ([errorcode intValue]==1)
//                {
//                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
//                }
//                else if ([errorcode intValue]==3)
//                {
////                    [CommenMethods onRefreshToken];
////                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
////                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
////                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
////                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
////                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
////                    
////                    PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
////                    [self.navigationController pushViewController:wallet animated:YES];
//                    
//                    [self logoutMethod];
//                }
//            }
//            
//        }];
//    }
//    else
//    {
//        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
//    }
    
}

-(void)logoutMethod
{
    if ([appDelegate internetConnected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn refreshMethod_NoLoader:MD_REFRESH_TOKEN withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
            if (response)
            {
                NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
                [user setValue:response[@"token_type"] forKey:UD_TOKEN_TYPE];
                [user setValue:response[@"access_token"] forKey:UD_ACCESS_TOKEN];
                [user setValue:response[@"refresh_token"] forKey:UD_REFERSH_TOKEN];
            }
            else
            {
                [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
            }
        }];
    }
    else
    {
        [CommenMethods alertviewController_title:LocalizedString(@"Alert") MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setDesignStyles
{

}

#pragma mark -- Table View Delegates Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dateArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TripsTableViewCell *cell = (TripsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"TripsTableViewCell"];
    
    if (cell == nil)
    {
        cell = (TripsTableViewCell *) [[[NSBundle mainBundle] loadNibNamed:@"TripsTableViewCell" owner:self options:nil] lastObject];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSUserDefaults *user=[NSUserDefaults standardUserDefaults];
    NSString *currencyStr=[user valueForKey:@"currency"];
    
    cell.dateLbl.text = [NSString stringWithFormat:@"%@", [bookingArray objectAtIndex:indexPath.row]];
    cell.timeLbl.text = [NSString stringWithFormat:@"%@-%@",[dateArray objectAtIndex:indexPath.row], [timeArray objectAtIndex:indexPath.row]];
    
    [CSS_Class APP_fieldValue:cell.dateLbl];
    [CSS_Class APP_fieldValue_Small:cell.timeLbl];
    [CSS_Class APP_fieldValue:cell.amountLbl];
    cell.timeLbl.textColor = TEXTCOLOR_LIGHT;
    
    NSString *strVal=imageArray [indexPath.row];
    NSString *escapedString =[strVal stringByReplacingOccurrencesOfString:@"%7C" withString:@"|"];
     NSURL *mapUrl = [NSURL URLWithString:[escapedString stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
    
    [cell.mapImg sd_setImageWithURL:mapUrl  placeholderImage:[UIImage imageNamed:@"rd-map"]];
   
    
    cell.cancelBtn.layer.cornerRadius = 5.0f;
    cell.cancelBtn.layer.borderWidth = 1.0f;
    cell.cancelBtn.layer.borderColor = TEXTCOLOR_LIGHT.CGColor;
    
    if (![identifierStr isEqualToString:@"PAST"])
    {
        [cell.amountLbl setHidden:YES];
        [cell.cancelBtn setHidden:NO];
        cell.cancelBtn.layer.borderColor = [UIColor grayColor].CGColor;
        
    }
    else
    {
        [cell.amountLbl setHidden:NO];
        [cell.cancelBtn setHidden:YES];
        cell.amountLbl.text = [NSString stringWithFormat:@"%@%@", currencyStr, [amountArray objectAtIndex:indexPath.row]];
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    requestIdStr =[NSString stringWithFormat:@"%@",[idArray objectAtIndex:indexPath.row]];
    NSLog(@"orderIdString ...%@", requestIdStr);
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"History" bundle: nil];

    HistoryViewController *wallet = [mainStoryboard instantiateViewControllerWithIdentifier:@"HistoryViewController"];
    wallet.historyHintStr = identifierStr;
    wallet.strID=requestIdStr;
    [self.navigationController pushViewController:wallet animated:YES];
}


-(IBAction)backBtn:(id)sender
{
	if (self.navigationController.viewControllers.count <= 1) {
		[self.navigationController.presentingViewController dismissViewControllerAnimated:true completion:NULL];
	}
	else {
		[self.navigationController popViewControllerAnimated:YES];
	}
}

//    MARK:- Action Responders

- (void)backAction:(UIButton *)sender {
    if (self.navigationController.viewControllers.count <= 1) {
        [self.navigationController.presentingViewController dismissViewControllerAnimated:true completion:NULL];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(IBAction)upcomingBtn:(id)sender
{
    identifierStr = @"UPCOMING";
    [_upcomingLbl setHidden:NO];
    [_pastLbl setHidden:YES];
    
     [self onGetHistory];
}
-(IBAction)pastBtn:(id)sender
{
    identifierStr = @"PAST";
    [_upcomingLbl setHidden:YES];
    [_pastLbl setHidden:NO];
    
    [self onGetHistory];
}


-(IBAction)cancelActionBtn:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tripTableView];
    NSIndexPath *indexPath = [_tripTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"INDEXPATH...%ld", (long)indexPath.row);
    
    NSString *req_Id = [NSString stringWithFormat:@"%@", [idArray objectAtIndex:indexPath.row]];
    
    NSDictionary *params=@{@"request_id":req_Id};
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [appDelegate onStartLoader];
    [afn getDataFromPath:MD_CANCEL_REQUEST WithType:POST_METHOD WithParameters:params WithCompletedSuccess:^(id response) {
        [appDelegate onEndLoader];
        [self onGetHistory];
    } OrValidationFailure:^(NSString *errorMessage) {
        [appDelegate onEndLoader];
        [CommenMethods alertviewController_title:@"" MessageAlert:errorMessage viewController:self okPop:NO];
    } OrErrorCode:^(NSString *error) {
        [appDelegate onEndLoader];
        [CommenMethods alertviewController_title:@"" MessageAlert:error viewController:self okPop:NO];
    } OrIntentet:^(NSString *internetFailure) {
        [appDelegate onEndLoader];
        [CommenMethods alertviewController_title:@"" MessageAlert:internetFailure viewController:self okPop:NO];
    }];
    
//    if ([appDelegate internetConnected])
//    {
//        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tripTableView];
//        NSIndexPath *indexPath = [_tripTableView indexPathForRowAtPoint:buttonPosition];
//        NSLog(@"INDEXPATH...%ld", (long)indexPath.row);
//        
//        NSString *req_Id = [NSString stringWithFormat:@"%@", [idArray objectAtIndex:indexPath.row]];
//        
//        NSDictionary *params=@{@"request_id":req_Id};
//        
//        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
//        [appDelegate onStartLoader];
//        [afn getDataFromPath:MD_CANCEL_REQUEST withParamData:params withBlock:^(id response, NSDictionary *error, NSString *errorcode) {
//            [appDelegate onEndLoader];
//            if (response)
//            {
//                [self onGetHistory];
//            }
//            else
//            {
//                if ([errorcode intValue]==1)
//                {
//                    [CommenMethods alertviewController_title:@"Error!" MessageAlert:[error objectForKey:@"error"] viewController:self okPop:NO];
//                }
//                else if ([errorcode intValue]==3)
//                {
//                    [self logoutMethod];
//                    
////                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isLoggedin"];
////                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_IMG];
////                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_PROFILE_NAME];
////                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_TOKEN_TYPE];
////                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ACCESS_TOKEN];
////                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_REFERSH_TOKEN];
////                    
////                    PageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
////                    [self.navigationController pushViewController:wallet animated:YES];
//                }
//                else
//                {
//                    [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"ERRORMSG") viewController:self okPop:NO];
//                }
//            }
//            
//        }];
//    }
//    else
//    {
//        [CommenMethods alertviewController_title:@"" MessageAlert:LocalizedString(@"CHKNET") viewController:self okPop:NO];
//    }
}


@end
